package th.co.digix.tpacare.http_service

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.TextUtils


/**
 * Created by android on 24/6/2558.
 */
class DefaultParameterAPI(var activity: Activity) {

    var udid = ""
    var machine  = ""
    var versionApp = ""
    var versionOS  = ""
    var network = ""
    var ipAddress = ""

    var platform = ""
    var device  = ""
    var os = ""

    fun getDeviceName(): String? {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            return capitalize(model)
        } else {
            return capitalize(manufacturer) + " " + model
        }

    }

    private fun capitalize(str: String): String? {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true

        val phrase = StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }

        return phrase.toString()
    }

    init {
        /////////////////////////////////////////////////////////////
        //-----------------------add defult parameter--------------//
        /////////////////////////////////////////////////////////////

        val android_id = Settings.System.getString(activity.contentResolver, Settings.Secure.ANDROID_ID)
        udid = android_id

        try {
            val pInfo = activity.packageManager.getPackageInfo(activity.packageName, 0)
            versionApp = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        val manager = activity.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val carrierName = manager.networkOperatorName
        network = carrierName

        platform = "android"
        os = "android"

        versionOS = android.os.Build.VERSION.RELEASE

        machine = getDeviceName().toString()

        //        String sAddr = "";
        //        boolean useIPv4 = true;
        //        try {
        //            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
        //
        //            for (NetworkInterface intf : interfaces) {
        //                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
        //                for (InetAddress addr : addrs) {
        //                    if (!addr.isLoopbackAddress()) {
        //                        sAddr = addr.getHostAddress().toUpperCase();
        //                        boolean isIPv4 = sAddr.indexOf(':')<0;
        //
        //                        if (!useIPv4) {
        //                            if (!isIPv4) {
        //                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
        //                                sAddr = delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
        //                            }
        //                        }
        //                        if (isIPv4) {
        ////                            Log.d("android isIPv4", "is " + sAddr);
        //                        }else {
        ////                            Log.d("android isIPv6", "is " + sAddr);
        //                        }
        //                    }
        //                }
        //            }
        //
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        } // for now eat exceptions


        //        Log.d("android", "is add defult parameter");
        //        Log.d("android udid", "is " + android_id);
        //        Log.d("android app version", "is " + app_version);
        //        Log.d("android version", "is " + os_version);
        //        Log.d("android operator", "is " + carrierName);
        //        Log.d("android ipaddress", "is " + sAddr);
        //        Log.d("android machine", "is " + platform);
        //        Log.d("android model geoName", "is " + android_model_name);
        //        Log.d("android package geoName", "is " + applicationid);

        /////////////////////////////////////////////////////////////
        //----------------------- defult parameter-----------------//
        /////////////////////////////////////////////////////////////
    }
}
