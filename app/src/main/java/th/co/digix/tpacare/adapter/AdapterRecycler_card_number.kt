package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.os.Build
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import okhttp3.FormBody
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.CardPolicyObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.menu_card.Fragment_Card_Policy
import th.co.digix.tpacare.menu_card.Page_Fragment_Card
import th.co.digix.tpacare.utility.CryptLib
import java.util.ArrayList


class AdapterRecycler_card_number(
    val activity: Activity?,
    val fragmentParent: Fragment?,
    val fragment: Fragment_Card_Policy?,
    var cardPolicyObject_List: ArrayList<CardPolicyObject>,
    val alertDialog_NeedHelp: android.support.v7.app.AlertDialog,
    val position: Int?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        val httpServerURL = HttpServerURL()
        var root_url = httpServerURL.host_server
        val api_policy_request_otp = httpServerURL.api_policy_request_otp
        val salt_key = httpServerURL.salt_key

        if (cardPolicyObject_List.size > 0) {
            val cardPolicyObject = cardPolicyObject_List.get(position)

            var card_telephone = cardPolicyObject.telNo

            try {
                card_telephone = card_telephone.substring(0, 3) +
                        "-XXX-X" +
                        card_telephone.substring(7, card_telephone.length)
            } catch (e: Exception) {
            }

            holder.textView_card_number.setText("" + card_telephone)

            try {
                val text_card_call_otp = "<u>" + activity!!.resources.getString(R.string.text_card_call_otp) + "</u>"
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.textView_call_otp.setText(
                        Html.fromHtml(text_card_call_otp, Html.FROM_HTML_MODE_COMPACT))
                } else {
                    holder.textView_call_otp.setText(
                        Html.fromHtml(text_card_call_otp))
                }
            } catch (e: Exception) {
            }

            holder.textView_call_otp.setOnClickListener {
                var tel_encrypt = ""

                val cryptLib = CryptLib()
                tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(cardPolicyObject.telNo, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(activity!!)
                val formBody = FormBody.Builder()
                formBody.add("tel", "" + tel_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url, api_policy_request_otp, "api_policy_request_otp" + position)

                try{
                    val parentFrag = fragmentParent as Page_Fragment_Card
                    parentFrag.progressBackground?.visibility = View.VISIBLE
                    fragment?.selected_telephone = "" + cardPolicyObject.telNo
//                    fragment?.showDialog_card_otp(activity,cardPolicyObject.telNo)
                }catch (e:Exception){}

                alertDialog_NeedHelp?.dismiss()
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_card_number, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (cardPolicyObject_List != null) {
            var listSize = cardPolicyObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textView_card_number: TextView
        internal var textView_call_otp: TextView

        init {
            textView_card_number = itemView.findViewById(R.id.textView_card_number) as TextView
            textView_call_otp = itemView.findViewById(R.id.textView_call_otp) as TextView
        }
    }

}