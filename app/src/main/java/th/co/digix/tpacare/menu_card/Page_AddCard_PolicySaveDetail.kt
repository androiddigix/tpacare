package th.co.digix.tpacare.menu_card

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.CoverageObject
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.NotificationBarColor
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class Page_AddCard_PolicySaveDetail : BaseActivity() {


    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authForgotUserRequestOTP = httpServerURL.api_authForgotUserRequestOTP
    val salt_key = httpServerURL.salt_key


    var progressBackground: RelativeLayout? = null

    var editText_treat_price: EditText? = null
    var editText_treat_room: EditText? = null
    var editText_treat_operate: EditText? = null
    var editText_treat_drug: EditText? = null

    var linearLayout_save_add_card_datail: LinearLayout? = null

    var textView_popup_title: TextView? = null

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var coverage_list = JSONArray()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_add_card_policy_save_detail)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialWorking() {

        linearLayout_save_add_card_datail?.setOnClickListener(View.OnClickListener {

            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = getCurrentFocus()
            if (view == null) {
                view = View(this@Page_AddCard_PolicySaveDetail)
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)

            var coverage_list_array = JSONArray()

            var coverageObject_treat_price = CoverageObject()
            coverageObject_treat_price.cov_no = "0"
            coverageObject_treat_price.cov_desc = "" + resources.getString(R.string.text_treat_price)
            var treat_price_str = editText_treat_price?.text.toString()
            treat_price_str = treat_price_str.replace(",", "")
            coverageObject_treat_price.cov_limit = "" + treat_price_str
            coverageObject_treat_price.cov_utilized = "-"
            var coverageObject_treat_room = CoverageObject()
            coverageObject_treat_room.cov_no = "1"
            coverageObject_treat_room.cov_desc = "" + resources.getString(R.string.text_treat_room)
            var treat_room_str = editText_treat_room?.text.toString()
            treat_room_str = treat_room_str.replace(",", "")
            coverageObject_treat_room.cov_limit = "" + treat_room_str
            coverageObject_treat_room.cov_utilized = "-"
            var coverageObject_treat_operate = CoverageObject()
            coverageObject_treat_operate.cov_no = "2"
            coverageObject_treat_operate.cov_desc = "" + resources.getString(R.string.text_treat_operate)
            var treat_operate_str = editText_treat_operate?.text.toString()
            treat_operate_str = treat_operate_str.replace(",", "")
            coverageObject_treat_operate.cov_limit = "" + treat_operate_str
            coverageObject_treat_operate.cov_utilized = "-"
            var coverageObject_treat_drug = CoverageObject()
            coverageObject_treat_drug.cov_no = "3"
            coverageObject_treat_drug.cov_desc = "" + resources.getString(R.string.text_treat_drug)
            var treat_drug_str = editText_treat_drug?.text.toString()
            treat_drug_str = treat_drug_str.replace(",", "")
            coverageObject_treat_drug.cov_limit = "" + treat_drug_str
            coverageObject_treat_drug.cov_utilized = "-"

            coverage_list_array.put(coverageObject_treat_price.toJsonEdit())
            coverage_list_array.put(coverageObject_treat_room.toJsonEdit())
            coverage_list_array.put(coverageObject_treat_operate.toJsonEdit())
            coverage_list_array.put(coverageObject_treat_drug.toJsonEdit())

            Log.d("coke", "coverage_list " + coverage_list_array.toString())

            val returnIntent = Intent()
            returnIntent.putExtra("coverage_list", coverage_list_array.toString())
            setResult(Activity.RESULT_OK, returnIntent)
            finish()

        })

    }


    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        editText_treat_price = findViewById(R.id.editText_treat_price) as EditText
        editText_treat_room = findViewById(R.id.editText_treat_room) as EditText
        editText_treat_operate = findViewById(R.id.editText_treat_operate) as EditText
        editText_treat_drug = findViewById(R.id.editText_treat_drug) as EditText

        editText_treat_price?.addTextChangedListener(onTextChangedSetFormatter(editText_treat_price!!))
        editText_treat_room?.addTextChangedListener(onTextChangedSetFormatter(editText_treat_room!!))
        editText_treat_operate?.addTextChangedListener(onTextChangedSetFormatter(editText_treat_operate!!))
        editText_treat_drug?.addTextChangedListener(onTextChangedSetFormatter(editText_treat_drug!!))

        var coverage_list_JSONArray = coverage_list
        if (coverage_list_JSONArray != null) {
            for (i in 0..coverage_list_JSONArray.length() - 1) {
                var coverageObject_json: JSONObject? = null
                try {
                    coverageObject_json = coverage_list_JSONArray.get(i) as JSONObject
                } catch (e: Exception) {
                }
                if (coverageObject_json != null) {
                    val coverageObject = CoverageObject()
                    coverageObject.setParam(coverageObject_json)
                    if (i == 0) {
                        editText_treat_price?.setText("" + coverageObject.cov_limit)
                    } else if (i == 1) {
                        editText_treat_room?.setText("" + coverageObject.cov_limit)
                    } else if (i == 2) {
                        editText_treat_operate?.setText("" + coverageObject.cov_limit)
                    } else if (i == 3) {
                        editText_treat_drug?.setText("" + coverageObject.cov_limit)
                    } else if (i > 3) {
                    }
                }
            }

        }


        linearLayout_save_add_card_datail = findViewById(R.id.linearLayout_save_add_card_datail) as LinearLayout
    }

    private fun onTextChangedSetFormatter(editText: EditText): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                editText.removeTextChangedListener(this)

                try {
                    var originalString = s.toString()

                    if(originalString.equals("")) {
                        originalString = "0"
                    }

                    val longval: Long?
                    if (originalString.contains(",")) {
                        originalString = originalString.replace(",".toRegex(), "")
                    }
                    if (originalString.contains(".")) {
                        originalString = originalString.replace(".".toRegex(), "")
                    }
                    longval = java.lang.Long.parseLong(originalString)

                    val formatter = NumberFormat.getInstance(Locale.US) as DecimalFormat
                    formatter.applyPattern("#,###,###,###,###,###")
                    val formattedString = formatter.format(longval)

                    //setting text after format to EditText
                    editText.setText(formattedString)
                    editText.setSelection(editText.getText().length)
                } catch (nfe: NumberFormatException) {
                    nfe.printStackTrace()
                }

                editText.addTextChangedListener(this)
            }
        }
    }

    private fun initialIntentData() {
        var coverage_list_json = ""
        try {
            coverage_list_json = intent.getStringExtra("coverage_list")
        } catch (e: Exception) {
        }
        if (coverage_list_json != null && !coverage_list_json.equals("") && !coverage_list_json.equals("null")) {
            coverage_list = JSONArray(coverage_list_json)
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_add_card))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authForgotUserRequestOTP")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {


                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_AddCard_PolicySaveDetail, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_PolicySaveDetail, resources.getString(R.string.text_service_error)
                )
            }
        }

    }

}