package th.co.digix.tpacare.menu_setting

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import okhttp3.MultipartBody
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.BuildConfig
import th.co.digix.tpacare.R
import th.co.digix.tpacare.http_service.*
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date

class Page_EditProfile : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authUpdateProfile = httpServerURL.api_authUpdateProfile
    val api_authUpdateImageProfile = httpServerURL.api_authUpdateImageProfile
    val api_authUpdateProfileRequestOTP = httpServerURL.api_authUpdateProfileRequestOTP
    val api_authUpdateProfileCheckOTP = httpServerURL.api_authUpdateProfileCheckOTP
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var progressBackground: RelativeLayout? = null

    var linearLayout_idcard_form: LinearLayout? = null
    var edittext_idcard_form: EditText? = null

    var textView_idcard_1: TextView? = null
    var textView_idcard_2: TextView? = null
    var textView_idcard_3: TextView? = null
    var textView_idcard_4: TextView? = null
    var textView_idcard_5: TextView? = null
    var textView_idcard_6: TextView? = null

    var imageView_profile: ImageView? = null
    var textView_username: TextView? = null

    var editText_UserName: EditText? = null
    var editText_first_name: EditText? = null
    var editText_last_name: EditText? = null
    var editText_phone: EditText? = null

    var relativeLayout_profile: RelativeLayout? = null
    var linearLayout_save_profile: LinearLayout? = null

    var selectedImageUri: Uri? = null
    var base64String: String? = null

    val PICK_FROM_CAMERA = 20
    val PICK_FROM_FILE_image_profile = 21
    val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_OPEN_CAMERA = 64
    val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_image_profil = 66

    val modifyImage = ModifyImage()

    var otp_code = ""
    var phone_new = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_edit_profile)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialProfileImage()
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialProfileImage() {

        val sharedPreferencesManager = SharedPreferencesManager(this)
        val userObject = sharedPreferencesManager.getUserObject()

        textView_username?.setText(resources.getString(R.string.text_hello_prefix) + userObject.first_name + " " + userObject.last_name)

        if (imageView_profile != null) {

            val url_image = "" + userObject.image_profile

            val placeholder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_profile_default_home)
            val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), placeholder)
            circularBitmapDrawable.isCircular = true
            Glide.with(this)
                .load(url_image)
                .apply(
                    RequestOptions
                        .circleCropTransform()
                        .placeholder(circularBitmapDrawable)
                        .error(circularBitmapDrawable)
                )
                .into(imageView_profile!!)
        }

        relativeLayout_profile?.setOnClickListener {
            openDialogSelectImageOrCamera()
        }

        linearLayout_save_profile?.setOnClickListener {

            var userName = editText_UserName?.text.toString().trim()
            var first_name = editText_first_name?.text.toString().trim()
            var last_name = editText_last_name?.text.toString().trim()
            phone_new = editText_phone?.text.toString().trim()

            var tel_decrypt = ""
            val cryptLib = CryptLib()
            if (!userObject.tel.equals("")) {
                tel_decrypt = cryptLib.decryptCipherTextWithRandomIV(userObject.tel, salt_key)
            }

            val characterSet = CharacterSet()

            if (userName.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_username)
                )
            } else if (first_name.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_first_name)
                )
            } else if (last_name.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_last_name)
                )
            } else if (phone_new.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_phone)
                )
            } else if (!characterSet.checkPhone(phone_new)) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile,
                    "" + resources.getString(R.string.text_alert_phone_format)
                )
            } else if (phone_new.length < 10) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile,
                    "" + resources.getString(R.string.text_alert_phone_10_number)
                )
            } else if (!phone_new.equals(tel_decrypt)) {
                var old_tel_encrypt = ""
                var new_tel_encrypt = ""

                val sharedPreferencesManager = SharedPreferencesManager(this)
                val userObject = sharedPreferencesManager.getUserObject()
                val username = userObject.username
                val tel = userObject.tel

                val cryptLib = CryptLib()
//            old_tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(new_tel, salt_key)
                new_tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone_new, salt_key)


                val defaultParameterAPI = DefaultParameterAPI(this@Page_EditProfile)
                val formBody = FormBody.Builder()
                formBody.add("username", "" + username)
                formBody.add("old_tel", "" + tel)
                formBody.add("new_tel", "" + new_tel_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url, api_authUpdateProfileRequestOTP, "api_authUpdateProfileRequestOTP"
                )
                progressBackground?.visibility = View.VISIBLE
            } else {
                var userName_encrypt = ""
                var first_name_encrypt = ""
                var last_name_encrypt = ""
                var phone_encrypt = ""

                val cryptLib = CryptLib()
                userName_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(userName, salt_key)
                first_name_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(first_name, salt_key)
                last_name_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(last_name, salt_key)
                phone_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone_new, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(this)
                val formBody = FormBody.Builder()
                formBody.add("username", "" + userName_encrypt)
                formBody.add("first_name", "" + first_name)
                formBody.add("last_name", "" + last_name)
                formBody.add("tel", "" + phone_encrypt)
                formBody.add("token_key", "" + resources.getString(R.string.token_key))
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url,
                    api_authUpdateProfile,
                    "api_authUpdateProfile"
                )

                progressBackground?.visibility = View.VISIBLE
            }
        }

    }

    public fun showDialog_card_otp(activity: Activity) {
        otp_code = ""
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_card_otp, null)

        val textView_card_re_otp = dialogLayout_NeedHelp?.findViewById(R.id.textView_card_re_otp) as TextView
        val textView_popup_title = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_title) as TextView
        val linearLayout_card_re_otp =
            dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_card_re_otp) as LinearLayout
        val linearLayout_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_ok) as LinearLayout
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        linearLayout_idcard_form = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_idcard_form) as LinearLayout
        edittext_idcard_form = dialogLayout_NeedHelp?.findViewById(R.id.edittext_idcard_form) as EditText

        textView_idcard_1 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_1) as TextView
        textView_idcard_2 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_2) as TextView
        textView_idcard_3 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_3) as TextView
        textView_idcard_4 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_4) as TextView
        textView_idcard_5 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_5) as TextView
        textView_idcard_6 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_6) as TextView

        linearLayout_idcard_form?.setOnClickListener {
            if (otp_code.length == 0) {
                textView_idcard_1?.setText("|")
                textView_idcard_1?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 1) {
                textView_idcard_2?.setText("|")
                textView_idcard_2?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 2) {
                textView_idcard_3?.setText("|")
                textView_idcard_3?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 3) {
                textView_idcard_4?.setText("|")
                textView_idcard_4?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 4) {
                textView_idcard_5?.setText("|")
                textView_idcard_5?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 5) {
                textView_idcard_6?.setText("|")
                textView_idcard_6?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            }

            Log.d("coke", "linearLayout_idcard_form")

            edittext_idcard_form?.requestFocus()


            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.getCurrentFocus()
            if (view == null) {
                view = View(activity)
            }
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)


        }

        edittext_idcard_form?.addTextChangedListener(
            onTextChangedSetControlEdittext(
            )
        )

        var card_telephone = phone_new
        try {
            card_telephone = card_telephone.substring(0, 3) +
                    "-XXX-X" +
                    card_telephone.substring(7, card_telephone.length)
        } catch (e: Exception) {
        }

        try {
            val text_card_add_number =
                "<u>" + this@Page_EditProfile.resources.getString(R.string.popup_card_re_otp) + "</u>"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView_card_re_otp.setText(
                    Html.fromHtml(text_card_add_number, Html.FROM_HTML_MODE_COMPACT)
                )
            } else {
                textView_card_re_otp.setText(
                    Html.fromHtml(text_card_add_number)
                )
            }
        } catch (e: Exception) {
        }
        textView_popup_title.setText(resources.getString(R.string.popup_card_otp_title) + " " + card_telephone)

        linearLayout_card_re_otp.setOnClickListener(View.OnClickListener {
            var old_tel_encrypt = ""
            var new_tel_encrypt = ""

            val sharedPreferencesManager = SharedPreferencesManager(this)
            val userObject = sharedPreferencesManager.getUserObject()
            val username = userObject.username
            val tel = userObject.tel

            val cryptLib = CryptLib()
//            old_tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(new_tel, salt_key)
            new_tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone_new, salt_key)


            val defaultParameterAPI = DefaultParameterAPI(this@Page_EditProfile)
            val formBody = FormBody.Builder()
            formBody.add("username", "" + username)
            formBody.add("old_tel", "" + tel)
            formBody.add("new_tel", "" + new_tel_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_authUpdateProfileRequestOTP, "api_authUpdateProfileRequestOTP"
            )
            progressBackground?.visibility = View.VISIBLE

            alertDialog_NeedHelp?.dismiss()
        })

        linearLayout_popup_ok.setOnClickListener(View.OnClickListener {
            var old_tel_encrypt = ""
            var new_tel_encrypt = ""
            var otp_code_encrypt = ""

            val sharedPreferencesManager = SharedPreferencesManager(this)
            val userObject = sharedPreferencesManager.getUserObject()
            val username = userObject.username
            val tel = userObject.tel

            val cryptLib = CryptLib()
//            old_tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(new_tel, salt_key)
            new_tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone_new, salt_key)
            otp_code_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(otp_code, salt_key)

            val defaultParameterAPI = DefaultParameterAPI(this@Page_EditProfile)
            val formBody = FormBody.Builder()
            formBody.add("otp_code", "" + otp_code_encrypt)
            formBody.add("username", "" + username)
            formBody.add("old_tel", "" + tel)
            formBody.add("new_tel", "" + new_tel_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_authUpdateProfileCheckOTP, "api_authUpdateProfileCheckOTP"
            )
            progressBackground?.visibility = View.VISIBLE

            alertDialog_NeedHelp?.dismiss()

        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
//        alertDialog_NeedHelp?.getWindow()?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
//                or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        alertDialog_NeedHelp?.show()


    }

    private fun onTextChangedSetControlEdittext(): TextWatcher {

        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                Log.d("coke", "onTextChanged s " + s + " start " + start + " before " + before + " count " + count)

                var textChange = s.toString()
                otp_code = "" + textChange

                var otp_code_1 = ""
                var otp_code_2 = ""
                var otp_code_3 = ""
                var otp_code_4 = ""
                var otp_code_5 = ""
                var otp_code_6 = ""
                try {
                    otp_code_1 = "" + s.get(0)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_2 = "" + s.get(1)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_3 = "" + s.get(2)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_4 = "" + s.get(3)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_5 = "" + s.get(4)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_6 = "" + s.get(5)
                } catch (e: java.lang.Exception) {
                }


                textView_idcard_1?.setText("" + otp_code_1)
                textView_idcard_2?.setText("" + otp_code_2)
                textView_idcard_3?.setText("" + otp_code_3)
                textView_idcard_4?.setText("" + otp_code_4)
                textView_idcard_5?.setText("" + otp_code_5)
                textView_idcard_6?.setText("" + otp_code_6)

                textView_idcard_1?.setTextColor(ContextCompat.getColor(this@Page_EditProfile, R.color.text_blue))
                textView_idcard_2?.setTextColor(ContextCompat.getColor(this@Page_EditProfile, R.color.text_blue))
                textView_idcard_3?.setTextColor(ContextCompat.getColor(this@Page_EditProfile, R.color.text_blue))
                textView_idcard_4?.setTextColor(ContextCompat.getColor(this@Page_EditProfile, R.color.text_blue))
                textView_idcard_5?.setTextColor(ContextCompat.getColor(this@Page_EditProfile, R.color.text_blue))
                textView_idcard_6?.setTextColor(ContextCompat.getColor(this@Page_EditProfile, R.color.text_blue))

                if (textChange.length == 0) {
                    textView_idcard_1?.setText("|")
                    textView_idcard_1?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_EditProfile,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 1) {
                    textView_idcard_2?.setText("|")
                    textView_idcard_2?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_EditProfile,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 2) {
                    textView_idcard_3?.setText("|")
                    textView_idcard_3?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_EditProfile,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 3) {
                    textView_idcard_4?.setText("|")
                    textView_idcard_4?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_EditProfile,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 4) {
                    textView_idcard_5?.setText("|")
                    textView_idcard_5?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_EditProfile,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 5) {
                    textView_idcard_6?.setText("|")
                    textView_idcard_6?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_EditProfile,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 6) {
                    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    var view = getCurrentFocus()
                    if (view == null) {
                        view = View(this@Page_EditProfile)
                    }
//                    imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        }
    }

    private fun openDialogSelectImageOrCamera() {

        val builder = AlertDialog.Builder(this@Page_EditProfile)
        val OptionDialog = builder.create()
        val dialoglayout = LayoutInflater.from(baseContext).inflate(R.layout.popup_dialog_select_photo_or_camera, null)
        OptionDialog.setCanceledOnTouchOutside(true)
        OptionDialog.setView(dialoglayout)
        run {
            val button_take_photo = dialoglayout.findViewById(R.id.button_take_photo) as LinearLayout
            val button_choose_form_gallery = dialoglayout.findViewById(R.id.button_choose_form_gallery) as LinearLayout

            button_take_photo.setOnClickListener(View.OnClickListener {
                openCamera_checkPermission()
                OptionDialog.dismiss()
            })

            button_choose_form_gallery.setOnClickListener(View.OnClickListener {
                checkPermissionPickPhoto()
                OptionDialog.dismiss()
            })

            OptionDialog.dismiss()

        }

        OptionDialog.show()
    }

    private fun openCamera_checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionsList = ArrayList<String>()
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.CAMERA)
            }
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            if (permissionsList.size > 0) {
                requestPermissions(
                    permissionsList.toTypedArray(),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_OPEN_CAMERA
                )
            } else {
                openCamera()
            }
        } else {
            openCamera()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {

        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_OPEN_CAMERA) {
            val perms = HashMap<String, Int>()
            for (i in 0 until permissions.size) {
                perms.put(permissions[i], grantResults[i])
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ) {
                    openCamera_checkPermission()
                } else {
                    Toast.makeText(this, "Sorry, Permission Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (requestCode == REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_image_profil) {
            val perms = HashMap<String, Int>()
            for (i in 0 until permissions.size) {
                perms.put(permissions[i], grantResults[i])
            }
            if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            ) {
                // All Permissions Granted
                checkPermissionPickPhoto()
            } else {
                Toast.makeText(this, "Sorry, Permission Denied", Toast.LENGTH_SHORT).show()


            }
        }

    }

    private fun checkPermissionPickPhoto() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionsList = ArrayList<String>()
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            }
            if (permissionsList.size > 0) {
                requestPermissions(
                    permissionsList.toTypedArray(),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_image_profil
                )
            } else {
                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, PICK_FROM_FILE_image_profile)
            }
        } else {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent, PICK_FROM_FILE_image_profile)
        }
    }


    private fun openCamera() {
        val storageState = Environment.getExternalStorageState()
        if (storageState == Environment.MEDIA_MOUNTED) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                selectedImageUri = FileProvider.getUriForFile(
                    this@Page_EditProfile,
                    BuildConfig.APPLICATION_ID + ".provider",
                    createImageFile()
                )
                intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri)
                startActivityForResult(intent, PICK_FROM_CAMERA)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            AlertDialogUtil().showMessageDialog(this@Page_EditProfile, "need permission external storage (SD Card)")
            //            new AlertDialog.Builder(Page_Edit_Profile.this)
            //                    .setMessage("External Storeage (SD Card) is required.\n\nCurrent state: " + storageState)
            //                    .setCancelable(true).create().show();
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file geoName
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
            ), "Camera"
        )

        return File.createTempFile(
            imageFileName, /* prefix */
            ".png", /* suffix */
            storageDir      /* directory */
        )
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PICK_FROM_FILE_image_profile) {
                if (data != null) {
                    var selectedImageUri = data.getData()
                    var selectedImagePath = modifyImage.getRealPathFromURI(selectedImageUri, this)

                    if (selectedImagePath == null) {

                        Toast.makeText(
                            this@Page_EditProfile,
                            "File not found or directory not found",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (selectedImagePath.equals("")) {

                        Toast.makeText(
                            this@Page_EditProfile,
                            "File not found or directory not found",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        if (selectedImagePath!!.startsWith("/0/1/content:")) {
                            Toast.makeText(
                                this@Page_EditProfile,
                                "File not found or directory not found",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {

                            var bitmap: Bitmap? = BitmapFactory.decodeFile(selectedImagePath)
                            var path_new = ""
                            if (bitmap == null) {
                                path_new = selectedImagePath?.replace("external_files", "sdcard")!!
                                bitmap = BitmapFactory.decodeFile(path_new)
                            }

                            var bitmapResizeBitmap = modifyImage.resizeBitmap(bitmap!!)

                            val streamBitmapFlipOrRotate = ByteArrayOutputStream()

                            var bitmapFile: Bitmap? = null
                            try {
                                bitmapFile = modifyImage.modifyOrientation(bitmapResizeBitmap, path_new)
                                bitmapFile!!.compress(Bitmap.CompressFormat.PNG, 50, streamBitmapFlipOrRotate)
                            } catch (e: IOException) {
                                bitmapFile = bitmapResizeBitmap
                                e.printStackTrace()
                            }

//                            if (bitmapFile != null) {
//                                var path_name = "" + resources.getString(R.string.app_name) + "_Image" + ".jpg"
//                                fileImage_NeedHelp = modifyImage.bitMapToFile(bitmapFile, path_name, this)
//                            }

                            val baos = ByteArrayOutputStream()
                            bitmapFile?.compress(Bitmap.CompressFormat.PNG, 100, baos)
                            val b = baos.toByteArray()
                            base64String = Base64.encodeToString(b, Base64.DEFAULT)

                            if (bitmapFile != null) {
                                if (imageView_profile != null) {
                                    val placeholder =
                                        BitmapFactory.decodeResource(
                                            getResources(),
                                            R.drawable.icon_profile_default_home
                                        )
                                    val circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getResources(), placeholder)
                                    circularBitmapDrawable.isCircular = true
                                    Glide.with(this@Page_EditProfile)
                                        .load(bitmapFile)
                                        .apply(
                                            RequestOptions
                                                .circleCropTransform()
                                                .placeholder(circularBitmapDrawable)
                                                .error(circularBitmapDrawable)
                                        )
                                        .into(this@Page_EditProfile.imageView_profile!!)
                                }
                            } else {
                                if (imageView_profile != null) {
                                    Glide.with(this@Page_EditProfile)
                                        .load(R.drawable.icon_profile_default_home)
                                        .apply(RequestOptions.circleCropTransform())
                                        .into(this@Page_EditProfile.imageView_profile!!)
                                }
                            }
//                            callService_UploadImage()
                        }
                    }
                }

            }
            if (requestCode == PICK_FROM_CAMERA) {
                if (selectedImageUri != null) {
                    var selectedImagePath = selectedImageUri?.getPath()
                    var gallaryPhotoSelected = false

//                    resizeAndSendToPageProfile();

                    if (gallaryPhotoSelected) {
                        selectedImagePath = modifyImage.getRealPathFromURI(selectedImageUri!!, this)
                    }
                    var bitmap: Bitmap? = BitmapFactory.decodeFile(selectedImagePath)
                    var path_new = ""
                    if (bitmap == null) {
                        path_new = selectedImagePath?.replace("external_files", "sdcard")!!
                        bitmap = BitmapFactory.decodeFile(path_new)
                    }

                    var bitmapResizeBitmap = modifyImage.resizeBitmap(bitmap!!)

                    val streamBitmapFlipOrRotate = ByteArrayOutputStream()

                    var bitmapFile: Bitmap? = null
                    try {
                        bitmapFile = modifyImage.modifyOrientation(bitmapResizeBitmap, path_new)
                        bitmapFile!!.compress(Bitmap.CompressFormat.PNG, 50, streamBitmapFlipOrRotate)
                    } catch (e: IOException) {
                        bitmapFile = bitmapResizeBitmap
                        e.printStackTrace()
                    }

//                    if (bitmapFile != null) {
//                        var path_name = "" + resources.getString(R.string.app_name) + "_Image" + ".jpg"
//                        fileImage_NeedHelp = modifyImage.bitMapToFile(bitmapFile, path_name, this)
//                    }

                    val baos = ByteArrayOutputStream()
                    bitmapFile?.compress(Bitmap.CompressFormat.PNG, 100, baos)
                    val b = baos.toByteArray()
                    base64String = Base64.encodeToString(b, Base64.DEFAULT)

                    if (bitmapFile != null) {
                        if (imageView_profile != null) {
                            val placeholder =
                                BitmapFactory.decodeResource(getResources(), R.drawable.icon_profile_default_home)
                            val circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), placeholder)
                            circularBitmapDrawable.isCircular = true
                            Glide.with(this@Page_EditProfile)
                                .load(bitmapFile)
                                .apply(
                                    RequestOptions
                                        .circleCropTransform()
                                        .placeholder(circularBitmapDrawable)
                                        .error(circularBitmapDrawable)
                                )
                                .into(this@Page_EditProfile.imageView_profile!!)
                        }
                    } else {
                        if (imageView_profile != null) {
                            Glide.with(this@Page_EditProfile)
                                .load(R.drawable.icon_profile_default_home)
                                .apply(RequestOptions.circleCropTransform())
                                .into(this@Page_EditProfile.imageView_profile!!)
                        }
                    }
//                    callService_UploadImage()

                }

            }
        }
    }


    private fun callService_UploadImage() {

        var sharedPreferencesManager = SharedPreferencesManager(this)

        val defaultParameterAPI = DefaultParameterAPI(this)

        var formBody = MultipartBody.Builder()

        if (base64String != null) {


            val defaultParameterAPI = DefaultParameterAPI(this)
            val formBody = FormBody.Builder()
            formBody.add("base64String", "" + base64String)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url,
                api_authUpdateImageProfile,
                "api_authUpdateImageProfile"
            )

            progressBackground?.visibility = View.VISIBLE
        }
    }


    private fun initialIntentData() {
//        try {
//            formLogin = intent.getBooleanExtra("formLogin", false)
//        } catch (e: Exception) {
//        }
    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        imageView_profile = findViewById(R.id.imageView_profile) as ImageView
        textView_username = findViewById(R.id.textView_username) as TextView

        editText_UserName = findViewById(R.id.editText_UserName) as EditText
        editText_first_name = findViewById(R.id.editText_first_name) as EditText
        editText_last_name = findViewById(R.id.editText_last_name) as EditText
        editText_phone = findViewById(R.id.editText_phone) as EditText

        linearLayout_save_profile = findViewById(R.id.linearLayout_save_profile) as LinearLayout
        relativeLayout_profile = findViewById(R.id.relativeLayout_profile) as RelativeLayout

        val sharedPreferencesManager = SharedPreferencesManager(this)
        val userObject = sharedPreferencesManager.getUserObject()

        val cryptLib = CryptLib()
        var username_decrypt = ""
        var phone_decrypt = ""
        if (!userObject.username.equals("")) {
            username_decrypt = cryptLib.decryptCipherTextWithRandomIV(userObject.username, salt_key)
        }
        if (!userObject.tel.equals("")) {
            phone_decrypt = cryptLib.decryptCipherTextWithRandomIV(userObject.tel, salt_key)
        }

        editText_UserName?.setText("" + username_decrypt)
        editText_first_name?.setText("" + userObject.first_name)
        editText_last_name?.setText("" + userObject.last_name)
        editText_phone?.setText("" + phone_decrypt)

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.text_edittect_title))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authUpdateProfileCheckOTP")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    var userName = editText_UserName?.text.toString().trim()
                    var first_name = editText_first_name?.text.toString().trim()
                    var last_name = editText_last_name?.text.toString().trim()
                    phone_new = editText_phone?.text.toString().trim()

                    var userName_encrypt = ""
                    var first_name_encrypt = ""
                    var last_name_encrypt = ""
                    var phone_encrypt = ""

                    val cryptLib = CryptLib()
                    userName_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(userName, salt_key)
                    first_name_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(first_name, salt_key)
                    last_name_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(last_name, salt_key)
                    phone_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone_new, salt_key)

                    val defaultParameterAPI = DefaultParameterAPI(this)
                    val formBody = FormBody.Builder()
                    formBody.add("username", "" + userName_encrypt)
                    formBody.add("first_name", "" + first_name)
                    formBody.add("last_name", "" + last_name)
                    formBody.add("tel", "" + phone_encrypt)
                    formBody.add("token_key", "" + resources.getString(R.string.token_key))
                    OkHttpPost(formBody, defaultParameterAPI).execute(
                        root_url,
                        api_authUpdateProfile,
                        "api_authUpdateProfile"
                    )

                    progressBackground?.visibility = View.VISIBLE

                } else {
                    progressBackground?.visibility = View.GONE
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_EditProfile, "" + message)
                }
            } else {
                progressBackground?.visibility = View.GONE
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile, resources.getString(R.string.text_service_error)
                )
            }
        }
        if (event.getHttpName().equals("api_authUpdateProfileRequestOTP")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    showDialog_card_otp(this)

                } else {
                    progressBackground?.visibility = View.GONE
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_EditProfile, "" + message)
                }
            } else {
                progressBackground?.visibility = View.GONE
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile, resources.getString(R.string.text_service_error)
                )
            }
        }

        if (event.getHttpName().equals("api_authUpdateProfile")) {

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    val sharedPreferencesManager = SharedPreferencesManager(this)
                    val userObject = sharedPreferencesManager.getUserObject()
                    if (result != null) {
                        userObject.setUpdateProfile(result)
                        userObject.state_login = "login"
                        sharedPreferencesManager.setUserObject(userObject)

//                        val intent_go = Intent(this, ActivityMain::class.java)
//                        intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//                        startActivity(intent_go)

                        if (base64String != null) {
                            callService_UploadImage()
                        } else {
                            progressBackground?.visibility = View.GONE
                            finish()
//                            message = "" + resources.getString(R.string.text_success)
//                            val alertDialogUtil = AlertDialogUtil()
//                            alertDialogUtil.showMessageDialog_finish(this@Page_EditProfile, "" + message)
                        }
                    }
                } else {
                    progressBackground?.visibility = View.GONE
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_EditProfile, "" + message)
                }
            } else {
                progressBackground?.visibility = View.GONE
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile, resources.getString(R.string.text_service_error)
                )
            }
        }
        if (event.getHttpName().equals("api_authUpdateImageProfile")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result = ""
                    try {
                        result = responseJson.getString("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    val sharedPreferencesManager = SharedPreferencesManager(this)
                    val userObject = sharedPreferencesManager.getUserObject()
                    userObject.image_profile = result
                    sharedPreferencesManager.setUserObject(userObject)

                    finish()

//                    message = "" + resources.getString(R.string.text_success)
//                    val alertDialogUtil = AlertDialogUtil()
//                    alertDialogUtil.showMessageDialog_finish(this@Page_EditProfile, "" + message)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_EditProfile, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_EditProfile, resources.getString(R.string.text_service_error)
                )
            }
        }

    }


}
