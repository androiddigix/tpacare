package th.co.digix.tpacare.utility

import java.text.SimpleDateFormat
import java.util.*

class DateTimeHelper {

    public fun parseStringToDate3(strDate: String): Date {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
        var date: Date? = null
        try {
            date = dateFormat.parse(strDate)
        } catch (e: Exception) {
        }

        return date!!
    }

    public fun parseStringToDate4(strDate: String): Date {
        val dateFormat = SimpleDateFormat("dd-MM-yyyy")
        var date: Date? = null
        try {
            date = dateFormat.parse(strDate)
        } catch (e: Exception) {
        }

        return date!!
    }

    public fun parseStringToDate(strDate: String): Date {
        val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm")
        var date: Date? = null
        try {
            date = dateFormat.parse(strDate)
        } catch (e: Exception) {
        }

        return date!!
    }

    public fun parseStringToDate2(strDate: String): Date {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        var date: Date? = null
        try {
            date = dateFormat.parse(strDate)
        } catch (e: Exception) {
        }

        return date!!
    }

    public fun parseDateToString(strDate: Date): String {
        val simpleDate = SimpleDateFormat("dd MMM yyyy")
        val strDt = simpleDate.format(strDate)
        return strDt!!
    }

    public fun parseDateToString2(date: Date): String {
        val df = SimpleDateFormat("HH:mm")
        var str_date = ""
        try {
            str_date = df.format(date)
        } catch (e: Exception) {
        }
        return str_date!!
    }

    public fun parseDateToString3(strDate: Date): String {
        val simpleDate = SimpleDateFormat("dd/MM/yyyy")
        val strDt = simpleDate.format(strDate)
        return strDt!!
    }

    public fun parseDateString_format_Mounth(strDate: String): String {
        val dateFormat = SimpleDateFormat("dd-MM-yyyy")
        var date: Date? = null
        try {
            date = dateFormat.parse(strDate)
        } catch (e: Exception) {
        }
        if (date != null) {
            val simpleDate = SimpleDateFormat("dd MMM yyyy")
            val strDt = simpleDate.format(date)
            return strDt!!
        }
        return ""
    }

    public fun parseStringToCalendar(strDate: String): Calendar {
//        val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
        var calendar: Calendar? = Calendar.getInstance()
        try {
            calendar?.time = dateFormat.parse(strDate)
        } catch (e: Exception) {
        }

        return calendar!!
    }


}