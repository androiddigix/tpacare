package th.co.digix.tpacare.menu_card

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.CardPolicyObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.menu_home.Page_Fragment_Home
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.SharedPreferencesManager

class Page_Fragment_FindMySave : Fragment() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_policy_list = httpServerURL.api_policy_list
    val api_auto_policy_activate = httpServerURL.api_auto_policy_activate
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var linearLayout_root_bg: LinearLayout? = null
    var linearLayout_idcard_form: LinearLayout? = null

    var edittext_idcard_form: EditText? = null

    var textView_idcard_1: TextView? = null
    var textView_idcard_2: TextView? = null
    var textView_idcard_3: TextView? = null
    var textView_idcard_4: TextView? = null
    var textView_idcard_5: TextView? = null
    var textView_idcard_6: TextView? = null
    var textView_idcard_7: TextView? = null
    var textView_idcard_8: TextView? = null
    var textView_idcard_9: TextView? = null
    var textView_idcard_10: TextView? = null
    var textView_idcard_11: TextView? = null
    var textView_idcard_12: TextView? = null
    var textView_idcard_13: TextView? = null

    var linearLayout_find: LinearLayout? = null

    var progressBackground: RelativeLayout? = null
    var rootView: View? = null

    var isRegisterBus = false

    var idcard = ""
    var insured_type = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.page_fragment_find_my_policy, container, false)
        if (!isRegisterBus) {
            BusProvider.getInstance().register(this)
            isRegisterBus = true
        }

        initialIntentData()

        initInstanceToolbar()

        initialView()

        setViewWorking()

//        callService()

        return rootView
    }

    private fun initialIntentData() {
        val bundle = this.arguments
        if (bundle != null) {
            try {
                insured_type = bundle.getString("insured_type", "")
            } catch (e: Exception) {
            }
        }
    }

    private fun setViewWorking() {

        linearLayout_idcard_form?.setOnClickListener {
            if (idcard.length == 0) {
                textView_idcard_1?.setText("|")
                textView_idcard_1?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 1) {
                textView_idcard_2?.setText("|")
                textView_idcard_2?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 2) {
                textView_idcard_3?.setText("|")
                textView_idcard_3?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 3) {
                textView_idcard_4?.setText("|")
                textView_idcard_4?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 4) {
                textView_idcard_5?.setText("|")
                textView_idcard_5?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 5) {
                textView_idcard_6?.setText("|")
                textView_idcard_6?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 6) {
                textView_idcard_7?.setText("|")
                textView_idcard_7?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 7) {
                textView_idcard_8?.setText("|")
                textView_idcard_8?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 8) {
                textView_idcard_9?.setText("|")
                textView_idcard_9?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 9) {
                textView_idcard_10?.setText("|")
                textView_idcard_10?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 10) {
                textView_idcard_11?.setText("|")
                textView_idcard_11?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            } else if (idcard.length == 11) {
                textView_idcard_12?.setText("|")
                textView_idcard_12?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
            }

            edittext_idcard_form?.requestFocus()

            val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity?.getCurrentFocus()
            if (view == null) {
                view = View(activity)
            }
            imm.showSoftInput(view, 0)

        }

        linearLayout_find?.setOnClickListener {

            if (idcard.length == 13) {

                val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(linearLayout_find?.getWindowToken(), 0)

                val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                val userObject = sharedPreferencesManager.getUserObject()

                val cryptLib = CryptLib()
                val citizen_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(idcard, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(activity!!)
                val formBody = FormBody.Builder()
                formBody.add("insured_type", insured_type)
                formBody.add("citizen_id", "" + citizen_id_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(root_url, api_policy_list, "api_policy_list_find")
                progressBackground?.visibility = View.VISIBLE
            } else {

            }
        }

        edittext_idcard_form?.addTextChangedListener(
            onTextChangedSetControlEdittext(
            )
        )

    }

    private fun onTextChangedSetControlEdittext(): TextWatcher {

        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                Log.d("coke", "onTextChanged s " + s + " start " + start + " before " + before + " count " + count)

                var textChange = s.toString()
                idcard = "" + textChange

                var idcard_1 = ""
                var idcard_2 = ""
                var idcard_3 = ""
                var idcard_4 = ""
                var idcard_5 = ""
                var idcard_6 = ""
                var idcard_7 = ""
                var idcard_8 = ""
                var idcard_9 = ""
                var idcard_10 = ""
                var idcard_11 = ""
                var idcard_12 = ""
                var idcard_13 = ""
                try {
                    idcard_1 = "" + s.get(0)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_2 = "" + s.get(1)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_3 = "" + s.get(2)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_4 = "" + s.get(3)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_5 = "" + s.get(4)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_6 = "" + s.get(5)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_7 = "" + s.get(6)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_8 = "" + s.get(7)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_9 = "" + s.get(8)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_10 = "" + s.get(9)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_11 = "" + s.get(10)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_12 = "" + s.get(11)
                } catch (e: java.lang.Exception) {
                }
                try {
                    idcard_13 = "" + s.get(12)
                } catch (e: java.lang.Exception) {
                }

                textView_idcard_1?.setText("" + idcard_1)
                textView_idcard_2?.setText("" + idcard_2)
                textView_idcard_3?.setText("" + idcard_3)
                textView_idcard_4?.setText("" + idcard_4)
                textView_idcard_5?.setText("" + idcard_5)
                textView_idcard_6?.setText("" + idcard_6)
                textView_idcard_7?.setText("" + idcard_7)
                textView_idcard_8?.setText("" + idcard_8)
                textView_idcard_9?.setText("" + idcard_9)
                textView_idcard_10?.setText("" + idcard_10)
                textView_idcard_11?.setText("" + idcard_11)
                textView_idcard_12?.setText("" + idcard_12)
                textView_idcard_13?.setText("" + idcard_13)

                textView_idcard_1?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_2?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_3?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_4?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_5?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_6?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_7?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_8?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_9?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_10?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_11?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_12?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_13?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))

                if (textChange.length == 0) {
                    textView_idcard_1?.setText("|")
                    textView_idcard_1?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 1) {
                    textView_idcard_2?.setText("|")
                    textView_idcard_2?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 2) {
                    textView_idcard_3?.setText("|")
                    textView_idcard_3?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 3) {
                    textView_idcard_4?.setText("|")
                    textView_idcard_4?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 4) {
                    textView_idcard_5?.setText("|")
                    textView_idcard_5?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 5) {
                    textView_idcard_6?.setText("|")
                    textView_idcard_6?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 6) {
                    textView_idcard_7?.setText("|")
                    textView_idcard_7?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 7) {
                    textView_idcard_8?.setText("|")
                    textView_idcard_8?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 8) {
                    textView_idcard_9?.setText("|")
                    textView_idcard_9?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 9) {
                    textView_idcard_10?.setText("|")
                    textView_idcard_10?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 10) {
                    textView_idcard_11?.setText("|")
                    textView_idcard_11?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 11) {
                    textView_idcard_12?.setText("|")
                    textView_idcard_12?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 12) {
                    textView_idcard_13?.setText("|")
                    textView_idcard_13?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (textChange.length == 13) {
                    val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    var view = activity?.getCurrentFocus()
                    if (view == null) {
                        view = View(activity)
                    }
                    imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        }
    }

    private fun initInstanceToolbar() {
        btnBack = rootView?.findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener {
            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.replaceFragment(Page_Fragment_Home())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 0
        }

        textView_toolbar_title = rootView?.findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_card_policy))
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    fun showMessageDialog_check_idcard(activity: Activity, isAllowUser: Boolean) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_message_check_idcard, null)

        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()

            val activity: ActivityMain = getActivity() as ActivityMain
            var page_Fragment_Card = Page_Fragment_Card()
            val bundle = Bundle()
            bundle.putBoolean("isAllowUser", isAllowUser)
            bundle.putString("idcard", idcard)
            bundle.putString("insured_type", insured_type)
            page_Fragment_Card.setArguments(bundle)
            activity?.replaceFragment(page_Fragment_Card)
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()
    }

    private fun initialView() {

        linearLayout_root_bg = rootView?.findViewById(R.id.linearLayout_root_bg) as LinearLayout
        progressBackground = rootView?.findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }
        linearLayout_root_bg?.setOnClickListener { }
        linearLayout_root_bg?.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                //Find the currently focused view, so we can grab the correct window token from it.
                var view = activity?.getCurrentFocus()
                //If no view currently has focus, create a new one, just so we can grab a window token from it
                if (view == null) {
                    view = View(activity)
                }
                imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)

                if (idcard.length == 0) {
                    textView_idcard_1?.setText("")
                    textView_idcard_1?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 1) {
                    textView_idcard_2?.setText("")
                    textView_idcard_2?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 2) {
                    textView_idcard_3?.setText("")
                    textView_idcard_3?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 3) {
                    textView_idcard_4?.setText("")
                    textView_idcard_4?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 4) {
                    textView_idcard_5?.setText("")
                    textView_idcard_5?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 5) {
                    textView_idcard_6?.setText("")
                    textView_idcard_6?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 6) {
                    textView_idcard_7?.setText("")
                    textView_idcard_7?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 7) {
                    textView_idcard_8?.setText("")
                    textView_idcard_8?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 8) {
                    textView_idcard_9?.setText("")
                    textView_idcard_9?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 9) {
                    textView_idcard_10?.setText("")
                    textView_idcard_10?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 10) {
                    textView_idcard_11?.setText("")
                    textView_idcard_11?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 11) {
                    textView_idcard_12?.setText("")
                    textView_idcard_12?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                } else if (idcard.length == 12) {
                    textView_idcard_13?.setText("")
                    textView_idcard_13?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_black))
                }
            }

        }

        linearLayout_idcard_form = rootView?.findViewById(R.id.linearLayout_idcard_form) as LinearLayout
        edittext_idcard_form = rootView?.findViewById(R.id.edittext_idcard_form) as EditText

        textView_idcard_1 = rootView?.findViewById(R.id.textView_idcard_1) as TextView
        textView_idcard_2 = rootView?.findViewById(R.id.textView_idcard_2) as TextView
        textView_idcard_3 = rootView?.findViewById(R.id.textView_idcard_3) as TextView
        textView_idcard_4 = rootView?.findViewById(R.id.textView_idcard_4) as TextView
        textView_idcard_5 = rootView?.findViewById(R.id.textView_idcard_5) as TextView
        textView_idcard_6 = rootView?.findViewById(R.id.textView_idcard_6) as TextView
        textView_idcard_7 = rootView?.findViewById(R.id.textView_idcard_7) as TextView
        textView_idcard_8 = rootView?.findViewById(R.id.textView_idcard_8) as TextView
        textView_idcard_9 = rootView?.findViewById(R.id.textView_idcard_9) as TextView
        textView_idcard_10 = rootView?.findViewById(R.id.textView_idcard_10) as TextView
        textView_idcard_11 = rootView?.findViewById(R.id.textView_idcard_11) as TextView
        textView_idcard_12 = rootView?.findViewById(R.id.textView_idcard_12) as TextView
        textView_idcard_13 = rootView?.findViewById(R.id.textView_idcard_13) as TextView

        linearLayout_find = rootView?.findViewById(R.id.linearLayout_find) as LinearLayout

    }


    fun showMessageDialog_wow(activity: Activity, message: String, result: JSONArray) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_message, null)

        val textView_popup_message = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_message) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_message.setText("" + message)

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()

            val sharedPreferencesManager = SharedPreferencesManager(activity!!)
            val userObject = sharedPreferencesManager.getUserObject()
            val user_phone = userObject.tel
            val username = userObject.username

            var isAllowUser = false
            for (i in 0..result.length() - 1) {
                var resultJson: JSONObject? = null
                try {
                    resultJson = result.get(i) as JSONObject
                } catch (e: Exception) {
                }
                if (resultJson != null) {
                    val cardPolicyObject = CardPolicyObject()
                    cardPolicyObject.setParam(resultJson)
                    val cryptLib = CryptLib()
                    var user_phone_decrypt = ""
                    if (!userObject.tel.equals("")) {
                        user_phone_decrypt = cryptLib.decryptCipherTextWithRandomIV(user_phone, salt_key)
                    }
                    if (user_phone_decrypt.equals(cardPolicyObject.telNo)) {
                        if (!cardPolicyObject.policyFrom.equals("dummy")) {
                            isAllowUser = true
                        }
                    }
                }
            }
            if (isAllowUser) {
                val cryptLib = CryptLib()
                val idcard_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(idcard, salt_key)
                val username_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(username, salt_key)


                val defaultParameterAPI = DefaultParameterAPI(activity!!)
                val formBody = FormBody.Builder()
                formBody.add("username", "" + username)
                formBody.add("citizen_id", "" + idcard_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url,
                    api_auto_policy_activate,
                    "api_auto_policy_activate"
                )
                progressBackground?.visibility = View.VISIBLE
            } else {
                showMessageDialog_check_idcard(activity!!, isAllowUser)
            }
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(true)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {
        if (event.getHttpName().equals("api_policy_list_find")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    var isTPAPolicyFindNotFound: String? = ""

                    try {
                        isTPAPolicyFindNotFound = responseJson.getString("isTPAPolicyFindNotFound")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (result != null) {
                        if (isTPAPolicyFindNotFound.equals("true", ignoreCase = true)) {
                            val message_fix = "" + resources.getString(R.string.popup_find_my_card_found)
                            showMessageDialog_wow(activity!!, "" + message_fix, result)
                        } else {
                            val message_fix = "" + resources.getString(R.string.popup_find_my_card_not_found)
                            showMessageDialog_wow(activity!!, "" + message_fix, result)
                        }
                    }
//                    else {
//                        val message_fix = "" + resources.getString(R.string.popup_find_my_card_not_found)
//                        val alertDialogUtil = AlertDialogUtil()
//                        alertDialogUtil.showMessageDialog(activity!!, "" + message_fix)
//                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }

        if (event.getHttpName().equals("api_auto_policy_activate")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {

                    }

                    val cryptLib = CryptLib()
                    val idcard_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(idcard, salt_key)

                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    userObject.citizen_id = idcard_encrypt
                    sharedPreferencesManager.setUserObject(userObject)

                    showMessageDialog_check_idcard(activity!!, true)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
    }
}
