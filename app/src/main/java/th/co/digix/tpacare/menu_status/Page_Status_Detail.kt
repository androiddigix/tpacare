package th.co.digix.tpacare.menu_status

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import com.pvdproject.adapter.AdapterRecycler_ClaimStatus_detail
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.ClaimStatusObject
import th.co.digix.tpacare.custom_object.ClaimStatusSubObject
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.utility.NotificationBarColor
import java.util.ArrayList

class Page_Status_Detail : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_values_hospital = httpServerURL.api_values_hospital
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var progressBackground: RelativeLayout? = null

    var textView_cure_date: TextView? = null
    var textView_conpanee_name: TextView? = null
    var textView_card_type: TextView? = null
    var textView_policy_no: TextView? = null

    var recyclerView_claim_status_sub: RecyclerView? = null
    var adapterRecycler_ClaimStatus_detail: AdapterRecycler_ClaimStatus_detail? = null

    var claimStatusObject = ClaimStatusObject()

    val statusCodsSubObject_List = ArrayList<ClaimStatusSubObject>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_status_detail)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

//        callService_hospital()
    }


    private fun initialWorking() {

        textView_cure_date?.setText("" + claimStatusObject.visitDate)
        textView_conpanee_name?.setText("" + claimStatusObject.insurer_name)
        textView_card_type?.setText("" + claimStatusObject.card_type)
        textView_policy_no?.setText("" + claimStatusObject.pol_no)

        val claim_status = claimStatusObject.claim_status

        var claim_status_Array: JSONArray? = null
        try {
            claim_status_Array = JSONArray(claim_status)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (claim_status_Array != null) {
            statusCodsSubObject_List.clear()
            for (i in 0..claim_status_Array.length() - 1) {
                var claimStatusSubJson: JSONObject? = null
                try {
                    claimStatusSubJson = claim_status_Array.get(i) as JSONObject
                } catch (e: Exception) {
                }
                if (claimStatusSubJson != null) {
                    val statusCodsSubObject = ClaimStatusSubObject()
                    statusCodsSubObject.setParam(claimStatusSubJson)
                    statusCodsSubObject_List.add(statusCodsSubObject)
                }
            }
            adapterRecycler_ClaimStatus_detail?.notifyDataSetChanged()
        }
    }


    private fun initialView() {
        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        textView_cure_date = findViewById(R.id.textView_cure_date) as TextView
        textView_conpanee_name = findViewById(R.id.textView_conpanee_name) as TextView
        textView_card_type = findViewById(R.id.textView_card_type) as TextView
        textView_policy_no = findViewById(R.id.textView_policy_no) as TextView

        recyclerView_claim_status_sub = findViewById(R.id.recyclerView_claim_status_sub) as RecyclerView
        recyclerView_claim_status_sub?.layoutManager =
            LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recyclerView_claim_status_sub?.setNestedScrollingEnabled(false)
        adapterRecycler_ClaimStatus_detail = AdapterRecycler_ClaimStatus_detail(this, statusCodsSubObject_List)
        recyclerView_claim_status_sub?.setAdapter(adapterRecycler_ClaimStatus_detail)

    }

    private fun initialIntentData() {
        try {
            claimStatusObject = intent.getParcelableExtra("claimStatusObject")
        } catch (e: Exception) {
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_status))
    }


}