package th.co.digix.tpacare.login

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager

class Page_PasswordSetNew : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authForgotPassword = httpServerURL.api_authForgotPassword
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var progressBackground: RelativeLayout? = null

    var editText_new_password: EditText? = null
    var editText_new_password_confirm: EditText? = null

    var linearLayout_confirm_username: LinearLayout? = null
    var linearLayout_cancel_username: LinearLayout? = null

    var otp_code = ""
    var userName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_password_setnew)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialWorking() {
        linearLayout_cancel_username?.setOnClickListener {
            finish()
        }
        linearLayout_confirm_username?.setOnClickListener(View.OnClickListener {

            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = getCurrentFocus()
            if (view == null) {
                view = View(this@Page_PasswordSetNew)
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)

            var new_password = editText_new_password?.text.toString()
            var new_confirm_password = editText_new_password_confirm?.text.toString()

            if (new_password.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_PasswordSetNew,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_new_password)
                )
            } else if (new_password.length != 6) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_PasswordSetNew,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_password_wrong)
                )
            } else if (new_confirm_password.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_PasswordSetNew,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_new_password_confirm)
                )
            } else if (!new_confirm_password.equals("" + new_password)) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_PasswordSetNew,
                    "" + resources.getString(R.string.text_alert_new_password_not_match)
                )
            } else {
                var userName_encrypt = ""
                var new_password_encrypt = ""
                var otp_code_encrypt = ""


//                val sharedPreferencesManager = SharedPreferencesManager(this)
//                val userObject = sharedPreferencesManager.getUserObject()

//                val userName = userObject.username

                val cryptLib = CryptLib()
                otp_code_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(otp_code, salt_key)
                userName_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(userName, salt_key)
                new_password_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(new_password, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(this)
                val formBody = FormBody.Builder()
                formBody.add("otp_code", "" + otp_code_encrypt)
                formBody.add("username", "" + userName_encrypt)
                formBody.add("newpassword", "" + new_password_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url, api_authForgotPassword, "api_authForgotPassword"
                )

                progressBackground?.visibility = View.VISIBLE
            }
        })
    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        editText_new_password = findViewById(R.id.editText_new_password) as EditText
        editText_new_password_confirm = findViewById(R.id.editText_new_password_confirm) as EditText

        linearLayout_confirm_username = findViewById(R.id.linearLayout_confirm_username) as LinearLayout
        linearLayout_cancel_username = findViewById(R.id.linearLayout_cancel_username) as LinearLayout

    }

    private fun initialIntentData() {

        otp_code = intent.extras?.getString("otp_code", "").toString()
        userName = intent.extras?.getString("userName", "").toString()

        if(otp_code.equals("null")){
            otp_code = ""
        }
        if(userName.equals("null")){
            userName = ""
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.text_passwordforget_title))
    }


    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authForgotPassword")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result = ""
                    try {
                        result = responseJson.getString("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    finish()

//                    val alertDialogUtil = AlertDialogUtil()
//                    alertDialogUtil.showMessageDialog_finish(this@Page_PasswordSetNew, "" + message)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_PasswordSetNew, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_PasswordSetNew, resources.getString(R.string.text_service_error)
                )
            }
        }
    }

}