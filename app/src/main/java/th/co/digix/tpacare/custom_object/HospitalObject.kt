package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class HospitalObject() : Parcelable {

    var thumbnail_list: String = ""
    var provider_code: String = ""
    var hospital_type: String = ""
    var hospital_name: String = ""
    var website: String = ""
    var latitude: String = ""
    var longitude: String = ""
    var address: String = ""
    var tel: String = ""
    var nearby: String = ""


    fun setParam(result: JSONObject) {
        try {
            thumbnail_list = result.getString("thumbnail_list")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            provider_code = result.getString("provider_code")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            hospital_type = result.getString("hospital_type")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            hospital_name = result.getString("hospital_name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            website = result.getString("website")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            latitude = result.getString("latitude")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            longitude = result.getString("longitude")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            address = result.getString("address")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            tel = result.getString("tel")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            nearby = result.getString("nearby")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    constructor(parcel: Parcel) : this() {
        thumbnail_list = parcel.readString()
        provider_code = parcel.readString()
        hospital_type = parcel.readString()
        hospital_name = parcel.readString()
        website = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        address = parcel.readString()
        tel = parcel.readString()
        nearby = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(thumbnail_list)
        parcel.writeString(provider_code)
        parcel.writeString(hospital_type)
        parcel.writeString(hospital_name)
        parcel.writeString(website)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(address)
        parcel.writeString(tel)
        parcel.writeString(nearby)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HospitalObject> {
        override fun createFromParcel(parcel: Parcel): HospitalObject {
            return HospitalObject(parcel)
        }

        override fun newArray(size: Int): Array<HospitalObject?> {
            return arrayOfNulls(size)
        }
    }

}