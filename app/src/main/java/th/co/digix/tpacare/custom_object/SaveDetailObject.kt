package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class SaveDetailObject() : Parcelable {

    var covNo: String = ""
    var covDesc: String = ""
    var covLimit: String = ""
    var covUtilized: String = ""

    constructor(parcel: Parcel) : this() {
        covNo = parcel.readString()
        covDesc = parcel.readString()
        covLimit = parcel.readString()
        covUtilized = parcel.readString()
    }

    fun setParam(result: JSONObject) {
        try {
            covNo = result.getString("covNo")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            covDesc = result.getString("covDesc")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            covLimit = result.getString("covLimit")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            val covLimit_kk = covLimit.replace(",", "")
            var covLimit_Double = covLimit_kk.toDouble()
            covLimit = "" + covLimit_Double.toLong()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            covUtilized = result.getString("covUtilized")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            var covUtilized_kk = covUtilized.replace(",", "")
            var covUtilized_Double = covUtilized_kk.toDouble()
            covUtilized = "" + covUtilized_Double.toLong()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(covNo)
        parcel.writeString(covDesc)
        parcel.writeString(covLimit)
        parcel.writeString(covUtilized)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SaveDetailObject> {
        override fun createFromParcel(parcel: Parcel): SaveDetailObject {
            return SaveDetailObject(parcel)
        }

        override fun newArray(size: Int): Array<SaveDetailObject?> {
            return arrayOfNulls(size)
        }
    }

}