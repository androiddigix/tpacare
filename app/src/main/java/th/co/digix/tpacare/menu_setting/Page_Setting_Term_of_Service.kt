package th.co.digix.tpacare.menu_setting

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.UserObject
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.utility.NotificationBarColor

class Page_Setting_Term_of_Service : BaseActivity() {

    val httpServerURL = HttpServerURL()
    val host_azurewebsites = httpServerURL.host_azurewebsites
    val azurewebsitesTermAndCondition = httpServerURL.azurewebsitesTermAndCondition

    var toolbar: Toolbar? = null

    var btnBack: ImageView? = null

    var imageView_logo_title: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var webview_content: WebView? = null
    var progressBackground: RelativeLayout? = null

    var url_term_of_service = ""

    var userObject = UserObject()

    var formLogin = false
    var close_button_accept = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_setting_term_of_service)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        settingWebview()

    }

    private fun settingWebview() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        webview_content?.getSettings()!!.loadWithOverviewMode = true
        webview_content?.getSettings()!!.useWideViewPort = true

        webview_content?.getSettings()!!.setJavaScriptEnabled(true)
        webview_content?.setWebChromeClient(WebChromeClient())
        webview_content?.setWebViewClient(object : WebViewClient() {

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBackground?.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                progressBackground?.visibility = View.GONE
            }

        })

//        val sharedPreferencesManager = SharedPreferencesManager(this)
//        val app_language = sharedPreferencesManager.getLanguage()
//
//        if (url_term_of_service.equals("")) {
//            url_term_of_service = host_server + terms_and_conditions
//        }
//        webview_content?.loadUrl(url_term_of_service + "?lang=" + app_language)
        webview_content?.loadUrl(host_azurewebsites + azurewebsitesTermAndCondition+"?lang=th" )
    }

    private fun initialIntentData() {
        try {
            userObject = intent.getParcelableExtra("userObject")
        } catch (e: Exception) {
        }
        try {
            url_term_of_service = intent.getStringExtra("url_term_of_service")
        } catch (e: Exception) {
        }
        if (url_term_of_service.equals("null")) {
            url_term_of_service = ""
        }

        try {
            formLogin = intent.getBooleanExtra("formLogin", false)
        } catch (e: Exception) {
        }
        try {
            close_button_accept = intent.getBooleanExtra("close_button_accept", false)
        } catch (e: Exception) {
        }
    }

    private fun initialView() {

        webview_content = findViewById(R.id.webview_content) as WebView

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_term_of_service))
    }
}
