package th.co.digix.tpacare.menu_home.banner_activity_notuse

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.utility.NotificationBarColor

class Page_WebView_Article : BaseActivity() {

    var btnBack: ImageView? = null

    var imageView_logo_title: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var webview_content: WebView? = null
    var progressBackground: RelativeLayout? = null

    var reportTitle = ""
    var currentUrl = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_webview_pdf)

        initialIntentData()

        initInstanceToolbar()

        settingWebview()
    }

    override fun onResume() {
        super.onResume()
        BusProvider.getInstance().register(this)
    }

    override fun onPause() {
        super.onPause()
        BusProvider.getInstance().unregister(this)
    }

    private fun settingWebview() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        webview_content?.getSettings()?.loadWithOverviewMode = true
        webview_content?.getSettings()?.useWideViewPort = true

//        webview_content?.getSettings()?.setSupportZoom(true)
        webview_content?.getSettings()?.setBuiltInZoomControls(true)
        webview_content?.getSettings()?.setDisplayZoomControls(false)

        webview_content?.getSettings()!!.setJavaScriptEnabled(true)
        webview_content?.setWebChromeClient(WebChromeClient())
        webview_content?.setWebViewClient(object : WebViewClient() {

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBackground?.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                progressBackground?.visibility = View.GONE
            }

        })

//        currentUrl =  "https://www.google.com"

        if (currentUrl.toString().endsWith(".pdf")) {
            val googleDrive = "https://docs.google.com/viewer?embedded=true&url="
            currentUrl = googleDrive + currentUrl
            webview_content?.loadUrl(currentUrl)
        } else {
            webview_content?.loadUrl(currentUrl)
        }

    }

    private fun initialIntentData() {
        try {
            reportTitle = intent.getStringExtra("reportTitle")
        } catch (e: Exception) {
        }
        try {
            currentUrl = intent.getStringExtra("currentUrl")
        } catch (e: Exception) {
        }
    }

    private fun initialView() {

    }

    private fun initInstanceToolbar() {

        imageView_logo_title = findViewById(R.id.imageView_logo_title) as ImageView
        imageView_logo_title?.visibility = View.GONE

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + reportTitle)

        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE

        btnBack?.setOnClickListener() {
            finish()
        }

        webview_content = findViewById(R.id.webview_content) as WebView
    }

}
