package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class InboxObject() : Parcelable {

    var noti_id: String = ""
    var message_lable: String = ""
    var message_text: String = ""
    var delivery_date: String = ""
    var read_status: String = ""

    constructor(parcel: Parcel) : this() {
        noti_id = parcel.readString()
        message_lable = parcel.readString()
        message_text = parcel.readString()
        delivery_date = parcel.readString()
        read_status = parcel.readString()
    }

    fun setParam(result: JSONObject) {
        try {
            noti_id = result.getString("noti_id")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            message_lable = result.getString("message_lable")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            message_text = result.getString("message_text")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            delivery_date = result.getString("delivery_date")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            read_status = result.getString("read_status")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(noti_id)
        parcel.writeString(message_lable)
        parcel.writeString(message_text)
        parcel.writeString(delivery_date)
        parcel.writeString(read_status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<InboxObject> {
        override fun createFromParcel(parcel: Parcel): InboxObject {
            return InboxObject(parcel)
        }

        override fun newArray(size: Int): Array<InboxObject?> {
            return arrayOfNulls(size)
        }
    }

}