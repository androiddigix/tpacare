package th.co.digix.tpacare.menu_home

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.pvdproject.adapter.AdapterRecycler_MenuHome
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.MenuHomeObject
import th.co.digix.tpacare.login.Page_Login
import th.co.digix.tpacare.login.Page_PinLogin
import th.co.digix.tpacare.menu_home.banner_fagment.Page_Fragment_List_Article
import th.co.digix.tpacare.menu_home.banner_fagment.Page_Fragment_List_Promotion
import th.co.digix.tpacare.utility.SharedPreferencesManager

class Page_Fragment_Home : Fragment() {

    var imageView_profile: ImageView? = null
    var textView_username: TextView? = null
    var textView_hello: TextView? = null

    var textView_register_home: TextView? = null
    var linearLayout_register_home: LinearLayout? = null

    var recyclerView_menu_home: RecyclerView? = null
    var adapterRecycler_MenuHome: AdapterRecycler_MenuHome? = null

    var imageView_promotion: ImageView? = null
    var imageView_article: ImageView? = null

    var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.page_fragment_home, container, false)

        initInstanceToolbar()

        initialView()

        initialViewWorking()

        return rootView
    }

    private fun initialViewWorking() {

        imageView_promotion?.setOnClickListener {
//            val intent_go = Intent(activity, Page_PromotionList::class.java)
//            startActivity(intent_go)

            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.addFragment(Page_Fragment_List_Promotion())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 97
        }
        imageView_article?.setOnClickListener {
//            val intent_go = Intent(activity, Page_ArticleList::class.java)
//            startActivity(intent_go)

            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.addFragment(Page_Fragment_List_Article())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 96
        }

    }

    private fun initInstanceToolbar() {

        imageView_profile = rootView?.findViewById(R.id.imageView_profile) as ImageView
        textView_username = rootView?.findViewById(R.id.textView_username) as TextView
        textView_hello = rootView?.findViewById(R.id.textView_hello) as TextView

        textView_register_home = rootView?.findViewById(R.id.textView_register_home) as TextView
        linearLayout_register_home = rootView?.findViewById(R.id.linearLayout_register_home) as LinearLayout

        val sharedPreferencesManager = SharedPreferencesManager(rootView?.context!!)
        val userObject = sharedPreferencesManager.getUserObject()

        if (!userObject.state_login.equals("login")) {
            if (userObject.user_id.equals("")) {
                linearLayout_register_home?.setBackgroundResource(R.drawable.button_bg_register)
                textView_register_home?.visibility = View.VISIBLE
                linearLayout_register_home?.setOnClickListener {
                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    val user_id = userObject.user_id

                    if (user_id.equals("")) {
                        val intent_go = Intent(activity!!, Page_Login::class.java)
                        startActivity(intent_go)
                    } else {
                        val intent_go = Intent(activity!!, Page_PinLogin::class.java)
                        startActivity(intent_go)
                    }
                }
            }else{
                textView_hello?.setText(resources.getString(R.string.text_hello))
                textView_username?.setText(resources.getString(R.string.text_hello_prefix) + userObject.first_name + " " + userObject.last_name)

                if (imageView_profile != null) {
                    val url_image = "" + userObject.image_profile
                    val placeholder = BitmapFactory.decodeResource(getResources(),
                        R.drawable.icon_profile_default_home
                    )
                    val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), placeholder)
                    circularBitmapDrawable.isCircular = true
                    Glide.with(this)
                        .load(url_image)
                        .apply(
                            RequestOptions
                                .circleCropTransform()
                                .placeholder(circularBitmapDrawable)
                                .error(circularBitmapDrawable)
                        )
                        .into(imageView_profile!!)
                }

                linearLayout_register_home?.setBackgroundResource(R.drawable.button_bg_register)
                textView_register_home?.visibility = View.VISIBLE
                linearLayout_register_home?.setOnClickListener {
                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    val user_id = userObject.user_id

                    if (user_id.equals("")) {
                        val intent_go = Intent(activity!!, Page_Login::class.java)
                        startActivity(intent_go)
                    } else {
                        val intent_go = Intent(activity!!, Page_PinLogin::class.java)
                        startActivity(intent_go)
                    }
                }
            }

        } else {
            linearLayout_register_home?.setBackgroundColor(ContextCompat.getColor(activity!!,
                R.color.transparent
            ))
            textView_register_home?.visibility = View.GONE
            linearLayout_register_home?.setOnClickListener {
            }

            textView_hello?.setText(resources.getString(R.string.text_hello))
            textView_username?.setText(resources.getString(R.string.text_hello_prefix) + userObject.first_name + " " + userObject.last_name)

            if (imageView_profile != null) {
                val url_image = "" + userObject.image_profile
                val placeholder = BitmapFactory.decodeResource(getResources(),
                    R.drawable.icon_profile_default_home
                )
                val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), placeholder)
                circularBitmapDrawable.isCircular = true
                Glide.with(this)
                    .load(url_image)
                    .apply(
                        RequestOptions
                            .circleCropTransform()
                            .placeholder(circularBitmapDrawable)
                            .error(circularBitmapDrawable)
                    )
                    .into(imageView_profile!!)
            }
        }
    }

    private fun initialView() {

        recyclerView_menu_home = rootView?.findViewById(R.id.recyclerView_menu_home) as RecyclerView

        imageView_promotion = rootView?.findViewById(R.id.imageView_promotion) as ImageView
        imageView_article = rootView?.findViewById(R.id.imageView_article) as ImageView

        val gridLayoutManager = GridLayoutManager(activity, 3)
        recyclerView_menu_home?.setLayoutManager(gridLayoutManager)

        val menuHomeObject_List = ArrayList<MenuHomeObject>()

        val menuHomeObject_my_save = MenuHomeObject()
        menuHomeObject_my_save.menu_image = R.drawable.icon_menu_home_my_save
        menuHomeObject_my_save.menu_name = "" + resources.getString(R.string.menu_name_my_save)
        val menuHomeObject_myfamily_save = MenuHomeObject()
        menuHomeObject_myfamily_save.menu_image = R.drawable.icon_menu_home_myfamily_save
        menuHomeObject_myfamily_save.menu_name = "" + resources.getString(R.string.menu_name_myfamily_save)
        val menuHomeObject_claim_status = MenuHomeObject()
        menuHomeObject_claim_status.menu_image = R.drawable.icon_menu_home_claim_status
        menuHomeObject_claim_status.menu_name = "" + resources.getString(R.string.menu_name_claim_status)
        val menuHomeObject_find_hospital = MenuHomeObject()
        menuHomeObject_find_hospital.menu_image = R.drawable.icon_menu_home_find_hospital
        menuHomeObject_find_hospital.menu_name = "" + resources.getString(R.string.menu_name_find_hospital)
        val menuHomeObject_fitme = MenuHomeObject()
        menuHomeObject_fitme.menu_image = R.drawable.icon_menu_home_fitme
        menuHomeObject_fitme.menu_name = "" + resources.getString(R.string.menu_name_fitme)
        val menuHomeObject_diet = MenuHomeObject()
        menuHomeObject_diet.menu_image = R.drawable.icon_menu_home_diet
        menuHomeObject_diet.menu_name = "" + resources.getString(R.string.menu_name_diet)
        val menuHomeObject_health = MenuHomeObject()
        menuHomeObject_health.menu_image = R.drawable.icon_menu_home_health
        menuHomeObject_health.menu_name = "" + resources.getString(R.string.menu_name_health)

        menuHomeObject_List.add(menuHomeObject_my_save)
        menuHomeObject_List.add(menuHomeObject_myfamily_save)
        menuHomeObject_List.add(menuHomeObject_claim_status)
        menuHomeObject_List.add(menuHomeObject_find_hospital)
        menuHomeObject_List.add(menuHomeObject_fitme)
        menuHomeObject_List.add(menuHomeObject_diet)
        menuHomeObject_List.add(menuHomeObject_health)

        adapterRecycler_MenuHome = AdapterRecycler_MenuHome(activity, menuHomeObject_List)
        recyclerView_menu_home?.setAdapter(adapterRecycler_MenuHome)
        recyclerView_menu_home?.setNestedScrollingEnabled(false)
    }

}
