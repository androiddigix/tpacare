package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class ClaimDetailObject() : Parcelable {

    var clmCard: String = ""
    var clmInsurer: String = ""
    var clmStatus: String = ""
    var clmStatusTxt: String = ""
    var clmPayable: String = ""
    var clmVisitDate: String = ""
    var clmProvider: String = ""

    constructor(parcel: Parcel) : this() {
        clmCard = parcel.readString()
        clmInsurer = parcel.readString()
        clmStatus = parcel.readString()
        clmStatusTxt = parcel.readString()
        clmPayable = parcel.readString()
        clmVisitDate = parcel.readString()
        clmProvider = parcel.readString()
    }

    fun setParam(result: JSONObject) {
        try {
            clmCard = result.getString("clmCard")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            clmInsurer = result.getString("clmInsurer")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            clmStatus = result.getString("clmStatus")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            clmStatusTxt = result.getString("clmStatusTxt")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            clmPayable = result.getString("clmPayable")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            clmVisitDate = result.getString("clmVisitDate")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            clmProvider = result.getString("clmProvider")
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(clmCard)
        parcel.writeString(clmInsurer)
        parcel.writeString(clmStatus)
        parcel.writeString(clmStatusTxt)
        parcel.writeString(clmPayable)
        parcel.writeString(clmVisitDate)
        parcel.writeString(clmProvider)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ClaimDetailObject> {
        override fun createFromParcel(parcel: Parcel): ClaimDetailObject {
            return ClaimDetailObject(parcel)
        }

        override fun newArray(size: Int): Array<ClaimDetailObject?> {
            return arrayOfNulls(size)
        }
    }

}