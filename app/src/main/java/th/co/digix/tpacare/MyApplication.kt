package th.co.digix.tpacare

import android.app.Application
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import android.view.WindowManager
import android.R
import android.graphics.drawable.Drawable
import android.os.Build
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import th.co.digix.tpacare.menu_fitme.Authenication.*
import th.co.digix.tpacare.utility.NotificationBarColor


class MyApplication : Application() {

//    private val CLIENT_SECRET = "3c3f59bfac9539df1e6a09c4b83e769c" //coke
//    private val CLIENT_SECRET = "2947f86e650e7a729e3f949e0dd36eaf" //nan
    private val CLIENT_SECRET = "dc7d2fef6d6dda813748430a7d78e8d3" //digix

    /**
     * This key was generated using the SecureKeyGenerator [java] class. Run as a Java application (not Android)
     * This key is used to encrypt the authentication token in Android user preferences. If someone decompiles
     * your application they'll have access to this key, and access to your user's authentication token
     */
    //!! THIS SHOULD BE IN AN ANDROID KEYSTORE!! See https://developer.android.com/training/articles/keystore.html
//    private val SECURE_KEY = "CHocolatePlayTCUGIxdi44" // "CVPdQNAT6fBI4rrPLEn9x0+UV84DoqLFiNHpKOPLRW0=" //keystore
    private val SECURE_KEY = "CVPdQNAT6fBI4rrPLEn9x0+UV84DoqLFiNHpKOPLRW0="

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        AuthenticationManager.configure(this, generateAuthenticationConfiguration(this, ActivityMain::class.java))

    }

    fun generateAuthenticationConfiguration(
        context: Context,
        mainActivityClass: Class<out Activity>
    ): AuthenticationConfiguration {

        try {
            val ai = context.packageManager.getApplicationInfo(context.packageName, PackageManager.GET_META_DATA)
            val bundle = ai.metaData

            // Load clientId and redirectUrl from application manifest
            val clientId = bundle.getString("th.co.digix.tpacare.CLIENT_ID")
            val redirectUrl = bundle.getString("th.co.digix.tpacare.REDIRECT_URL")


            val CLIENT_CREDENTIALS = ClientCredentials(clientId, CLIENT_SECRET, redirectUrl)

            return AuthenticationConfigurationBuilder()

                .setClientCredentials(CLIENT_CREDENTIALS)
                .setEncryptionKey(SECURE_KEY)
                .setTokenExpiresIn(2592000L) // 30 days
                .setBeforeLoginActivity(Intent(context, mainActivityClass))
                .addRequiredScopes(Scope.profile, Scope.settings, Scope.sleep, Scope.location, Scope.social)
                .addOptionalScopes(Scope.activity, Scope.weight, Scope.heartrate, Scope.nutrition)
                .setLogoutOnAuthFailure(true)

                .build()

        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }


}