package th.co.digix.tpacare.menu_home.banner_activity_notuse

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import com.pvdproject.adapter.AdapterRecycler_Article
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.RecyclerViewListener
import th.co.digix.tpacare.custom_object.ArticleObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.NotificationBarColor
import java.util.*

class Page_ArticleList : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_article = httpServerURL.api_article
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null
    var imageView_search_top: ImageView? = null

    var progressBackground: RelativeLayout? = null

    var recyclerView_list_bank: RecyclerView? = null
    var adapterRecycler_Article: AdapterRecycler_Article? = null

//    var keyword = ""

    var page = 1
    var per_page = 10
    var total_record = 0
    var loadMoreEnable = false

    var isDistance = true
    var name_bank = ""
    var name_companee = ""

    val articleObject_List = ArrayList<ArticleObject>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_article)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

        callService_article()
    }

    fun callService_article() {

        val defaultParameterAPI = DefaultParameterAPI(this)
        var formBody = "?page=" + "1" +
                "&pageSize=" + per_page

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_article + formBody,
            "api_article"
        )

        progressBackground?.visibility = View.VISIBLE
    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }


    private fun initialWorking() {
        recyclerView_list_bank?.addOnItemTouchListener(
            RecyclerViewListener(
                this,
                recyclerView_list_bank!!,
                object : RecyclerViewListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        if (articleObject_List.size > position) {
                            val articleObject = articleObject_List.get(position)
                            var reportTitle = "" + articleObject.title
                            var currentUrl = "" + articleObject.content
                            val intent_go = Intent(this@Page_ArticleList, Page_WebView_Article::class.java)
                            intent_go.putExtra("reportTitle", reportTitle)
                            intent_go.putExtra("currentUrl", currentUrl)
                            startActivity(intent_go)
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }

                })
        )


        recyclerView_list_bank?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                //                    Log.d("per", "newState : " + newState);
                val view1 = recyclerView!!.getChildAt(recyclerView.childCount - 1)
                if (view1 != null) {
                    val diff = view1.bottom - (recyclerView.height + recyclerView.scrollY)
                    if (diff <= 0) {
                        if (loadMoreEnable) {
                            loadMoreEnable = false

                            if (total_record >= page * per_page) {
                                page++
                                callService_article()
                            }
                        }
                    } else {
                        loadMoreEnable = true
                    }
                } else {
                    loadMoreEnable = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

    }


    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        recyclerView_list_bank = findViewById(R.id.recyclerView_list_bank) as RecyclerView
        recyclerView_list_bank?.layoutManager =
            LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recyclerView_list_bank?.setNestedScrollingEnabled(false)

        adapterRecycler_Article = AdapterRecycler_Article(this, articleObject_List)
        recyclerView_list_bank?.setAdapter(adapterRecycler_Article)

    }

    private fun initialIntentData() {

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_article))


        imageView_search_top = findViewById(R.id.imageView_search_top) as ImageView
        imageView_search_top?.visibility = View.GONE
        imageView_search_top?.setOnClickListener {

        }

    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_article")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {

                        for (i in 0..results.length() - 1) {
                            var article_Json: JSONObject? = null
                            try {
                                article_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (article_Json != null) {
                                val articleObject = ArticleObject()
                                articleObject.setParam(article_Json)
                                articleObject_List.add(articleObject)
                            }

                        }
                        adapterRecycler_Article?.notifyDataSetChanged()
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_ArticleList, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_ArticleList, resources.getString(R.string.text_service_error)
                )
            }
        }
    }

}