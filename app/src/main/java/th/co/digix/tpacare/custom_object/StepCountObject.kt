package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class StepCountObject() : Parcelable {

    var dateTime: String = ""
    var value: String = ""

    constructor(parcel: Parcel) : this() {
        dateTime = parcel.readString()
        value = parcel.readString()

    }

    fun setParam(result: JSONObject) {
        try {
            dateTime = result.getString("dateTime")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            value = result.getString("value")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dateTime)
        parcel.writeString(value)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StepCountObject> {
        override fun createFromParcel(parcel: Parcel): StepCountObject {
            return StepCountObject(parcel)
        }

        override fun newArray(size: Int): Array<StepCountObject?> {
            return arrayOfNulls(size)
        }
    }

}