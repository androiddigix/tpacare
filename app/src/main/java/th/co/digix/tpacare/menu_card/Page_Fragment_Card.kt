package th.co.digix.tpacare.menu_card

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.pvdproject.adapter.AdapterRecycler_CardDetail
import com.pvdproject.adapter.AdapterRecycler_claim_history
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.PagerAdapter_Card_Policy
import th.co.digix.tpacare.custom_object.CardPolicyObject
import th.co.digix.tpacare.custom_object.ClaimDetailObject
import th.co.digix.tpacare.custom_object.ListOfPolDetObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.menu_home.Page_Fragment_Home
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.*
import java.util.*

class Page_Fragment_Card : Fragment() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_policy_list = httpServerURL.api_policy_list
    val api_policy_detail = httpServerURL.api_policy_detail
    val api_insurance_delete_card = httpServerURL.api_insurance_delete_card
    val salt_key = httpServerURL.salt_key

    var requestCode_addCard_Policy = 40

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var imageView_back_to_idcard: ImageView? = null

    var imageView_pager_back: ImageView? = null
    var imageView_pager_next: ImageView? = null
    var imageView_pager_back_event: ImageView? = null
    var imageView_pager_next_event: ImageView? = null

    var viewPager_my_card: ViewPager? = null
    var tabLayout_my_card: TabLayout? = null
    var pagerAdapter_Card_Policy: PagerAdapter_Card_Policy? = null

    var linearLayout_card_expire: LinearLayout? = null
    var textView_card_expire: TextView? = null

    var linearLayout_2tab: LinearLayout? = null
    var linearLayout_tab_rigth: LinearLayout? = null
    var linearLayout_tab_left: LinearLayout? = null

    var linearLayout_content_save_detail: LinearLayout? = null
    var linearLayout_content_claim_history: LinearLayout? = null

    var progressBackground: RelativeLayout? = null
    var rootView: View? = null

    val cardPolicyObject_List = ArrayList<CardPolicyObject>()
    val listOfPolDetObject_List = ArrayList<ListOfPolDetObject>()

    val claimJSONObject_List = ArrayList<JSONObject?>()
    val claimDetailObject_List = ArrayList<ClaimDetailObject>()

    var recyclerView_save_detail: RecyclerView? = null
    var adapterRecycler_CardDetail: AdapterRecycler_CardDetail? = null

    var recyclerView_claim_history: RecyclerView? = null
    var adapterRecycler_claim_history: AdapterRecycler_claim_history? = null

    var isRegisterBus = false
    var isAllowUser = true
    var idcard = ""
    var insured_type = ""

    var position_card = -1

    var policy_id_added = ""
    var insurer_code_added = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.page_fragment_card, container, false)
        if (!isRegisterBus) {
            BusProvider.getInstance().register(this)
            isRegisterBus = true
        }

        initialIntentData()

        initInstanceToolbar()

        initialView()

        setting_pager()

        setting_2tab()

        callService_policy_list()

        return rootView
    }


    fun showTextExpire(position: Int) {

        if (-1 < position && position < cardPolicyObject_List.size) {
            val cardPolicyObject = cardPolicyObject_List.get(position)

            val dateTimeHelper = DateTimeHelper()
            val currentTime = Calendar.getInstance().getTime()
            var effTo_date = Calendar.getInstance().getTime()
            try {
                effTo_date = dateTimeHelper.parseStringToDate3(cardPolicyObject.effTo)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (currentTime.after(effTo_date) || currentTime == effTo_date) {
                linearLayout_card_expire?.visibility = View.VISIBLE
            } else {
                linearLayout_card_expire?.visibility = View.GONE
            }
            textView_card_expire?.setText(resources.getString(R.string.text_card_expire) + " " + cardPolicyObject.effTo)
        } else {
            linearLayout_card_expire?.visibility = View.GONE
        }


    }

    fun callService_detail(position: Int) {

        if (cardPolicyObject_List.size > position) {
            val cardPolicyObject = cardPolicyObject_List.get(position)

            val sharedPreferencesManager = SharedPreferencesManager(activity!!)
            val userObject = sharedPreferencesManager.getUserObject()
            val citizen_id = userObject.citizen_id
            val pol_no = cardPolicyObject.polNo
            val insurer_code = cardPolicyObject.insurerCode
            val memberCode = cardPolicyObject.memberCode

            val cryptLib = CryptLib()
//            val citizen_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(citizen_id, salt_key)
            val pol_no_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(pol_no, salt_key)
            val insurer_code_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(insurer_code, salt_key)
            var memberCode_encrypt = ""
            if (!memberCode.equals("")) {
                memberCode_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(memberCode, salt_key)
            }

//            Log.d("coke", "memberCode " + memberCode)
//            Log.d("coke", "memberCode_encrypt " + memberCode_encrypt)
//            Log.d("coke", "api_policy_detail position " + position)
            val defaultParameterAPI = DefaultParameterAPI(activity!!)
            val formBody = FormBody.Builder()
            formBody.add("policy_from", "" + cardPolicyObject.policyFrom)
            formBody.add("citizen_id", "" + citizen_id)
            formBody.add("pol_no", "" + pol_no_encrypt)
            formBody.add("insurer_code", "" + insurer_code_encrypt)
            formBody.add("member_code", "" + memberCode_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_policy_detail, "api_policy_detail_pagecard"
            )
            progressBackground?.visibility = View.VISIBLE
        }
    }

    fun callService_detail_add(position: Int) {

        if (cardPolicyObject_List.size > position) {
            val cardPolicyObject = cardPolicyObject_List.get(position)

            val sharedPreferencesManager = SharedPreferencesManager(activity!!)
            val userObject = sharedPreferencesManager.getUserObject()
            val citizen_id = userObject.citizen_id
            val pol_no = cardPolicyObject.polNo
            val insurer_code = cardPolicyObject.insurerCode

            val cryptLib = CryptLib()
//            val citizen_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(citizen_id, salt_key)
            val pol_no_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(pol_no, salt_key)
            val insurer_code_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(insurer_code, salt_key)

            Log.d("coke", "api_policy_detail position " + position)
            val defaultParameterAPI = DefaultParameterAPI(activity!!)
            val formBody = FormBody.Builder()
            formBody.add("policy_from", "" + cardPolicyObject.policyFrom)
            formBody.add("citizen_id", "" + citizen_id)
            formBody.add("pol_no", "" + pol_no_encrypt)
            formBody.add("insurer_code", "" + insurer_code_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_policy_detail, "api_policy_detail_pagecard_add"
            )
            progressBackground?.visibility = View.VISIBLE
        }
    }

    private fun initialIntentData() {
        val bundle = this.arguments
        if (bundle != null) {
            try {
                isAllowUser = bundle.getBoolean("isAllowUser", true)
            } catch (e: Exception) {
            }
            try {
                idcard = bundle.getString("idcard", "")
            } catch (e: Exception) {
            }
            try {
                insured_type = bundle.getString("insured_type", "")
            } catch (e: Exception) {
            }
        }
    }


    private fun setting_2tab() {

        linearLayout_card_expire = rootView?.findViewById(R.id.linearLayout_card_expire) as LinearLayout
        textView_card_expire = rootView?.findViewById(R.id.textView_card_expire) as TextView

        linearLayout_2tab = rootView?.findViewById(R.id.linearLayout_2tab) as LinearLayout
        linearLayout_tab_rigth = rootView?.findViewById(R.id.linearLayout_tab_rigth) as LinearLayout
        linearLayout_tab_left = rootView?.findViewById(R.id.linearLayout_tab_left) as LinearLayout

        linearLayout_tab_rigth?.setOnClickListener {
            linearLayout_content_save_detail?.visibility = View.VISIBLE
            linearLayout_content_claim_history?.visibility = View.GONE
            linearLayout_tab_rigth?.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.bg_blue_ligth))
            linearLayout_tab_left?.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.bg_blue_heavy2))
        }
        linearLayout_tab_left?.setOnClickListener {
            linearLayout_content_save_detail?.visibility = View.GONE
            linearLayout_content_claim_history?.visibility = View.VISIBLE
            linearLayout_tab_rigth?.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.bg_blue_heavy2))
            linearLayout_tab_left?.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.bg_blue_ligth))
        }

        linearLayout_content_save_detail = rootView?.findViewById(R.id.linearLayout_content_save_detail) as LinearLayout
        linearLayout_content_claim_history =
            rootView?.findViewById(R.id.linearLayout_content_claim_history) as LinearLayout

        recyclerView_save_detail = rootView?.findViewById(R.id.recyclerView_save_detail) as RecyclerView
        recyclerView_save_detail?.layoutManager =
            LinearLayoutManager(rootView?.context!!, LinearLayout.VERTICAL, false)
        recyclerView_save_detail?.setNestedScrollingEnabled(false)
        adapterRecycler_CardDetail = AdapterRecycler_CardDetail(activity, listOfPolDetObject_List)
        recyclerView_save_detail?.setAdapter(adapterRecycler_CardDetail)

        recyclerView_claim_history = rootView?.findViewById(R.id.recyclerView_claim_history) as RecyclerView
        recyclerView_claim_history?.layoutManager =
            LinearLayoutManager(rootView?.context!!, LinearLayout.VERTICAL, false)
        recyclerView_claim_history?.setNestedScrollingEnabled(false)
        adapterRecycler_claim_history = AdapterRecycler_claim_history(activity, claimDetailObject_List)
        recyclerView_claim_history?.setAdapter(adapterRecycler_claim_history)
    }

    private fun initInstanceToolbar() {
        btnBack = rootView?.findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener {
            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.replaceFragment(Page_Fragment_Home())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 0
        }

        textView_toolbar_title = rootView?.findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        if (insured_type.equals("Family", ignoreCase = true)) {
            textView_toolbar_title?.setText("" + resources.getString(R.string.title_card_policy_myfamily))
        } else {
            textView_toolbar_title?.setText("" + resources.getString(R.string.title_card_policy))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    fun callService_policy_list() {

        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
        val userObject = sharedPreferencesManager.getUserObject()

        var citizen_id = ""

        val cryptLib = CryptLib()

        if (!idcard.equals("")) {
            citizen_id = idcard
        } else {
            citizen_id = userObject.citizen_id
            if (!citizen_id.equals("")) {
                citizen_id = cryptLib.decryptCipherTextWithRandomIV(citizen_id, salt_key)
            }
        }

        val citizen_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(citizen_id, salt_key)

//     insured_type - Member ของฉัน - Family ครอบครัวของฉัน
        val defaultParameterAPI = DefaultParameterAPI(activity!!)
        val formBody = FormBody.Builder()
        formBody.add("insured_type", insured_type)
        formBody.add("citizen_id", "" + citizen_id_encrypt)
        OkHttpPost(formBody, defaultParameterAPI).execute(root_url, api_policy_list, "api_policy_list")
        progressBackground?.visibility = View.VISIBLE
    }

    fun callService_policy_list_add(policy_id: String, insurer_code: String) {

        policy_id_added = policy_id
        insurer_code_added = insurer_code

        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
        val userObject = sharedPreferencesManager.getUserObject()

        var citizen_id = ""
        val cryptLib = CryptLib()

        if (!idcard.equals("")) {
            citizen_id = idcard
        } else {
            citizen_id = userObject.citizen_id
            if (!citizen_id.equals("")) {
                citizen_id = cryptLib.decryptCipherTextWithRandomIV(citizen_id, salt_key)
            }
        }

        val citizen_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(citizen_id, salt_key)

//     insured_type - Member ของฉัน - Family ครอบครัวของฉัน
        val defaultParameterAPI = DefaultParameterAPI(activity!!)
        val formBody = FormBody.Builder()
        formBody.add("insured_type", insured_type)
        formBody.add("citizen_id", "" + citizen_id_encrypt)
        OkHttpPost(formBody, defaultParameterAPI).execute(root_url, api_policy_list, "api_policy_list_add")
        progressBackground?.visibility = View.VISIBLE
    }

    private fun initialView() {

        progressBackground = rootView?.findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        imageView_back_to_idcard = rootView?.findViewById(R.id.imageView_back_to_idcard) as ImageView

        if (isAllowUser) {
            imageView_back_to_idcard?.visibility = View.GONE
        } else {
            imageView_back_to_idcard?.visibility = View.VISIBLE
        }
        imageView_back_to_idcard?.setOnClickListener {
            val activityMain: ActivityMain = activity as ActivityMain
            var page_Fragment_FindMySave =
                Page_Fragment_FindMySave()
            val bundle = Bundle()
            bundle.putString("insured_type", "Member")
            page_Fragment_FindMySave.setArguments(bundle)
            activityMain?.replaceFragment(page_Fragment_FindMySave)
        }
    }

    private fun set_policy_detail(result: JSONObject?) {

        var listOfPolDet: JSONArray? = null
        try {
            listOfPolDet = result?.getJSONArray("listOfPolDet")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        listOfPolDetObject_List.clear()
        if (listOfPolDet != null) {
            if (listOfPolDet.length() > 0) {
                for (i in 0..listOfPolDet.length() - 1) {
                    var listOfPolDetJson: JSONObject? = null
                    try {
                        listOfPolDetJson = listOfPolDet.get(i) as JSONObject
                    } catch (e: Exception) {
                    }
                    if (listOfPolDetJson != null) {
                        val listOfPolDetObject = ListOfPolDetObject()
                        listOfPolDetObject.listOfPolDet = listOfPolDetJson.toString()
                        listOfPolDetObject_List.add(listOfPolDetObject)
                    }
                }
            }

        }
        adapterRecycler_CardDetail?.notifyDataSetChanged()

        var listOfPolClaim: JSONArray? = null
        try {
            listOfPolClaim = result?.getJSONArray("listOfPolClaim")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        claimDetailObject_List?.clear()
        if (listOfPolClaim != null) {
            if (listOfPolClaim.length() > 0) {
                for (i in 0..listOfPolClaim.length() - 1) {
                    var listOfPolClaim_Json: JSONObject? = null
                    try {
                        listOfPolClaim_Json = listOfPolClaim.get(i) as JSONObject
                    } catch (e: Exception) {
                    }
                    if (listOfPolClaim_Json != null) {
                        val claimDetailObject = ClaimDetailObject()
                        claimDetailObject.setParam(listOfPolClaim_Json)
                        claimDetailObject_List.add(claimDetailObject)
                    }

                }
            }
        }
        adapterRecycler_claim_history?.notifyDataSetChanged()
    }

    private fun setting_pager() {

        imageView_pager_back = rootView?.findViewById(R.id.imageView_pager_back) as ImageView
        imageView_pager_next = rootView?.findViewById(R.id.imageView_pager_next) as ImageView

        imageView_pager_back_event = rootView?.findViewById(R.id.imageView_pager_back_event) as ImageView
        imageView_pager_next_event = rootView?.findViewById(R.id.imageView_pager_next_event) as ImageView

        viewPager_my_card = rootView?.findViewById(R.id.viewPager_my_card) as ViewPager
        tabLayout_my_card = rootView?.findViewById(R.id.tabLayout_my_card) as TabLayout

        imageView_pager_back_event?.setOnClickListener {
            if (0 < position_card) {
                position_card--
                viewPager_my_card?.setCurrentItem(position_card)
            }
        }
        imageView_pager_next_event?.setOnClickListener {
            if (position_card < cardPolicyObject_List.size) {
                position_card++
                viewPager_my_card?.setCurrentItem(position_card)
            }
        }
        pagerAdapter_Card_Policy =
            PagerAdapter_Card_Policy(
                getChildFragmentManager(),
                cardPolicyObject_List,
                isAllowUser,
                idcard,
                insured_type
            )
        viewPager_my_card?.setAdapter(pagerAdapter_Card_Policy)
        viewPager_my_card?.setOffscreenPageLimit(0)
        tabLayout_my_card?.setupWithViewPager(viewPager_my_card, true)
        viewPager_my_card?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                showTextExpire(position)

                if (0 == position) {
                    imageView_pager_back?.setImageResource(R.drawable.icon_pager_back_normal)
                    imageView_pager_next?.setImageResource(R.drawable.icon_pager_next_active)
                } else if (0 < position && position < cardPolicyObject_List.size) {
                    imageView_pager_back?.setImageResource(R.drawable.icon_pager_back_active)
                    imageView_pager_next?.setImageResource(R.drawable.icon_pager_next_active)
                } else if (cardPolicyObject_List.size == position) {
                    imageView_pager_back?.setImageResource(R.drawable.icon_pager_back_active)
                    imageView_pager_next?.setImageResource(R.drawable.icon_pager_next_normal)
                }

                val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                val userObject = sharedPreferencesManager.getUserObject()
                val citizen_id = userObject.citizen_id
                if (!citizen_id.equals("")) {
                    val cryptLib = CryptLib()
                    val citizen_id_encrypt = cryptLib.decryptCipherTextWithRandomIV(citizen_id, salt_key)
                    if (!citizen_id_encrypt.equals("")) {
                        if (claimJSONObject_List.size > position) {
                            set_policy_detail(claimJSONObject_List.get(position))
                        } else {
                            if (cardPolicyObject_List.size > position) {
                                callService_detail(position)
                            } else {
                                val json_fix =
                                    JSONObject(" {\"result\":{\"listOfPolDet\":null,\"listOfPolClaim\":null},\"status\":true,\"message\":\"success\",\"errorMessage\":\"\"}\n")
                                set_policy_detail(json_fix)
                            }
                        }

                    }
                }


                position_card = position
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })


        val configuration = getResources().getConfiguration()
        val fontScale = configuration.fontScale - 0.19

        if(fontScale > 1){
            val pager_card_height_InPixels = resources.getDimension(R.dimen.pager_card_height)
            val pager_card_height_changeScale = (pager_card_height_InPixels * fontScale).toInt()
            viewPager_my_card?.getLayoutParams()?.height = pager_card_height_changeScale
            Log.d("coke", "pager_card_height_InPixels " + pager_card_height_InPixels)
            Log.d("coke", "pager_card_height_changeScale " + pager_card_height_changeScale)
        }

        Log.d("coke", "configuration.fontScale " + fontScale)
    }

    fun showMessageDialog_duplicateCard(activity: Activity, message: String) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_message, null)

        val textView_popup_message = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_message) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_message.setText("" + message)

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            pagerAdapter_Card_Policy?.setAllowUser(isAllowUser)
            pagerAdapter_Card_Policy?.notifyDataSetChanged()

            val cryptLib = CryptLib()
            val idcard_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(idcard, salt_key)

            val sharedPreferencesManager = SharedPreferencesManager(activity!!)
            val userObject = sharedPreferencesManager.getUserObject()
            userObject.citizen_id = idcard_encrypt
            sharedPreferencesManager.setUserObject(userObject)

//                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
//                    val userObject = sharedPreferencesManager.getUserObject()

            if (cardPolicyObject_List.size > 0) {
                callService_detail(0)
            }

            alertDialog_NeedHelp?.dismiss()

        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }


    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {
        if (event.getHttpName().equals("api_policy_list")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {
                        cardPolicyObject_List.clear()
                        for (i in 0..result.length() - 1) {
                            var resultJson: JSONObject? = null
                            try {
                                resultJson = result.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (resultJson != null) {
                                val cardPolicyObject = CardPolicyObject()
                                cardPolicyObject.setParam(resultJson)
//                                cardPolicyObject.telNo = "0803100120" //ใส่เบอร์โทรที่อยากเทส
                                cardPolicyObject_List.add(cardPolicyObject)
                            }
                        }
                        pagerAdapter_Card_Policy?.notifyDataSetChanged()
                        position_card = 0

                        showTextExpire(0)

                        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                        val userObject = sharedPreferencesManager.getUserObject()
                        val citizen_id = userObject.citizen_id
                        if (!citizen_id.equals("")) {
                            val httpServerURL = HttpServerURL()
                            val salt_key = httpServerURL.salt_key
                            val cryptLib = CryptLib()
                            val citizen_id_encrypt = cryptLib.decryptCipherTextWithRandomIV(citizen_id, salt_key)

                            if (!citizen_id_encrypt.equals("")) {
                                if (cardPolicyObject_List.size > 0) {
                                    callService_detail(0)
                                }
                            }
                        }

                        val cardPolicyObject_List_temp = ArrayList<CardPolicyObject>()
                        var isCardDuplicate = false
                        for (i in 0..cardPolicyObject_List.size - 1) {

                            val polNo = cardPolicyObject_List.get(i).polNo
                            val insurerCode = cardPolicyObject_List.get(i).insurerCode
                            val policyFrom = cardPolicyObject_List.get(i).policyFrom

                            if (cardPolicyObject_List_temp.size == 0) {
                                cardPolicyObject_List_temp.add(cardPolicyObject_List.get(i))
                            } else {
                                var isHaveAdd = false
                                for (j in 0..cardPolicyObject_List_temp.size - 1) {

                                    val polNoo_temp = cardPolicyObject_List_temp.get(j).polNo
                                    val insurerCode_temp = cardPolicyObject_List_temp.get(j).insurerCode
                                    val policyFrom_temp = cardPolicyObject_List_temp.get(j).policyFrom
                                    val characterSet = CharacterSet()
                                    val cardPolicyObject_polNo = characterSet.removeAllSymbol(polNo)
                                    val cardPolicyObject_polNo_temp = characterSet.removeAllSymbol(polNoo_temp)

                                    if (cardPolicyObject_polNo.equals(cardPolicyObject_polNo_temp)) {
                                        if (insurerCode.equals(insurerCode_temp)) {
                                            if (policyFrom_temp.equals("dummy") || policyFrom.equals("dummy")) {
                                                isCardDuplicate = true
                                                val pol_no = cardPolicyObject_List.get(i).polNo

                                                val cryptLib = CryptLib()
                                                val pol_no_encrypt =
                                                    cryptLib.encryptPlainTextWithRandomIV_fixDigix(pol_no, salt_key)

                                                val defaultParameterAPI = DefaultParameterAPI(activity!!)
                                                val formBody = FormBody.Builder()
                                                formBody.add("pol_no", "" + pol_no_encrypt)
                                                OkHttpPost(formBody, defaultParameterAPI).execute(
                                                    root_url,
                                                    api_insurance_delete_card,
                                                    "auto_api_insurance_delete_card"
                                                )
                                            } else {
                                                isHaveAdd = true
                                            }
                                        } else {
                                            isHaveAdd = true
                                        }
                                    } else {
                                        isHaveAdd = true
                                    }


                                }
                                if (isHaveAdd) {
                                    cardPolicyObject_List_temp.add(cardPolicyObject_List.get(i))
                                }
                            }
                        }

                        if (isCardDuplicate) {
                            val message_duplicateCard =
                                "" + activity!!.resources.getString(R.string.popup_card_auto_delete)
                            val alertDialogUtil = AlertDialogUtil()
                            alertDialogUtil.showMessageDialog(activity!!, "" + message_duplicateCard)

                            cardPolicyObject_List.clear()
                            for (i in 0..cardPolicyObject_List_temp.size - 1) {
                                cardPolicyObject_List.add(cardPolicyObject_List_temp.get(i))
                            }
                            pagerAdapter_Card_Policy?.notifyDataSetChanged()
                        }


                        if (cardPolicyObject_List.size == 0) {
                            imageView_pager_back?.setImageResource(R.drawable.icon_pager_back_normal)
                            imageView_pager_next?.setImageResource(R.drawable.icon_pager_next_normal)
                        } else {
                            imageView_pager_back?.setImageResource(R.drawable.icon_pager_back_normal)
                            imageView_pager_next?.setImageResource(R.drawable.icon_pager_next_active)
                        }
                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
        if (event.getHttpName().equals("api_policy_list_add")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {
                        cardPolicyObject_List.clear()
                        for (i in 0..result.length() - 1) {
                            var resultJson: JSONObject? = null
                            try {
                                resultJson = result.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (resultJson != null) {
                                val cardPolicyObject = CardPolicyObject()
                                cardPolicyObject.setParam(resultJson)
                                cardPolicyObject_List.add(cardPolicyObject)

                                if (policy_id_added.equals("" + cardPolicyObject.polNo)
                                    && insurer_code_added.equals("" + cardPolicyObject.insurerCode)
                                ) {
                                    position_card = i
                                }
                            }
                        }
                        pagerAdapter_Card_Policy?.notifyDataSetChanged()
//                        position_card = cardPolicyObject_List.size - 1

                        showTextExpire(position_card)

                        if (0 < position_card && position_card < cardPolicyObject_List.size) {
                            viewPager_my_card?.setCurrentItem(position_card)
                        }

                        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                        val userObject = sharedPreferencesManager.getUserObject()
                        val citizen_id = userObject.citizen_id
                        if (!citizen_id.equals("")) {
                            val httpServerURL = HttpServerURL()
                            val salt_key = httpServerURL.salt_key
                            val cryptLib = CryptLib()
                            val citizen_id_encrypt = cryptLib.decryptCipherTextWithRandomIV(citizen_id, salt_key)

                            if (!citizen_id_encrypt.equals("")) {
                                if (cardPolicyObject_List.size > 0) {
                                    callService_detail_add(position_card)
                                }
                            }
                        }
                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
        if (event.getHttpName().startsWith("api_insurance_delete_card")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
//                    var result: JSONArray? = null
//                    try {
//                        result = responseJson.getJSONArray("result")
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//                    if (result != null) {
//
//                    }
                    var position = -1
                    try {
                        val position_str = event.getHttpName()
                            .substring("api_insurance_delete_card".length, event.getHttpName().length)
                        position = position_str.toInt()
                    } catch (e: Exception) {
                    }

                    if (-1 < position && position < cardPolicyObject_List.size) {
                        cardPolicyObject_List.removeAt(position)
                        if (claimJSONObject_List.size > position) {
                            claimJSONObject_List.removeAt(position)
                        }
                    }
                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    val citizen_id = userObject.citizen_id
                    if (!citizen_id.equals("")) {
                        val cryptLib = CryptLib()
                        val citizen_id_encrypt = cryptLib.decryptCipherTextWithRandomIV(citizen_id, salt_key)
                        if (!citizen_id_encrypt.equals("")) {
                            if (claimJSONObject_List.size > position) {
                                set_policy_detail(claimJSONObject_List.get(position))
                            } else {
                                if (cardPolicyObject_List.size > position) {
                                    callService_detail(position)
                                } else {
                                    val json_fix =
                                        JSONObject(" {\"result\":{\"listOfPolDet\":null,\"listOfPolClaim\":null},\"status\":true,\"message\":\"success\",\"errorMessage\":\"\"}\n")
                                    set_policy_detail(json_fix)
                                }
                            }

                        }
                    }
                    pagerAdapter_Card_Policy?.notifyDataSetChanged()
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }

        if (event.getHttpName().equals("api_policy_activate")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {

                    }
                    isAllowUser = true
                    if (isAllowUser) {
                        imageView_back_to_idcard?.visibility = View.GONE
                    } else {
                        imageView_back_to_idcard?.visibility = View.VISIBLE
                    }

                    var cardPolicyObject_List_search: ArrayList<CardPolicyObject> = cardPolicyObject_List
                    val cardPolicyObject_List_temp = ArrayList<CardPolicyObject>()

                    var isCardDuplicate = false
                    for (i in 0..cardPolicyObject_List_search.size - 1) {

                        val polNo = cardPolicyObject_List_search.get(i).polNo
                        val insurerCode = cardPolicyObject_List_search.get(i).insurerCode
                        val policyFrom = cardPolicyObject_List_search.get(i).policyFrom

                        if (cardPolicyObject_List_temp.size == 0) {
                            cardPolicyObject_List_temp.add(cardPolicyObject_List_search.get(i))
                        } else {
                            var isHaveAdd = false
                            for (j in 0..cardPolicyObject_List_temp.size - 1) {

                                val polNoo_temp = cardPolicyObject_List_temp.get(j).polNo
                                val insurerCode_temp = cardPolicyObject_List_temp.get(j).insurerCode
                                val policyFrom_temp = cardPolicyObject_List_temp.get(j).policyFrom

                                val characterSet = CharacterSet()
                                val cardPolicyObject_polNo = characterSet.removeAllSymbol(polNo)
                                val cardPolicyObject_polNo_temp = characterSet.removeAllSymbol(polNoo_temp)

                                if (cardPolicyObject_polNo.equals(cardPolicyObject_polNo_temp)) {
                                    if (insurerCode.equals(insurerCode_temp)) {
                                        if (policyFrom_temp.equals("dummy") || policyFrom.equals("dummy")) {
                                            isCardDuplicate = true
                                            val pol_no = cardPolicyObject_List.get(i).polNo

                                            val cryptLib = CryptLib()
                                            val pol_no_encrypt =
                                                cryptLib.encryptPlainTextWithRandomIV_fixDigix(pol_no, salt_key)

                                            val defaultParameterAPI = DefaultParameterAPI(activity!!)
                                            val formBody = FormBody.Builder()
                                            formBody.add("pol_no", "" + pol_no_encrypt)
                                            OkHttpPost(formBody, defaultParameterAPI).execute(
                                                root_url,
                                                api_insurance_delete_card,
                                                "auto_api_insurance_delete_card"
                                            )
                                        } else {
                                            isHaveAdd = true
                                        }
                                    } else {
                                        isHaveAdd = true
                                    }
                                } else {
                                    isHaveAdd = true
                                }


                            }
                            if (isHaveAdd) {
                                cardPolicyObject_List_temp.add(cardPolicyObject_List_search.get(i))
                            }

                        }
                    }

                    if (isCardDuplicate) {
                        val message_duplicateCard = "" + activity!!.resources.getString(R.string.popup_card_auto_delete)
                        showMessageDialog_duplicateCard(activity!!, message_duplicateCard)

                        cardPolicyObject_List.clear()
                        for (i in 0..cardPolicyObject_List_temp.size - 1) {
                            cardPolicyObject_List.add(cardPolicyObject_List_temp.get(i))
                        }
                        pagerAdapter_Card_Policy?.notifyDataSetChanged()
                    } else {
                        pagerAdapter_Card_Policy?.setAllowUser(isAllowUser)
                        pagerAdapter_Card_Policy?.notifyDataSetChanged()

                        val cryptLib = CryptLib()
                        val idcard_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(idcard, salt_key)

                        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                        val userObject = sharedPreferencesManager.getUserObject()
                        userObject.citizen_id = idcard_encrypt
                        sharedPreferencesManager.setUserObject(userObject)

//                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
//                    val userObject = sharedPreferencesManager.getUserObject()

                        if (cardPolicyObject_List.size > 0) {
                            callService_detail(0)
                        }
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
        if (event.getHttpName().equals("api_auto_policy_activate")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {

                    }
                    isAllowUser = true
                    if (isAllowUser) {
                        imageView_back_to_idcard?.visibility = View.GONE
                    } else {
                        imageView_back_to_idcard?.visibility = View.VISIBLE
                    }

                    var cardPolicyObject_List_search: ArrayList<CardPolicyObject> = cardPolicyObject_List
                    val cardPolicyObject_List_temp = ArrayList<CardPolicyObject>()

                    var isCardDuplicate = false
                    for (i in 0..cardPolicyObject_List_search.size - 1) {

                        val polNo = cardPolicyObject_List_search.get(i).polNo
                        val insurerCode = cardPolicyObject_List_search.get(i).insurerCode
                        val policyFrom = cardPolicyObject_List_search.get(i).policyFrom

                        if (cardPolicyObject_List_temp.size == 0) {
                            cardPolicyObject_List_temp.add(cardPolicyObject_List_search.get(i))
                        } else {
                            var isHaveAdd = false
                            for (j in 0..cardPolicyObject_List_temp.size - 1) {

                                val polNoo_temp = cardPolicyObject_List_temp.get(j).polNo
                                val insurerCode_temp = cardPolicyObject_List_temp.get(j).insurerCode
                                val policyFrom_temp = cardPolicyObject_List_temp.get(j).policyFrom

                                val characterSet = CharacterSet()
                                val cardPolicyObject_polNo = characterSet.removeAllSymbol(polNo)
                                val cardPolicyObject_polNo_temp = characterSet.removeAllSymbol(polNoo_temp)

                                if (cardPolicyObject_polNo.equals(cardPolicyObject_polNo_temp)) {
                                    if (insurerCode.equals(insurerCode_temp)) {
                                        if (policyFrom_temp.equals("dummy") || policyFrom.equals("dummy")) {
                                            isCardDuplicate = true
                                            val pol_no = cardPolicyObject_List_search.get(i).polNo

                                            val cryptLib = CryptLib()
                                            val pol_no_encrypt =
                                                cryptLib.encryptPlainTextWithRandomIV_fixDigix(pol_no, salt_key)

                                            val defaultParameterAPI = DefaultParameterAPI(activity!!)
                                            val formBody = FormBody.Builder()
                                            formBody.add("pol_no", "" + pol_no_encrypt)
                                            OkHttpPost(formBody, defaultParameterAPI).execute(
                                                root_url, api_insurance_delete_card, "auto_api_insurance_delete_card"
                                            )
                                        }
                                    } else {
                                        isHaveAdd = true
                                    }
                                } else {
                                    isHaveAdd = true
                                }

                            }
                            if (isHaveAdd) {
                                cardPolicyObject_List_temp.add(cardPolicyObject_List_search.get(i))
                            }

                        }
                    }

                    if (isCardDuplicate) {
                        val message_duplicateCard = "" + activity!!.resources.getString(R.string.popup_card_auto_delete)
                        showMessageDialog_duplicateCard(activity!!, message_duplicateCard)

                        cardPolicyObject_List.clear()
                        for (i in 0..cardPolicyObject_List_temp.size - 1) {
                            cardPolicyObject_List.add(cardPolicyObject_List_temp.get(i))
                        }
                        pagerAdapter_Card_Policy?.notifyDataSetChanged()


                    } else {
                        pagerAdapter_Card_Policy?.setAllowUser(isAllowUser)
                        pagerAdapter_Card_Policy?.notifyDataSetChanged()

                        val cryptLib = CryptLib()
                        val idcard_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(idcard, salt_key)

                        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                        val userObject = sharedPreferencesManager.getUserObject()
                        userObject.citizen_id = idcard_encrypt
                        sharedPreferencesManager.setUserObject(userObject)

//                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
//                    val userObject = sharedPreferencesManager.getUserObject()

                        if (cardPolicyObject_List.size > 0) {
                            callService_detail(0)
                        }
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }

        if (event.getHttpName().equals("api_policy_detail_pagecard")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {
                var status: String = ""
                var message: String = ""
                var result = JSONObject()

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    result = responseJson.getJSONObject("result")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (status.equals("true", ignoreCase = true)) {
                    // set default tab
                    linearLayout_2tab?.visibility = View.VISIBLE
                    linearLayout_content_save_detail?.visibility = View.VISIBLE
                    linearLayout_content_claim_history?.visibility = View.GONE
                    linearLayout_tab_rigth?.setBackgroundColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.bg_blue_ligth
                        )
                    )
                    linearLayout_tab_left?.setBackgroundColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.bg_blue_heavy2
                        )
                    )
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }

                set_policy_detail(result)
                claimJSONObject_List.add(result)
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }

        if (event.getHttpName().equals("api_policy_detail_pagecard_add")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {
                var status: String = ""
                var message: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                var result = JSONObject()
                try {
                    result = responseJson.getJSONObject("result")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (status.equals("true", ignoreCase = true)) {
                    // set default tab
                    linearLayout_2tab?.visibility = View.VISIBLE
                    linearLayout_content_save_detail?.visibility = View.VISIBLE
                    linearLayout_content_claim_history?.visibility = View.GONE
                    linearLayout_tab_rigth?.setBackgroundColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.bg_blue_ligth
                        )
                    )
                    linearLayout_tab_left?.setBackgroundColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.bg_blue_heavy2
                        )
                    )
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
                set_policy_detail(result)
                claimJSONObject_List.add(position_card, result)
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
    }
}
