package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class StepCountIntradayObject() : Parcelable {

    var time: String = ""
    var value: String = ""

    constructor(parcel: Parcel) : this() {
        time = parcel.readString()
        value = parcel.readString()

    }

    fun setParam(result: JSONObject) {
        try {
            time = result.getString("time")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            value = result.getString("value")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(time)
        parcel.writeString(value)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StepCountIntradayObject> {
        override fun createFromParcel(parcel: Parcel): StepCountIntradayObject {
            return StepCountIntradayObject(parcel)
        }

        override fun newArray(size: Int): Array<StepCountIntradayObject?> {
            return arrayOfNulls(size)
        }
    }

}