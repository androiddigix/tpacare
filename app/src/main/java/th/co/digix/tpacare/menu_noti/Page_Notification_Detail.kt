package th.co.digix.tpacare.menu_noti

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import com.pvdproject.adapter.AdapterRecycler_ClaimStatus_detail
import okhttp3.FormBody
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.ClaimStatusObject
import th.co.digix.tpacare.custom_object.ClaimStatusSubObject
import th.co.digix.tpacare.custom_object.InboxObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.DateTimeHelper
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.util.ArrayList

class Page_Notification_Detail : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_inboxReadMessage = httpServerURL.api_inboxReadMessage
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var progressBackground: RelativeLayout? = null


     var imageView_read_status: ImageView? = null
     var textView_inbox_name: TextView? = null
     var textView_inbox_message: TextView? = null
     var textView_inbox_date: TextView? = null
     var textView_inbox_time: TextView? = null

    var inboxObject = InboxObject()

    val statusCodsSubObject_List = ArrayList<ClaimStatusSubObject>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_notification_detail)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

        callservice_reading()
    }

    fun callservice_reading() {

        val noti_id = inboxObject.noti_id

        val cryptLib = CryptLib()
        val noti_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(noti_id, salt_key)

        val defaultParameterAPI = DefaultParameterAPI(this)
        val formBody = FormBody.Builder()
        formBody.add("noti_id", "" + noti_id_encrypt)
        OkHttpPost(formBody, defaultParameterAPI).execute(root_url, api_inboxReadMessage, "api_inboxReadMessage")

    }


    private fun initialWorking() {
//        val inboxObject = inboxObject_List.get(position)

        var message_lable = ""  + inboxObject.message_lable
        var message_text = ""  + inboxObject.message_text
        var delivery_date = ""  + inboxObject.delivery_date
        var read_status = ""  + inboxObject.read_status

        val dateTimeHelper = DateTimeHelper()
        val deliveryDate = dateTimeHelper.parseStringToDate2(delivery_date)

        val date_inbox = dateTimeHelper.parseDateToString3(deliveryDate)
        val time_inbox = dateTimeHelper.parseDateToString2(deliveryDate)

        textView_inbox_name?.setText("" + message_lable)
        textView_inbox_message?.setText("" + message_text)

        textView_inbox_date?.setText("" + date_inbox)
        textView_inbox_time?.setText("" + time_inbox)

        if(read_status.equals("R")){
            imageView_read_status?.setImageResource(R.drawable.bg_circle_inbox_status_normal)
        }else{
            imageView_read_status?.setImageResource(R.drawable.bg_circle_inbox_status_active)
        }


    }


    private fun initialView() {
        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        imageView_read_status = findViewById(R.id.imageView_read_status) as ImageView
        textView_inbox_name = findViewById(R.id.textView_inbox_name) as TextView
        textView_inbox_message = findViewById(R.id.textView_inbox_message) as TextView
        textView_inbox_date = findViewById(R.id.textView_inbox_date) as TextView
        textView_inbox_time = findViewById(R.id.textView_inbox_time) as TextView
    }

    private fun initialIntentData() {
        try {
            inboxObject = intent.getParcelableExtra("inboxObject")
        } catch (e: Exception) {
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_notification))
    }


}