package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.ListOfPolDetObject
import th.co.digix.tpacare.custom_object.SaveDetailObject
import java.util.ArrayList


class AdapterRecycler_CardDetail(
    val activity: Activity?, var listOfPolDetObject_List: ArrayList<ListOfPolDetObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (0 < listOfPolDetObject_List.size  &&  position < listOfPolDetObject_List.size ) {

            var listOfPolDet_Json: JSONObject? = null
            try {
                listOfPolDet_Json = JSONObject(listOfPolDetObject_List.get(position).listOfPolDet)
            } catch (e: Exception) {
            }
            if (listOfPolDet_Json != null) {

                var mainBeneit: String = ""
                var coverage: JSONArray? = null
                try {
                    mainBeneit = listOfPolDet_Json.getString("mainBeneit")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    coverage = listOfPolDet_Json.getJSONArray("coverage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (coverage != null) {
                    if (!mainBeneit.equals("null") || mainBeneit.equals("") ) {
                        holder.textView_save_detail_mainBeneit?.visibility = View.VISIBLE
                        holder.textView_save_detail_mainBeneit?.setText("" + mainBeneit)

                    } else {
                        holder.textView_save_detail_mainBeneit?.setText("")
                        holder.textView_save_detail_mainBeneit?.visibility = View.GONE
                    }

                    val saveDetailObject_List_coverage = ArrayList<SaveDetailObject>()
                    val saveDetailObject_List_caution = ArrayList<SaveDetailObject>()
                    for (i in 0..coverage.length() - 1) {
                        var coverage_Json: JSONObject? = null
                        try {
                            coverage_Json = coverage.get(i) as JSONObject
                        } catch (e: Exception) {
                        }
                        if (coverage_Json != null) {
                            val saveDetailObject = SaveDetailObject()
                            saveDetailObject.setParam(coverage_Json)

                            try{
                                saveDetailObject.covNo.toLong()
                                saveDetailObject_List_coverage.add(saveDetailObject)
                            }catch (e:Exception){
                                saveDetailObject_List_caution.add(saveDetailObject)
                            }

                        }
                    }


                    holder.recyclerView_save_detail_coverage?.layoutManager =
                        LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                    holder.recyclerView_save_detail_coverage?.setNestedScrollingEnabled(false)
                    var adapterRecycler_CardDetail_Coverage =
                        AdapterRecycler_CardDetail_Coverage(activity, saveDetailObject_List_coverage)
                    holder.recyclerView_save_detail_coverage?.setAdapter(adapterRecycler_CardDetail_Coverage)

                    holder.recyclerView_save_detail_caution?.layoutManager =
                        LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                    holder.recyclerView_save_detail_caution?.setNestedScrollingEnabled(false)
                    var adapterRecycler_CardDetail_Caution =
                        AdapterRecycler_CardDetail_Caution(activity, saveDetailObject_List_caution)
                    holder.recyclerView_save_detail_caution?.setAdapter(adapterRecycler_CardDetail_Caution)

                }
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_card_detail, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        if (listOfPolDetObject_List != null) {
            var listSize = listOfPolDetObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var recyclerView_save_detail_coverage: RecyclerView
        internal var recyclerView_save_detail_caution: RecyclerView
        internal var textView_save_detail_mainBeneit: TextView

        init {
            recyclerView_save_detail_coverage =
                itemView.findViewById(R.id.recyclerView_save_detail_coverage) as RecyclerView
            recyclerView_save_detail_caution =
                itemView.findViewById(R.id.recyclerView_save_detail_caution) as RecyclerView
            textView_save_detail_mainBeneit = itemView.findViewById(R.id.textView_save_detail_mainBeneit) as TextView
        }
    }

}