package th.co.digix.tpacare.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.*
import java.util.ArrayList


class AdapterRecycler_ClaimStatus(
    val activity: Activity?, var claimStatusObject_List: ArrayList<ClaimStatusObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (claimStatusObject_List.size > 0) {
            val claimStatusObject = claimStatusObject_List.get(position)

            holder.linearLayout_claim_detail.setOnClickListener {

            }

            if (position % 2 == 0) {
                holder.linearLayout_claim_detail.setBackgroundColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.bg_blue_heavy
                    )
                )
            } else {
                holder.linearLayout_claim_detail.setBackgroundColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.bg_blue_ligth
                    )
                )
            }

            holder.textView_cure_date.setText("" + claimStatusObject.visitDate)
            holder.textView_conpanee_name.setText("" + claimStatusObject.insurer_name)
            holder.textView_card_type.setText("" + claimStatusObject.card_type)

            val claim_status = claimStatusObject.claim_status

            val statusCodsSubObject_List = ArrayList<ClaimStatusSubObject>()

            var claim_status_Array: JSONArray? = null
            try {
                claim_status_Array = JSONArray(claim_status)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (claim_status_Array != null) {
                for (i in 0..claim_status_Array.length() - 1) {
                    var claimStatusSubJson: JSONObject? = null
                    try {
                        claimStatusSubJson = claim_status_Array.get(i) as JSONObject
                    } catch (e: Exception) {
                    }
                    if (claimStatusSubJson != null) {
                        val statusCodsSubObject = ClaimStatusSubObject()
                        statusCodsSubObject.setParam(claimStatusSubJson)
                        statusCodsSubObject_List.add(statusCodsSubObject)
                    }
                }
            }

            var last_status = ""
            for (i in 0..statusCodsSubObject_List.size - 1) {
                if (statusCodsSubObject_List.get(i).status_condition.equals("Done", ignoreCase = true)) {
                    last_status = statusCodsSubObject_List.get(i).status_name
                }else  if (statusCodsSubObject_List.get(i).status_condition.equals("Completed", ignoreCase = true)) {
                    last_status = statusCodsSubObject_List.get(i).status_name
                }
            }

            holder.textView_claim_status.setText("" + last_status)

            holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_accept)
//            if (claim_status.equals("approved", ignoreCase = true)) {
//            } else if (claim_status.equals("Paid", ignoreCase = true)) {
//                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_wait)
//            } else if (claim_status.equals("Rejected", ignoreCase = true)) {
//                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_cancel)
//            } else {
//                holder.imageView_claim_status.setImageResource(R.drawable.bg_circle_profile2)
//            }


        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_claim_status, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (claimStatusObject_List != null) {
            var listSize = claimStatusObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var linearLayout_claim_detail: LinearLayout
        internal var textView_cure_date: TextView
        internal var textView_conpanee_name: TextView
        internal var textView_card_type: TextView
        internal var textView_claim_status: TextView
        internal var imageView_claim_status: ImageView

        init {
            linearLayout_claim_detail =
                itemView.findViewById(R.id.linearLayout_claim_detail) as LinearLayout
            textView_cure_date = itemView.findViewById(R.id.textView_cure_date) as TextView
            textView_conpanee_name = itemView.findViewById(R.id.textView_conpanee_name) as TextView
            textView_card_type = itemView.findViewById(R.id.textView_card_type) as TextView
            textView_claim_status = itemView.findViewById(R.id.textView_claim_status) as TextView
            imageView_claim_status = itemView.findViewById(R.id.imageView_claim_status) as ImageView
        }
    }

}