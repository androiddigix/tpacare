package th.co.digix.tpacare.otto_bus;

/**
 * Created by android on 20/2/2558.
 */
public class OKHttpEvent {

    String responseStr;
    String httpName;

    public OKHttpEvent(String responseStr, String httpName) {
        this.responseStr = responseStr ;
        this.httpName = httpName ;
    }

    public String getResponseStr() {
        return responseStr;
    }

    public String getHttpName() {
        return httpName;
    }
}
