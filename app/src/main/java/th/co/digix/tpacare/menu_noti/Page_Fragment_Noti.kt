package th.co.digix.tpacare.menu_noti

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.pvdproject.adapter.AdapterRecycler_Inbox
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.RecyclerViewListener
import th.co.digix.tpacare.custom_object.InboxObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.menu_home.Page_Fragment_Home
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import java.util.ArrayList

class Page_Fragment_Noti : Fragment() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_inbox = httpServerURL.api_inbox
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var swipe_refresh_layout: SwipeRefreshLayout? = null
    var progressBackground: RelativeLayout? = null

    var textView_inbox_no_data: TextView? = null
    var recyclerView_notification: RecyclerView? = null
    var adapterRecycler_Inbox: AdapterRecycler_Inbox? = null

    var rootView: View? = null

    var page = 1
    var per_page = 10
    var total_record = 0
    var loadMoreEnable = false

    val inboxObject_List = ArrayList<InboxObject>()

    var isRegisterBus = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.page_fragment_noti, container, false)

        if (!isRegisterBus) {
            BusProvider.getInstance().register(this)
            isRegisterBus = true
        }

        initInstanceToolbar()

        initialView()

        callService(false)

        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    fun callService(isRefresh: Boolean) {

        val defaultParameterAPI = DefaultParameterAPI(activity!!)
        var formBody = "?page=" + page +
                "&pageSize=" + per_page

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_inbox + formBody,
            "api_inbox"
        )

        if (!isRefresh) {
            progressBackground?.visibility = View.VISIBLE
        }
    }

    private fun initialView() {

        swipe_refresh_layout = rootView?.findViewById(R.id.swipe_refresh_layout) as SwipeRefreshLayout
        progressBackground = rootView?.findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        textView_inbox_no_data = rootView?.findViewById(R.id.textView_inbox_no_data) as TextView
        recyclerView_notification = rootView?.findViewById(R.id.recyclerView_notification) as RecyclerView
        recyclerView_notification?.layoutManager =
            LinearLayoutManager(rootView?.context!!, LinearLayout.VERTICAL, false)
        recyclerView_notification?.setNestedScrollingEnabled(false)
        adapterRecycler_Inbox = AdapterRecycler_Inbox(activity, inboxObject_List)
        recyclerView_notification?.setAdapter(adapterRecycler_Inbox)
        recyclerView_notification?.addOnItemTouchListener(
            RecyclerViewListener(
                activity!!,
                recyclerView_notification!!,
                object : RecyclerViewListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        if (inboxObject_List.size > position) {
                            val inboxObject = inboxObject_List.get(position)
                            val intent_go = Intent(activity, Page_Notification_Detail::class.java)
                            intent_go.putExtra("inboxObject", inboxObject)
                            startActivity(intent_go)
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }

                })
        )
        recyclerView_notification?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                //                    Log.d("per", "newState : " + newState);
                val view1 = recyclerView!!.getChildAt(recyclerView.childCount - 1)
                if (view1 != null) {
                    val diff = view1.bottom - (recyclerView.height + recyclerView.scrollY)
                    if (diff <= 0) {
                        if (loadMoreEnable) {
                            loadMoreEnable = false

                            if (total_record >= page * per_page) {
                                page++
                                callService(false)
                            }
                        }
                    } else {
                        loadMoreEnable = true
                    }
                } else {
                    loadMoreEnable = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        swipe_refresh_layout?.setColorSchemeResources(R.color.blue)
        swipe_refresh_layout?.setSize(SwipeRefreshLayout.DEFAULT)

        swipe_refresh_layout?.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Refresh items
            page = 1
            inboxObject_List.clear()

            callService(true)

            adapterRecycler_Inbox?.notifyDataSetChanged()

        })
    }

    private fun initInstanceToolbar() {
        btnBack = rootView?.findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener {
            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.replaceFragment(Page_Fragment_Home())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 0
        }

        textView_toolbar_title = rootView?.findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_notification))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {
        if (event.getHttpName().equals("api_inbox")) {
            progressBackground?.visibility = View.GONE
            if (swipe_refresh_layout != null) {
                swipe_refresh_layout?.setRefreshing(false)
            }
            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {
//                        inboxObject_List.clear()
                        for (i in 0..result.length() - 1) {
                            var statusCodeJson: JSONObject? = null
                            try {
                                statusCodeJson = result.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (statusCodeJson != null) {
                                val inboxObject = InboxObject()
                                inboxObject.setParam(statusCodeJson)
                                inboxObject_List.add(inboxObject)
                            }
                        }
                        if (inboxObject_List.size > 0) {
                            textView_inbox_no_data?.visibility = View.GONE
                            recyclerView_notification?.visibility = View.VISIBLE
                            adapterRecycler_Inbox?.notifyDataSetChanged()
                        } else {
                            textView_inbox_no_data?.visibility = View.VISIBLE
                            recyclerView_notification?.visibility = View.GONE
                        }
                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
    }

}
