package th.co.digix.tpacare.utility

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.http.SslError
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import th.co.digix.tpacare.R
import th.co.digix.tpacare.login.Page_Login
import th.co.digix.tpacare.login.Page_PinLogin

class AlertDialogUtil {

    fun getSslErrorMessage(error: SslError): String {
        when (error.primaryError) {
            SslError.SSL_DATE_INVALID -> return "The certificate date is invalid."
            SslError.SSL_EXPIRED -> return "The certificate has expired."
            SslError.SSL_IDMISMATCH -> return "The certificate hostname mismatch."
            SslError.SSL_INVALID -> return "The certificate is invalid."
            SslError.SSL_NOTYETVALID -> return "The certificate is not yet valid"
            SslError.SSL_UNTRUSTED -> return "The certificate is untrusted."
            else -> return "SSL Certificate error."
        }
    }

//    fun showMessageDialog(context: Context, errorMessage: String) {
//        val builder: AlertDialog.Builder
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder = AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert)
//        } else {
//            builder = AlertDialog.Builder(context)
//        }
//        builder.setTitle(context.resources.getString(R.string.text_title_alert))
//                .setMessage("" + errorMessage)
////                .setNegativeButton(activity.resources.getString(R.string.text_cancel)){dialog, which -> }
//                .setPositiveButton(context.resources.getString(R.string.text_ok), DialogInterface.OnClickListener { dialog, whichButton ->
//
//                })
//        try {
//            builder.show()
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }

    fun show_alertDialog_lnot_login(activity: Activity) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_not_login, null)

        val textView_popup_cancel = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_cancel) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_cancel.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            val sharedPreferencesManager = SharedPreferencesManager(activity)
            val userObject = sharedPreferencesManager.getUserObject()
            val user_id = userObject.user_id

            if (user_id.equals("")) {
                val intent_go = Intent(activity, Page_Login::class.java)
                activity.startActivity(intent_go)
            } else {
                val intent_go = Intent(activity, Page_PinLogin::class.java)
                activity.startActivity(intent_go)
            }
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(true)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }


    fun showMessageDialog(activity: Activity, message: String) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_message, null)

        val textView_popup_message = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_message) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_message.setText("" + message)

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(true)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }

    fun showMessageDialog_finish(activity: Activity, message: String) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_message, null)

        val textView_popup_message = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_message) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_message.setText("" + message)

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            activity.finish()
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
//            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }


    fun showMessageDialog_internet_conection(context: Context, title: String, message: String) {
        val builder: AlertDialog.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert)
        } else {
            builder = AlertDialog.Builder(context)
        }
        builder.setTitle("" + title)
                .setMessage("" + message)
                .setPositiveButton(context.resources.getString(R.string.text_ok), DialogInterface.OnClickListener { dialog, whichButton ->
                })
        builder.setCancelable(false)
        try {
            builder.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showMessageDialog(context: Context, titleDialog: String, errorMessage: String) {
        val builder: AlertDialog.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert)
        } else {
            builder = AlertDialog.Builder(context)
        }
        builder.setTitle("" + titleDialog)
                .setMessage("" + errorMessage)
//                .setNegativeButton(activity.resources.getString(R.string.text_cancel)){dialog, which -> }
                .setPositiveButton(context.resources.getString(R.string.text_ok), DialogInterface.OnClickListener { dialog, whichButton ->

                })
        try {
            builder.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun showMessageDialogFinishPage(activity: Activity, errorMessage: String) {
        val builder: AlertDialog.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = AlertDialog.Builder(activity, android.R.style.Theme_Material_Light_Dialog_Alert)
        } else {
            builder = AlertDialog.Builder(activity)
        }
        builder.setTitle(activity.resources.getString(R.string.text_title_message))
                .setMessage("" + errorMessage)
                .setPositiveButton(activity.resources.getString(R.string.text_ok)) { dialog, which -> activity.finish() }
        try {
            builder.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}