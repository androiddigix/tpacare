package th.co.digix.tpacare.login

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager


class Page_SplashScreen : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_splashscreen)
//        checkAppUpdated()

        checkLogin()

//            checkInternetConnection()

    }

//    fun checkInternetConnection() {
//
//        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//        val netInfo: NetworkInfo? = cm.getActiveNetworkInfo()
//        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
//            TempValue.alertNoInternet_isShow = false //set default
//            Log.d("coke", "Internet Connectioning")
//        } else {
//            TempValue.alertNoInternet_isShow = true //set default
//            Log.d("coke", "No Internet Connection")
//        }
//    }

//    override fun attachBaseContext(newBase: Context) {
//        val setLanguage = SetLanguage()
//        val newBase_changeLangugage = setLanguage.checkLanguage(newBase)
//        if (newBase_changeLangugage != null) {
//            super.attachBaseContext(newBase_changeLangugage)
//        } else {
//            super.attachBaseContext(newBase)
//        }
//    }

    private fun checkLogin() {
        Handler().postDelayed({

            val intent_go = Intent(this@Page_SplashScreen, ActivityMain::class.java)
            intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent_go)

            val sharedPreferencesManager = SharedPreferencesManager(this@Page_SplashScreen)
            val userObject = sharedPreferencesManager.getUserObject()
            val state_login = userObject.state_login
            if (state_login.equals("login")) {
                userObject.state_login = "logout"
                sharedPreferencesManager.setUserObject(userObject)
            }

//            val user_id = userObject.user_id
//            if (user_id.equals("")) {
//                val intent_go = Intent(this@Page_SplashScreen, Page_Login::class.java)
//                intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//                startActivity(intent_go)
//            } else {
//                val intent_go = Intent(this@Page_SplashScreen, Page_PinLogin::class.java)
//                intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//                startActivity(intent_go)
//            }

        }, 3000)
    }


//    private fun checkAppUpdated() {
//        val sharedPreferencesManager = SharedPreferencesManager(this@Page_SplashScreen)
//        val myAppVersion = sharedPreferencesManager.getAppVersion()
//        try {
//            val pInfo = packageManager.getPackageInfo(packageName, 0)
//            val app_version = pInfo.versionName
//
//            val logServerTemp = LogServerTemp()
//
//            if (myAppVersion == "") {
//
//                sharedPreferencesManager.setAppVersion(app_version)
//
//                logServerTemp.sendLogInstallApp(this@Page_SplashScreen)
//
//            } else if (myAppVersion != app_version) {
//
//                sharedPreferencesManager.setAppVersion(app_version)
//
//                logServerTemp.sendLogUpdateApp(this@Page_SplashScreen)
//
//            }
//
//
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//    }
}