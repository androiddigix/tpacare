package th.co.digix.tpacare.utility

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}