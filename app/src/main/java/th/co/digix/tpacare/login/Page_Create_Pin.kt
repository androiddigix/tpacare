package th.co.digix.tpacare.login

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.UserObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager


class Page_Create_Pin : BaseActivity() {

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null
    var imageView_profile: ImageView? = null
    var textView_username: TextView? = null

    var textView_no1: TextView? = null
    var textView_no2: TextView? = null
    var textView_no3: TextView? = null
    var textView_no4: TextView? = null
    var textView_no5: TextView? = null
    var textView_no6: TextView? = null
    var textView_no7: TextView? = null
    var textView_no8: TextView? = null
    var textView_no9: TextView? = null
    var textView_no0: TextView? = null
    var imageView_no_delete: ImageView? = null

    var imageView_IndicatorDot_1: ImageView? = null
    var imageView_IndicatorDot_2: ImageView? = null
    var imageView_IndicatorDot_3: ImageView? = null
    var imageView_IndicatorDot_4: ImageView? = null
    var imageView_IndicatorDot_5: ImageView? = null
    var imageView_IndicatorDot_6: ImageView? = null

    var password_number = ""
    var confirm_password_number = ""

    var confirmPasswordMode = false

    var userObject = UserObject()

    var wrongPasswordCount = 0
    var stateCheckPinOk = false

    var progressBackground: RelativeLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_pin_custom)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        setOnClick()

    }

    private fun initInstanceToolbar() {

        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_login_create_pin))

        imageView_profile = findViewById(R.id.imageView_profile) as ImageView
        textView_username = findViewById(R.id.textView_username) as TextView

        textView_username?.setText(resources.getString(R.string.text_hello_prefix) + userObject.first_name + " " + userObject.last_name)

        if (imageView_profile != null) {

            val url_image = "" + userObject.image_profile
//            val image_profile_stream = "" + userObject.image_profile_stream
//            val image_profile_ByteArray = Base64.decode(image_profile_stream, Base64.DEFAULT)

            val placeholder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_profile_default_home)
            val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), placeholder)
            circularBitmapDrawable.isCircular = true
            Glide.with(this)
                .load(url_image)
                .apply(
                    RequestOptions
                        .circleCropTransform()
                        .placeholder(circularBitmapDrawable)
                        .error(circularBitmapDrawable)
                )
                .into(imageView_profile!!)
        }

    }


    override fun onDestroy() {
        super.onDestroy()

        BusProvider.getInstance().unregister(this)
    }

    private fun initialIntentData() {
        try {
            userObject = intent.getParcelableExtra("userObject")
        } catch (e: Exception) {
        }
    }

//    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
//        if (keyCode == KeyEvent.KEYCODE_BACK
//            && event?.getRepeatCount() == 0
//        ) {
//            return true
//        }
//        return super.onKeyDown(keyCode, event)
//    }

    private fun setOnClick() {

        textView_no1?.setOnClickListener() {
            addPassword("1")
        }
        textView_no2?.setOnClickListener() {
            addPassword("2")
        }
        textView_no3?.setOnClickListener() {
            addPassword("3")
        }
        textView_no4?.setOnClickListener() {
            addPassword("4")
        }
        textView_no5?.setOnClickListener() {
            addPassword("5")
        }
        textView_no6?.setOnClickListener() {
            addPassword("6")
        }
        textView_no7?.setOnClickListener() {
            addPassword("7")
        }
        textView_no8?.setOnClickListener() {
            addPassword("8")
        }
        textView_no9?.setOnClickListener() {
            addPassword("9")
        }
        textView_no0?.setOnClickListener() {
            addPassword("0")
        }

        imageView_no_delete?.setOnClickListener() {
            deletePassword()
        }


    }


    private fun deletePassword() {

        if (!confirmPasswordMode) {
            if (password_number.length > 0) {
                password_number = password_number.substring(0, password_number.length - 1)
                setIndicatorDot(password_number)
            }
        } else {
            if (confirm_password_number.length > 0) {
                confirm_password_number = confirm_password_number.substring(0, confirm_password_number.length - 1)
                setIndicatorDot(confirm_password_number)
            }
        }
//            Log.d("coke", "password_number " + password_number)
//            Log.d("coke", "confirm_password_number " + confirm_password_number)

    }

    private fun addPassword(addPassword_str: String?) {

        if (!confirmPasswordMode) {
            if (password_number.length < 6) {
                password_number = password_number + addPassword_str
            }
            setIndicatorDot(password_number)
        } else {
            if (confirm_password_number.length < 6) {
                confirm_password_number = confirm_password_number + addPassword_str
            }
            setIndicatorDot(confirm_password_number)
        }
//        Log.d("coke", "password_number " + password_number)
//        Log.d("coke", "confirm_password_number " + confirm_password_number)
    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        textView_no1 = findViewById(R.id.textView_no1) as TextView
        textView_no2 = findViewById(R.id.textView_no2) as TextView
        textView_no3 = findViewById(R.id.textView_no3) as TextView
        textView_no4 = findViewById(R.id.textView_no4) as TextView
        textView_no5 = findViewById(R.id.textView_no5) as TextView
        textView_no6 = findViewById(R.id.textView_no6) as TextView
        textView_no7 = findViewById(R.id.textView_no7) as TextView
        textView_no8 = findViewById(R.id.textView_no8) as TextView
        textView_no9 = findViewById(R.id.textView_no9) as TextView
        textView_no0 = findViewById(R.id.textView_no0) as TextView
        imageView_no_delete = findViewById(R.id.imageView_no_delete) as ImageView

        imageView_IndicatorDot_1 = findViewById(R.id.imageView_IndicatorDot_1) as ImageView
        imageView_IndicatorDot_2 = findViewById(R.id.imageView_IndicatorDot_2) as ImageView
        imageView_IndicatorDot_3 = findViewById(R.id.imageView_IndicatorDot_3) as ImageView
        imageView_IndicatorDot_4 = findViewById(R.id.imageView_IndicatorDot_4) as ImageView
        imageView_IndicatorDot_5 = findViewById(R.id.imageView_IndicatorDot_5) as ImageView
        imageView_IndicatorDot_6 = findViewById(R.id.imageView_IndicatorDot_6) as ImageView

    }

    private fun setIndicatorDot(check_number_indicator: String) {

        if (check_number_indicator.length >= 1) {
            imageView_IndicatorDot_1?.isSelected = true
        } else {
            imageView_IndicatorDot_1?.isSelected = false
        }
        if (check_number_indicator.length >= 2) {
            imageView_IndicatorDot_2?.isSelected = true
        } else {
            imageView_IndicatorDot_2?.isSelected = false
        }
        if (check_number_indicator.length >= 3) {
            imageView_IndicatorDot_3?.isSelected = true
        } else {
            imageView_IndicatorDot_3?.isSelected = false
        }
        if (check_number_indicator.length >= 4) {
            imageView_IndicatorDot_4?.isSelected = true
        } else {
            imageView_IndicatorDot_4?.isSelected = false
        }
        if (check_number_indicator.length >= 5) {
            imageView_IndicatorDot_5?.isSelected = true
        } else {
            imageView_IndicatorDot_5?.isSelected = false
        }
        if (check_number_indicator.length >= 6) {
            imageView_IndicatorDot_6?.isSelected = true
        } else {
            imageView_IndicatorDot_6?.isSelected = false
        }

        if (check_number_indicator.length >= 6) {

            val intent_go = Intent(this@Page_Create_Pin, Page_Create_Pin_confirm::class.java)
            intent_go.putExtra("userObject", userObject)
            intent_go.putExtra("pin_setting", check_number_indicator)
            startActivity(intent_go)
        }
    }

}