package th.co.digix.tpacare.menu_home.search_hospital

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.PagerAdapter_Hospital_Image
import th.co.digix.tpacare.custom_object.HospitalObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CallLatLng
import th.co.digix.tpacare.utility.NotificationBarColor
import java.util.*
import kotlin.collections.ArrayList

class Page_SearchHospital_Detail : BaseActivity(), OnMapReadyCallback {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_values_hospital = httpServerURL.api_values_hospital

    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var mapFragment: SupportMapFragment? = null
    var progressBackground: RelativeLayout? = null

    var textView_hospital_name: TextView? = null
    var linearLayout_hospital_phone: LinearLayout? = null
    var linearLayout_hospital_map: LinearLayout? = null
    var textView_hospital_open: TextView? = null
    var textView_hospital_telephone: TextView? = null
    var textView_hospital_url: TextView? = null
    var textView_hospital_address: TextView? = null

    var imageView_find_list: ImageView? = null
    var editText_keyword: EditText? = null

    var imageView_hospital: ImageView? = null
    var relativeLayout_hospital: RelativeLayout? = null
    var viewPager_hospital_image: ViewPager? = null
    var tabLayout_hospital_image: TabLayout? = null
    var pagerAdapter_hospital_image: PagerAdapter_Hospital_Image? = null

    var keyword = ""

    var requestCode_LOCATION = 63

    var page = 1
    var per_page = 10
    var total_record = 0

    var hospitalObject = HospitalObject()
    val hospitalObject_List = ArrayList<HospitalObject>()

    var current_latitude = 13.7248936 //default กทม
    var current_longitude = 102.8240698 //default กทม

    var googleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_search_hospital_detail)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

//        callService_hospital()
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        this.googleMap = googleMap

        var lat = 13.7248936
        var lng = 102.8240698

        try {
            lat = hospitalObject.latitude.toDouble()
        } catch (e: Exception) {
        }
        try {
            lng = hospitalObject.longitude.toDouble()
        } catch (e: Exception) {
        }


        val current = LatLng(lat, lng)
        val icon_pin_small = BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_small)
        val hospital_name = "" + hospitalObject.hospital_name
        val address = "" + hospitalObject.address

        val myMarker = googleMap?.addMarker(
            MarkerOptions()
                .position(current)
                .title(hospital_name)
                .snippet(address)
                .icon(icon_pin_small)
        )
        myMarker?.showInfoWindow()
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 15f))

        googleMap?.uiSettings!!.isMapToolbarEnabled = false

        val mapView = mapFragment?.getView()
        if (mapView != null) {
            val locationButton = (((mapView?.findViewById(Integer.parseInt("1")) as View)
                .getParent() as View)
                .findViewById(Integer.parseInt("2")) as View)
            val rlp = locationButton.getLayoutParams() as RelativeLayout.LayoutParams

            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            rlp.setMargins(0, 0, 30, 30)
        }

        getLocation_checkPermission()
    }

    fun callService_hospital() {

        val callLatLng = CallLatLng(this)
        val location = callLatLng.getLocation_checkGPS()
        var latitudeSTR = "0"
        var longitudeSTR = "0"

        if (location != null) {
            val latitude = location!!.getLatitude()
            val longitude = location!!.getLongitude()
            Log.d("android latitude", "is $latitude")
            Log.d("android longitude", "is $longitude")
            latitudeSTR = "" + latitude
            longitudeSTR = "" + longitude
        }

        keyword = editText_keyword?.text.toString()

        val defaultParameterAPI = DefaultParameterAPI(this)
        val formBody = "?page=" + "1" +
                "&pageSize=" + per_page +
                "&keyword=" + keyword +
                "&sLat=" + latitudeSTR +
                "&sLng=" + longitudeSTR +
                "&order_by=" + "distance"

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_values_hospital + formBody,
            "api_values_hospital_search_detail"
        )

        progressBackground?.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        BusProvider.getInstance().register(this)
    }

    override fun onPause() {
        super.onPause()
        BusProvider.getInstance().unregister(this)
    }


    private fun initialWorking() {

        var hospital_telephone = hospitalObject.tel

        textView_hospital_name?.setText("" + hospitalObject.hospital_name)
        if (hospitalObject.hospital_type.equals("hospital")) {
            textView_hospital_open?.setText("24 ชม.")
        } else {
            textView_hospital_open?.setText("-")
        }
        textView_hospital_telephone?.setText("" + hospitalObject.tel)
        textView_hospital_url?.setText("" + hospitalObject.website)
        textView_hospital_url?.setOnClickListener {
            var url = "" + hospitalObject.website
            var intent_go = Intent(Intent.ACTION_VIEW)
            intent_go.setData(Uri.parse(url))
            startActivity(intent_go)
        }
        textView_hospital_address?.setText("" + hospitalObject.address)

        linearLayout_hospital_phone?.setOnClickListener {
            val intent_go = Intent(Intent.ACTION_DIAL)
            intent_go.data = Uri.parse("tel:" + hospital_telephone)
            startActivity(intent_go)
        }
        linearLayout_hospital_map?.setOnClickListener {

            var lat = 13.7248936
            var lng = 102.8240698

            try {
                lat = hospitalObject.latitude.toDouble()
            } catch (e: Exception) {
            }
            try {
                lng = hospitalObject.longitude.toDouble()
            } catch (e: Exception) {
            }

            val uri = String.format(
                Locale.ENGLISH,
                "http://maps.google.com/maps?daddr=%f,%f (%s)",
                lat,
                lng,
                "Where the party is at"
            )

            val map_package = "com.google.android.apps.maps"

            val intent_go = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            intent_go.setPackage("" + map_package)
            try {
                startActivity(intent_go)
            } catch (e: Exception) {
                val intent_go = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + map_package)
                )
                startActivity(intent_go)
            }
        }


        var thumbnail_jsonarray: JSONArray? = null
        try {
            thumbnail_jsonarray = JSONArray(hospitalObject.thumbnail_list)
        } catch (e: Exception) {
        }
        if (thumbnail_jsonarray != null && thumbnail_jsonarray.length() > 0) {

            if (thumbnail_jsonarray.length() == 1) {
                if (imageView_hospital != null) {
                    relativeLayout_hospital?.visibility = View.GONE
                    imageView_hospital?.visibility = View.VISIBLE
                    val url_image = "" + thumbnail_jsonarray.get(0)

                    val requestOptions = RequestOptions()
                        .placeholder(R.drawable.icon_hospital_default)
                        .error(R.drawable.icon_hospital_default)

                    Glide.with(this)
                        .load(url_image)
                        .apply(requestOptions)
                        .into(imageView_hospital!!)

                }
            } else {
                relativeLayout_hospital?.visibility = View.VISIBLE
                imageView_hospital?.visibility = View.GONE

                var url_image_list = ArrayList<String>()

                var thumbnail_list_jsonarray: JSONArray? = null
                try {
                    thumbnail_list_jsonarray = JSONArray(hospitalObject.thumbnail_list)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (thumbnail_list_jsonarray != null) {
                    for (i in 0..thumbnail_list_jsonarray.length() - 1) {
                        url_image_list.add(thumbnail_list_jsonarray.get(i).toString())
                    }
                }

                pagerAdapter_hospital_image =
                    PagerAdapter_Hospital_Image(supportFragmentManager, url_image_list)
                viewPager_hospital_image?.setAdapter(pagerAdapter_hospital_image)
                viewPager_hospital_image?.setOffscreenPageLimit(1)
                tabLayout_hospital_image?.setupWithViewPager(viewPager_hospital_image, true)

                viewPager_hospital_image?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                    }

                    override fun onPageSelected(position: Int) {
                    }

                    override fun onPageScrollStateChanged(state: Int) {
                    }
                })
            }


        }


    }

    private fun getLocation_checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionsList = ArrayList<String>()
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            }
            if (permissionsList.size > 0) {
                requestPermissions(
                    permissionsList.toTypedArray(),
                    requestCode_LOCATION
                )
            } else {
                getLocation()
            }
        } else {
            getLocation()
        }
    }

    @SuppressLint("MissingPermission")
    fun getLocation() {

        val callLatLng = CallLatLng(this)
        val location = callLatLng.getLocation_checkGPS()
        var latitudeSTR = "0"
        var longitudeSTR = "0"

        if (location != null) {
            val latitude = location!!.getLatitude()
            val longitude = location!!.getLongitude()
//            Log.d("android latitude", "is $latitude")
//            Log.d("android longitude", "is $longitude")
            current_latitude = latitude
            current_longitude = longitude

            val current = LatLng(latitude, longitude)
            googleMap?.isMyLocationEnabled = true
        }

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        if (requestCode == requestCode_LOCATION) {
            val perms = HashMap<String, Int>()
            for (i in 0 until permissions.size) {
                perms.put(permissions[i], grantResults[i])
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                ) {
                    getLocation_checkPermission()
                } else {
//                    Toast.makeText(this, "Sorry, Permission Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun initialView() {

        mapFragment = supportFragmentManager.findFragmentById(R.id.fragment_map_container) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        imageView_hospital = findViewById(R.id.imageView_hospital) as ImageView
        textView_hospital_name = findViewById(R.id.textView_hospital_name) as TextView
        linearLayout_hospital_phone = findViewById(R.id.linearLayout_hospital_phone) as LinearLayout
        linearLayout_hospital_map = findViewById(R.id.linearLayout_hospital_map) as LinearLayout
        textView_hospital_open = findViewById(R.id.textView_hospital_open) as TextView
        textView_hospital_telephone = findViewById(R.id.textView_hospital_telephone) as TextView
        textView_hospital_url = findViewById(R.id.textView_hospital_url) as TextView
        textView_hospital_address = findViewById(R.id.textView_hospital_address) as TextView

        imageView_find_list = findViewById(R.id.imageView_find_list) as ImageView
        editText_keyword = findViewById(R.id.editText_keyword) as EditText

        imageView_find_list?.setOnClickListener {
            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = getCurrentFocus()
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(this)
            }
            imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)

            //            hospitalObject_List.clear()
            page = 1
            callService_hospital()
        }


        relativeLayout_hospital = findViewById(R.id.relativeLayout_hospital) as RelativeLayout
        viewPager_hospital_image = findViewById(R.id.viewPager_hospital_image) as ViewPager
        tabLayout_hospital_image = findViewById(R.id.tabLayout_hoapital_image) as TabLayout


    }

    private fun initialIntentData() {

        try {
            hospitalObject = intent.getParcelableExtra("hospitalObject")
        } catch (e: Exception) {
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_search_hospital))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_values_hospital_search_detail")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {
                        hospitalObject_List.clear()
                        for (i in 0..results.length() - 1) {
                            var hospital_Json: JSONObject? = null
                            try {
                                hospital_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (hospital_Json != null) {
                                val hospitalObject = HospitalObject()
                                hospitalObject.setParam(hospital_Json)
                                hospitalObject_List.add(hospitalObject)
                            }

                        }
                        val intent_go = Intent(this@Page_SearchHospital_Detail, Page_SearchHospital_Map::class.java)
                        intent_go.putExtra("hospitalObject_List", hospitalObject_List)
                        intent_go.putExtra("keyword", ""+ keyword)
                        startActivity(intent_go)
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_SearchHospital_Detail, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_SearchHospital_Detail, resources.getString(R.string.text_service_error)
                )
            }
        }
    }

}