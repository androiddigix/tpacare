package th.co.digix.tpacare.menu_home.search_hospital

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import th.co.digix.tpacare.R

class Fragment_Hospital_Image : Fragment() {

    val ARG_SECTION_NUMBER = "section_number"
    var rootView: View? = null
    var imageView_hospital_image: ImageView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.fragment_hospital_image, container, false)

        getArgumentsData()

        var url_image = ""

        try {
            url_image = arguments!!.getString("url_image")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        imageView_hospital_image = rootView?.findViewById(R.id.imageView_hospital_image) as ImageView

        if (imageView_hospital_image != null) {

            val requestOptions = RequestOptions()
                .placeholder(R.drawable.icon_hospital_default)
                .error(R.drawable.icon_hospital_default)

            Glide.with(activity!!)
                .load(url_image)
                .apply(requestOptions)
                .into(imageView_hospital_image!!)
        }

        return rootView
    }

    private fun getArgumentsData() {

    }

}