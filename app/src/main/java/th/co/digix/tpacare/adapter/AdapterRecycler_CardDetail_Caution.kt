package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.SaveDetailObject
import java.text.DecimalFormat


class AdapterRecycler_CardDetail_Caution(
    val activity: Activity?, val saveDetailObject_List: ArrayList<SaveDetailObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (saveDetailObject_List.size > position) {
            val saveDetailObject = saveDetailObject_List.get(position)

            val covDesc = saveDetailObject.covDesc
            val covNo = saveDetailObject.covNo
            val covLimit = saveDetailObject.covLimit
            val covUtilized = saveDetailObject.covUtilized

            if (position == 0) {
                holder.textView_caution.setText("" + activity?.resources!!.getString(R.string.text_caution))
            }
            holder.textView_caution_text.setText("" + covDesc)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_card_detail_caution, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (saveDetailObject_List != null) {
            var listSize = saveDetailObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textView_caution_text: TextView
        internal var textView_caution: TextView

        init {
            textView_caution_text = itemView.findViewById(R.id.textView_caution_text) as TextView
            textView_caution = itemView.findViewById(R.id.textView_caution) as TextView

        }
    }

}