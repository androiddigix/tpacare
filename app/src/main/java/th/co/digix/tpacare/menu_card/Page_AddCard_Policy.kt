package th.co.digix.tpacare.menu_card

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.pvdproject.adapter.AdapterRecycler_AddCardPolicyDetail
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.AddCardObject
import th.co.digix.tpacare.custom_object.CardPolicyObject
import th.co.digix.tpacare.custom_object.CoverageObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.*
import java.text.DecimalFormat
import java.util.*

class Page_AddCard_Policy : BaseActivity() {


    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_insurance_addcard = httpServerURL.api_insurance_addcard
    val salt_key = httpServerURL.salt_key

    var requestCode_add_card_listbank = 14
    var requestCode_add_card_detail = 15
    var requestCode_add_card_detail_orther = 16

    var progressBackground: RelativeLayout? = null

    var editText_bank_name: EditText? = null
    var editText_policy_id: EditText? = null
    var editText_name_receive: EditText? = null
    var editText_date_save_start: EditText? = null
    var editText_date_save_end: EditText? = null

    var linearLayout_edit_policy_save_detail: LinearLayout? = null
    var editText_treat_price: EditText? = null
    var editText_treat_room: EditText? = null
    var editText_treat_operate: EditText? = null
    var editText_treat_drug: EditText? = null

    var linearLayout_policy_save_detail: LinearLayout? = null
    var imageView_add_card_policy_detail: ImageView? = null

    var recyclerView_policy_save_detail: RecyclerView? = null
    var adapterRecycler_AddCardPolicyDetail: AdapterRecycler_AddCardPolicyDetail? = null

    var linearLayout_save_add_card: LinearLayout? = null

    var textView_popup_title: TextView? = null

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var bday_text_start = ""
    var bday_text_end = ""

    var coverage_list_main = JSONArray()
    var coverageObject_list = ArrayList<CoverageObject>()

    var insured_type = ""

    var isBankList = true
    var insurer_code = ""

    var cardPolicyObject_List = ArrayList<CardPolicyObject>()

    var effFrom_date = Calendar.getInstance().getTime()
    var effTo_date = Calendar.getInstance().getTime()

    var isAllowUser = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_add_card_policy)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestCode_add_card_detail_orther) {
            if (resultCode == Activity.RESULT_OK) {
                var coverage_list = ""
                try {
                    coverage_list = data?.getStringExtra("coverage_list")!!
                } catch (e: Exception) {
                }
                Log.d("coke", "requestCode coverage_list " + coverage_list)

                var coverage_list_JSONArray = JSONArray(coverage_list)

                if (coverage_list_main.length() >= 4) {
                    for (i in 0..coverage_list_JSONArray.length() - 1) {
                        val coverageObject = CoverageObject()
                        coverageObject.setParam(coverage_list_JSONArray.get(i) as JSONObject)
                        coverageObject.cov_no = "" + (i + 4)

                        coverage_list_main.put(coverageObject.toJsonEdit())
                        coverageObject_list.add(coverageObject)
                    }
                }

                if (coverageObject_list != null && coverageObject_list.size > 0) {
                    adapterRecycler_AddCardPolicyDetail?.notifyDataSetChanged()
                }

            }

        }
        if (requestCode == requestCode_add_card_detail) {
            if (resultCode == Activity.RESULT_OK) {
                var coverage_list = ""
                try {
                    coverage_list = data?.getStringExtra("coverage_list")!!
                } catch (e: Exception) {
                }
                Log.d("coke", "requestCode coverage_list " + coverage_list)

                var coverage_list_JSONArray = JSONArray(coverage_list)

                if (coverage_list_main.length() < 4) {
                    coverage_list_main = coverage_list_JSONArray
                } else {
                    coverage_list_main.remove(0)
                    coverage_list_main.put(0, coverage_list_JSONArray.get(0) as JSONObject)
                    coverage_list_main.remove(1)
                    coverage_list_main.put(1, coverage_list_JSONArray.get(1) as JSONObject)
                    coverage_list_main.remove(2)
                    coverage_list_main.put(2, coverage_list_JSONArray.get(2) as JSONObject)
                    coverage_list_main.remove(3)
                    coverage_list_main.put(3, coverage_list_JSONArray.get(3) as JSONObject)
                }

                linearLayout_policy_save_detail?.visibility = View.VISIBLE

                if (coverage_list_JSONArray != null) {
                    for (i in 0..coverage_list_JSONArray.length() - 1) {
                        var coverageObject_json: JSONObject? = null
                        try {
                            coverageObject_json = coverage_list_JSONArray.get(i) as JSONObject
                        } catch (e: Exception) {
                        }
                        if (coverageObject_json != null) {
                            val coverageObject = CoverageObject()
                            coverageObject.setParam(coverageObject_json)
                            if (i == 0) {
                                val mFormat = DecimalFormat("###,###,###,###,###,###")
                                var cov_limit_show = ""
                                try {
                                    val cov_limit_long = coverageObject.cov_limit.toLong()
                                    cov_limit_show = "" + mFormat.format(cov_limit_long)
                                } catch (e: Exception) {
                                }
                                editText_treat_price?.setText("" + cov_limit_show)
                            } else if (i == 1) {
                                val mFormat = DecimalFormat("###,###,###,###,###,###")
                                var cov_limit_show = ""
                                try {
                                    val cov_limit_long = coverageObject.cov_limit.toLong()
                                    cov_limit_show = "" + mFormat.format(cov_limit_long)
                                } catch (e: Exception) {
                                }
                                editText_treat_room?.setText("" + cov_limit_show)
                            } else if (i == 2) {
                                val mFormat = DecimalFormat("###,###,###,###,###,###")
                                var cov_limit_show = ""
                                try {
                                    val cov_limit_long = coverageObject.cov_limit.toLong()
                                    cov_limit_show = "" + mFormat.format(cov_limit_long)
                                } catch (e: Exception) {
                                }
                                editText_treat_operate?.setText("" + cov_limit_show)
                            } else if (i == 3) {
                                val mFormat = DecimalFormat("###,###,###,###,###,###")
                                var cov_limit_show = ""
                                try {
                                    val cov_limit_long = coverageObject.cov_limit.toLong()
                                    cov_limit_show = "" + mFormat.format(cov_limit_long)
                                } catch (e: Exception) {
                                }
                                editText_treat_drug?.setText("" + cov_limit_show)
                            } else if (i > 3) {
                            }
                        }
                    }

                }
            }
        }
        if (requestCode == requestCode_add_card_listbank) {
            if (resultCode == Activity.RESULT_OK) {
                var bank_name = ""
                try {
                    bank_name = data?.getStringExtra("bank_name")!!
                } catch (e: Exception) {
                }
                try {
                    insurer_code = data?.getStringExtra("insurer_code")!!
                } catch (e: Exception) {
                }
                try {
                    isBankList = data?.getBooleanExtra("isBankList", true)!!
                } catch (e: Exception) {
                }
                editText_bank_name?.setText("" + bank_name)
            }
        }
    }

    private fun initialWorking() {

        editText_bank_name?.setOnClickListener {
            val intent_go = Intent(this@Page_AddCard_Policy, Page_AddCard_Policy_ListBank::class.java)
            startActivityForResult(intent_go, requestCode_add_card_listbank)
        }

        editText_date_save_start?.setOnClickListener {
            showDialogPicker_start()
            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = getCurrentFocus()
            if (view == null) {
                view = View(this)
            }
            imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
        }

        editText_date_save_end?.setOnClickListener {
            showDialogPicker_end()
            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = getCurrentFocus()
            if (view == null) {
                view = View(this)
            }
            imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
        }

        imageView_add_card_policy_detail?.setOnClickListener {
            if (linearLayout_policy_save_detail?.visibility == View.VISIBLE) {
                val intent_go = Intent(this@Page_AddCard_Policy, Page_AddCard_PolicySaveDetail_orther::class.java)
                startActivityForResult(intent_go, requestCode_add_card_detail_orther)
            } else {
                val intent_go = Intent(this@Page_AddCard_Policy, Page_AddCard_PolicySaveDetail::class.java)
                startActivityForResult(intent_go, requestCode_add_card_detail)
            }
        }

        linearLayout_edit_policy_save_detail?.setOnClickListener {
            val intent_go = Intent(this@Page_AddCard_Policy, Page_AddCard_PolicySaveDetail::class.java)
            intent_go.putExtra("coverage_list", coverage_list_main.toString())
            startActivityForResult(intent_go, requestCode_add_card_detail)
        }

        linearLayout_save_add_card?.setOnClickListener(View.OnClickListener {

            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = getCurrentFocus()
            if (view == null) {
                view = View(this@Page_AddCard_Policy)
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)


            val bank_name = editText_bank_name?.text.toString()
            val policy_id = editText_policy_id?.text.toString()
            val name_receive = editText_name_receive?.text.toString()
            val date_save_start = bday_text_start
            val date_save_end = bday_text_end

            if (bank_name.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_bank_name)
                )
            } else if (policy_id.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_policy_id)
                )
            } else if (name_receive.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_name_receive)
                )
            } else if (date_save_start.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_date_save_start)
                )
            } else if (date_save_end.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_date_save_end)
                )
            } else if (effFrom_date.after(effTo_date) || effFrom_date == effTo_date) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy,
                    resources.getString(R.string.text_alert_date_save_end) + "ต้องอยู่หลังจาก" + resources.getString(R.string.text_alert_date_save_start)
                )
            } else if (coverage_list_main.length() == 0) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_policy_save_detail)
                )

            } else {

                var isCardDuplicate = false

                var cardPolicyObject_List_search: ArrayList<CardPolicyObject> = cardPolicyObject_List

                val insurerCode_add = insurer_code

                for (i in 0..cardPolicyObject_List_search.size - 1) {

                    val polNo = cardPolicyObject_List_search.get(i).polNo
                    val insurerCode = cardPolicyObject_List_search.get(i).insurerCode
                    val policyFrom = cardPolicyObject_List_search.get(i).policyFrom


                    val characterSet = CharacterSet()
                    val cardPolicyObject_polNo = characterSet.removeAllSymbol(polNo)
                    val cardPolicyObject_polNo_add = characterSet.removeAllSymbol(policy_id)

                    if (cardPolicyObject_polNo.equals(cardPolicyObject_polNo_add)) {
                        if (insurerCode.equals(insurerCode_add)) {
                            if (policyFrom.equals("dummy")) {
                                isCardDuplicate = true
                            } else {
                                if (isAllowUser) {
                                    isCardDuplicate = true
                                }
                            }
                        }
                    }

                }

                if (isCardDuplicate) {
                    val message = "" + resources.getString(R.string.popup_card_cant_add)
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_AddCard_Policy, "" + message)
                } else {

                    var phone_encrypt = ""

                    val cryptLib = CryptLib()
//                phone_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone_new, salt_key)

                    val sharedPreferencesManager = SharedPreferencesManager(this)
                    val userObject = sharedPreferencesManager.getUserObject()

                    var jsonObject = JSONObject()


                    val space_index = name_receive.indexOf(" ")

                    var name = name_receive
                    var surname = ""

                    if (space_index > -1) {
                        name = name_receive.substring(0, space_index)
                        surname = name_receive.substring(space_index, name_receive.length)
                    }

                    var company_name = ""
                    var insurer_name = ""
//                if (isBankList) {
//                    company_name = "" + bank_name
//                } else {
//                    insurer_name = "" + bank_name
//                }
                    insurer_name = "" + bank_name

                    var addCardObject = AddCardObject()
                    addCardObject.name = "" + name
                    addCardObject.surname = "" + surname
                    addCardObject.insured_type = "" + insured_type
                    addCardObject.card_type = ""
                    addCardObject.insurer_code = "" + insurer_code
                    addCardObject.insurer_name = "" + insurer_name
                    addCardObject.insurer_dedicateed_line = ""
                    addCardObject.company_name = "" + company_name
                    addCardObject.staff_no = ""
                    addCardObject.effective_date = "" + date_save_start
                    addCardObject.expire_date = "" + date_save_end
                    addCardObject.pol_no = "" + policy_id
                    addCardObject.tel_no = ""
                    addCardObject.coverage_list = coverage_list_main

                    jsonObject = addCardObject.toJsonEdit()
                    var addCard_Str = jsonObject.toString()

                    val defaultParameterAPI = DefaultParameterAPI(this)
                    var okhttp = OkHttpPost(null, defaultParameterAPI)
                    okhttp.setJson("" + addCard_Str)
                    okhttp.execute(root_url, api_insurance_addcard, "api_insurance_addcard")

                    progressBackground?.visibility = View.VISIBLE
                }

            }
        })
    }

    private fun showDialogPicker_start() {
        val dateTimeHelper = DateTimeHelper()

        var calendar_bDate_start: Calendar = Calendar.getInstance(TimeZone.getDefault())
        if (!bday_text_start.equals("")) {
            try {
                calendar_bDate_start = dateTimeHelper.parseStringToCalendar(bday_text_start)
            } catch (e: Exception) {
            }
        }
        if (calendar_bDate_start == null) {
            calendar_bDate_start = Calendar.getInstance(TimeZone.getDefault())
        }

        val dialog = DatePickerDialog(
            this, object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(p0: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                    val bday_text = String.format("%02d", dayOfMonth) + "-" + String.format(
                        "%02d",
                        (month + 1)
                    ) + "-" + year.toString()
                    val dateTimeHelper = DateTimeHelper()
                    val bday_text_show_date = dateTimeHelper.parseStringToDate4(bday_text)
                    val bday_text_show = dateTimeHelper.parseDateToString3(bday_text_show_date)
                    bday_text_start = bday_text
                    editText_date_save_start?.setText(bday_text_show)



                    try {
                        effFrom_date = dateTimeHelper.parseStringToDate4(bday_text)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                }

            },
            calendar_bDate_start.get(Calendar.YEAR), calendar_bDate_start.get(Calendar.MONTH),
            calendar_bDate_start.get(Calendar.DAY_OF_MONTH)
        )
        dialog.show()
    }


    private fun showDialogPicker_end() {
        val dateTimeHelper = DateTimeHelper()
        var calendar_bDate_end: Calendar = Calendar.getInstance(TimeZone.getDefault())
        if (!bday_text_end.equals("")) {
            try {
                calendar_bDate_end = dateTimeHelper.parseStringToCalendar(bday_text_end)
            } catch (e: Exception) {
            }
        }
        if (calendar_bDate_end == null) {
            calendar_bDate_end = Calendar.getInstance(TimeZone.getDefault())
        }

        val dialog = DatePickerDialog(
            this, object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(p0: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                    val bday_text = String.format("%02d", dayOfMonth) + "-" + String.format(
                        "%02d",
                        (month + 1)
                    ) + "-" + year.toString()
                    val dateTimeHelper = DateTimeHelper()
                    val bday_text_show_date = dateTimeHelper.parseStringToDate4(bday_text)
                    val bday_text_show = dateTimeHelper.parseDateToString3(bday_text_show_date)
                    bday_text_end = bday_text
                    editText_date_save_end?.setText(bday_text_show)

                    try {
                        effTo_date = dateTimeHelper.parseStringToDate4(bday_text)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            },
            calendar_bDate_end.get(Calendar.YEAR), calendar_bDate_end.get(Calendar.MONTH),
            calendar_bDate_end.get(Calendar.DAY_OF_MONTH)
        )
        dialog.show()
    }


    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        editText_bank_name = findViewById(R.id.editText_bank_name) as EditText
        editText_policy_id = findViewById(R.id.editText_policy_id) as EditText
        editText_name_receive = findViewById(R.id.editText_name_receive) as EditText
        editText_date_save_start = findViewById(R.id.editText_date_save_start) as EditText
        editText_date_save_end = findViewById(R.id.editText_date_save_end) as EditText

        linearLayout_edit_policy_save_detail = findViewById(R.id.linearLayout_edit_policy_save_detail) as LinearLayout
        editText_treat_price = findViewById(R.id.editText_treat_price) as EditText
        editText_treat_room = findViewById(R.id.editText_treat_room) as EditText
        editText_treat_operate = findViewById(R.id.editText_treat_operate) as EditText
        editText_treat_drug = findViewById(R.id.editText_treat_drug) as EditText

        recyclerView_policy_save_detail =
            findViewById(R.id.recyclerView_policy_save_detail) as RecyclerView
        recyclerView_policy_save_detail?.layoutManager =
            LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recyclerView_policy_save_detail?.setNestedScrollingEnabled(false)

        linearLayout_policy_save_detail = findViewById(R.id.linearLayout_policy_save_detail) as LinearLayout
        imageView_add_card_policy_detail = findViewById(R.id.imageView_add_card_policy_detail) as ImageView

        linearLayout_save_add_card = findViewById(R.id.linearLayout_save_add_card) as LinearLayout

        adapterRecycler_AddCardPolicyDetail = AdapterRecycler_AddCardPolicyDetail(
            this, coverageObject_list
        )
        recyclerView_policy_save_detail?.setAdapter(adapterRecycler_AddCardPolicyDetail)

    }

    private fun initialIntentData() {
        try {
            insured_type = intent.getStringExtra("insured_type")
        } catch (e: Exception) {
        }
        try {
            isAllowUser = intent.getBooleanExtra("isAllowUser", true)
        } catch (e: Exception) {
        }
        try {
            cardPolicyObject_List = intent.getParcelableArrayListExtra("cardPolicyObject_List")
        } catch (e: Exception) {
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_add_card))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_insurance_addcard")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {


                    }

                    val policy_id = editText_policy_id?.text.toString()

                    val returnIntent = Intent()
                    returnIntent.putExtra("insurer_code", insurer_code)
                    returnIntent.putExtra("policy_id", policy_id)
                    setResult(Activity.RESULT_OK, returnIntent)
                    finish()
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_AddCard_Policy, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy, resources.getString(R.string.text_service_error)
                )
            }
        }
    }

}