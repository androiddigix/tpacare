package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class ListBankObject() : Parcelable {

    var no: String = ""
    var name_th: String = ""
    var name_en: String = ""
    var insurer_code: String = ""

    constructor(parcel: Parcel) : this() {
        no = parcel.readString()
        name_th = parcel.readString()
        name_en = parcel.readString()
        insurer_code = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(no)
        parcel.writeString(name_th)
        parcel.writeString(name_en)
        parcel.writeString(insurer_code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ListBankObject> {
        override fun createFromParcel(parcel: Parcel): ListBankObject {
            return ListBankObject(parcel)
        }

        override fun newArray(size: Int): Array<ListBankObject?> {
            return arrayOfNulls(size)
        }
    }

}