package th.co.digix.tpacare.menu_fitme.Authenication;


/**
 * Created by jboggess on 9/26/16.
 */

public interface RequestSigner {

    void signRequest(BasicHttpRequestBuilder builder);

}
