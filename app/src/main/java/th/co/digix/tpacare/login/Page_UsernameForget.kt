package th.co.digix.tpacare.login

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor

class Page_UsernameForget : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authForgotUserRequestOTP = httpServerURL.api_authForgotUserRequestOTP
    val salt_key = httpServerURL.salt_key


    var progressBackground: RelativeLayout? = null

    var editText_phone: EditText? = null
    var linearLayout_confirm_username: LinearLayout? = null

    var textView_popup_title: TextView? = null

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var otp_code = ""
    var phone = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_username_forget)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialWorking() {

        linearLayout_confirm_username?.setOnClickListener(View.OnClickListener {

            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = getCurrentFocus()
            if (view == null) {
                view = View(this@Page_UsernameForget)
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0)

            phone = editText_phone?.text.toString()
            if (phone.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_UsernameForget,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_phone)
                )
            } else {
                var phone_encrypt = ""

                val cryptLib = CryptLib()
                phone_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(this)
                val formBody = FormBody.Builder()
                formBody.add("tel", "" + phone_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url, api_authForgotUserRequestOTP, "api_authForgotUserRequestOTP"
                )

                progressBackground?.visibility = View.VISIBLE

            }
        })
    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        editText_phone = findViewById(R.id.editText_phone) as EditText
        linearLayout_confirm_username = findViewById(R.id.linearLayout_confirm_username) as LinearLayout
    }

    private fun initialIntentData() {

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.text_usernameforget_title))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authForgotUserRequestOTP")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {


                    }
                    val intent_go = Intent(this@Page_UsernameForget, Page_UsernameForgotOTP::class.java)
                    intent_go.putExtra("otp_code", "" + otp_code)
                    intent_go.putExtra("phone", "" + phone)
                    startActivity(intent_go)
                    finish()
//                    message = "" + resources.getString(R.string.text_success)
//                    val alertDialogUtil = AlertDialogUtil()
//                    alertDialogUtil.showMessageDialog(this@Page_UsernameForget, "" + message)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_UsernameForget, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_UsernameForget, resources.getString(R.string.text_service_error)
                )
            }
        }
    }
}