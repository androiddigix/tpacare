package th.co.digix.tpacare.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import th.co.digix.tpacare.menu_home.search_hospital.Fragment_Hospital_Image;

import java.util.ArrayList;


/**
 * Created by android on 29/3/2560.
 */

public class PagerAdapter_Hospital_Image extends FragmentPagerAdapter {
    ArrayList<String> url_image_List;

    public PagerAdapter_Hospital_Image(FragmentManager fm, ArrayList<String> url_image_List) {
        super(fm);
        this.url_image_List = url_image_List;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment_Hospital_Image fragment = new Fragment_Hospital_Image();
        Bundle args = new Bundle();
        args.putInt("ARG_SECTION_NUMBER", position);
        args.putString("url_image", url_image_List.get(position));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        if (url_image_List == null) {
            return 0;
        }
        return url_image_List.size();
    }

}