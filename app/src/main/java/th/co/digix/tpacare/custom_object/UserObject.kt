package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class UserObject() : Parcelable {

    var image_profile: String = ""
    var token: String = ""
    var refresh_token: String = ""
    var tpa_access_token: String = ""
    var token_expires_in: String = ""
    var token_expires_date: String = ""
    var user_id: String = ""
    var citizen_id: String = ""
    var username: String = ""
    var first_name: String = ""
    var last_name: String = ""
    var tel: String = ""

    var state_login: String = ""

    constructor(parcel: Parcel) : this() {
        token = parcel.readString()
        username = parcel.readString()
        user_id = parcel.readString()
        image_profile = parcel.readString()
        tpa_access_token = parcel.readString()
        citizen_id = parcel.readString()
        tel = parcel.readString()
        first_name = parcel.readString()
        last_name = parcel.readString()
    }

    fun setParam(result: JSONObject) {
        try {
            token = result.getString("token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            username = result.getString("username")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            user_id = result.getString("user_id")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            image_profile = result.getString("image_profile")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            tpa_access_token = result.getString("tpa_access_token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            citizen_id = result.getString("citizen_id")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            tel = result.getString("tel")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            first_name = result.getString("first_name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            last_name = result.getString("last_name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            refresh_token = result.getString("refresh_token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            token_expires_in = result.getString("token_expires_in")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            token_expires_date = result.getString("token_expires_date")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    fun setLoginPin(result: JSONObject) {
        try {
            token = result.getString("token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            username = result.getString("username")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            user_id = result.getString("user_id")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            image_profile = result.getString("image_profile")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            tpa_access_token = result.getString("tpa_access_token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
//        try {
//            citizen_id = result.getString("citizen_id")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
        try {
            tel = result.getString("tel")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            first_name = result.getString("first_name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            last_name = result.getString("last_name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setLoginRefreshToken(result: JSONObject) {
        try {
            token = result.getString("token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            refresh_token = result.getString("refresh_token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            tpa_access_token = result.getString("tpa_access_token")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            token_expires_in = result.getString("token_expires_in")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            token_expires_date = result.getString("token_expires_date")
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    fun setUpdateProfile(result: JSONObject) {
//        try {
//            token = result.getString("token")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
        try {
            username = result.getString("username")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            user_id = result.getString("user_id")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            image_profile = result.getString("image_profile")
        } catch (e: Exception) {
            e.printStackTrace()
        }
//        try {
//            tpa_access_token = result.getString("tpa_access_token")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
        try {
            citizen_id = result.getString("citizen_id")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            tel = result.getString("tel")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            first_name = result.getString("first_name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            last_name = result.getString("last_name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(token)
        parcel.writeString(username)
        parcel.writeString(user_id)
        parcel.writeString(image_profile)
        parcel.writeString(tpa_access_token)
        parcel.writeString(citizen_id)
        parcel.writeString(tel)
        parcel.writeString(first_name)
        parcel.writeString(last_name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserObject> {
        override fun createFromParcel(parcel: Parcel): UserObject {
            return UserObject(parcel)
        }

        override fun newArray(size: Int): Array<UserObject?> {
            return arrayOfNulls(size)
        }
    }

}