package th.co.digix.tpacare.login

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import android.support.v4.app.ActivityCompat
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.*
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.UserObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey


class Page_Login : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authLogin = httpServerURL.api_authLogin
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var editText_UserName: EditText? = null
    var editText_Password: EditText? = null

    var progressBackground: RelativeLayout? = null
    var linearLayout_login: LinearLayout? = null

    var textView_register: TextView? = null
    var textView_forgot_password: TextView? = null
    var textView_forgot_username: TextView? = null

//    var isOnBackButton = false
//    var isPasswordView = false

    var canFingerprint = false

    private var mFingerprintManager: FingerprintManagerCompat? = null
    private var mKeyguardManager: KeyguardManager? = null

    var firebase_Tokens: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        setContentView(R.layout.page_login)


        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            firebase_Tokens = instanceIdResult.token
        }

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        setOnClick()

        checkFingerPrint()
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_login))
    }

    override fun onDestroy() {
        super.onDestroy()

        BusProvider.getInstance().unregister(this)
    }

    private fun initialIntentData() {
//        isOnBackButton = intent.getBooleanExtra("isOnBackButton", false)
    }

    private fun setOnClick() {

        linearLayout_login?.setOnClickListener() {

            val userName = editText_UserName?.text.toString().trim()
            val password = editText_Password?.text.toString().trim()

            if (userName.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Login, "" +
                            resources.getString(R.string.text_please_put) +
                            resources.getString(R.string.text_alert_username)
                )
            } else if (password.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Login, "" +
                            resources.getString(R.string.text_please_put) +
                            resources.getString(R.string.text_alert_password)
                )
            } else {

                Log.d("coke", "firebase_Tokens " + firebase_Tokens)

                var userName_encrypt = ""
                var password_encrypt = ""
                var firebase_Tokens_encrypt = ""

                val cryptLib = CryptLib()
                userName_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(userName, salt_key)
                password_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(password, salt_key)
                firebase_Tokens_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(firebase_Tokens, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(this)
                val formBody = FormBody.Builder()
                formBody.add("username", "" + userName_encrypt)
                formBody.add("password", "" + password_encrypt)
                formBody.add("firebase_token", "" + firebase_Tokens_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(root_url, api_authLogin, "api_authLogin")

                progressBackground?.visibility = View.VISIBLE
            }
        }

        textView_register?.setOnClickListener() {
            val intent_go = Intent(this, Page_Register_Term_of_Service::class.java)
            startActivity(intent_go)
        }

        textView_forgot_password?.setOnClickListener() {
            val intent_go = Intent(this, Page_PasswordForget::class.java)
            startActivity(intent_go)
        }

        textView_forgot_username?.setOnClickListener() {
            val intent_go = Intent(this, Page_UsernameForget::class.java)
            startActivity(intent_go)
        }
    }


    private fun initialView() {
        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        editText_UserName = findViewById(R.id.editText_UserName) as EditText
        editText_Password = findViewById(R.id.editText_Password) as EditText

        linearLayout_login = findViewById(R.id.linearLayout_login) as LinearLayout

        textView_register = findViewById(R.id.textView_register) as TextView
        textView_forgot_password = findViewById(R.id.textView_forgot_password) as TextView
        textView_forgot_username = findViewById(R.id.textView_forgot_username) as TextView
    }

    private fun checkFingerPrint() {

        val sharedPreferencesManager = SharedPreferencesManager(this)
        val isUseFingerPrint = sharedPreferencesManager.getUseFingerPrint()
        if (isUseFingerPrint) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                try {
                    mKeyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    mFingerprintManager = FingerprintManagerCompat.from(this@Page_Login)
                } catch (e: Exception) {
                    e.printStackTrace()
                }


                if (ActivityCompat.checkSelfPermission(
                        this, Manifest.permission.USE_FINGERPRINT
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // Checking fingerprint permission.
                    Log.d("tpa", "Checking fingerprint permission.")
                } else if (mKeyguardManager == null) {
                    // Checking Device support fingerprint or not.
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                } else if (mFingerprintManager == null) {
                    // Checking Device support fingerprint or not.
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                } else if (!mFingerprintManager!!.isHardwareDetected()) {
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                    // Checking Device support fingerprint or not.
                } else if (!mKeyguardManager!!.isKeyguardSecure()) {
                    Log.d("tpa", "Not setting lock screen with imprint.")
                    // Not setting lock screen with imprint.
                } else if (!mFingerprintManager!!.hasEnrolledFingerprints()) {
                    // Not registered at least one fingerprint in Settings.
                    Log.d("tpa", "Not registered at least one fingerprint in Settings.")
                } else {
                    canFingerprint = true
                }
            }
        } else {
        }

    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authLogin")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    val userObject = UserObject()
                    if (result != null) {
                        userObject.setParam(result)
                        val sharedPreferencesManager = SharedPreferencesManager(this)
                        userObject.state_login = "login"
                        sharedPreferencesManager.setUserObject(userObject)

                        if (canFingerprint) {
                            val intent_go = Intent(this@Page_Login, Page_FingerPrint_Set::class.java)
                            intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent_go)
                        } else {
                            val intent_go = Intent(this@Page_Login, ActivityMain::class.java)
                            intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            intent_go.putExtra("userObject", userObject)
                            startActivity(intent_go)
                        }

                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_Login, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(this@Page_Login, resources.getString(R.string.text_service_error))
            }
        }

    }

}