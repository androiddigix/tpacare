package th.co.digix.tpacare.menu_fitme

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.GridLabelRenderer
import com.jjoe64.graphview.LegendRenderer
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.*
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGetFitme
import th.co.digix.tpacare.menu_home.Page_Fragment_Home
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import th.co.digix.tpacare.menu_fitme.Authenication.AuthenticationHandler
import th.co.digix.tpacare.menu_fitme.Authenication.AuthenticationManager
import th.co.digix.tpacare.menu_fitme.Authenication.AuthenticationResult


class Page_Fragment_Fitme : Fragment() {

    val httpServerURL = HttpServerURL()
    var root_url_fitbit = httpServerURL.host_fitbit
    val api_activities_heart = httpServerURL.api_activities_heart
    val api_activities_steps = httpServerURL.api_activities_steps
    val api_activities_calories = httpServerURL.api_activities_calories

    var btnBack: ImageView? = null
    var progressBackground: RelativeLayout? = null
    var textView_toolbar_title: TextView? = null


    var linearLayout_fitme_day: LinearLayout? = null
    var linearLayout_fitme_week: LinearLayout? = null
    var linearLayout_fitme_month: LinearLayout? = null
    var linearLayout_fitme_year: LinearLayout? = null

    var textview_day: TextView? = null
    var textview_week: TextView? = null
    var textview_month: TextView? = null
    var textview_year: TextView? = null

    var graph_heart_rate: GraphView? = null
    var graph_step_count: GraphView? = null
    var graph_energy_burned: GraphView? = null

    val stepCountObject_List = ArrayList<StepCountObject>()
    val stepCountIntradayObject_List = ArrayList<StepCountIntradayObject>()

    val caloriesObjectList = ArrayList<CaloriesObject>()
    val caloriesIntradayObjectList = ArrayList<CaloriesIntradayObject>()

    val heartObjectList = ArrayList<HeartObject>()

    var rootView: View? = null
    var isRegisterBus = false
    var tab_day: Boolean = false
    var tab_week: Boolean = false
    var tab_month: Boolean = false
    var tab_year: Boolean = false
    var time_period = "1w"
    var start_date = "today" //"2019-08-16"
    var series: LineGraphSeries<DataPoint>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.page_fragment_fitme, container, false)

        if (!isRegisterBus) {
            BusProvider.getInstance().register(this)
            isRegisterBus = true
        }

        initInstanceToolbar()

        initialView()

        setting_pager()

        callService_fitmeCalories()

//        callService_fitmeHeartRate()

        callService_fitmeStepCount()

        return rootView

    }

    fun callService_fitmeStepCount() {
        var formBody = "date/" + start_date + "/" + time_period + "/" + "15min.json"
        OkHttpGetFitme().execute(
            root_url_fitbit,
            api_activities_steps + formBody,
            "api_activities_steps"
        )
        progressBackground?.visibility = VISIBLE
    }

    fun callService_fitmeHeartRate() {
        var formBody = "date/" + start_date + "/" + time_period + "/" + "1min.json"
        OkHttpGetFitme().execute(
            root_url_fitbit,
            api_activities_heart + formBody,
            "api_activities_heart"
        )
        progressBackground?.visibility = VISIBLE

    }

    fun callService_fitmeCalories() {
        var formBody = "date/" + start_date + "/" + time_period + "/" + "15min.json"
        OkHttpGetFitme().execute(
            root_url_fitbit,
            api_activities_calories + formBody,
            "api_activities_calories"
        )
        progressBackground?.visibility = VISIBLE

    }


    /*private fun setGraphFitbitHeartRate() {
        graph_heart_rate?.removeAllSeries()
        series = LineGraphSeries()

        val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.US)
        for (i in 0..heartObjectList.size - 1) {
            var date: Date = dateFormat.parse(heartObjectList.get(i).time)
            var y = heartObjectList.get(i).value.toDouble()
            series!!.appendData(DataPoint(date, y), true, heartObjectList.size)
        }
        val dateFormat2 = SimpleDateFormat("HH:mm", Locale.US)
        graph_heart_rate?.getGridLabelRenderer()?.setNumHorizontalLabels(5)
        graph_heart_rate?.getGridLabelRenderer()?.setLabelFormatter(DateAsXAxisLabelFormatter(context, dateFormat2))
        graph_heart_rate?.addSeries(series)
    }
*/

    private fun setGraphFitbitEnergyBurned() {
        graph_energy_burned?.removeAllSeries()
        series = LineGraphSeries()

        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val dateFormat_week_month = SimpleDateFormat("dd/MM", Locale.US)
        val dateFormat_year = SimpleDateFormat("MM/yy", Locale.US)
        val dateFormatInterval = SimpleDateFormat("HH:mm:ss", Locale.US)
        val dateFormatInterval2 = SimpleDateFormat("HH:mm", Locale.US)
        if (caloriesIntradayObjectList.size > 0) {
            for (i in 0..caloriesIntradayObjectList.size - 1) {
                var date: Date = dateFormatInterval.parse(caloriesIntradayObjectList.get(i).time)
                var y = caloriesIntradayObjectList.get(i).value.toDouble()
                series!!.appendData(DataPoint(date, y), true, caloriesIntradayObjectList.size)
                if (time_period.equals("1y")) {
                    graph_energy_burned?.getGridLabelRenderer()
                        ?.setLabelFormatter(DateAsXAxisLabelFormatter(context, dateFormat_year))
                    graph_energy_burned?.getGridLabelRenderer()?.setNumHorizontalLabels(4)
                }
                else{
                    graph_energy_burned?.getGridLabelRenderer()
                        ?.setLabelFormatter(DateAsXAxisLabelFormatter(context, dateFormat_week_month))
                    graph_energy_burned?.getGridLabelRenderer()?.setNumHorizontalLabels(5)
                }

            }

        } else {
            for (i in 0..caloriesObjectList.size - 1) {
                    var date: Date = dateFormat.parse(caloriesObjectList.get(i).dateTime)
                    var y = caloriesObjectList.get(i).value.toDouble()
                    series!!.appendData(DataPoint(date, y), true, caloriesObjectList.size)
                    if (time_period.equals("1y")) {
                        graph_energy_burned?.getGridLabelRenderer()
                            ?.setLabelFormatter(DateAsXAxisLabelFormatter(context, dateFormat_year))
                        graph_energy_burned?.getGridLabelRenderer()?.setNumHorizontalLabels(7)
                        graph_energy_burned?.getGridLabelRenderer()?.setHorizontalLabelsAngle(145)
                        graph_energy_burned?.getGridLabelRenderer()?.textSize = 18F
                    } else if (time_period.equals("1m")) {
                        graph_energy_burned?.getGridLabelRenderer()
                            ?.setLabelFormatter(
                                DateAsXAxisLabelFormatter(
                                    context,
                                    dateFormat_week_month
                                )
                            )
                        graph_energy_burned?.getGridLabelRenderer()?.setNumHorizontalLabels(7)
                        graph_energy_burned?.getGridLabelRenderer()?.setHorizontalLabelsAngle(145)
                        graph_energy_burned?.getGridLabelRenderer()?.textSize = 18F
                    } else {
                        graph_energy_burned?.getGridLabelRenderer()
                            ?.setLabelFormatter(
                                DateAsXAxisLabelFormatter(
                                    context,
                                    dateFormat_week_month
                                )
                            )
                        graph_energy_burned?.getGridLabelRenderer()?.setNumHorizontalLabels(5)
                        graph_energy_burned?.getGridLabelRenderer()?.setHorizontalLabelsAngle(145)
                        graph_energy_burned?.getGridLabelRenderer()?.textSize = 18F
                    }

            }
        }

        graph_energy_burned?.addSeries(series)
    }

    private fun setGraphFitbitStepCount() {

        graph_step_count?.removeAllSeries()
        series = LineGraphSeries()

        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val dateFormat_week_month = SimpleDateFormat("dd/MM", Locale.US)
        val dateFormat_year = SimpleDateFormat("MM/yy", Locale.US)
        val dateFormatInterval = SimpleDateFormat("HH:mm:ss", Locale.US)
        val dateFormatInterval2 = SimpleDateFormat("HH:mm", Locale.US)
        if (stepCountIntradayObject_List.size > 0) {
            for (i in 0..stepCountIntradayObject_List.size - 1) {
                val date = dateFormatInterval.parse(stepCountIntradayObject_List.get(i).time)
                val y = stepCountIntradayObject_List.get(i).value.toDouble()
                series?.appendData(DataPoint(date, y), true, stepCountIntradayObject_List.size)
                graph_step_count?.getGridLabelRenderer()
                    ?.setLabelFormatter(DateAsXAxisLabelFormatter(context, dateFormatInterval2))
                graph_step_count?.getGridLabelRenderer()?.setNumHorizontalLabels(5)

            }
        } else {
            for (i in 0..stepCountObject_List.size - 1) {

                    val date = dateFormat.parse(stepCountObject_List.get(i).dateTime)
                    val y = stepCountObject_List.get(i).value.toDouble()
                    series?.appendData(DataPoint(date, y), true, stepCountObject_List.size)
                    if (time_period.equals("1y")) {
                        graph_step_count?.getGridLabelRenderer()
                            ?.setLabelFormatter(DateAsXAxisLabelFormatter(context, dateFormat_year))
                        graph_step_count?.getGridLabelRenderer()?.setNumHorizontalLabels(7)
                        graph_step_count?.getGridLabelRenderer()?.setHorizontalLabelsAngle(145)
                        graph_step_count?.getGridLabelRenderer()?.textSize = 18F
                        graph_step_count?.getLegendRenderer()?.align =
                            LegendRenderer.LegendAlign.TOP

                    } else if (time_period.equals("1m")) {
                        graph_step_count?.getGridLabelRenderer()
                            ?.setLabelFormatter(
                                DateAsXAxisLabelFormatter(
                                    context,
                                    dateFormat_week_month
                                )
                            )
                        graph_step_count?.getGridLabelRenderer()?.setNumHorizontalLabels(7)
                        graph_step_count?.getGridLabelRenderer()?.setHorizontalLabelsAngle(145)
                        graph_step_count?.getGridLabelRenderer()?.textSize = 18F
                        graph_step_count?.getLegendRenderer()?.align =
                            LegendRenderer.LegendAlign.TOP

                    } else {
                        graph_step_count?.getGridLabelRenderer()
                            ?.setLabelFormatter(
                                DateAsXAxisLabelFormatter(
                                    context,
                                    dateFormat_week_month
                                )
                            )
                        graph_step_count?.getGridLabelRenderer()?.setNumHorizontalLabels(5)
                        graph_step_count?.getGridLabelRenderer()?.setHorizontalLabelsAngle(145)
                        graph_step_count?.getGridLabelRenderer()?.textSize = 18F
                        graph_step_count?.getLegendRenderer()?.align =
                            LegendRenderer.LegendAlign.TOP

                    }

            }

//                graph_step_count?.getViewport()?.setScrollable(true) // enables horizontal scrolling
//                graph_step_count?.getViewport()?.setScrollableY(true) // enables vertical scrolling
//                graph_step_count?.getViewport()?.setScalable(true) // enables horizontal zooming and scrolling
//                graph_step_count?.getViewport()?.setScalableY(true) // enables vertical zooming and scrolling

        }
        graph_step_count?.addSeries(series)

    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }


    private fun initInstanceToolbar() {
        btnBack = rootView?.findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        textView_toolbar_title = rootView?.findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText(resources.getString(R.string.title_card_fitme))
    }

    private fun initialView() {
//        linearLayout_fitme_day = rootView?.findViewById(R.id.linearLayout_fitme_day) as LinearLayout
        linearLayout_fitme_week =
            rootView?.findViewById(R.id.linearLayout_fitme_week) as LinearLayout
        linearLayout_fitme_month =
            rootView?.findViewById(R.id.linearLayout_fitme_month) as LinearLayout
        linearLayout_fitme_year =
            rootView?.findViewById(R.id.linearLayout_fitme_year) as LinearLayout

//        textview_day = rootView?.findViewById(R.id.textview_day) as TextView
        textview_week = rootView?.findViewById(R.id.textview_week) as TextView
        textview_month = rootView?.findViewById(R.id.textview_month) as TextView
        textview_year = rootView?.findViewById(R.id.textview_year) as TextView

//        graph_heart_rate = rootView?.findViewById(R.id.graph_heart_rate) as GraphView
        graph_step_count = rootView?.findViewById(R.id.graph_step_count) as GraphView
        graph_energy_burned = rootView?.findViewById(R.id.graph_energy_burned) as GraphView

        progressBackground = rootView?.findViewById(R.id.progressBackground) as RelativeLayout

    }

    private fun setting_pager() {
        btnBack?.setOnClickListener(View.OnClickListener {
            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.replaceFragment(Page_Fragment_Home())
        })

//        linearLayout_fitme_day?.setOnClickListener(View.OnClickListener {
//            tab_week = false
//            tab_month = false
//            tab_year = false
//            if (tab_day == false) {
//                time_period = "1d"
//                stepCountObject_List.clear()
//                caloriesObjectList.clear()
//                caloriesIntradayObjectList.clear()
//                stepCountIntradayObject_List.clear()
//                callService_fitmeStepCount()
//                callService_fitmeHeartRate()
//                callService_fitmeCalories()
//                linearLayout_fitme_day!!.setBackgroundResource(R.color.bg_blue)
//                textview_day?.setTextColor(resources.getColor(R.color.white))
//                //change
//                linearLayout_fitme_week!!.setBackgroundResource(R.color.bg_blue_ligth)
//                textview_week?.setTextColor(ContextCompat.getColor(rootView?.context!!,R.color.text_blue))
//                linearLayout_fitme_month!!.setBackgroundResource(R.color.bg_blue_ligth)
//                textview_month?.setTextColor(ContextCompat.getColor(rootView?.context!!,R.color.text_blue))
//                linearLayout_fitme_year!!.setBackgroundResource(R.color.bg_blue_ligth)
//                textview_year?.setTextColor(ContextCompat.getColor(rootView?.context!!,R.color.text_blue))
//                tab_day = true
//            }
//
//
//        })
        linearLayout_fitme_week?.setOnClickListener(View.OnClickListener {
            tab_day = false
            tab_month = false
            tab_year = false
            if (tab_week == false) {
                time_period = "1w"
                stepCountObject_List.clear()
                caloriesObjectList.clear()
                caloriesIntradayObjectList.clear()
                stepCountIntradayObject_List.clear()
                callService_fitmeStepCount()
                callService_fitmeHeartRate()
                callService_fitmeCalories()
                linearLayout_fitme_week!!.setBackgroundResource(R.color.bg_blue)
                textview_week?.setTextColor(resources.getColor(R.color.white))
                //change
                //linearLayout_fitme_day!!.setBackgroundResource(R.color.bg_blue_ligth)
//                textview_day?.setTextColor(ContextCompat.getColor(rootView?.context!!,R.color.text_blue))
                linearLayout_fitme_month!!.setBackgroundResource(R.color.bg_blue_ligth)
                textview_month?.setTextColor(
                    ContextCompat.getColor(
                        rootView?.context!!,
                        R.color.text_blue
                    )
                )
                linearLayout_fitme_year!!.setBackgroundResource(R.color.bg_blue_ligth)
                textview_year?.setTextColor(
                    ContextCompat.getColor(
                        rootView?.context!!,
                        R.color.text_blue
                    )
                )
                tab_week = true
            }

        })
        linearLayout_fitme_month?.setOnClickListener(View.OnClickListener {
            tab_day = false
            tab_week = false
            tab_year = false
            if (tab_month == false) {
                time_period = "1m"
                stepCountObject_List.clear()
                caloriesObjectList.clear()
                caloriesIntradayObjectList.clear()
                stepCountIntradayObject_List.clear()
                callService_fitmeStepCount()
                callService_fitmeHeartRate()
                callService_fitmeCalories()
                linearLayout_fitme_month!!.setBackgroundResource(R.color.bg_blue)
                textview_month?.setTextColor(resources.getColor(R.color.white))
                //change
//                linearLayout_fitme_day!!.setBackgroundResource(R.color.bg_blue_ligth)
//                textview_day?.setTextColor(ContextCompat.getColor(rootView?.context!!,R.color.text_blue))
                linearLayout_fitme_week!!.setBackgroundResource(R.color.bg_blue_ligth)
                textview_week?.setTextColor(
                    ContextCompat.getColor(
                        rootView?.context!!,
                        R.color.text_blue
                    )
                )
                linearLayout_fitme_year!!.setBackgroundResource(R.color.bg_blue_ligth)
                textview_year?.setTextColor(
                    ContextCompat.getColor(
                        rootView?.context!!,
                        R.color.text_blue
                    )
                )
                tab_month = true

            }
        })
        linearLayout_fitme_year?.setOnClickListener(View.OnClickListener {
            tab_day = false
            tab_week = false
            tab_month = false
            if (tab_year == false) {
                time_period = "1y"
                stepCountObject_List.clear()
                caloriesObjectList.clear()
                caloriesIntradayObjectList.clear()
                stepCountIntradayObject_List.clear()
                callService_fitmeStepCount()
                callService_fitmeHeartRate()
                callService_fitmeCalories()
                linearLayout_fitme_year!!.setBackgroundResource(R.color.bg_blue)
                textview_year?.setTextColor(resources.getColor(R.color.white))
                //change
//                linearLayout_fitme_day!!.setBackgroundResource(R.color.bg_blue_ligth)
//                textview_day?.setTextColor(ContextCompat.getColor(rootView?.context!!,R.color.text_blue))
                linearLayout_fitme_month!!.setBackgroundResource(R.color.bg_blue_ligth)
                textview_month?.setTextColor(
                    ContextCompat.getColor(
                        rootView?.context!!,
                        R.color.text_blue
                    )
                )
                linearLayout_fitme_week!!.setBackgroundResource(R.color.bg_blue_ligth)
                textview_week?.setTextColor(
                    ContextCompat.getColor(
                        rootView?.context!!,
                        R.color.text_blue
                    )
                )
                tab_year = true

            }
        })
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {
        if (event.getHttpName().equals("api_activities_steps")) {
            progressBackground?.visibility = GONE
            stepCountObject_List.clear()
            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {
                var activities_steps: JSONArray? = null
                try {
                    activities_steps = responseJson.getJSONArray("activities-steps")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (activities_steps != null) {
                    stepCountObject_List.clear()
                    for (i in 0..activities_steps.length() - 1) {
                        var stepcount_json: JSONObject? = null
                        try {
                            stepcount_json = activities_steps.get(i) as JSONObject
                        } catch (e: Exception) {
                        }
                        if (stepcount_json != null) {
                            var stepCountObject = StepCountObject()
                            stepCountObject.setParam(stepcount_json)
                            stepCountObject_List.add(stepCountObject)
                            setGraphFitbitStepCount()
                        }
                    }
                }
                var activities_stepsIntraday: JSONObject? = null
                try {
                    activities_stepsIntraday =
                        responseJson.getJSONObject("activities-steps-intraday")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (activities_stepsIntraday != null) {
                    var dataset: JSONArray? = null
                    try {
                        dataset = activities_stepsIntraday.getJSONArray("dataset")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (dataset != null) {
                        for (i in 0..dataset.length() - 1) {
                            var stepCount: JSONObject? = null
                            try {
                                stepCount = dataset.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (stepCount != null) {
                                var stepCountIntradayObject = StepCountIntradayObject()
                                stepCountIntradayObject.setParam(stepCount)
                                stepCountIntradayObject_List.add(stepCountIntradayObject)
                                setGraphFitbitStepCount()
                            }
                        }
                    }
                }


            }
        }

        if (event.getHttpName().equals("api_activities_calories")) {
            caloriesObjectList.clear()
            progressBackground?.visibility = GONE
            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {
                var activities_calories: JSONArray? = null
                try {
                    activities_calories = responseJson.getJSONArray("activities-calories")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (activities_calories != null) {
                    caloriesObjectList.clear()
                    for (i in 0..activities_calories.length() - 1) {
                        var calories: JSONObject? = null
                        try {
                            calories = activities_calories.get(i) as JSONObject
                        } catch (e: Exception) {
                        }
                        if (calories != null) {
                            var caloriesObject = CaloriesObject()
                            caloriesObject.setParam(calories)
                            caloriesObjectList.add(caloriesObject)
                            setGraphFitbitEnergyBurned()
                        }
                    }
                }
                var activities_caloriesListIntraday: JSONObject? = null
                try {
                    activities_caloriesListIntraday =
                        responseJson.getJSONObject("activities-calories-intraday")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (activities_caloriesListIntraday != null) {
                    var dataset: JSONArray? = null
                    try {
                        dataset = activities_caloriesListIntraday.getJSONArray("dataset")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (dataset != null) {
                        for (i in 0..dataset.length() - 1) {
                            var calories: JSONObject? = null
                            try {
                                calories = dataset.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (calories != null) {
                                var caloriesIntradayObject = CaloriesIntradayObject()
                                caloriesIntradayObject.setParam(calories)
                                caloriesIntradayObjectList.add(caloriesIntradayObject)
                                setGraphFitbitEnergyBurned()
                            }
                        }

                    }
                }


            }
        }

        if (event.getHttpName().equals("api_activities_heart")) {
            progressBackground?.visibility = GONE
            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {
                var activities_heart: JSONObject? = null
                try {
                    activities_heart = responseJson.getJSONObject("activities-heart-intraday")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (activities_heart != null) {
                    var dataset: JSONArray? = null
                    try {
                        dataset = activities_heart.getJSONArray("dataset")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    heartObjectList.clear()
                    if (dataset != null) {
                        for (i in 0..dataset.length() - 1) {
                            var heart: JSONObject? = null
                            try {
                                heart = dataset.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (heart != null) {
                                var heartObject = HeartObject()
                                heartObject.setParam(heart)
                                heartObjectList.add(heartObject)
                                //setGraphFitbitHeartRate()
                            }
                        }

                    }
                }
            }
        }
    }
}