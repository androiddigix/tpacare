package th.co.digix.tpacare.menu_status

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.AdapterRecycler_ClaimStatus
import th.co.digix.tpacare.adapter.RecyclerViewListener
import th.co.digix.tpacare.custom_object.ClaimStatusObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.menu_home.Page_Fragment_Home
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.util.ArrayList

class Page_Fragment_Status : Fragment() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_claim_status = httpServerURL.api_claim_status
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var progressBackground: RelativeLayout? = null

    var textView_inbox_no_data: TextView? = null
    var recyclerView_claim_status: RecyclerView? = null
    var adapterRecycler_ClaimStatus: AdapterRecycler_ClaimStatus? = null

    var rootView: View? = null

    val claimStatusObject_List = ArrayList<ClaimStatusObject>()

    var isRegisterBus = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.page_fragment_status, container, false)

        if (!isRegisterBus) {
            BusProvider.getInstance().register(this)
            isRegisterBus = true
        }

        initInstanceToolbar()

        initialView()

        callService()

        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialView() {

        progressBackground = rootView?.findViewById(R.id.progressBackground) as RelativeLayout

        textView_inbox_no_data = rootView?.findViewById(R.id.textView_inbox_no_data) as TextView
        recyclerView_claim_status = rootView?.findViewById(R.id.recyclerView_claim_status) as RecyclerView
        recyclerView_claim_status?.layoutManager =
            LinearLayoutManager(rootView?.context!!, LinearLayout.VERTICAL, false)
        recyclerView_claim_status?.setNestedScrollingEnabled(false)
        adapterRecycler_ClaimStatus = AdapterRecycler_ClaimStatus(activity, claimStatusObject_List)
        recyclerView_claim_status?.setAdapter(adapterRecycler_ClaimStatus)

        recyclerView_claim_status?.addOnItemTouchListener(
            RecyclerViewListener(
                activity!!,
                recyclerView_claim_status!!,
                object : RecyclerViewListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        if (claimStatusObject_List.size > position) {
                            val claimStatusObject = claimStatusObject_List.get(position)

                            val intent_go = Intent(activity, Page_Status_Detail::class.java)
                            intent_go.putExtra("claimStatusObject", claimStatusObject)
                            startActivity(intent_go)
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }

                })
        )

    }

    private fun callService() {

        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
        val userObject = sharedPreferencesManager.getUserObject()

        var username = userObject.username

//        val cryptLib = CryptLib()
//        val citizen_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(citizen_id, salt_key)

//     insured_type - Member ของฉัน - Family ครอบครัวของฉัน
        val defaultParameterAPI = DefaultParameterAPI(activity!!)
        val formBody = FormBody.Builder()
        formBody.add("username", "" + username)
        OkHttpPost(formBody, defaultParameterAPI).execute(root_url, api_claim_status, "api_claim_status")
        progressBackground?.visibility = View.VISIBLE
    }


    private fun initInstanceToolbar() {
        btnBack = rootView?.findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener {
            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.replaceFragment(Page_Fragment_Home())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 0
        }

        textView_toolbar_title = rootView?.findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_status))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {
        if (event.getHttpName().equals("api_claim_status")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {
                        claimStatusObject_List.clear()
                        for (i in 0..result.length() - 1) {
                            var statusCodeJson: JSONObject? = null
                            try {
                                statusCodeJson = result.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (statusCodeJson != null) {
                                val claimStatusObject = ClaimStatusObject()
                                claimStatusObject.setParam(statusCodeJson)
                                claimStatusObject_List.add(claimStatusObject)
                            }
                        }
                        if(claimStatusObject_List.size > 0){
                            textView_inbox_no_data?.visibility = View.GONE
                            recyclerView_claim_status?.visibility = View.VISIBLE
                            adapterRecycler_ClaimStatus?.notifyDataSetChanged()
                        }else{
                            textView_inbox_no_data?.visibility = View.VISIBLE
                            recyclerView_claim_status?.visibility = View.GONE
                        }
                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
    }

}
