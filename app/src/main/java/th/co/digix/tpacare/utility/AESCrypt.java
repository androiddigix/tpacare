package th.co.digix.tpacare.utility;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class AESCrypt {

    private static final String ALGORITHM = "AES";
    private static final String KEY = "android_kmypvd_k";

    public String encrypt(String value) throws Exception {
        Key key = generateKey();
        Cipher cipher = Cipher.getInstance(AESCrypt.ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedByteValue = cipher.doFinal(value.getBytes("utf-8"));
        String encryptedValue64 = Base64.encodeToString(encryptedByteValue, Base64.DEFAULT);
        encryptedValue64 = encryptedValue64.replaceAll("\\+", "_1");
        encryptedValue64 = encryptedValue64.replaceAll("/", "_2");
        encryptedValue64 = encryptedValue64.replaceAll("=", "_3");
        encryptedValue64 = encryptedValue64.replaceAll("\n", "");
        return encryptedValue64;

    }

    public String decrypt(String value) throws Exception {
        Key key = generateKey();
        Cipher cipher = Cipher.getInstance(AESCrypt.ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        value = value.replaceAll("_1", "+");
        value = value.replaceAll("_2", "/");
        value = value.replaceAll("_3", "=");
        byte[] decryptedValue64 = Base64.decode(value, Base64.DEFAULT);
        byte[] decryptedByteValue = cipher.doFinal(decryptedValue64);
        String decryptedValue = new String(decryptedByteValue, "utf-8");
        return decryptedValue;

    }

    private Key generateKey() throws Exception {
        Key key = new SecretKeySpec(AESCrypt.KEY.getBytes(), AESCrypt.ALGORITHM);
        return key;
    }
}