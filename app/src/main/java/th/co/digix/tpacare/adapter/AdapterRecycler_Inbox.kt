package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.ArticleObject
import th.co.digix.tpacare.custom_object.InboxObject
import th.co.digix.tpacare.utility.DateTimeHelper
import java.util.ArrayList


class AdapterRecycler_Inbox(
    val activity: Activity?,
    var inboxObject_List: ArrayList<InboxObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (position < inboxObject_List.size) {
            val inboxObject = inboxObject_List.get(position)

            var message_lable = ""  + inboxObject.message_lable
            var message_text = ""  + inboxObject.message_text
            var delivery_date = ""  + inboxObject.delivery_date
            var read_status = ""  + inboxObject.read_status

            val dateTimeHelper = DateTimeHelper()
            val deliveryDate = dateTimeHelper.parseStringToDate2(delivery_date)

            val date_inbox = dateTimeHelper.parseDateToString3(deliveryDate)
            val time_inbox = dateTimeHelper.parseDateToString2(deliveryDate)

            holder.textView_inbox_name.setText("" + message_lable)
            holder.textView_inbox_message.setText("" + message_text)

            holder.textView_inbox_date.setText("" + date_inbox)
            holder.textView_inbox_time.setText("" + time_inbox)

            if(read_status.equals("R")){
                holder.imageView_read_status?.setImageResource(R.drawable.bg_circle_inbox_status_normal)
            }else{
                holder.imageView_read_status?.setImageResource(R.drawable.bg_circle_inbox_status_active)
            }


        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_list_inbox, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (inboxObject_List != null) {
            var listSize = inboxObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var linearLayout_bg_item: LinearLayout
        internal var imageView_read_status: ImageView
        internal var textView_inbox_name: TextView
        internal var textView_inbox_message: TextView
        internal var textView_inbox_date: TextView
        internal var textView_inbox_time: TextView

        init {
            linearLayout_bg_item = itemView.findViewById(R.id.linearLayout_bg_item) as LinearLayout
            imageView_read_status = itemView.findViewById(R.id.imageView_read_status) as ImageView
            textView_inbox_name = itemView.findViewById(R.id.textView_inbox_name) as TextView
            textView_inbox_message = itemView.findViewById(R.id.textView_inbox_message) as TextView
            textView_inbox_date = itemView.findViewById(R.id.textView_inbox_date) as TextView
            textView_inbox_time = itemView.findViewById(R.id.textView_inbox_time) as TextView
        }
    }

}