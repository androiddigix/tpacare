package th.co.digix.tpacare

import android.support.v7.app.AppCompatActivity
import android.os.*
import android.util.Log
import org.json.JSONObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.utility.LogoutManage
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.lang.Exception
import java.util.*
import android.view.View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS
import android.os.Build
import android.annotation.TargetApi
import android.view.View


open class BaseActivity : AppCompatActivity() {

    var checkexp_time: Long = 60000 // 1 min = 60000 ms = 1 * 60 * 1000 ms
    var inactive_timeout: Long = 10L * 60L * 1000L // 10 min = 360000 ms = 10 * 60 * 1000 ms

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)

        disableAutoFill()
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun disableAutoFill() {
        window.decorView.importantForAutofill = View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS
    }

    public override fun onResume() {
        super.onResume()
        resetDisconnectTimer()
        resetCheckEXPTimer()
    }

    public override fun onStop() {
        super.onStop()
//        stopDisconnectTimer()
        stopCheckEXPTimer()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopDisconnectTimer()
    }

    private val checkexpHandler = Handler(object : Handler.Callback {
        override fun handleMessage(msg: Message): Boolean {
            return true
        }
    })

    private val checkexpCallback = Runnable {
//        Log.d("coke", "BaseActivity checkexpCallback")
        // Perform any required operation on disconnect
        resetCheckEXPTimer()
        val sharedPreferencesManager = SharedPreferencesManager(this)
        val userObject = sharedPreferencesManager.getUserObject()
        if (userObject.state_login.equals("login")) {
            var token_expires_date = 0L
            try {
                token_expires_date = userObject.token_expires_date.toLong()
            } catch (e: Exception) {
            }
            if (token_expires_date > 0) {
                val date = Date(token_expires_date)
                val todayDate = Calendar.getInstance().time
                if (date.after(todayDate)) {

                    val httpServerURL = HttpServerURL()
                    var root_url = httpServerURL.host_server
                    val api_authRefreshtoken = httpServerURL.api_authRefreshtoken

                    var jsonObject = JSONObject()
                    jsonObject.put("AccessToken", "" + userObject.token)
                    jsonObject.put("RefreshToken", "" + userObject.refresh_token)
                    jsonObject.put("FirebaseToken", "")

                    var refreshtoken_str = jsonObject.toString()

                    val defaultParameterAPI = DefaultParameterAPI(this)
                    var okhttp = OkHttpPost(null, defaultParameterAPI)
                    okhttp.setJson("" + refreshtoken_str)
                    okhttp.execute(root_url, api_authRefreshtoken, "api_authRefreshtoken")
                }
            }
        }

    }

    fun resetCheckEXPTimer() {
        checkexpHandler.removeCallbacks(checkexpCallback)
        checkexpHandler.postDelayed(checkexpCallback, checkexp_time)
    }

    fun stopCheckEXPTimer() {
        checkexpHandler.removeCallbacks(checkexpCallback)
    }

    override fun onUserInteraction() {
        resetDisconnectTimer()
    }

    fun resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback)
        disconnectHandler.postDelayed(disconnectCallback, inactive_timeout)
    }

    fun stopDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback)
    }

    private val disconnectHandler = Handler(object : Handler.Callback {
        override fun handleMessage(msg: Message): Boolean {
            return true
        }
    })

    private val disconnectCallback = Runnable {
        Log.d("coke", "BaseActivity disconnectCallback")
        // Perform any required operation on disconnect

        val sharedPreferencesManager = SharedPreferencesManager(this)
        val userObject = sharedPreferencesManager.getUserObject()
        if (userObject.state_login.equals("login")) {
            val logoutManage = LogoutManage()
            logoutManage.logout_6min(this)
        }


    }

}