package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.HospitalObject
import th.co.digix.tpacare.menu_home.search_hospital.Page_SearchHospital_Detail
import java.util.ArrayList


class AdapterRecycler_SearchHospital(
    val activity: Activity?,
    var hospitalObject_List: ArrayList<HospitalObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (position < hospitalObject_List.size) {
            val hospitalObject = hospitalObject_List.get(position)

            var hospital_name = "" + hospitalObject.hospital_name
            var hospital_distance = "" + hospitalObject.nearby
            var hospital_phone = "" + hospitalObject.tel

//            val mFormat = DecimalFormat("###,###,##0")
//            var nearby_int = 0.0
//            try {
//                nearby_int = hospitalObject.nearby.toDouble() * 1000
//                hospital_distance = "" + mFormat.format(nearby_int)
//            } catch (e: Exception) {
//            }


            holder.textView_hospital_name.setText("" + hospital_name)
            holder.textView_hospital_distance.setText("" + hospital_distance + " " + activity!!.resources.getString(R.string.text_distance_unit))
            holder.textView_hospital_phone.setText("" + hospital_phone + " " )
            holder.linearLayout_search_hospital.setOnClickListener {

                val intent_go = Intent(activity, Page_SearchHospital_Detail::class.java)
                intent_go.putExtra("hospitalObject",hospitalObject)
                activity.startActivity(intent_go)
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_search_hospital, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (hospitalObject_List != null) {
            var listSize = hospitalObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var linearLayout_search_hospital: LinearLayout
        internal var textView_hospital_name: TextView
        internal var textView_hospital_distance: TextView
        internal var textView_hospital_phone: TextView

        init {
            linearLayout_search_hospital = itemView.findViewById(R.id.linearLayout_search_hospital) as LinearLayout
            textView_hospital_name = itemView.findViewById(R.id.textView_hospital_name) as TextView
            textView_hospital_distance = itemView.findViewById(R.id.textView_hospital_distance) as TextView
            textView_hospital_phone = itemView.findViewById(R.id.textView_hospital_phone) as TextView
        }
    }

}