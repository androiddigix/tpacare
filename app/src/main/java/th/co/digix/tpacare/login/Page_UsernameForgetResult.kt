package th.co.digix.tpacare.login

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor

class Page_UsernameForgetResult : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val salt_key = httpServerURL.salt_key


    var progressBackground: RelativeLayout? = null

    var editText_UserName: EditText? = null
    var linearLayout_confirm_username: LinearLayout? = null

    var textView_popup_title: TextView? = null

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var userName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_username_forget_result)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialWorking() {

        linearLayout_confirm_username?.setOnClickListener(View.OnClickListener {

           finish()

        })

    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        editText_UserName = findViewById(R.id.editText_UserName) as EditText
        linearLayout_confirm_username = findViewById(R.id.linearLayout_confirm_username) as LinearLayout

        editText_UserName?.setText("" + userName)

    }


    private fun initialIntentData() {

        userName = intent.extras?.getString("userName", "").toString()

        if (userName.equals("null")) {
            userName = ""
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.text_usernameforget_title))
    }

}