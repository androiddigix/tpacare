package th.co.digix.tpacare.custom_object

import org.json.JSONObject

class ConfigurationObject {

    var code: String = ""
    var name: String = ""
    var value: String = ""
    var comment: String = ""

    fun setParam(result: JSONObject) {
        try {
            code = result.getString("code")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            name = result.getString("name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            value = result.getString("value")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            comment = result.getString("comment")
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}