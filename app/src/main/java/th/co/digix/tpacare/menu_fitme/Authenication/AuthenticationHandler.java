package th.co.digix.tpacare.menu_fitme.Authenication;

/**
 * Created by jboggess on 9/14/16.
 */
public interface AuthenticationHandler {

    void onAuthFinished(AuthenticationResult result);

}
