package th.co.digix.tpacare.http_service

import android.app.Activity
import android.os.AsyncTask
import android.util.Log
import okhttp3.*
import org.json.JSONObject
import th.co.digix.tpacare.R
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.LogoutManage
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit


class OkHttpPost(formBody: FormBody.Builder?, defaultParameterAPI: DefaultParameterAPI) :
    AsyncTask<String, Void, String>() {

    internal var eventName = ""
    internal var responseStr = ""

    internal var json: String? = null

    internal var formBody: FormBody.Builder? = null

    internal var parameter_udid = ""
    internal var parameter_machine = ""
    internal var parameter_app_version = ""
    internal var parameter_network = ""
    internal var parameter_platform = ""
    internal var parameter_os_version = ""
    internal var parameter_os = ""

    internal var activity: Activity? = null

    init {
        this.formBody = formBody

        activity = defaultParameterAPI.activity
        parameter_udid = defaultParameterAPI.udid
        parameter_machine = defaultParameterAPI.machine
        parameter_app_version = defaultParameterAPI.versionApp
        parameter_network = defaultParameterAPI.network
        parameter_platform = defaultParameterAPI.platform
        parameter_os_version = defaultParameterAPI.versionOS
        parameter_os = defaultParameterAPI.os
    }

    fun setJson( json: String) {
        this.json = json
    }

    override fun doInBackground(vararg url: String): String? {

        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
        val app_language = "EN"
//        app_language = sharedPreferencesManager.getLanguage()
        var formBody: RequestBody? = null
        if (this.formBody != null) {
            formBody = this.formBody!!
                .add("UDID", "" + parameter_udid)
                .add("device_name", "" + parameter_machine)
                .add("VersionApp", "" + parameter_app_version)
                .add("VersionOS", "" + parameter_os_version)
                .add("Network", "" + parameter_network)
                .add("platform", "" + parameter_platform)
                .add("OS", "" + parameter_os)
                .add("lang", "" + app_language)
                .build()
        } else if (json != null) {
            formBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json!!)
        }

        if (formBody != null) {
            var httpServer = ""
            var servicePath = ""
            eventName = ""
            var token = ""

            try {
                httpServer = url[0]
                servicePath = url[1]
                eventName = url[2]
            } catch (e: Exception) {
                e.printStackTrace()
            }
            try {
                token = url[3]
            } catch (e: Exception) {
                token = ""
            }

            var tpa_access_token = ""

            val userObject = sharedPreferencesManager.getUserObject()
            if (!userObject.token.equals("")) {
                token = userObject.token
            }
            if (!userObject.tpa_access_token.equals("")) {
                tpa_access_token = userObject.tpa_access_token
            }

//            Log.d("coke", "api token " + token)

            var request: Request? = null

//            val certPinner = CertificatePinner.Builder()
//                    .add("appmattus.com","sha256/4hw5tz+scE+TW+mlai5YipDfFWn1dqvfLG+nU7tq1V8=")
//                    .build()

//            val resourceStream: InputStream = activity.resources.openRawResource(R.raw.certificate)
//            val keyStoreType = KeyStore.getDefaultType()
//            val keyStore = KeyStore.getInstance(keyStoreType)
//            val cf: CertificateFactory = CertificateFactory.getInstance("X.509")
//            val ca: X509Certificate = resourceStream.use {
//                cf.generateCertificate(it) as X509Certificate
//            }
////            keyStore.load(resourceStream, null)
//            keyStore.apply {
//                load(null, null)
//                setCertificateEntry("ca", ca)
//            }
//
//            val trustManagerAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
//            val trustManagerFactory = TrustManagerFactory.getInstance(trustManagerAlgorithm)
//            trustManagerFactory.init(keyStore)
//
//            val sslContext = SSLContext.getInstance("TLS")
//            sslContext.init(null, trustManagerFactory.trustManagers, null)
//
//            val x509TrustManager = trustManagerFactory.trustManagers[0] as X509TrustManager

            val client = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
//                    .sslSocketFactory(sslContext.getSocketFactory(),x509TrustManager)
                .build()


            var token_key = "" + activity?.resources?.getString(R.string.token_key)
            if (eventName.equals("api_authRegisterRequestOTP") || eventName.equals("api_authRegisterRequestOTP_re")
                || eventName.equals("api_authForgotPasswordRequestOTP") || eventName.equals("api_authForgotUserRequestOTP")
            ) {
                request = Request.Builder()
                    .url(httpServer + servicePath)
                    .header("token_key", token_key)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .post(formBody)
                    .build()
            } else if (tpa_access_token.equals("")) {
                request = Request.Builder()
                    .url(httpServer + servicePath)
                    .header("Authorization", "Bearer " + token)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .post(formBody)
                    .build()
            } else {
                request = Request.Builder()
                    .url(httpServer + servicePath)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("Authorization", "Bearer " + token)
                    .header("TPACareAuthorization", tpa_access_token)
                    .post(formBody)
                    .build()
            }


            if (request != null) {
                try {
                    val response = client.newCall(request).execute()
                    Log.d("response", "$eventName html response is $response")

                    responseStr = response.body()!!.string()
                    Log.d("responseStr", "$eventName is $responseStr")

                    val service_code = response.code()
                    val service_message = response.message()
                    if (service_code == 401) {
                        val jsonObjectError401Authen = JSONObject()
                        try {
                            jsonObjectError401Authen.put("status", "fix_code")
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        responseStr = jsonObjectError401Authen.toString()

                        if (!eventName.equals("api_accountLogout") && !eventName.equals("api_globalSetting")) {
                            if (activity != null) {
                                val logoutManage = LogoutManage()
                                logoutManage.logout_401(activity!!)
                            }
                        }

                    }
                    if (service_code == 503) {
                        val jsonObjectError401Authen = JSONObject()
                        try {
                            jsonObjectError401Authen.put("status", "fix_code")
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        responseStr = jsonObjectError401Authen.toString()

//                        if (!eventName.equals("api_accountLogout") && !eventName.equals("api_globalSetting")) {
//                            if (activity != null) {
//                                val logoutManage = LogoutManage()
//                                logoutManage.logout_503(activity!!)
//                            }
//                        }

                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

        }

        return ""

    }

    override fun onPostExecute(result: String) {

        if (responseStr == "") {
            val jsonObjectTimeOut = JSONObject()
            try {
                jsonObjectTimeOut.put("status", false)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                jsonObjectTimeOut.put("alert", "Android okHttp time out")
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (Locale.getDefault().language.equals("th", ignoreCase = true)) {
                try {
                    jsonObjectTimeOut.put("errorMessage", "ไม่สามารถเชื่อมต่อเซิฟเวอร์ได้")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else {
                try {
                    jsonObjectTimeOut.put("errorMessage", "Cannot connect server.")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
            responseStr = jsonObjectTimeOut.toString()
        }

        BusProvider.getInstance().post(OKHttpEvent(responseStr, eventName))
    }

    companion object {
        private val TAG = "BackgroundWebRunner"
    }

}
