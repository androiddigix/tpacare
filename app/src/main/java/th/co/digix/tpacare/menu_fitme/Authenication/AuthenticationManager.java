package th.co.digix.tpacare.menu_fitme.Authenication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import th.co.digix.tpacare.custom_object.UserObject;
import th.co.digix.tpacare.menu_fitme.FintMEAuthen_Activity;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jboggess on 9/14/16.
 */
public class AuthenticationManager {

    static final int RESULT_CODE = 154;
    private static final String AUTH_TOKEN_KEY = "AUTH_TOKEN";
    private static boolean configured = false;
    static AuthenticationConfiguration authenticationConfiguration;
    private static AccessToken currentAccessToken;
    private static SecurePreferences preferences;


    public static void configure(Context context, AuthenticationConfiguration authenticationConfiguration) {
        AuthenticationManager.authenticationConfiguration = authenticationConfiguration;
        preferences = new SecurePreferences(context, "FITBIT_AUTHENTICATION_PREFERENCES", authenticationConfiguration.getEncryptionKey(), true);
        configured = true;
    }

    public static synchronized AccessToken getCurrentAccessToken() {
        checkPreconditions();
        if (currentAccessToken == null) {
            currentAccessToken = AccessToken.fromBase64(preferences.getString(AUTH_TOKEN_KEY));
        }
        return currentAccessToken;
    }

    public static synchronized void setCurrentAccessToken(AccessToken currentAccessToken) {
        checkPreconditions();
        Context context = null;
        AuthenticationManager.currentAccessToken = currentAccessToken;

        //Save to shared preferences
        preferences.put(AUTH_TOKEN_KEY, currentAccessToken == null ? null : currentAccessToken.toBase64String());
        UserObject userObject = new UserObject();
        Fitbit_Token.fitbit_token = currentAccessToken.getAccessToken();
        Log.d("token","= "+ Fitbit_Token.fitbit_token);


    }

    public static void login(Activity activity) {
        Set<Scope> scopes = new HashSet<>();
        scopes.addAll(authenticationConfiguration.getRequiredScopes());
        scopes.addAll(authenticationConfiguration.getOptionalScopes());

        Intent intent = FintMEAuthen_Activity.createIntent(
                activity.getApplicationContext(),
                authenticationConfiguration.getClientCredentials(),
                authenticationConfiguration.getTokenExpiresIn(),
                scopes);

        activity.startActivityForResult(intent, RESULT_CODE);
    }

    public static boolean onActivityResult(int requestCode, int resultCode, Intent data, @NonNull AuthenticationHandler authenticationHandler) {
        checkPreconditions();
        switch (requestCode) {
            case (RESULT_CODE): {
                if (resultCode == Activity.RESULT_OK) {
                    AuthenticationResult authenticationResult = data.getParcelableExtra(FintMEAuthen_Activity.AUTHENTICATION_RESULT_KEY);

                    if (authenticationResult.isSuccessful()) {
                        Set<Scope> grantedScopes = new HashSet<>(authenticationResult.getAccessToken().getScopes());
                        Set<Scope> requiredScopes = new HashSet<>(authenticationConfiguration.getRequiredScopes());

                        requiredScopes.removeAll(grantedScopes);
                        if (requiredScopes.size() > 0) {
                            authenticationResult = AuthenticationResult.missingRequiredScopes(requiredScopes);
                        } else {
                            setCurrentAccessToken(authenticationResult.getAccessToken());
                        }
                    }

                    authenticationHandler.onAuthFinished(authenticationResult);
                } else {
                    authenticationHandler.onAuthFinished(AuthenticationResult.dismissed());
                }
                return true;
            }
        }

        return false;
    }

    public static boolean isLoggedIn() {
        checkPreconditions();
        AccessToken currentAccessToken = getCurrentAccessToken();
        if(currentAccessToken == null){
            return false ;
        }else{
            if(currentAccessToken.hasExpired() ){
                return false ;
            }else{
                return true ;
            }
        }
    }

    public static void logout(final Activity contextActivity) {
        logout(contextActivity, null);
    }

    public static void logout(final Activity contextActivity, @Nullable final LogoutTaskCompletionHandler logoutTaskCompletionHandler) {
        checkPreconditions();
        if (!isLoggedIn()) {
            return;
        }

        Handler handler = new Handler();
        new LogoutTask(getAuthenticationConfiguration().getClientCredentials(), getCurrentAccessToken(),
                new LogoutTaskCompletionHandler() {
                    @Override
                    public void logoutSuccess() {
                        Intent beforeLoginActivity = authenticationConfiguration.getBeforeLoginActivity();
                        if (beforeLoginActivity != null) {
                            contextActivity.startActivity(beforeLoginActivity);
                        }
                        if (logoutTaskCompletionHandler != null) {
                            logoutTaskCompletionHandler.logoutSuccess();
                        }
                    }

                    @Override
                    public void logoutError(String message) {
                        Intent beforeLoginActivity = authenticationConfiguration.getBeforeLoginActivity();
                        if (beforeLoginActivity != null) {
                            contextActivity.startActivity(beforeLoginActivity);
                        }
                        if (logoutTaskCompletionHandler != null) {
                            logoutTaskCompletionHandler.logoutError(message);
                        }
                    }
                })
                .execute(handler);

        setCurrentAccessToken(null);
    }

    public static AuthenticationConfiguration getAuthenticationConfiguration() {
        checkPreconditions();
        return authenticationConfiguration;
    }

    public static BasicHttpRequestBuilder createSignedRequest() {
        BasicHttpRequestBuilder basicHttpRequestBuilder = BasicHttpRequestBuilder.create();
        authenticationConfiguration.getRequestSigner().signRequest(basicHttpRequestBuilder);
        return basicHttpRequestBuilder;
    }


    private static void checkPreconditions() {
        if (!configured) {
            throw new IllegalArgumentException("You must call `configure` on AuthenticationManager before using its methods!");
        }
    }

}
