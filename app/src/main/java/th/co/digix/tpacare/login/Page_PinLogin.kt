package th.co.digix.tpacare.login

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import android.support.v4.app.ActivityCompat
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat
import android.support.v4.os.CancellationSignal
import android.support.v7.app.AlertDialog
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.UserObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.io.IOException
import java.security.*
import java.security.cert.CertificateException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey


class Page_PinLogin : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authLoginByPasscode = httpServerURL.api_authLoginByPasscode
    val api_authRefreshtoken = httpServerURL.api_authRefreshtoken
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null
    var imageView_profile: ImageView? = null
    var textView_username: TextView? = null

    var textView_forgot_password: TextView? = null
    var textView_forgot_username: TextView? = null

    var textView_no1: TextView? = null
    var textView_no2: TextView? = null
    var textView_no3: TextView? = null
    var textView_no4: TextView? = null
    var textView_no5: TextView? = null
    var textView_no6: TextView? = null
    var textView_no7: TextView? = null
    var textView_no8: TextView? = null
    var textView_no9: TextView? = null
    var textView_no0: TextView? = null
    var imageView_no_delete: ImageView? = null

    var imageView_IndicatorDot_1: ImageView? = null
    var imageView_IndicatorDot_2: ImageView? = null
    var imageView_IndicatorDot_3: ImageView? = null
    var imageView_IndicatorDot_4: ImageView? = null
    var imageView_IndicatorDot_5: ImageView? = null
    var imageView_IndicatorDot_6: ImageView? = null

    var password_number = ""
//    var userObject = UserObject()

    private var mFingerprintManager: FingerprintManagerCompat? = null
    private var mKeyguardManager: KeyguardManager? = null
    private var mKeyStore: KeyStore? = null
    private var mKeyGenerator: KeyGenerator? = null
    private var cipher: Cipher? = null
    private var mCryptoObject: FingerprintManagerCompat.CryptoObject? = null
    private var mFingerprintHelper: FingerprintHelper? = null

    var stateCheckPinOk = false
    var isOnBackButton = false

    var progressBackground: RelativeLayout? = null

    var check_lock_send_service = false

    var wrongPasswordCount = 0

    var alertDialog_NeedHelp: AlertDialog? = null

    var firebase_Tokens: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_pin_custom)

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            firebase_Tokens = instanceIdResult.token
        }

        BusProvider.getInstance().register(this)

        initInstanceToolbar()

        initialIntentData()

        initialView()

        setOnClick()

        checkFingerPrint()

    }

    private fun checkFingerPrint() {

        val sharedPreferencesManager = SharedPreferencesManager(this)
        val isUseFingerPrint = sharedPreferencesManager.getUseFingerPrint()
        if (isUseFingerPrint) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                try {
                    mKeyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    mFingerprintManager = FingerprintManagerCompat.from(this@Page_PinLogin)
                } catch (e: Exception) {
                    e.printStackTrace()
                }


                if (ActivityCompat.checkSelfPermission(
                        this, Manifest.permission.USE_FINGERPRINT
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // Checking fingerprint permission.
                    Log.d("tpa", "Checking fingerprint permission.")
                } else if (mKeyguardManager == null) {
                    // Checking Device support fingerprint or not.
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                } else if (mFingerprintManager == null) {
                    // Checking Device support fingerprint or not.
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                } else if (!mFingerprintManager!!.isHardwareDetected()) {
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                    // Checking Device support fingerprint or not.
                } else if (!mKeyguardManager!!.isKeyguardSecure()) {
                    Log.d("tpa", "Not setting lock screen with imprint.")
                    // Not setting lock screen with imprint.
                } else if (!mFingerprintManager!!.hasEnrolledFingerprints()) {
                    // Not registered at least one fingerprint in Settings.
                    Log.d("tpa", "Not registered at least one fingerprint in Settings.")
                } else {
                    fingerPrint()
                    showMessageDialog_fingerprint(this@Page_PinLogin)
                }
            }
        } else {
        }

    }

    @SuppressLint("NewApi")
    fun fingerPrint() {

        generateKey()
        if (initCipher()) {
            if (cipher != null) {
                mCryptoObject = FingerprintManagerCompat.CryptoObject(cipher!!)
                mFingerprintHelper = FingerprintHelper(this, progressBackground)

                if (mFingerprintHelper != null) {
                    if (mFingerprintManager != null) {
                        if (mCryptoObject != null) {
                            mFingerprintHelper?.startAuth(mFingerprintManager!!, mCryptoObject!!)
                        }
                    }
                }
            }

        }
    }


    @SuppressLint("NewApi")
    protected fun generateKey() {

        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            mKeyGenerator = KeyGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_AES,
                "AndroidKeyStore"
            )
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(
                "Failed to get KeyGenerator instance", e
            )
        } catch (e: NoSuchProviderException) {
            throw RuntimeException("Failed to get KeyGenerator instance", e)
        }

        try {
            //Alias for our key in the Android Key Store
            val KEY_NAME = resources.getString(R.string.key_name)

            mKeyStore?.load(null)
            mKeyGenerator?.init(
                KeyGenParameterSpec.Builder(
                    KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                )
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                        KeyProperties.ENCRYPTION_PADDING_PKCS7
                    )
                    .build()
            )
            mKeyGenerator?.generateKey()
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        } catch (e: InvalidAlgorithmParameterException) {
            throw RuntimeException(e)
        } catch (e: CertificateException) {
            throw RuntimeException(e)
        } catch (e: IOException) {
            throw RuntimeException(e)
        }

    }

    @SuppressLint("NewApi")
    private fun initCipher(): Boolean {
        //Alias for our key in the Android Key Store
        val KEY_NAME = resources.getString(R.string.key_name)

        try {
            cipher = Cipher.getInstance(
                KeyProperties.KEY_ALGORITHM_AES + "/"
                        + KeyProperties.BLOCK_MODE_CBC + "/"
                        + KeyProperties.ENCRYPTION_PADDING_PKCS7
            )
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to get Cipher", e)
        } catch (e: NoSuchPaddingException) {
            throw RuntimeException("Failed to get Cipher", e)
        }

        try {
            mKeyStore?.load(null)
            val key = mKeyStore?.getKey(KEY_NAME, null) as SecretKey
            cipher?.init(Cipher.ENCRYPT_MODE, key)
            return true
        } catch (e: KeyPermanentlyInvalidatedException) {
            return false
        } catch (e: KeyStoreException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: CertificateException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: UnrecoverableKeyException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: IOException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException("Failed to init Cipher", e)
        } catch (e: InvalidKeyException) {
            throw RuntimeException("Failed to init Cipher", e)
        }

    }

    @SuppressLint("NewApi")
    class FingerprintHelper(private val activity: Activity, var progressBackground: RelativeLayout?) :
        FingerprintManagerCompat.AuthenticationCallback() {

        val httpServerURL = HttpServerURL()
        var root_url = httpServerURL.host_server
        val api_authRefreshtoken = httpServerURL.api_authRefreshtoken
        val salt_key = httpServerURL.salt_key

        private var cancellationSignal: CancellationSignal? = null
        var alertDialog_NeedHelp: AlertDialog? = null

        fun addAlertDialog(alertDialog_NeedHelp: AlertDialog?) {
            this.alertDialog_NeedHelp = alertDialog_NeedHelp
        }

        fun startAuth(
            manager: FingerprintManagerCompat,
            cryptoObject: FingerprintManagerCompat.CryptoObject
        ) {

            cancellationSignal = CancellationSignal()

            if (ActivityCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.USE_FINGERPRINT
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            manager.authenticate(cryptoObject, 0, cancellationSignal, this, null)
        }

        fun stopListening() {
            if (cancellationSignal != null) {
                cancellationSignal!!.cancel()
                cancellationSignal = null
            }
        }

        override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
            super.onAuthenticationError(errorCode, errString)
            Log.e("FingerprintHelper", "onAuthenticationError:$errString")
        }


        override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence) {
            super.onAuthenticationHelp(helpCode, helpString)
//            Toast.makeText(activity,
//                    "Authentication help\n$helpString",
//                    Toast.LENGTH_LONG).show()
        }

        override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult) {
            super.onAuthenticationSucceeded(result)
//            Toast.makeText(activity,
//                    "Authentication succeeded.",
//                    Toast.LENGTH_LONG).show()
            alertDialog_NeedHelp?.dismiss()
            alertDialog_NeedHelp = null

            var sharedPreferencesManager = SharedPreferencesManager(activity)
            val userObject = sharedPreferencesManager.getUserObject()

            var firebase_Tokens: String = ""
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
                firebase_Tokens = instanceIdResult.token
            }
            Log.d("coke", "firebase_Tokens " + firebase_Tokens)
            var firebase_Tokens_encrypt = ""
            val cryptLib = CryptLib()
            firebase_Tokens_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(firebase_Tokens, salt_key)

            var jsonObject = JSONObject()
            jsonObject.put("AccessToken", "" + userObject.token)
            jsonObject.put("RefreshToken", "" + userObject.refresh_token)
            jsonObject.put("FirebaseToken", "" + firebase_Tokens_encrypt)

            var refreshtoken_str = jsonObject.toString()

            val defaultParameterAPI = DefaultParameterAPI(activity)
            var okhttp = OkHttpPost(null, defaultParameterAPI)
            okhttp.setJson("" + refreshtoken_str)
            okhttp.execute(root_url, api_authRefreshtoken, "api_authRefreshtoken")

            progressBackground?.visibility = View.VISIBLE
            alertDialog_NeedHelp?.dismiss()
//            val sharedPreferencesManager = SharedPreferencesManager(activity)
//            val userObject = sharedPreferencesManager.getUserObject()
//            userObject.state_login = "login"
//            sharedPreferencesManager.setUserObject(userObject)
//
//            val intent = Intent(activity, MainActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//            activity.startActivity(intent)
        }

        override fun onAuthenticationFailed() {
            super.onAuthenticationFailed()
//            Toast.makeText(activity,
//                    "Authentication failed.",
//                    Toast.LENGTH_LONG).show()
        }
    }


    override fun onDestroy() {
        super.onDestroy()

        BusProvider.getInstance().unregister(this)
    }

    private fun initInstanceToolbar() {

        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_login_pin))

        imageView_profile = findViewById(R.id.imageView_profile) as ImageView
        textView_username = findViewById(R.id.textView_username) as TextView

        val sharedPreferencesManager = SharedPreferencesManager(this)
        val userObject = sharedPreferencesManager.getUserObject()

        textView_username?.setText(resources.getString(R.string.text_hello_prefix) + userObject.first_name + " " + userObject.last_name)

        if (imageView_profile != null) {

            val url_image = "" + userObject.image_profile
//            val image_profile_stream = "" + userObject.image_profile_stream
//            val image_profile_ByteArray = Base64.decode(image_profile_stream, Base64.DEFAULT)

            val placeholder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_profile_default_home)
            val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), placeholder)
            circularBitmapDrawable.isCircular = true
            Glide.with(this)
                .load(url_image)
                .apply(
                    RequestOptions
                        .circleCropTransform()
                        .placeholder(circularBitmapDrawable)
                        .error(circularBitmapDrawable)
                )
                .into(imageView_profile!!)
        }

    }

    fun showMessageDialog_fingerprint(activity: Activity) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_fingerprint, null)

        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }


    private fun initialIntentData() {
//        try {
//            userObject = intent.getParcelableExtra("userObject")
//        } catch (e: Exception) {
//        }
        try {
            isOnBackButton = intent.getBooleanExtra("isOnBackButton", false)
        } catch (e: Exception) {
        }
    }

    private fun setOnClick() {

        textView_no1?.setOnClickListener() {
            addPassword("1")
        }
        textView_no2?.setOnClickListener() {
            addPassword("2")
        }
        textView_no3?.setOnClickListener() {
            addPassword("3")
        }
        textView_no4?.setOnClickListener() {
            addPassword("4")
        }
        textView_no5?.setOnClickListener() {
            addPassword("5")
        }
        textView_no6?.setOnClickListener() {
            addPassword("6")
        }
        textView_no7?.setOnClickListener() {
            addPassword("7")
        }
        textView_no8?.setOnClickListener() {
            addPassword("8")
        }
        textView_no9?.setOnClickListener() {
            addPassword("9")
        }
        textView_no0?.setOnClickListener() {
            addPassword("0")
        }

        imageView_no_delete?.setOnClickListener() {
            if (password_number.length > 0) {
                password_number = password_number.substring(0, password_number.length - 1)
                setIndicatorDot(password_number)
            }
//            Log.d("coke", "password_number " + password_number)
        }


    }


    private fun addPassword(addPassword_str: String?) {
        if (password_number.length < 6) {
            password_number = password_number + addPassword_str
        }
        setIndicatorDot(password_number)
//        Log.d("coke", "password_number " + password_number)
    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        textView_forgot_password = findViewById(R.id.textView_forgot_password) as TextView
        val text_forgot_password_str = "<u>" + resources.getString(R.string.text_forgot_password) + "</u>"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView_forgot_password?.setText(
                Html.fromHtml(text_forgot_password_str, Html.FROM_HTML_MODE_COMPACT)
            )
        } else {
            textView_forgot_password?.setText(
                Html.fromHtml(text_forgot_password_str)
            )
        }
        textView_forgot_password?.visibility = View.VISIBLE
        textView_forgot_password?.setOnClickListener() {
            val intent_go = Intent(this, Page_PasswordForget::class.java)
            startActivity(intent_go)
        }

        textView_forgot_username = findViewById(R.id.textView_forgot_username) as TextView
        val text_forgot_username_str = "<u>" + resources.getString(R.string.text_forgot_username) + "</u>"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView_forgot_username?.setText(
                Html.fromHtml(text_forgot_username_str, Html.FROM_HTML_MODE_COMPACT)
            )
        } else {
            textView_forgot_username?.setText(
                Html.fromHtml(text_forgot_username_str)
            )
        }
        textView_forgot_username?.visibility = View.VISIBLE
        textView_forgot_username?.setOnClickListener() {
            val intent_go = Intent(this, Page_UsernameForget::class.java)
            startActivity(intent_go)
        }

        textView_no1 = findViewById(R.id.textView_no1) as TextView
        textView_no2 = findViewById(R.id.textView_no2) as TextView
        textView_no3 = findViewById(R.id.textView_no3) as TextView
        textView_no4 = findViewById(R.id.textView_no4) as TextView
        textView_no5 = findViewById(R.id.textView_no5) as TextView
        textView_no6 = findViewById(R.id.textView_no6) as TextView
        textView_no7 = findViewById(R.id.textView_no7) as TextView
        textView_no8 = findViewById(R.id.textView_no8) as TextView
        textView_no9 = findViewById(R.id.textView_no9) as TextView
        textView_no0 = findViewById(R.id.textView_no0) as TextView
        imageView_no_delete = findViewById(R.id.imageView_no_delete) as ImageView

        imageView_IndicatorDot_1 = findViewById(R.id.imageView_IndicatorDot_1) as ImageView
        imageView_IndicatorDot_2 = findViewById(R.id.imageView_IndicatorDot_2) as ImageView
        imageView_IndicatorDot_3 = findViewById(R.id.imageView_IndicatorDot_3) as ImageView
        imageView_IndicatorDot_4 = findViewById(R.id.imageView_IndicatorDot_4) as ImageView
        imageView_IndicatorDot_5 = findViewById(R.id.imageView_IndicatorDot_5) as ImageView
        imageView_IndicatorDot_6 = findViewById(R.id.imageView_IndicatorDot_6) as ImageView

    }

    private fun setIndicatorDot(check_number_indicator: String) {

        if (check_number_indicator.length >= 1) {
            imageView_IndicatorDot_1?.isSelected = true
        } else {
            imageView_IndicatorDot_1?.isSelected = false
        }
        if (check_number_indicator.length >= 2) {
            imageView_IndicatorDot_2?.isSelected = true
        } else {
            imageView_IndicatorDot_2?.isSelected = false
        }
        if (check_number_indicator.length >= 3) {
            imageView_IndicatorDot_3?.isSelected = true
        } else {
            imageView_IndicatorDot_3?.isSelected = false
        }
        if (check_number_indicator.length >= 4) {
            imageView_IndicatorDot_4?.isSelected = true
        } else {
            imageView_IndicatorDot_4?.isSelected = false
        }
        if (check_number_indicator.length >= 5) {
            imageView_IndicatorDot_5?.isSelected = true
        } else {
            imageView_IndicatorDot_5?.isSelected = false
        }
        if (check_number_indicator.length >= 6) {
            imageView_IndicatorDot_6?.isSelected = true
        } else {
            imageView_IndicatorDot_6?.isSelected = false
        }

        if (check_number_indicator.length >= 6) {

            var userName_encrypt = ""
            var password_encrypt = ""

            val sharedPreferencesManager = SharedPreferencesManager(this)
            val userObject = sharedPreferencesManager.getUserObject()
            val username = userObject.username

            val cryptLib = CryptLib()
//            userName_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(username, salt_key)
            password_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(check_number_indicator, salt_key)

            val defaultParameterAPI = DefaultParameterAPI(this)
            val formBody = FormBody.Builder()
            formBody.add("username", "" + username)
            formBody.add("password", "" + password_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url,
                api_authLoginByPasscode,
                "api_authLoginByPasscode"
            )

            Log.d("coke", "firebase_Tokens " + firebase_Tokens)
            var firebase_Tokens_encrypt = ""
            firebase_Tokens_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(firebase_Tokens, salt_key)

            var jsonObject = JSONObject()
            jsonObject.put("AccessToken", "" + userObject.token)
            jsonObject.put("RefreshToken", "" + userObject.refresh_token)
            jsonObject.put("FirebaseToken", "" + firebase_Tokens_encrypt)

            var refreshtoken_str = jsonObject.toString()

            var okhttp = OkHttpPost(null, defaultParameterAPI)
            okhttp.setJson("" + refreshtoken_str)
            okhttp.execute(root_url, api_authRefreshtoken, "api_authRefreshtoken_pinlogin")

            progressBackground?.visibility = View.VISIBLE

//            val cryptLib = CryptLib()
//            val passcode_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(check_number_indicator, salt_key)
//
//            val sharedPreferencesManager = SharedPreferencesManager(this)
//            val userObject = sharedPreferencesManager.getUserObject()
//
//            val defaultParameterAPI = DefaultParameterAPI(this)
//            val formBody = FormBody.Builder()
//            formBody.add("passcode", "" + passcode_encrypt)
//            formBody.add("token", "" + userObject.token)
//            OkHttpPost(formBody, defaultParameterAPI).execute(root_url_fitbit, api_authLoginByPin, "api_authLoginByPin")
//
//            progressBackground?.visibility = View.VISIBLE

        }
    }


    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {
        if (event.getHttpName().equals("api_authLoginByPasscode")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    val sharedPreferencesManager = SharedPreferencesManager(this)
                    val userObject = sharedPreferencesManager.getUserObject()
                    if (result != null) {
                        userObject.setLoginPin(result)
                        userObject.state_login = "login"
                        sharedPreferencesManager.setUserObject(userObject)
                        val intent_go = Intent(this, ActivityMain::class.java)
                        intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent_go)

//                        val intent_go = Intent(this@Page_Login, Page_Create_Pin::class.java)
//                        intent_go.putExtra("userObject",userObject)
//                        startActivity(intent_go)
                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_PinLogin, "" + message)

                    check_lock_send_service = false
                    password_number = ""
                    setIndicatorDot(password_number)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(this@Page_PinLogin, resources.getString(R.string.text_service_error))
            }
        }
        if (event.getHttpName().equals("api_authRefreshtoken")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    val sharedPreferencesManager = SharedPreferencesManager(this)
                    val userObject = sharedPreferencesManager.getUserObject()
                    if (result != null) {
                        userObject.setLoginRefreshToken(result)
                        userObject.state_login = "login"
                        sharedPreferencesManager.setUserObject(userObject)
                        val intent_go = Intent(this, ActivityMain::class.java)
                        intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent_go)

//                        val intent_go = Intent(this@Page_Login, Page_Create_Pin::class.java)
//                        intent_go.putExtra("userObject",userObject)
//                        startActivity(intent_go)
                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_PinLogin, "" + message)

                    check_lock_send_service = false
                    password_number = ""
                    setIndicatorDot(password_number)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(this@Page_PinLogin, resources.getString(R.string.text_service_error))
            }
        }

//        if (event.getHttpName().equals("api_authLoginByPin")) {
//            progressBackground?.visibility = View.GONE
//
//            val responseStr = event.responseStr
//            var responseJson: JSONObject? = null
//            try {
//                responseJson = JSONObject(responseStr)
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//            if (responseJson != null) {
//
//                var status: String = ""
//                var message: String = ""
//                var status_code: String = ""
//                try {
//                    status = responseJson.getString("status")
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
//                try {
//                    message = responseJson.getString("errorMessage")
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
//                try {
//                    status_code = responseJson.getString("status_code")
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
//
//                if (status.equals("fix_code", ignoreCase = true)) {
//                } else if (status.equals("true", ignoreCase = true)) {
//
//                    val sharedPreferencesManager = SharedPreferencesManager(this@Page_PinLogin)
//                    val userObject = sharedPreferencesManager.getUserObject()
//                    userObject.state_login = "login"
//                    sharedPreferencesManager.setUserObject(userObject)
//
//                    val intent_go = Intent(this, ActivityMain::class.java)
//                    intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//                    startActivity(intent_go)
//
//                } else {
//                    check_lock_send_service = false
//                    password_number = ""
//                    setIndicatorDot(password_number)
//
//                    val alertDialogUtil = AlertDialogUtil()
//                    alertDialogUtil.showMessageDialog(this, "" + message)
//                }
//            } else {
//                val alertDialogUtil = AlertDialogUtil()
//                alertDialogUtil.showMessageDialog(this, resources.getString(R.string.text_service_error))
//            }
//        }


    }

}