package th.co.digix.tpacare.login

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor

class Page_UsernameForgotOTP : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authForgotUserRequestOTP = httpServerURL.api_authForgotUserRequestOTP
    val api_authForgotUserCheckOTP = httpServerURL.api_authForgotUserCheckOTP
    val salt_key = httpServerURL.salt_key
    
    var linearLayout_root_bg: LinearLayout? = null
    var progressBackground: RelativeLayout? = null

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var textView_popup_title: TextView? = null

    var linearLayout_idcard_form: LinearLayout? = null
    var edittext_idcard_form: EditText? = null

    var textView_idcard_1: TextView? = null
    var textView_idcard_2: TextView? = null
    var textView_idcard_3: TextView? = null
    var textView_idcard_4: TextView? = null
    var textView_idcard_5: TextView? = null
    var textView_idcard_6: TextView? = null

    var otp_code = ""
    var phone = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_username_forget_otp)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorkingOTP()
    }

    private fun initialWorkingOTP() {

        textView_popup_title = findViewById(R.id.textView_popup_title) as TextView
        val textView_card_re_otp = findViewById(R.id.textView_card_re_otp) as TextView
        val linearLayout_card_re_otp = findViewById(R.id.linearLayout_card_re_otp) as LinearLayout
        val linearLayout_popup_ok = findViewById(R.id.linearLayout_popup_ok) as LinearLayout

//        val cryptLib = CryptLib()
//        val phone_decrypt = cryptLib.decryptCipherTextWithRandomIV(phone, salt_key)

        var card_telephone = "" + phone
        try {
            card_telephone =
                card_telephone.substring(0, 3) + "-XXX-X" + card_telephone.substring(
                    7,
                    card_telephone.length
                )
        } catch (e: Exception) {
        }
        textView_popup_title?.setText(resources.getString(R.string.popup_card_otp_title) + " " + card_telephone)


        linearLayout_idcard_form = findViewById(R.id.linearLayout_idcard_form) as LinearLayout
        edittext_idcard_form = findViewById(R.id.edittext_idcard_form) as EditText

        textView_idcard_1 = findViewById(R.id.textView_idcard_1) as TextView
        textView_idcard_2 = findViewById(R.id.textView_idcard_2) as TextView
        textView_idcard_3 = findViewById(R.id.textView_idcard_3) as TextView
        textView_idcard_4 = findViewById(R.id.textView_idcard_4) as TextView
        textView_idcard_5 = findViewById(R.id.textView_idcard_5) as TextView
        textView_idcard_6 = findViewById(R.id.textView_idcard_6) as TextView

        linearLayout_idcard_form?.setOnClickListener {
            if (otp_code.length == 0) {
                textView_idcard_1?.setText("|")
                textView_idcard_1?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
            } else if (otp_code.length == 1) {
                textView_idcard_2?.setText("|")
                textView_idcard_2?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
            } else if (otp_code.length == 2) {
                textView_idcard_3?.setText("|")
                textView_idcard_3?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
            } else if (otp_code.length == 3) {
                textView_idcard_4?.setText("|")
                textView_idcard_4?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
            } else if (otp_code.length == 4) {
                textView_idcard_5?.setText("|")
                textView_idcard_5?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
            } else if (otp_code.length == 5) {
                textView_idcard_6?.setText("|")
                textView_idcard_6?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
            }

            edittext_idcard_form?.requestFocus()

            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = getCurrentFocus()
            if (view == null) {
                view = View(this)
            }
            imm.showSoftInput(view, 0)

        }
        
        edittext_idcard_form?.addTextChangedListener(
            onTextChangedSetControlEdittext(
            )
        )

        try {
            val text_card_add_number = "<u>" + resources.getString(R.string.popup_card_re_otp) + "</u>"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView_card_re_otp?.setText(
                    Html.fromHtml(text_card_add_number, Html.FROM_HTML_MODE_COMPACT)
                )
            } else {
                textView_card_re_otp?.setText(
                    Html.fromHtml(text_card_add_number)
                )
            }
        } catch (e: Exception) {
        }

        linearLayout_card_re_otp?.setOnClickListener(View.OnClickListener {
            var phone_encrypt = ""

            val cryptLib = CryptLib()
            phone_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone, salt_key)

            val defaultParameterAPI = DefaultParameterAPI(this)
            val formBody = FormBody.Builder()
            formBody.add("tel", "" + phone_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_authForgotUserRequestOTP, "api_authForgotUserRequestOTP"
            )

            progressBackground?.visibility = View.VISIBLE

        })

        linearLayout_popup_ok?.setOnClickListener(View.OnClickListener {

            var tel_encrypt = ""
            var idcard_encrypt = ""

            val cryptLib = CryptLib()
            tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone, salt_key)
            idcard_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(otp_code, salt_key)


            val defaultParameterAPI = DefaultParameterAPI(this)
            val formBody = FormBody.Builder()
            formBody.add("otp_code", "" + idcard_encrypt)
            formBody.add("tel", "" + tel_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_authForgotUserCheckOTP, "api_authForgotUserCheckOTP"
            )

            progressBackground?.visibility = View.VISIBLE

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        linearLayout_root_bg = findViewById(R.id.linearLayout_root_bg) as LinearLayout
        linearLayout_root_bg?.setOnClickListener { }
        linearLayout_root_bg?.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                //Find the currently focused view, so we can grab the correct window token from it.
                var view = getCurrentFocus()
                //If no view currently has focus, create a new one, just so we can grab a window token from it
                if (view == null) {
                    view = View(this)
                }
                imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)

                if (otp_code.length == 0) {
                    textView_idcard_1?.setText("")
                    textView_idcard_1?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
                } else if (otp_code.length == 1) {
                    textView_idcard_2?.setText("")
                    textView_idcard_2?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
                } else if (otp_code.length == 2) {
                    textView_idcard_3?.setText("")
                    textView_idcard_3?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
                } else if (otp_code.length == 3) {
                    textView_idcard_4?.setText("")
                    textView_idcard_4?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
                } else if (otp_code.length == 4) {
                    textView_idcard_5?.setText("")
                    textView_idcard_5?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
                } else if (otp_code.length == 5) {
                    textView_idcard_6?.setText("")
                    textView_idcard_6?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_black))
                }
            }

        }
        
    }

    private fun onTextChangedSetControlEdittext(): TextWatcher {

        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                Log.d("coke", "onTextChanged s " + s + " start " + start + " before " + before + " count " + count)

                var textChange = s.toString()
                otp_code = "" + textChange

                var otp_code_1 = ""
                var otp_code_2 = ""
                var otp_code_3 = ""
                var otp_code_4 = ""
                var otp_code_5 = ""
                var otp_code_6 = ""
                try {
                    otp_code_1 = "" + s.get(0)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_2 = "" + s.get(1)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_3 = "" + s.get(2)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_4 = "" + s.get(3)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_5 = "" + s.get(4)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_6 = "" + s.get(5)
                } catch (e: java.lang.Exception) {
                }


                textView_idcard_1?.setText("" + otp_code_1)
                textView_idcard_2?.setText("" + otp_code_2)
                textView_idcard_3?.setText("" + otp_code_3)
                textView_idcard_4?.setText("" + otp_code_4)
                textView_idcard_5?.setText("" + otp_code_5)
                textView_idcard_6?.setText("" + otp_code_6)

                textView_idcard_1?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_blue))
                textView_idcard_2?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_blue))
                textView_idcard_3?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_blue))
                textView_idcard_4?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_blue))
                textView_idcard_5?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_blue))
                textView_idcard_6?.setTextColor(ContextCompat.getColor(this@Page_UsernameForgotOTP, R.color.text_blue))

                if (textChange.length == 0) {
                    textView_idcard_1?.setText("|")
                    textView_idcard_1?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_UsernameForgotOTP,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 1) {
                    textView_idcard_2?.setText("|")
                    textView_idcard_2?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_UsernameForgotOTP,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 2) {
                    textView_idcard_3?.setText("|")
                    textView_idcard_3?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_UsernameForgotOTP,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 3) {
                    textView_idcard_4?.setText("|")
                    textView_idcard_4?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_UsernameForgotOTP,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 4) {
                    textView_idcard_5?.setText("|")
                    textView_idcard_5?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_UsernameForgotOTP,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 5) {
                    textView_idcard_6?.setText("|")
                    textView_idcard_6?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_UsernameForgotOTP,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 6) {
                    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    var view = getCurrentFocus()
                    if (view == null) {
                        view = View(this@Page_UsernameForgotOTP)
                    }
                    imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        }
    }


    private fun initialIntentData() {

        otp_code = intent.extras?.getString("otp_code", "").toString()
        phone = intent.extras?.getString("phone", "").toString()

        if (otp_code.equals("null")) {
            otp_code = ""
        }
        if (phone.equals("null")) {
            phone = ""
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.text_usernameforget_title))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authForgotUserRequestOTP")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {

                    }
                    message = "" + resources.getString(R.string.text_success)
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_UsernameForgotOTP, "" + message)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_UsernameForgotOTP, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_UsernameForgotOTP, resources.getString(R.string.text_service_error)
                )
            }
        }
        if (event.getHttpName().equals("api_authForgotUserCheckOTP")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    var username = ""
                    var username_decode = ""
                    if (result != null) {
                        try {
                            username = result.getString("username")
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        val cryptLib = CryptLib()
                        if(!username.equals("")){
                            username_decode = cryptLib.decryptCipherTextWithRandomIV(username, salt_key)
                        }
                    }
                    val intent_go = Intent(this@Page_UsernameForgotOTP, Page_UsernameForgetResult::class.java)
//                    intent_go.putExtra("otp_code", "" + otp_code)
                    intent_go.putExtra("userName", "" + username_decode)
                    startActivity(intent_go)
                    finish()
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_UsernameForgotOTP, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_UsernameForgotOTP, resources.getString(R.string.text_service_error)
                )
            }
        }

    }

}