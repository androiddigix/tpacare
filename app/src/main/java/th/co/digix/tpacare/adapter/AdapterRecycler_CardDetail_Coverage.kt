package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.SaveDetailObject
import java.text.DecimalFormat


class AdapterRecycler_CardDetail_Coverage(
    val activity: Activity?, val saveDetailObject_List: ArrayList<SaveDetailObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (saveDetailObject_List.size > position) {
            val saveDetailObject = saveDetailObject_List.get(position)

            val covDesc = saveDetailObject.covDesc
            val covNo = saveDetailObject.covNo
            val covLimit = saveDetailObject.covLimit
            val covUtilized = saveDetailObject.covUtilized

            holder.textView_save_detail_no.setText("" + (position + 1))
            holder.textView_save_detail_receive.setText("" + covDesc)

            val mFormat = DecimalFormat("###,###,###,###,###,###")
            var covLimit_show = covLimit
            var cov_limit_long = 0L
            try {
                cov_limit_long = covLimit_show.toLong()
                covLimit_show = "" + mFormat.format(cov_limit_long)
            } catch (e: Exception) {
            }
            if (covLimit_show.equals("")) {
                covLimit_show = "0"
            }
            holder.textView_save_detail_saving.setText("" + covLimit_show)

            var covUtilized_show = covUtilized
            var covUtilized_long = 0L
            try {
                covUtilized_long = covUtilized_show.toLong()
                covUtilized_show = "" + mFormat.format(covUtilized_long)
            } catch (e: Exception) {
            }
            if (covUtilized_show.equals("")) {
                covUtilized_show = "0"
            }
            holder.textView_save_detail_used.setText("" + covUtilized_show)

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_card_detail_coverage, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (saveDetailObject_List != null) {
            var listSize = saveDetailObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textView_save_detail_no: TextView
        internal var textView_save_detail_receive: TextView
        internal var textView_save_detail_saving: TextView
        internal var textView_save_detail_used: TextView

        init {
            textView_save_detail_no = itemView.findViewById(R.id.textView_save_detail_no) as TextView
            textView_save_detail_receive = itemView.findViewById(R.id.textView_save_detail_receive) as TextView
            textView_save_detail_saving = itemView.findViewById(R.id.textView_save_detail_saving) as TextView
            textView_save_detail_used = itemView.findViewById(R.id.textView_save_detail_used) as TextView

        }
    }

}