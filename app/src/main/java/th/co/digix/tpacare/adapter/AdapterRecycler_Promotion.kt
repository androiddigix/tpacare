package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.PromotionObject
import th.co.digix.tpacare.utility.DateTimeHelper
import java.util.ArrayList


class AdapterRecycler_Promotion(
    val activity: Activity?,
    var promotionObject_List: ArrayList<PromotionObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (position < promotionObject_List.size) {
            val promotionObject = promotionObject_List.get(position)

            val promotion_name = "" + promotionObject.title
            var promotion_expire = "" + promotionObject.created_date

            val dateTimeHelper = DateTimeHelper()
            val date_exp = dateTimeHelper.parseStringToDate2(promotion_expire)
            promotion_expire = dateTimeHelper.parseDateToString3(date_exp)


            holder.textView_promotion_name.setText("" + promotion_name)
            holder.textView_promotion_expire.setText("" + promotion_expire)

            if (holder.imageView_promotion != null) {

                val url_image = "" + promotionObject.thumbnail
                Glide.with(activity!!)
                    .load(url_image)
                    .into(holder.imageView_promotion!!)
            }

            holder.linearLayout_bg_item.setOnClickListener {
                //                val intent_go = Intent(activity, Page_Article_Detail::class.java)
//                intent_go.putExtra("articleObject",articleObject)
//                activity.startActivity(intent_go)
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_list_promotion, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (promotionObject_List != null) {
            var listSize = promotionObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var linearLayout_bg_item: LinearLayout
        internal var imageView_promotion: ImageView
        internal var textView_promotion_name: TextView
        internal var textView_promotion_expire: TextView

        init {
            linearLayout_bg_item = itemView.findViewById(R.id.linearLayout_bg_item) as LinearLayout
            imageView_promotion = itemView.findViewById(R.id.imageView_promotion) as ImageView
            textView_promotion_name = itemView.findViewById(R.id.textView_promotion_name) as TextView
            textView_promotion_expire = itemView.findViewById(R.id.textView_promotion_expire) as TextView
        }
    }

}