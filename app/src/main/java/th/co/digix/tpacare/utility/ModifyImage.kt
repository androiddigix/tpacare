package th.co.digix.tpacare.utility

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.provider.MediaStore
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class ModifyImage {

    fun getRealPathFromURI(contentUri: Uri, activity: Activity): String? {
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = activity.getContentResolver().query(contentUri, proj, null, null, null)
        if (cursor.moveToFirst()) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(column_index)
        }
        cursor.close()
        return res
    }

    fun resizeBitmap(bm: Bitmap): Bitmap {

        val bitmapHeight = bm.height
        val bitmapWidth = bm.width

        val bitmapHeightResize: Int
        val bitmapWidthResize: Int

        if (bitmapHeight >= 2160) {
            bitmapHeightResize = bitmapHeight / 6
            bitmapWidthResize = bitmapWidth / 6
        } else if (bitmapHeight >= 1080) {
            bitmapHeightResize = bitmapHeight / 3
            bitmapWidthResize = bitmapWidth / 3
        } else if (bitmapHeight >= 760) {
            bitmapHeightResize = bitmapHeight * 2 / 5
            bitmapWidthResize = bitmapWidth * 2 / 5
        } else if (bitmapHeight >= 300) {
            bitmapHeightResize = bitmapHeight * 2 / 5
            bitmapWidthResize = bitmapWidth * 2 / 5
        } else {
            bitmapHeightResize = bitmapHeight
            bitmapWidthResize = bitmapWidth
        }

        return Bitmap.createScaledBitmap(bm, bitmapWidthResize, bitmapHeightResize, false)
    }

    @Throws(IOException::class)
    fun modifyOrientation(bitmap: Bitmap, image_absolute_path: String): Bitmap {
        val ei = ExifInterface(image_absolute_path)
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> return custom_rotate(bitmap, 90f)

            ExifInterface.ORIENTATION_ROTATE_180 -> return custom_rotate(bitmap, 180f)

            ExifInterface.ORIENTATION_ROTATE_270 -> return custom_rotate(bitmap, 270f)

            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> return custom_flip(bitmap, true, false)

            ExifInterface.ORIENTATION_FLIP_VERTICAL -> return custom_flip(bitmap, false, true)

            else -> return bitmap
        }
    }

    fun custom_rotate(bitmap: Bitmap, degrees: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degrees)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun custom_flip(bitmap: Bitmap, horizontal: Boolean, vertical: Boolean): Bitmap {
        val matrix = Matrix()
        matrix.preScale((if (horizontal) -1 else 1).toFloat(), (if (vertical) -1 else 1).toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun bitMapToFile(yourbitmap: Bitmap, file_path_name: String, activity: Activity): File {
        //create a file to write bitmap data
        var mFile = File(activity.getCacheDir(), file_path_name)
        mFile.createNewFile()

//Convert bitmap to byte array
        var bitmap = yourbitmap
        var bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
        var bitmapdata = bos.toByteArray()

//write the bytes in file
        var fos = FileOutputStream(mFile)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()
        return mFile
    }

}