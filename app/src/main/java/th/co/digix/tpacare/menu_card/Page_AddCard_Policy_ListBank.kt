package th.co.digix.tpacare.menu_card

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import com.pvdproject.adapter.AdapterRecycler_ListBank
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.RecyclerViewListener
import th.co.digix.tpacare.custom_object.ListBankObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.NotificationBarColor
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class Page_AddCard_Policy_ListBank : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_values_bank = httpServerURL.api_values_bank
    val api_values_insurance_company = httpServerURL.api_values_insurance_company
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var progressBackground: RelativeLayout? = null

    var imageView_find_list: ImageView? = null
    var editText_keyword: EditText? = null

    var linearLayout_tab_rigth: LinearLayout? = null
    var linearLayout_tab_left: LinearLayout? = null

    var recyclerView_list_bank: RecyclerView? = null
    var adapterRecycler_ListBank: AdapterRecycler_ListBank? = null

//    var keyword = ""

    var page = 1
    var per_page = 10
    var total_record = 0
    var loadMoreEnable = false

    var isBankList = false
    var name_bank = ""
    var name_companee = ""

    var listBankObject_List = ArrayList<ListBankObject>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_add_card_policy_list_bank)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

        callService(isBankList)
    }

    private fun callService(isBankList :Boolean) {

        val defaultParameterAPI = DefaultParameterAPI(this)
        val formBody = "?page=" + page +
                "&pageSize=" + per_page +
                "&keyword=" + editText_keyword?.text.toString()

        if (isBankList) {
            OkHttpGet(defaultParameterAPI).execute(
                root_url,
                api_values_bank + formBody,
                "api_values_bank"
            )
        } else {
            OkHttpGet(defaultParameterAPI).execute(
                root_url,
                api_values_insurance_company + formBody,
                "api_values_insurance_company"
            )
        }
        progressBackground?.visibility = View.VISIBLE
    }

    private fun callService_changetext(isBankList :Boolean) {

        val defaultParameterAPI = DefaultParameterAPI(this)
        val formBody = "?page=" + page +
                "&pageSize=" + per_page +
                "&keyword=" + editText_keyword?.text.toString()

        if (isBankList) {
            OkHttpGet(defaultParameterAPI).execute(
                root_url,
                api_values_bank + formBody,
                "api_values_bank"
            )
        } else {
            OkHttpGet(defaultParameterAPI).execute(
                root_url,
                api_values_insurance_company + formBody,
                "api_values_insurance_company"
            )
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }


    private fun initialWorking() {
        recyclerView_list_bank?.addOnItemTouchListener(
            RecyclerViewListener(
                this,
                recyclerView_list_bank!!,
                object : RecyclerViewListener.ClickListener {
                    override fun onClick(view: View, position: Int) {

                        if (listBankObject_List.size > position) {

                            val returnIntent = Intent()

                            var name_th = ""
                            var name_en = ""
                            var insurer_code = ""
                            name_th = listBankObject_List.get(position).name_th
                            name_en = listBankObject_List.get(position).name_en

                            insurer_code = listBankObject_List.get(position).insurer_code

                            if (!name_th.equals("") && !name_th.equals("null")) {
                                returnIntent.putExtra("bank_name", name_th)
                            } else {
                                returnIntent.putExtra("bank_name", name_en)
                            }

                            returnIntent.putExtra("insurer_code", insurer_code)
                            returnIntent.putExtra("isBankList", isBankList)
                            setResult(Activity.RESULT_OK, returnIntent)
                            finish()
                        }

                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }

                })
        )


        recyclerView_list_bank?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                //                    Log.d("per", "newState : " + newState);
                val view1 = recyclerView!!.getChildAt(recyclerView.childCount - 1)
                if (view1 != null) {
                    val diff = view1.bottom - (recyclerView.height + recyclerView.scrollY)
                    if (diff <= 0) {
                        if (loadMoreEnable) {
                            loadMoreEnable = false

                            if (total_record >= page * per_page) {
                                page++
                                callService(isBankList)
                            }
                        }
                    } else {
                        loadMoreEnable = true
                    }
                } else {
                    loadMoreEnable = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

    }


    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        imageView_find_list = findViewById(R.id.imageView_find_list) as ImageView
        editText_keyword = findViewById(R.id.editText_keyword) as EditText
        editText_keyword?.addTextChangedListener(onTextChangedSetFormatter(editText_keyword!!))

        linearLayout_tab_rigth = findViewById(R.id.linearLayout_tab_rigth) as LinearLayout
        linearLayout_tab_left = findViewById(R.id.linearLayout_tab_left) as LinearLayout

        recyclerView_list_bank = findViewById(R.id.recyclerView_list_bank) as RecyclerView
        recyclerView_list_bank?.layoutManager =
            LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recyclerView_list_bank?.setNestedScrollingEnabled(false)

        adapterRecycler_ListBank = AdapterRecycler_ListBank(this, listBankObject_List)
        recyclerView_list_bank?.setAdapter(adapterRecycler_ListBank)

        imageView_find_list?.setOnClickListener {
            listBankObject_List.clear()
            page = 1

            callService(isBankList)
        }

        linearLayout_tab_rigth?.setOnClickListener {
            name_bank = ""
            name_companee = ""
            listBankObject_List.clear()
            page = 1
            editText_keyword?.setHint("" + resources.getString(R.string.text_companee))
            isBankList = true
            linearLayout_tab_rigth?.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_blue_ligth))
            linearLayout_tab_left?.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_blue_heavy2))
            editText_keyword?.setText("")
//            callService(isBankList)
        }
        linearLayout_tab_left?.setOnClickListener {
            name_bank = ""
            name_companee = ""
            listBankObject_List.clear()
            page = 1
            editText_keyword?.setHint("" + resources.getString(R.string.text_bank))
            isBankList = false
            linearLayout_tab_rigth?.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_blue_heavy2))
            linearLayout_tab_left?.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_blue_ligth))
            editText_keyword?.setText("")
//            callService(isBankList)

        }
    }

    private fun onTextChangedSetFormatter(editText: EditText): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                listBankObject_List.clear()
                page = 1
                callService_changetext(isBankList)
            }

            override fun afterTextChanged(s: Editable) {
                editText.removeTextChangedListener(this)


                editText.addTextChangedListener(this)
            }
        }
    }


    private fun initialIntentData() {

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_add_card))
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_values_bank")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {

                        for (i in 0..results.length() - 1) {
                            var bank_Json: JSONObject? = null
                            try {
                                bank_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (bank_Json != null) {
                                var fi_name_th = ""
                                var fi_name_en = ""
                                try {
                                    fi_name_th = bank_Json.getString("fi_name_th")
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                try {
                                    fi_name_en = bank_Json.getString("fi_name_en")
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                val listBankObject = ListBankObject()
                                listBankObject.name_th = fi_name_th
                                listBankObject.name_en = fi_name_en
                                listBankObject_List.add(listBankObject)
                            }
                        }
                        adapterRecycler_ListBank?.notifyDataSetChanged()
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_AddCard_Policy_ListBank, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy_ListBank, resources.getString(R.string.text_service_error)
                )
            }
        }
        if (event.getHttpName().equals("api_values_insurance_company")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {

                        for (i in 0..results.length() - 1) {
                            var bank_Json: JSONObject? = null
                            try {
                                bank_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (bank_Json != null) {

                                var name_th = ""
                                var name_en = ""
                                var insurer_code = ""
                                try {
                                    name_th = bank_Json.getString("name_th")
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                try {
                                    name_en = bank_Json.getString("name_en")
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                try {
                                    insurer_code = bank_Json.getString("insurer_code")
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                val listBankObject = ListBankObject()
                                listBankObject.name_th = name_th
                                listBankObject.name_en = name_en
                                listBankObject.insurer_code = insurer_code
                                listBankObject_List.add(listBankObject)
                            }
                        }

                        adapterRecycler_ListBank?.notifyDataSetChanged()
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_AddCard_Policy_ListBank, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_AddCard_Policy_ListBank, resources.getString(R.string.text_service_error)
                )
            }
        }

    }

}