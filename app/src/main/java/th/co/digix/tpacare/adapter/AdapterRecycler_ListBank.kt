package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.ListBankObject
import th.co.digix.tpacare.custom_object.ListOfPolDetObject
import th.co.digix.tpacare.custom_object.SaveDetailObject
import java.util.ArrayList


class AdapterRecycler_ListBank(
    val activity: Activity?, var listBankObject_List: ArrayList<ListBankObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (listBankObject_List.size > 0) {
            var name_th = ""
            var name_en = ""
            name_th = listBankObject_List.get(position).name_th
            name_en = listBankObject_List.get(position).name_en

            if (!name_th.equals("") && !name_th.equals("null")) {
                holder.textView_nameBank?.setText("" + name_th)
            } else {
                holder.textView_nameBank?.setText("" + name_en)
            }

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_list_bank, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (listBankObject_List != null) {
            var listSize = listBankObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textView_nameBank: TextView

        init {
            textView_nameBank = itemView.findViewById(R.id.textView_nameBank) as TextView
        }
    }

}