package th.co.digix.tpacare.menu_home.banner_fagment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.pvdproject.adapter.AdapterRecycler_Article
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.RecyclerViewListener
import th.co.digix.tpacare.custom_object.ArticleObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.menu_home.Page_Fragment_Home
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import java.util.ArrayList

class Page_Fragment_List_Article : Fragment() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_article = httpServerURL.api_article
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var imageView_logo_title: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var rootView: View? = null
    var progressBackground: RelativeLayout? = null

    var recyclerView_list_bank: RecyclerView? = null
    var adapterRecycler_Article: AdapterRecycler_Article? = null

//    var keyword = ""

    var page = 1
    var per_page = 10
    var total_record = 0
    var loadMoreEnable = false

    var isDistance = true
    var name_bank = ""
    var name_companee = ""

    var isRegisterBus = false

    val articleObject_List = ArrayList<ArticleObject>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.page_fragment_list_article, container, false)

        if (!isRegisterBus) {
            BusProvider.getInstance().register(this)
            isRegisterBus = true
        }

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

        callService_article()

        return rootView
    }

    fun callService_article() {

        val defaultParameterAPI = DefaultParameterAPI(activity!!)
        var formBody = "?page=" + "1" +
                "&pageSize=" + per_page

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_article + formBody,
            "api_article"
        )

        progressBackground?.visibility = View.VISIBLE
    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }


    private fun initialWorking() {
        recyclerView_list_bank?.addOnItemTouchListener(
            RecyclerViewListener(
                activity!!,
                recyclerView_list_bank!!,
                object : RecyclerViewListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        if (articleObject_List.size > position) {
                            val articleObject = articleObject_List.get(position)
                            var reportTitle = "" + articleObject.title
                            var currentUrl = "" + articleObject.content

                            val activityMain: ActivityMain = activity as ActivityMain
                            var page_Fragment_Webview_Article =
                                Page_Fragment_Webview_Article()
                            val bundle = Bundle()
                            bundle.putString("reportTitle", "" + reportTitle)
                            bundle.putString("currentUrl", "" + currentUrl)
                            page_Fragment_Webview_Article.setArguments(bundle)
                            activityMain?.addFragment(page_Fragment_Webview_Article)

                            activityMain?.linearLayout_menu_home?.isSelected = true
                            activityMain?.linearLayout_menu_card?.isSelected = false
                            activityMain?.linearLayout_menu_status?.isSelected = false
                            activityMain?.linearLayout_menu_noti?.isSelected = false
                            activityMain?.linearLayout_menu_setting?.isSelected = false

                            activityMain?.page_select_temp = 95

//                            val intent_go = Intent(activity, Page_WebView_Article::class.java)
//                            intent_go.putExtra("reportTitle", reportTitle)
//                            intent_go.putExtra("currentUrl", currentUrl)
//                            startActivity(intent_go)
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }

                })
        )


        recyclerView_list_bank?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                //                    Log.d("per", "newState : " + newState);
                val view1 = recyclerView!!.getChildAt(recyclerView.childCount - 1)
                if (view1 != null) {
                    val diff = view1.bottom - (recyclerView.height + recyclerView.scrollY)
                    if (diff <= 0) {
                        if (loadMoreEnable) {
                            loadMoreEnable = false

                            if (total_record >= page * per_page) {
                                page++
                                callService_article()
                            }
                        }
                    } else {
                        loadMoreEnable = true
                    }
                } else {
                    loadMoreEnable = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

    }


    private fun initialView() {

        progressBackground = rootView?.findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        recyclerView_list_bank = rootView?.findViewById(R.id.recyclerView_list_bank) as RecyclerView
        recyclerView_list_bank?.layoutManager =
            LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        recyclerView_list_bank?.setNestedScrollingEnabled(false)

        adapterRecycler_Article = AdapterRecycler_Article(activity, articleObject_List)
        recyclerView_list_bank?.setAdapter(adapterRecycler_Article)

    }

    private fun initialIntentData() {

    }


    private fun initInstanceToolbar() {

        imageView_logo_title = rootView?.findViewById(R.id.imageView_logo_title) as ImageView
        imageView_logo_title?.visibility = View.GONE

        textView_toolbar_title = rootView?.findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_article))

        btnBack = rootView?.findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.replaceFragment(Page_Fragment_Home())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 0
        }
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_article")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {

                        for (i in 0..results.length() - 1) {
                            var article_Json: JSONObject? = null
                            try {
                                article_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (article_Json != null) {
                                val articleObject = ArticleObject()
                                articleObject.setParam(article_Json)
                                articleObject_List.add(articleObject)
                            }

                        }
                        adapterRecycler_Article?.notifyDataSetChanged()
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    activity!!, resources.getString(R.string.text_service_error)
                )
            }
        }
    }

}
