package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONArray
import org.json.JSONObject

class AddCardObject() : Parcelable {
    var name: String = ""
    var surname: String = ""
    var insured_type: String = ""
    var card_type: String = ""
    var insurer_code: String = ""
    var insurer_name: String = ""
    var insurer_dedicateed_line: String = ""
    var company_name: String = ""
    var staff_no: String = ""
    var effective_date: String = ""
    var expire_date: String = ""
    var pol_no: String = ""
    var tel_no: String = ""
    var coverage_list = JSONArray()
    var coverage_list_str: String = ""

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        surname = parcel.readString()
        insured_type = parcel.readString()
        card_type = parcel.readString()
        insurer_code = parcel.readString()
        insurer_name = parcel.readString()
        insurer_dedicateed_line = parcel.readString()
        company_name = parcel.readString()
        staff_no = parcel.readString()
        effective_date = parcel.readString()
        expire_date = parcel.readString()
        pol_no = parcel.readString()
        tel_no = parcel.readString()
        coverage_list_str = parcel.readString()
    }

    fun toJsonEdit(): JSONObject {
        var jsonObject = JSONObject()
        try {
            jsonObject.put("name", name)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("surname", surname)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("insured_type", insured_type)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("card_type", card_type)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("insurer_code", insurer_code)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("insurer_name", insurer_name)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("insurer_dedicateed_line", insurer_dedicateed_line)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("company_name", company_name)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("staff_no", staff_no)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("effective_date", effective_date)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("expire_date", expire_date)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("pol_no", pol_no)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("tel_no", tel_no)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            jsonObject.put("coverage_list", coverage_list)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return jsonObject
    }

//    fun setParam(result: JSONObject) {
//        try {
//            name = result.getString("name")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            surname = result.getString("surname")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            insured_type = result.getString("insured_type")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            card_type = result.getString("card_type")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            insurer_code = result.getString("insurer_code")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            insurer_name = result.getString("insurer_name")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            insurer_dedicateed_line = result.getString("insurer_dedicateed_line")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            company_name = result.getString("company_name")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            staff_no = result.getString("staff_no")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            effective_date = result.getString("effective_date")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            expire_date = result.getString("expire_date")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            pol_no = result.getString("pol_no")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            tel_no = result.getString("tel_no")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        try {
//            coverage_list = result.getString("coverage_list")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(surname)
        parcel.writeString(insured_type)
        parcel.writeString(card_type)
        parcel.writeString(insurer_code)
        parcel.writeString(insurer_name)
        parcel.writeString(insurer_dedicateed_line)
        parcel.writeString(company_name)
        parcel.writeString(staff_no)
        parcel.writeString(effective_date)
        parcel.writeString(expire_date)
        parcel.writeString(pol_no)
        parcel.writeString(tel_no)
        parcel.writeString(coverage_list_str)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AddCardObject> {
        override fun createFromParcel(parcel: Parcel): AddCardObject {
            return AddCardObject(parcel)
        }

        override fun newArray(size: Int): Array<AddCardObject?> {
            return arrayOfNulls(size)
        }
    }

}