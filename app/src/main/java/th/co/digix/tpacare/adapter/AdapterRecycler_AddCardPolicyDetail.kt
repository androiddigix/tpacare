package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.CoverageObject
import th.co.digix.tpacare.custom_object.ListOfPolDetObject
import th.co.digix.tpacare.custom_object.SaveDetailObject
import java.text.DecimalFormat
import java.util.ArrayList


class AdapterRecycler_AddCardPolicyDetail(
    val activity: Activity?, var coverageObject_list: ArrayList<CoverageObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (coverageObject_list.size > 0) {
            val coverageObject = coverageObject_list.get(position)

            val mFormat = DecimalFormat("###,###,##0")
            var cov_limit_show = ""
            try {
                val cov_limit_long = coverageObject.cov_limit.toLong()
                cov_limit_show = "" + mFormat.format(cov_limit_long)
            } catch (e: Exception) {
            }

            holder.textView_cov_desc?.setText("" + coverageObject.cov_desc)
            holder.editText_cov_limit?.setText("" + cov_limit_show)
            holder.imageView_delete_policy_detail?.setOnClickListener {
                coverageObject_list.remove(coverageObject)
                notifyDataSetChanged()
            }

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_add_card_policy_detail, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (coverageObject_list != null) {
            var listSize = coverageObject_list.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textView_cov_desc: TextView
        internal var editText_cov_limit: EditText
        internal var imageView_delete_policy_detail: ImageView

        init {
            textView_cov_desc = itemView.findViewById(R.id.textView_cov_desc) as TextView
            editText_cov_limit = itemView.findViewById(R.id.editText_cov_limit) as EditText
            imageView_delete_policy_detail = itemView.findViewById(R.id.imageView_delete_policy_detail) as ImageView
        }
    }

}