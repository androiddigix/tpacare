package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.menu_status.Page_Fragment_Status
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.MenuHomeObject
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.menu_card.Page_Fragment_Card
import th.co.digix.tpacare.menu_card.Page_Fragment_FindMySave
import th.co.digix.tpacare.menu_fitme.Authenication.AuthenticationManager
import th.co.digix.tpacare.menu_fitme.Authenication.Fitbit_Token
import th.co.digix.tpacare.menu_fitme.Page_Fragment_Fitme
import th.co.digix.tpacare.menu_home.Page_Fragment_Webview_Diet
import th.co.digix.tpacare.menu_home.Page_Fragment_Webview_Health
import th.co.digix.tpacare.menu_home.search_hospital.Page_SearchHospital_Map
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.SharedPreferencesManager


class AdapterRecycler_MenuHome(
    val activity: Activity?, val menuHomeObject_List: ArrayList<MenuHomeObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (menuHomeObject_List.size > position) {

            val menuHomeObject = menuHomeObject_List.get(position)

            holder.textView_menu_home.setText("" + menuHomeObject.menu_name)
            holder.linearLayout_menu_home.setOnClickListener {

                if (menuHomeObject.menu_name.equals(activity!!.resources.getString(R.string.menu_name_my_save))) {
                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    if (!userObject.state_login.equals("login")) {
                        val alertDialogUtil = AlertDialogUtil()
                        alertDialogUtil.show_alertDialog_lnot_login(activity!!)
                    } else {

                        val activityMain: ActivityMain = activity as ActivityMain

                        activityMain?.linearLayout_menu_home?.isSelected = false
                        activityMain?.linearLayout_menu_card?.isSelected = true
                        activityMain?.linearLayout_menu_status?.isSelected = false
                        activityMain?.linearLayout_menu_noti?.isSelected = false
                        activityMain?.linearLayout_menu_setting?.isSelected = false

                        activityMain?.page_select_temp = 1

                        val citizen_id = userObject.citizen_id
                        if (citizen_id.equals("")) {
                            var page_Fragment_FindMySave =
                                Page_Fragment_FindMySave()
                            val bundle = Bundle()
                            bundle.putString("insured_type", "Member")
                            page_Fragment_FindMySave.setArguments(bundle)
                            activityMain?.replaceFragment(page_Fragment_FindMySave)
                        } else {
                            val httpServerURL = HttpServerURL()
                            val salt_key = httpServerURL.salt_key
                            val cryptLib = CryptLib()
                            val citizen_id_encrypt = cryptLib.decryptCipherTextWithRandomIV(citizen_id, salt_key)

                            if (citizen_id_encrypt.equals("")) {
                                var page_Fragment_FindMySave =
                                    Page_Fragment_FindMySave()
                                val bundle = Bundle()
                                bundle.putString("insured_type", "Member")
                                page_Fragment_FindMySave.setArguments(bundle)
                                activityMain?.replaceFragment(page_Fragment_FindMySave)
                            } else {
                                var page_Fragment_Card = Page_Fragment_Card()
                                val bundle = Bundle()
                                bundle.putString("insured_type", "Member")
                                page_Fragment_Card.setArguments(bundle)
                                activityMain?.replaceFragment(page_Fragment_Card)
                            }

                        }

                        activityMain?.page_select_temp = 1
                    }
                } else if (menuHomeObject.menu_name.equals(activity!!.resources.getString(R.string.menu_name_myfamily_save))) {
                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    if (!userObject.state_login.equals("login")) {
                        val alertDialogUtil = AlertDialogUtil()
                        alertDialogUtil.show_alertDialog_lnot_login(activity!!)
                    } else {

                        val activityMain: ActivityMain = activity as ActivityMain

                        activityMain?.linearLayout_menu_home?.isSelected = false
                        activityMain?.linearLayout_menu_card?.isSelected = true
                        activityMain?.linearLayout_menu_status?.isSelected = false
                        activityMain?.linearLayout_menu_noti?.isSelected = false
                        activityMain?.linearLayout_menu_setting?.isSelected = false

                        activityMain?.page_select_temp = 1

                        val citizen_id = userObject.citizen_id
                        if (citizen_id.equals("")) {
                            var page_Fragment_FindMySave =
                                Page_Fragment_FindMySave()
                            val bundle = Bundle()
                            bundle.putString("insured_type", "Family")
                            page_Fragment_FindMySave.setArguments(bundle)
                            activityMain?.replaceFragment(page_Fragment_FindMySave)
                        } else {
                            var page_Fragment_Card = Page_Fragment_Card()
                            val bundle = Bundle()
                            bundle.putString("insured_type", "Family")
                            page_Fragment_Card.setArguments(bundle)
                            activityMain?.replaceFragment(page_Fragment_Card)
                        }

                        activity?.page_select_temp = 1
                    }
                } else if (menuHomeObject.menu_name.equals(activity!!.resources.getString(R.string.menu_name_claim_status))) {
                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    if (!userObject.state_login.equals("login")) {
                        val alertDialogUtil = AlertDialogUtil()
                        alertDialogUtil.show_alertDialog_lnot_login(activity!!)
                    } else {

                        val activity: ActivityMain = activity as ActivityMain

                        activity?.linearLayout_menu_home?.isSelected = false
                        activity?.linearLayout_menu_card?.isSelected = false
                        activity?.linearLayout_menu_status?.isSelected = true
                        activity?.linearLayout_menu_noti?.isSelected = false
                        activity?.linearLayout_menu_setting?.isSelected = false

                        activity?.replaceFragment(Page_Fragment_Status())

                        activity?.page_select_temp = 2
                    }
                } else if (menuHomeObject.menu_name.equals(activity!!.resources.getString(R.string.menu_name_find_hospital))) {
                    val intent_go = Intent(activity, Page_SearchHospital_Map::class.java)
                    activity.startActivity(intent_go)

                }else if (menuHomeObject.menu_name.equals(activity!!.resources.getString(R.string.menu_name_fitme))) {
                    val activityMain: ActivityMain = activity as ActivityMain
                    if (Fitbit_Token.fitbit_token != null) {
                        var page_Fragment_fitme = Page_Fragment_Fitme()
                        activityMain.replaceFragment(page_Fragment_fitme)
                    }
                    else{
                        AuthenticationManager.login(activityMain)
                    }
                    activityMain?.page_select_temp = 100
                } else if (menuHomeObject.menu_name.equals(activity!!.resources.getString(R.string.menu_name_diet))) {
                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    if (!userObject.state_login.equals("login")) {
                        val alertDialogUtil = AlertDialogUtil()
                        alertDialogUtil.show_alertDialog_lnot_login(activity!!)
                    } else {
                        val activityMain: ActivityMain = activity as ActivityMain
                        val httpServerURL = HttpServerURL()
                        var root_url = httpServerURL.host_server
                        val api_content_diet = httpServerURL.api_content_diet
                        val reportTitle = "" + activity?.resources.getString(R.string.menu_name_diet)
                        val currentUrl = "" + root_url + api_content_diet + "?username=" + userObject.username
                        var page_Fragment_Webview_Diet =
                            Page_Fragment_Webview_Diet()
                        val bundle = Bundle()
                        bundle.putString("reportTitle", reportTitle)
                        bundle.putString("currentUrl", currentUrl)
                        page_Fragment_Webview_Diet.setArguments(bundle)
                        activityMain?.replaceFragment(page_Fragment_Webview_Diet)

                        activityMain?.linearLayout_menu_home?.isSelected = true
                        activityMain?.linearLayout_menu_card?.isSelected = false
                        activityMain?.linearLayout_menu_status?.isSelected = false
                        activityMain?.linearLayout_menu_noti?.isSelected = false
                        activityMain?.linearLayout_menu_setting?.isSelected = false

                        activityMain?.page_select_temp = 98
                    }
                } else if (menuHomeObject.menu_name.equals(activity!!.resources.getString(R.string.menu_name_health))) {
                    val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                    val userObject = sharedPreferencesManager.getUserObject()
                    if (!userObject.state_login.equals("login")) {
                        val alertDialogUtil = AlertDialogUtil()
                        alertDialogUtil.show_alertDialog_lnot_login(activity!!)
                    } else {
                        val activityMain: ActivityMain = activity as ActivityMain
                        val httpServerURL = HttpServerURL()
                        var root_url = httpServerURL.host_server
                        val api_content_health = httpServerURL.api_content_health
                        val reportTitle = "" + activity?.resources.getString(R.string.menu_name_health)
                        val currentUrl = "" + root_url + api_content_health+ "?username=" + userObject.username
//                        val currentUrl = "http://128.199.72.215:8080/"
                        var page_Fragment_Webview_Health =
                            Page_Fragment_Webview_Health()
                        val bundle = Bundle()
                        bundle.putString("reportTitle", reportTitle)
                        bundle.putString("currentUrl", currentUrl)
                        page_Fragment_Webview_Health.setArguments(bundle)
                        activityMain?.replaceFragment(page_Fragment_Webview_Health)

                        activityMain?.linearLayout_menu_home?.isSelected = true
                        activityMain?.linearLayout_menu_card?.isSelected = false
                        activityMain?.linearLayout_menu_status?.isSelected = false
                        activityMain?.linearLayout_menu_noti?.isSelected = false
                        activityMain?.linearLayout_menu_setting?.isSelected = false

                        activityMain?.page_select_temp = 99
                    }
                }
            }
            val menu_image = menuHomeObject.menu_image
            if (!menu_image.equals("")) {
                if (activity != null) {
                    Glide.with(activity)
                        .load(menu_image)
                        .into(viewHolder.imageView_menu_home)
                }
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_menu_home, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (menuHomeObject_List != null) {
            var listSize = menuHomeObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var linearLayout_menu_home: LinearLayout
        internal var textView_menu_home: TextView
        internal var imageView_menu_home: ImageView

        init {
            linearLayout_menu_home = itemView.findViewById(R.id.linearLayout_menu_home) as LinearLayout
            textView_menu_home = itemView.findViewById(R.id.textView_menu_home) as TextView
            imageView_menu_home = itemView.findViewById(R.id.imageView_menu_home) as ImageView
        }
    }

}