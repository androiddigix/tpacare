package th.co.digix.tpacare.menu_home.banner_activity_notuse

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import com.pvdproject.adapter.AdapterRecycler_Promotion
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.RecyclerViewListener
import th.co.digix.tpacare.custom_object.PromotionObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.NotificationBarColor
import java.util.*

class Page_PromotionList : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_promotion = httpServerURL.api_promotion
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null
    var imageView_search_top: ImageView? = null

    var progressBackground: RelativeLayout? = null

    var recyclerView_list_bank: RecyclerView? = null
    var adapterRecycler_Promotion: AdapterRecycler_Promotion? = null

//    var keyword = ""

    var page = 1
    var per_page = 10
    var total_record = 0
    var loadMoreEnable = false

    var isDistance = true
    var name_bank = ""
    var name_companee = ""

    val promotionObject_List = ArrayList<PromotionObject>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_promotion)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

        callService_article()
    }

    fun callService_article() {

        val defaultParameterAPI = DefaultParameterAPI(this)
        var formBody = "?page=" + "1" +
                "&pageSize=" + per_page

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_promotion + formBody,
            "api_promotion"
        )

        progressBackground?.visibility = View.VISIBLE
    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }


    private fun initialWorking() {
        recyclerView_list_bank?.addOnItemTouchListener(
            RecyclerViewListener(
                this,
                recyclerView_list_bank!!,
                object : RecyclerViewListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        if (promotionObject_List.size > position) {
                            val promotionObject = promotionObject_List.get(position)
                            var reportTitle = "" + promotionObject.title
                            var currentUrl = "" + promotionObject.content
                            val intent_go = Intent(this@Page_PromotionList, Page_WebView_Promotion::class.java)
                            intent_go.putExtra("reportTitle", reportTitle)
                            intent_go.putExtra("currentUrl", currentUrl)
                            startActivity(intent_go)
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }

                })
        )


        recyclerView_list_bank?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                //                    Log.d("per", "newState : " + newState);
                val view1 = recyclerView!!.getChildAt(recyclerView.childCount - 1)
                if (view1 != null) {
                    val diff = view1.bottom - (recyclerView.height + recyclerView.scrollY)
                    if (diff <= 0) {
                        if (loadMoreEnable) {
                            loadMoreEnable = false

                            if (total_record >= page * per_page) {
                                page++
                                callService_article()
                            }
                        }
                    } else {
                        loadMoreEnable = true
                    }
                } else {
                    loadMoreEnable = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

    }


    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        recyclerView_list_bank = findViewById(R.id.recyclerView_list_bank) as RecyclerView
        recyclerView_list_bank?.layoutManager =
            LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recyclerView_list_bank?.setNestedScrollingEnabled(false)

        adapterRecycler_Promotion = AdapterRecycler_Promotion(this, promotionObject_List)
        recyclerView_list_bank?.setAdapter(adapterRecycler_Promotion)

    }

    private fun initialIntentData() {

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_promotion))


        imageView_search_top = findViewById(R.id.imageView_search_top) as ImageView
        imageView_search_top?.visibility = View.GONE
        imageView_search_top?.setOnClickListener {

        }

    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_promotion")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {

                        for (i in 0..results.length() - 1) {
                            var promotion_Json: JSONObject? = null
                            try {
                                promotion_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (promotion_Json != null) {
                                val promotionObject = PromotionObject()
                                promotionObject.setParam(promotion_Json)
                                promotionObject_List.add(promotionObject)
                            }

                        }
                        adapterRecycler_Promotion?.notifyDataSetChanged()
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_PromotionList, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_PromotionList, resources.getString(R.string.text_service_error)
                )
            }
        }
    }

}