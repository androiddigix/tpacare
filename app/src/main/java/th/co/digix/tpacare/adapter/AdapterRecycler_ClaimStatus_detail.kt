package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.*
import java.util.ArrayList


class AdapterRecycler_ClaimStatus_detail(
    val activity: Activity?, val statusCodsSubObject_List: ArrayList<ClaimStatusSubObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (statusCodsSubObject_List.size > 0) {
            val statusCodsSubObject = statusCodsSubObject_List.get(position)


            var last_status = "" + statusCodsSubObject.status_name


            holder.textView_claim_status.setText("" + last_status)

            if (statusCodsSubObject.status_condition.equals("Done", ignoreCase = true)) {
                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_done)
            }else  if (statusCodsSubObject.status_condition.equals("Completed", ignoreCase = true)) {
                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_accept)
            }else{
                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_none)
            }

            if (statusCodsSubObject_List.size == 1) {
                holder.view_connect_line_up.visibility = View.GONE
                holder.view_connect_line_down.visibility = View.GONE
            } else {
                if (position == 0) {
                    holder.view_connect_line_up.visibility = View.GONE
                    holder.view_connect_line_down.visibility = View.VISIBLE
                } else if (position == statusCodsSubObject_List.size - 1) {
                    holder.view_connect_line_up.visibility = View.VISIBLE
                    holder.view_connect_line_down.visibility = View.GONE
                } else{
                    holder.view_connect_line_up.visibility = View.VISIBLE
                    holder.view_connect_line_down.visibility = View.VISIBLE
                }
            }

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_claim_status_detail, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (statusCodsSubObject_List != null) {
            var listSize = statusCodsSubObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textView_claim_status: TextView
        internal var imageView_claim_status: ImageView
        internal var view_connect_line_up: View
        internal var view_connect_line_down: View

        init {
            textView_claim_status = itemView.findViewById(R.id.textView_claim_status) as TextView
            imageView_claim_status = itemView.findViewById(R.id.imageView_claim_status) as ImageView
            view_connect_line_up = itemView.findViewById(R.id.view_connect_line_up)
            view_connect_line_down = itemView.findViewById(R.id.view_connect_line_down)
        }
    }

}