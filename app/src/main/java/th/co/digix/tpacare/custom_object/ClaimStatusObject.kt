package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class ClaimStatusObject() : Parcelable {

    var pol_no: String = ""
    var visitDate: String = ""
    var card_type: String = ""
    var card_name: String = ""
    var insurer_code: String = ""
    var insurer_name: String = ""
    var claim_no: String = ""
    var claim_status: String = ""

    constructor(parcel: Parcel) : this() {
        pol_no = parcel.readString()
        visitDate = parcel.readString()
        card_type = parcel.readString()
        card_name = parcel.readString()
        insurer_code = parcel.readString()
        insurer_name = parcel.readString()
        claim_no = parcel.readString()
        claim_status = parcel.readString()
    }

    fun setParam(result: JSONObject) {
        try {
            pol_no = result.getString("polNo")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            visitDate = result.getString("visitDate")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            card_type = result.getString("cardType")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            card_name = result.getString("cardName")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            insurer_code = result.getString("insurerCode")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            insurer_name = result.getString("insurerName")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            claim_no = result.getString("claimNo")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            claim_status = result.getString("claimStatus")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(pol_no)
        parcel.writeString(visitDate)
        parcel.writeString(card_type)
        parcel.writeString(card_name)
        parcel.writeString(insurer_code)
        parcel.writeString(insurer_name)
        parcel.writeString(claim_no)
        parcel.writeString(claim_status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ClaimStatusObject> {
        override fun createFromParcel(parcel: Parcel): ClaimStatusObject {
            return ClaimStatusObject(parcel)
        }

        override fun newArray(size: Int): Array<ClaimStatusObject?> {
            return arrayOfNulls(size)
        }
    }
}