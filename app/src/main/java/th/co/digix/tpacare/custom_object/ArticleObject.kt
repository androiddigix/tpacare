package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class ArticleObject() : Parcelable {

    var type: String = ""
    var thumbnail: String = ""
    var title: String = ""
    var content_description: String = ""
    var content: String = ""
    var created_date: String = ""

    constructor(parcel: Parcel) : this() {
        type = parcel.readString()
        thumbnail = parcel.readString()
        title = parcel.readString()
        content_description = parcel.readString()
        content = parcel.readString()
        created_date = parcel.readString()
    }

    fun setParam(result: JSONObject) {
        try {
            type = result.getString("type")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            thumbnail = result.getString("thumbnail")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            title = result.getString("title")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            content_description = result.getString("content_description")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            content = result.getString("content")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            created_date = result.getString("created_date")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeString(thumbnail)
        parcel.writeString(title)
        parcel.writeString(content_description)
        parcel.writeString(content)
        parcel.writeString(created_date)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArticleObject> {
        override fun createFromParcel(parcel: Parcel): ArticleObject {
            return ArticleObject(parcel)
        }

        override fun newArray(size: Int): Array<ArticleObject?> {
            return arrayOfNulls(size)
        }
    }

}