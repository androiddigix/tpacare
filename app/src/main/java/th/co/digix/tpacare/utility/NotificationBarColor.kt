package th.co.digix.tpacare.utility

import android.app.Activity
import android.os.Build
import android.support.v4.content.ContextCompat
import android.view.WindowManager
import th.co.digix.tpacare.R

class NotificationBarColor {

    fun setStatusBarGradient(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = activity.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(activity, R.color.transparent)
            window.navigationBarColor = ContextCompat.getColor(activity, R.color.transparent)
            window.setBackgroundDrawableResource(R.drawable.bg_bar_gradient)
        }
    }
}