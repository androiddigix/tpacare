package th.co.digix.tpacare.menu_home.search_hospital

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import com.pvdproject.adapter.AdapterRecycler_SearchHospital
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.RecyclerViewListener
import th.co.digix.tpacare.custom_object.HospitalObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CallLatLng
import th.co.digix.tpacare.utility.NotificationBarColor
import java.util.*

class Page_SearchHospital : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_values_hospital = httpServerURL.api_values_hospital
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null
    var imageView_search_top: ImageView? = null

    var progressBackground: RelativeLayout? = null


    var linearLayout_tab_1st: LinearLayout? = null
    var linearLayout_tab_2nt: LinearLayout? = null

    var recyclerView_list_bank: RecyclerView? = null
    var adapterRecycler_SearchHospital: AdapterRecycler_SearchHospital? = null

//    var keyword = ""

    var page = 1
    var per_page = 10
    var total_record = 0
    var loadMoreEnable = false

    var isDistance = true
    var name_bank = ""
    var name_companee = ""

    val hospitalObject_List = ArrayList<HospitalObject>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_search_hospital)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

        callService_hospital()
    }

    fun callService_hospital() {

        val callLatLng = CallLatLng(this)
        val location = callLatLng.getLocation_checkGPS()
        var latitudeSTR = "0"
        var longitudeSTR = "0"

        if (location != null) {
            val latitude = location!!.getLatitude()
            val longitude = location!!.getLongitude()
            Log.d("android latitude", "is $latitude")
            Log.d("android longitude", "is $longitude")
            latitudeSTR = "" + latitude
            longitudeSTR = "" + longitude
        }

        val defaultParameterAPI = DefaultParameterAPI(this)
        var formBody = "?page=" + page +
                "&pageSize=" + per_page +
                "&keyword=" + "" +
                "&sLat=" + latitudeSTR +
                "&sLng=" + longitudeSTR


        if (isDistance) {
            formBody = formBody + "&order_by=" + "distance"
        } else {
            formBody = formBody + "&order_by=" + "name"
        }

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_values_hospital + formBody,
            "api_values_hospital_search"
        )

        progressBackground?.visibility = View.VISIBLE
    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }


    private fun initialWorking() {
        recyclerView_list_bank?.addOnItemTouchListener(
            RecyclerViewListener(
                this,
                recyclerView_list_bank!!,
                object : RecyclerViewListener.ClickListener {
                    override fun onClick(view: View, position: Int) {

                        if (hospitalObject_List.size > position) {


                        }

                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }

                })
        )


        recyclerView_list_bank?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                //                    Log.d("per", "newState : " + newState);
                val view1 = recyclerView!!.getChildAt(recyclerView.childCount - 1)
                if (view1 != null) {
                    val diff = view1.bottom - (recyclerView.height + recyclerView.scrollY)
                    if (diff <= 0) {
                        if (loadMoreEnable) {
                            loadMoreEnable = false

                            if (total_record >= page * per_page) {
                                page++
                                callService_hospital()
                            }
                        }
                    } else {
                        loadMoreEnable = true
                    }
                } else {
                    loadMoreEnable = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })

    }


    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }


        linearLayout_tab_1st = findViewById(R.id.linearLayout_tab_rigth) as LinearLayout
        linearLayout_tab_2nt = findViewById(R.id.linearLayout_tab_left) as LinearLayout

        recyclerView_list_bank = findViewById(R.id.recyclerView_list_bank) as RecyclerView
        recyclerView_list_bank?.layoutManager =
            LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recyclerView_list_bank?.setNestedScrollingEnabled(false)

        adapterRecycler_SearchHospital = AdapterRecycler_SearchHospital(this, hospitalObject_List)
        recyclerView_list_bank?.setAdapter(adapterRecycler_SearchHospital)


        linearLayout_tab_1st?.setOnClickListener {
            name_bank = ""
            name_companee = ""
            hospitalObject_List.clear()
            page = 1

            isDistance = true
            linearLayout_tab_1st?.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.bg_blue_ligth
                )
            )
            linearLayout_tab_2nt?.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.bg_blue_heavy2
                )
            )
            callService_hospital()
        }
        linearLayout_tab_2nt?.setOnClickListener {
            name_bank = ""
            name_companee = ""
            hospitalObject_List.clear()
            page = 1

            isDistance = false
            linearLayout_tab_1st?.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.bg_blue_heavy2
                )
            )
            linearLayout_tab_2nt?.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.bg_blue_ligth
                )
            )
            callService_hospital()
        }
    }

    private fun initialIntentData() {

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_search_hospital))


        imageView_search_top = findViewById(R.id.imageView_search_top) as ImageView
        imageView_search_top?.visibility = View.VISIBLE
        imageView_search_top?.setOnClickListener {

            val intent_go = Intent(this@Page_SearchHospital, Page_SearchHospital_Map::class.java)
//            intent_go.putExtra("hospitalObject",hospitalObject)
            startActivity(intent_go)
        }

    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_values_hospital_search")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {

                        for (i in 0..results.length() - 1) {
                            var hospital_Json: JSONObject? = null
                            try {
                                hospital_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (hospital_Json != null) {
                                val hospitalObject = HospitalObject()
                                hospitalObject.setParam(hospital_Json)
                                hospitalObject_List.add(hospitalObject)
                            }

                        }
                        adapterRecycler_SearchHospital?.notifyDataSetChanged()
                    }

                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_SearchHospital, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_SearchHospital, resources.getString(R.string.text_service_error)
                )
            }
        }
    }

}