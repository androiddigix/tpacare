package th.co.digix.tpacare.utility

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import okhttp3.FormBody
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.login.Page_Login
import th.co.digix.tpacare.custom_object.UserObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.login.Page_PinLogin


class LogoutManage {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authLogout = httpServerURL.api_authLogout
    val salt_key = httpServerURL.salt_key

    fun logout_6min(activity: Activity) {

        val sharedPreferencesManager = SharedPreferencesManager(activity)
        val userObject = sharedPreferencesManager.getUserObject()
        val state_login = userObject.state_login
        if (state_login.equals("login")) {
            userObject.state_login = "logout"
            sharedPreferencesManager.setUserObject(userObject)
        }

        val message = "" +activity.resources.getString(R.string.dialog_error_6min)
        showMessageDialog_6min(activity,message)

    }

    fun logout_mainmenu(activity: Activity) {

        val sharedPreferencesManager = SharedPreferencesManager(activity)
        val userObject = UserObject()
        sharedPreferencesManager.setUserObject(userObject)

        val intent_go = Intent(activity, ActivityMain::class.java)
        intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity.startActivity(intent_go)

        callservice_logout(activity)
    }

    fun logout_401(activity: Activity) {
        val sharedPreferencesManager = SharedPreferencesManager(activity)
        val userObject = UserObject()
        sharedPreferencesManager.setUserObject(userObject)

        val intent_go = Intent(activity, ActivityMain::class.java)
        intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity.startActivity(intent_go)

        callservice_logout(activity)
    }


    fun callservice_logout(activity: Activity) {

        val sharedPreferencesManager = SharedPreferencesManager(activity)
        val userObject = sharedPreferencesManager.getUserObject()

        val refresh_token = userObject.refresh_token

        val defaultParameterAPI = DefaultParameterAPI(activity)
        val formBody = FormBody.Builder()
        formBody.add("refreshtoken", "" + refresh_token)
        OkHttpPost(formBody, defaultParameterAPI).execute(root_url, api_authLogout, "api_authLogout")

    }

    fun showMessageDialog_6min(activity: Activity, message: String) {
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_message, null)

        val textView_popup_message = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_message) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_message.setText("" + message)

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()

            val intent_go = Intent(activity, ActivityMain::class.java)
            intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            activity.startActivity(intent_go)

            val intent_login = Intent(activity, Page_PinLogin::class.java)
            activity.startActivity(intent_login)
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
//            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }

}