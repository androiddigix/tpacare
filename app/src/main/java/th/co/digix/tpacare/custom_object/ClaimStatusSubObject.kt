package th.co.digix.tpacare.custom_object

import org.json.JSONObject

class ClaimStatusSubObject{

    var status_no: String = ""
    var status_name: String = ""
    var status_condition: String = ""
    var status_date: String = ""

    fun setParam(result: JSONObject) {
        try {
            status_no = result.getString("statusNo")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            status_name = result.getString("statusName")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            status_condition = result.getString("statusCondition")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            status_date = result.getString("statusDate")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}