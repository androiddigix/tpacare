package th.co.digix.tpacare.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.util.List;

/**
 * Created by android on 1/9/2558.
 */
public class CallLatLng {

    long starttime = 0;
    Context context;

    public CallLatLng(Context context) {

        this.context = context;
    }

    Handler handler = new Handler();
    Runnable run = new Runnable() {
        @Override
        public void run() {
            long millis = System.currentTimeMillis() - starttime;
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            if (seconds > 1) {

                handler.removeCallbacks(run);

            } else {
                Location location = getLastKnownLocation(context);
                if (location != null) {
                    handler.removeCallbacks(run);

                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
//                    nodeUpdateLatitude = "" + latitude;
//                    nodeUpdateLongitude = "" + longitude;
                    Log.d("android latitude", "is " + latitude);
                    Log.d("android longitude", "is " + longitude);

                } else {
                    handler.postDelayed(run, 0);
                }
            }
        }
    };

    public Location getLocation() {

        starttime = System.currentTimeMillis();
        handler.postDelayed(run, 0);

        long millis = System.currentTimeMillis() - starttime;
        int seconds = (int) (millis / 1000);
        Location location = null;

        while (seconds < 0.1) {
            millis = System.currentTimeMillis() - starttime;
            seconds = (int) (millis / 1000);
            location = getLastKnownLocation(context);
            if (location != null) {
                handler.removeCallbacks(run);

                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
//                Log.d("android latitude", "is " + latitude);
//                Log.d("android longitude", "is " + longitude);

                break;
            }
        }
        if (location == null) {
//            buildAlertMessageNoGps(context);
//            turnGPSOn();
        }
        return location;
    }

    public Location getLocation_checkGPS() {

        starttime = System.currentTimeMillis();
        handler.postDelayed(run, 0);

        long millis = System.currentTimeMillis() - starttime;
        int seconds = (int) (millis / 1000);
        Location location = null;

        while (seconds < 0.01) {
            millis = System.currentTimeMillis() - starttime;
            seconds = (int) (millis / 1000);
            location = getLastKnownLocation(context);
            if (location != null) {
                handler.removeCallbacks(run);

                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
//                Log.d("android latitude", "is " + latitude);
//                Log.d("android longitude", "is " + longitude);

                break;
            }
        }
        if (location == null) {
            buildAlertMessageNoGps(context);
//            turnGPSOn();
        }
        return location;
    }

    private void turnGPSOn() {
        String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled

            Log.d("test", "turn on GPS");
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            context.sendBroadcast(poke);
        }
    }

    private void buildAlertMessageNoGps(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("ไม่สามารถค้นหาตำแหน่งได้ กรุณาเปิด GPS")
                .setCancelable(false)
                .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                        context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation(Context context) {

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d("last known location", " provider: " + provider + " location: " + location);


            if (location == null) {
                continue;
            } else {


            }
            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                Log.d("found best last known ", "location: " + location);
                bestLocation = location;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }
}
