package th.co.digix.tpacare.utility

import android.content.Context
import th.co.digix.tpacare.custom_object.UserObject

//import th.co.digix.tpacare.Object.UserObject

class SharedPreferencesManager(internal var context: Context) {

    private val PROJECT_NAME = "tpa_"
    private val FILENAME_FIRSTTIME = "1"
    private val FILENAME_USER = "2"
    private val PIN_USER = "3"
    private val UseFingerPrint = "4"

    var aesCrypt = AESCrypt()

    fun setFirstTimeOpen(status: Boolean) {
        val sharedPref = context.getSharedPreferences(PROJECT_NAME + FILENAME_FIRSTTIME, 0)
        val editor = sharedPref.edit()
        var firstTime = aesCrypt.encrypt("firstTime")
        editor.putBoolean(firstTime, status)
        editor.commit()
    }

    fun getFirstTimeOpen(): Boolean {
        val sharedPref = context.getSharedPreferences(PROJECT_NAME + FILENAME_FIRSTTIME, 0)
        var firstTime = aesCrypt.encrypt("firstTime")
        return sharedPref.getBoolean(firstTime, true)
    }

    fun setUseFingerPrint(status: Boolean) {
        val sharedPref = context.getSharedPreferences(PROJECT_NAME + UseFingerPrint, 0)
        val editor = sharedPref.edit()
        var firstTime = aesCrypt.encrypt("UseFingerPrint")
        editor.putBoolean(firstTime, status)
        editor.commit()
    }

    fun getUseFingerPrint(): Boolean {
        val sharedPref = context.getSharedPreferences(PROJECT_NAME + UseFingerPrint, 0)
        var firstTime = aesCrypt.encrypt("UseFingerPrint")
        return sharedPref.getBoolean(firstTime, true)
    }

    fun setPIN(pin_set: String) {
        val sharedPref = context.getSharedPreferences(PROJECT_NAME + PIN_USER, 0)
        val editor = sharedPref.edit()
        var pin = aesCrypt.encrypt("pin")
        editor.putString(pin, pin_set)
        editor.commit()
    }

    fun getPIN(): String {
        val sharedPref = context.getSharedPreferences(PROJECT_NAME + PIN_USER, 0)
        var pin = aesCrypt.encrypt("pin")
        return sharedPref.getString(pin, "")
    }

    fun setUserObject(userObject: UserObject) {
        val sharedPref = context.getSharedPreferences(PROJECT_NAME + FILENAME_USER, 0)
        val editor = sharedPref.edit()
        editor.putString(aesCrypt.encrypt("token"), aesCrypt.encrypt(userObject.token))
        editor.putString(aesCrypt.encrypt("refresh_token"), aesCrypt.encrypt(userObject.refresh_token))
        editor.putString(aesCrypt.encrypt("username"), aesCrypt.encrypt(userObject.username))
        editor.putString(aesCrypt.encrypt("user_id"), aesCrypt.encrypt(userObject.user_id))
        editor.putString(aesCrypt.encrypt("image_profile"), aesCrypt.encrypt(userObject.image_profile))
        editor.putString(aesCrypt.encrypt("tpa_access_token"), aesCrypt.encrypt(userObject.tpa_access_token))
        editor.putString(aesCrypt.encrypt("citizen_id"), aesCrypt.encrypt(userObject.citizen_id))
        editor.putString(aesCrypt.encrypt("tel"), aesCrypt.encrypt(userObject.tel))
        editor.putString(aesCrypt.encrypt("first_name"), aesCrypt.encrypt(userObject.first_name))
        editor.putString(aesCrypt.encrypt("last_name"), aesCrypt.encrypt(userObject.last_name))
        editor.putString(aesCrypt.encrypt("state_login"), aesCrypt.encrypt(userObject.state_login))
        editor.commit()
    }

    fun getUserObject(): UserObject {
        val userObject = UserObject()
        val sharedPref = context.getSharedPreferences(PROJECT_NAME + FILENAME_USER, 0)
        try {
            userObject.token = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("token"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.refresh_token = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("refresh_token"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.username = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("username"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.user_id = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("user_id"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.image_profile = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("image_profile"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.tpa_access_token =
                aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("tpa_access_token"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.citizen_id = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("citizen_id"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.tel = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("tel"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.first_name = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("first_name"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.last_name = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("last_name"), ""))
        } catch (e: Exception) {
        }
        try {
            userObject.state_login = aesCrypt.decrypt(sharedPref.getString(aesCrypt.encrypt("state_login"), ""))
        } catch (e: Exception) {
        }
        return userObject
    }

}