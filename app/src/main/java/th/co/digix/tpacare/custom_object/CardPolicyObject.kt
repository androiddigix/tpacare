package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class CardPolicyObject() : Parcelable {

    var policyFrom: String = ""
    var logoImage: String = ""
    var name: String = ""
    var surname: String = ""
    var insuredType: String = ""
    var cardType: String = ""
    var insurerCode: String = ""
    var insurerName: String = ""
    var insurerDedicateedLine: String = ""
    var companyName: String = ""
    var staffNo: String = ""
    var effFrom: String = ""
    var effTo: String = ""
    var polNo: String = ""
    var telNo: String = ""
    var memberCode: String = ""

    constructor(parcel: Parcel) : this() {
        policyFrom = parcel.readString()
        logoImage = parcel.readString()
        name = parcel.readString()
        surname = parcel.readString()
        insuredType = parcel.readString()
        cardType = parcel.readString()
        insurerCode = parcel.readString()
        insurerName = parcel.readString()
        insurerDedicateedLine = parcel.readString()
        companyName = parcel.readString()
        staffNo = parcel.readString()
        effFrom = parcel.readString()
        effTo = parcel.readString()
        polNo = parcel.readString()
        telNo = parcel.readString()
        memberCode = parcel.readString()
    }

    fun setParam(result: JSONObject) {
        try {
            policyFrom = result.getString("policyFrom")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            logoImage = result.getString("logoImage")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            name = result.getString("name")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            surname = result.getString("surname")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            insuredType = result.getString("insuredType")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            cardType = result.getString("cardType")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            insurerCode = result.getString("insurerCode")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            insurerName = result.getString("insurerName")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            insurerDedicateedLine = result.getString("insurerDedicateedLine")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            companyName = result.getString("companyName")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            staffNo = result.getString("staffNo")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            effFrom = result.getString("effFrom")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            effTo = result.getString("effTo")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            polNo = result.getString("polNo")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            telNo = result.getString("telNo")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            memberCode = result.getString("memberCode")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (memberCode == null  || memberCode.equals("null")) {
            memberCode = ""
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(policyFrom)
        parcel.writeString(logoImage)
        parcel.writeString(name)
        parcel.writeString(surname)
        parcel.writeString(insuredType)
        parcel.writeString(cardType)
        parcel.writeString(insurerCode)
        parcel.writeString(insurerName)
        parcel.writeString(insurerDedicateedLine)
        parcel.writeString(companyName)
        parcel.writeString(staffNo)
        parcel.writeString(effFrom)
        parcel.writeString(effTo)
        parcel.writeString(polNo)
        parcel.writeString(telNo)
        parcel.writeString(memberCode)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CardPolicyObject> {
        override fun createFromParcel(parcel: Parcel): CardPolicyObject {
            return CardPolicyObject(parcel)
        }

        override fun newArray(size: Int): Array<CardPolicyObject?> {
            return arrayOfNulls(size)
        }
    }

}