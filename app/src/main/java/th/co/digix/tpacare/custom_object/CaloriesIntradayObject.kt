package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class CaloriesIntradayObject() : Parcelable {

    var level: String = ""
    var mets: String = ""
    var time: String = ""
    var value: String = ""

    constructor(parcel: Parcel) : this() {
        level = parcel.readString()
        mets = parcel.readString()
        time= parcel.readString()
        value = parcel.readString()

    }

    fun setParam(result: JSONObject) {
        try {
            level = result.getString("level")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            mets = result.getString("mets")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            time = result.getString("time")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            value = result.getString("value")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(level)
        parcel.writeString(mets)
        parcel.writeString(time)
        parcel.writeString(value)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CaloriesIntradayObject> {
        override fun createFromParcel(parcel: Parcel): CaloriesIntradayObject {
            return CaloriesIntradayObject(parcel)
        }

        override fun newArray(size: Int): Array<CaloriesIntradayObject?> {
            return arrayOfNulls(size)
        }
    }

}