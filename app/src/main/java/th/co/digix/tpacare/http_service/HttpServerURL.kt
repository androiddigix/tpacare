package th.co.digix.tpacare.http_service

class HttpServerURL {

    //************************main server**************************//
    internal val host_server = "https://tpacare-api.azurewebsites.net"
    internal val host_azurewebsites = "https://tpacare-api.azurewebsites.net"
    internal val host_fitbit = "https://api.fitbit.com"

    val salt_key = "b0e5e5efd27e3f5d8cd05d3d1r8hfd8s63dba94be54e6ee91c46cv7fde84c0fe"

    //************************test server**************************//
//    internal val host_server = "https://tpacare-api-dev.azurewebsites.net"
//    internal val host_azurewebsites = "https://tpacare-dev.azurewebsites.net"

    //-------------------------------------------------------------//

    //fitbit
    internal val api_activities_heart = "/1/user/-/activities/heart/"
    internal val api_activities_steps = "/1/user/-/activities/steps/"
    internal val api_activities_calories = "/1/user/-/activities/calories/"



    //login
    internal val api_authLogin = "/api/auth/login"
    internal val api_authLogout = "/api/auth/logout"
    internal val api_authLoginByPasscode = "/api/auth/login-by-passcode"
    internal val api_authRefreshtoken = "/api/auth/refreshtoken" // ใช้กับ fingerprint และ ตอนเวลา token หมด ดูที่ param token_expires_date - format "timestamp unit 1970"
    //regiseter
    internal val api_authRegisterRequestOTP = "/api/auth/register/request-otp"
    internal val api_authRegister = "/api/auth/register"
    internal val api_authSetPin = "/api/auth/set-pin"
    //edit profile
    internal val api_authUpdateProfile = "/api/auth/update-profile"
    internal val api_authUpdateProfileRequestOTP = "/api/auth/updateprofile/request-otp"
    internal val api_authUpdateProfileCheckOTP = "/api/auth/updateprofile/check-otp"
    internal val api_authChangePassword = "/api/auth/change-password"
    internal val api_authUpdateImageProfile = "/api/auth/update-image-profile"
    //forgot password
    internal val api_authForgotPasswordRequestOTP = "/api/auth/forgot-password/request-otp"
    internal val api_authForgotPasswordCheckOTP = "/api/auth/forgot-password/check-otp"
    internal val api_authForgotPassword = "/api/auth/forgot-password"
    //forgot user
    internal val api_authForgotUserRequestOTP = "/api/auth/forgot-user/request-otp"
    internal val api_authForgotUserCheckOTP = "/api/auth/forgot-user/check-otp"

    internal val azurewebsitesTermAndCondition =  "/TermAndCondition/Index"

    internal val api_authLoginByPin = "/api/auth/login-by-pin"
    internal val api_authActivated = "/api/auth/activated"

    //list card
    internal val api_auto_policy_activate = "/api/auto_policy_activate"
    internal val api_policy_activate = "/api/policy_activate"
    internal val api_eligibility_check = "/api/eligibility_check" // กดใช้บัตร
    internal val api_cancel_check = "/api/cancel_check" // ยกเลิกใช้บัตร
    internal val api_policy_list = "/api/policy_list"   // list card ---->  insured_type  [Member ของฉัน][Family ครอบครัวของฉัน]
    internal val api_insurance_delete_card = "/api/insurance/delete-card"   // list card ---->  insured_type  [Member ของฉัน][Family ครอบครัวของฉัน]
    //addcard
    internal val api_insurance_addcard = "/api/insurance/addcard"
    internal val api_values_configurations = "/api/values/configurations"
    internal val api_values_bank = "/api/values/bank"
    internal val api_values_insurance_company = "/api/values/insurance-company"

    //list
    internal val api_values_hospital = "/api/values/hospital"
    internal val api_article = "/api/article"
    internal val api_promotion = "/api/promotion"
    internal val api_inbox = "/api/inbox"

    internal val api_content_diet = "/api/content/diet"
    internal val api_content_health = "/api/content/health-data"

    internal val api_inboxReadMessage = "/api/inbox/read-message"

    internal val api_policy_detail = "/api/policy_detail"  // card detail 2 tabs
    internal val api_policy_request_otp = "/api/policy/request-otp"  // card detail 2 tabs

    internal val api_claim_status= "/api/claim_status"

}
