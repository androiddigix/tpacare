package th.co.digix.tpacare

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.menu_card.Page_Fragment_Card
import th.co.digix.tpacare.menu_card.Page_Fragment_FindMySave
import th.co.digix.tpacare.menu_setting.Page_Fragment_Setting
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.custom_object.ConfigurationObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.menu_home.Page_Fragment_Home
import th.co.digix.tpacare.menu_noti.Page_Fragment_Noti
import th.co.digix.tpacare.menu_status.Page_Fragment_Status
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.*
import java.lang.Exception
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.widget.Toast
import th.co.digix.tpacare.menu_fitme.Authenication.AuthenticationHandler
import th.co.digix.tpacare.menu_fitme.Authenication.AuthenticationManager
import th.co.digix.tpacare.menu_fitme.Authenication.AuthenticationResult
import th.co.digix.tpacare.menu_fitme.Authenication.Fitbit_Token
import th.co.digix.tpacare.menu_fitme.Page_Fragment_Fitme

class ActivityMain : BaseActivity(), AuthenticationHandler {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_values_configurations = httpServerURL.api_values_configurations
    val salt_key = httpServerURL.salt_key

    var requestCode_LOCATION = 63

    var linearLayout_root_bg: LinearLayout? = null

    var linearLayout_menu_home: LinearLayout? = null
    var linearLayout_menu_card: LinearLayout? = null
    var linearLayout_menu_status: LinearLayout? = null
    var linearLayout_menu_noti: LinearLayout? = null
    var linearLayout_menu_setting: LinearLayout? = null

    var page_select_temp = 0

    var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        setContentView(R.layout.activity_main)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initBottomNavigation()

        openLocation_checkPermission()



    }

    private fun openLocation_checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionsList = ArrayList<String>()
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            }
            if (permissionsList.size > 0) {
                requestPermissions(
                    permissionsList.toTypedArray(),
                    requestCode_LOCATION
                )
            } else {
                callService()
            }
        } else {
            callService()
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        AuthenticationManager.onActivityResult(requestCode, resultCode, data, this)

    }

    override fun onAuthFinished(authenticationResult: AuthenticationResult) {
        if (authenticationResult.isSuccessful()) {
            var page_Fragment_fitme = Page_Fragment_Fitme()
            replaceFragment(page_Fragment_fitme)


        } else {
            //displayAuthError(authenticationResult)
        }
    }

    private fun displayAuthError(authenticationResult: AuthenticationResult) {
        var message = ""

        when (authenticationResult.getStatus()) {
            AuthenticationResult.Status.dismissed -> message = getString(R.string.login_dismissed)
            AuthenticationResult.Status.error -> message = authenticationResult.getErrorMessage()
            AuthenticationResult.Status.missing_required_scopes -> {
                val missingScopes = authenticationResult.getMissingScopes()
                val missingScopesText = TextUtils.join(", ", missingScopes)
                message = getString(R.string.missing_scopes_error) + missingScopesText
            }
        }

        AlertDialog.Builder(this)
            .setTitle(R.string.login_title)
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { dialog, id -> })
            .create()
            .show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        if (requestCode == requestCode_LOCATION) {
            val perms = HashMap<String, Int>()
            for (i in 0 until permissions.size) {
                perms.put(permissions[i], grantResults[i])
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                ) {
                    openLocation_checkPermission()
                } else {
//                    Toast.makeText(this, "Sorry, Permission Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun callService() {

        val callLatLng = CallLatLng(this)
        val location = callLatLng.getLocation()
        var latitudeSTR = "0"
        var longitudeSTR = "0"

        if (location != null) {
            val latitude = location!!.getLatitude()
            val longitude = location!!.getLongitude()
            Log.d("android latitude", "is $latitude")
            Log.d("android longitude", "is $longitude")
            latitudeSTR = "" + latitude
            longitudeSTR = "" + longitude
        }

        val defaultParameterAPI = DefaultParameterAPI(this)
        val formBody = "?page=" + "1" +
                "&pageSize=" + "5" +
                "&sLat=" + latitudeSTR +
                "&sLng=" + longitudeSTR +
                "&distance=" + "0.5"

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_values_configurations + formBody,
            "api_values_configurations"
        )

    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialView() {

        linearLayout_root_bg = findViewById(R.id.linearLayout_root_bg) as LinearLayout

        linearLayout_menu_home = findViewById(R.id.linearLayout_menu_home) as LinearLayout
        linearLayout_menu_card = findViewById(R.id.linearLayout_menu_card) as LinearLayout
        linearLayout_menu_status = findViewById(R.id.linearLayout_menu_status) as LinearLayout
        linearLayout_menu_noti = findViewById(R.id.linearLayout_menu_noti) as LinearLayout
        linearLayout_menu_setting = findViewById(R.id.linearLayout_menu_setting) as LinearLayout

    }

    private fun initInstanceToolbar() {


    }

    private fun initialIntentData() {


    }


    private fun initBottomNavigation() {


        linearLayout_root_bg?.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                //Find the currently focused view, so we can grab the correct window token from it.
                var view = getCurrentFocus()
                //If no view currently has focus, create a new one, just so we can grab a window token from it
                if (view == null) {
                    view = View(this)
                }
                imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
            }
        }

        val sharedPreferencesManager = SharedPreferencesManager(this)

        //first set fragmentParent
        replaceFragment(Page_Fragment_Home())
        linearLayout_menu_home?.isSelected = true

        linearLayout_menu_home?.setOnClickListener() {
            val page_select = 0
            if (page_select_temp != page_select) {
                linearLayout_menu_home?.isSelected = true
                linearLayout_menu_card?.isSelected = false
                linearLayout_menu_status?.isSelected = false
                linearLayout_menu_noti?.isSelected = false
                linearLayout_menu_setting?.isSelected = false

                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

                replaceFragment(Page_Fragment_Home())
                page_select_temp = page_select
            }
        }
        linearLayout_menu_card?.setOnClickListener() {
            val userObject = sharedPreferencesManager.getUserObject()
            if (!userObject.state_login.equals("login")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.show_alertDialog_lnot_login(this)
            } else {
                val page_select = 1
                if (page_select_temp != page_select) {
                    linearLayout_menu_home?.isSelected = false
                    linearLayout_menu_card?.isSelected = true
                    linearLayout_menu_status?.isSelected = false
                    linearLayout_menu_noti?.isSelected = false
                    linearLayout_menu_setting?.isSelected = false

                    val citizen_id = userObject.citizen_id
                    if (citizen_id.equals("")) {
                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

                        var page_Fragment_FindMySave =
                            Page_Fragment_FindMySave()
                        val bundle = Bundle()
                        bundle.putString("insured_type", "Member")
                        page_Fragment_FindMySave.setArguments(bundle)
                        replaceFragment(page_Fragment_FindMySave)
                    } else {
                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

                        val httpServerURL = HttpServerURL()
                        val salt_key = httpServerURL.salt_key
                        val cryptLib = CryptLib()
                        val citizen_id_encrypt = cryptLib.decryptCipherTextWithRandomIV(citizen_id, salt_key)

                        if (citizen_id_encrypt.equals("")) {
                            var page_Fragment_FindMySave =
                                Page_Fragment_FindMySave()
                            val bundle = Bundle()
                            bundle.putString("insured_type", "Member")
                            page_Fragment_FindMySave.setArguments(bundle)
                            replaceFragment(page_Fragment_FindMySave)
                        } else {
                            var page_Fragment_Card = Page_Fragment_Card()
                            val bundle = Bundle()
                            bundle.putString("insured_type", "Member")
                            page_Fragment_Card.setArguments(bundle)
                            replaceFragment(page_Fragment_Card)
                        }

                    }
                    page_select_temp = page_select
                }
            }

        }

        linearLayout_menu_status?.setOnClickListener() {
            val userObject = sharedPreferencesManager.getUserObject()
            if (!userObject.state_login.equals("login")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.show_alertDialog_lnot_login(this)
            } else {
                val page_select = 2
                if (page_select_temp != page_select) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

                    linearLayout_menu_home?.isSelected = false
                    linearLayout_menu_card?.isSelected = false
                    linearLayout_menu_status?.isSelected = true
                    linearLayout_menu_noti?.isSelected = false
                    linearLayout_menu_setting?.isSelected = false

                    replaceFragment(Page_Fragment_Status())
                    page_select_temp = page_select
                }
            }

        }
        linearLayout_menu_noti?.setOnClickListener() {
            val userObject = sharedPreferencesManager.getUserObject()
            if (!userObject.state_login.equals("login")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.show_alertDialog_lnot_login(this)
            } else {
                val page_select = 3
                if (page_select_temp != page_select) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

                    linearLayout_menu_home?.isSelected = false
                    linearLayout_menu_card?.isSelected = false
                    linearLayout_menu_status?.isSelected = false
                    linearLayout_menu_noti?.isSelected = true
                    linearLayout_menu_setting?.isSelected = false

                    replaceFragment(Page_Fragment_Noti())
                    page_select_temp = page_select
                }
            }
        }
        linearLayout_menu_setting?.setOnClickListener() {
            val page_select = 4
            if (page_select_temp != page_select) {
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

                linearLayout_menu_home?.isSelected = false
                linearLayout_menu_card?.isSelected = false
                linearLayout_menu_status?.isSelected = false
                linearLayout_menu_noti?.isSelected = false
                linearLayout_menu_setting?.isSelected = true

                replaceFragment(Page_Fragment_Setting())
                page_select_temp = page_select
            }
        }

    }


    fun replaceFragment(fragment: Fragment) {
        val mfragment = fragment
//        val bundle = Bundle()
//        bundle.putString("status", "" + status)
//        mfragment.setArguments(bundle)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.menu_container, mfragment)
        transaction.commit()
    }

    fun addFragment(fragment: Fragment) {
        val mfragment = fragment
//        val bundle = Bundle()
//        bundle.putString("status", "" + status)
//        mfragment.setArguments(bundle)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.menu_container, mfragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {
        if (event.getHttpName().equals("api_values_configurations")) {

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {
                        for (i in 0..results.length() - 1) {
                            var configuration_Json: JSONObject? = null
                            try {
                                configuration_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (configuration_Json != null) {
                                val configurationObject = ConfigurationObject()
                                configurationObject.setParam(configuration_Json)

                                if (configurationObject.code.equals("countdown")) {
                                    try {
                                        TempData.countdown_min = configurationObject.value
                                    } catch (e: Exception) {
                                    }
                                } else if (configurationObject.code.equals("hospital_distance")) {
                                    try {
                                        TempData.hospital_distance = configurationObject.value
                                    } catch (e: Exception) {
                                    }
                                }
                            }

                        }
                    }
                } else {
                }
            } else {
            }
        }
    }

    override fun onBackPressed() {
        if (page_select_temp != 0) {

            if (page_select_temp == 95 || page_select_temp == 94) {
//                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                val fragment = supportFragmentManager.findFragmentById(R.id.menu_container)
                (fragment as? IOnBackPressed)?.onBackPressed()?.not()?.let {
                    //                    super.onBackPressed()
                }
            } else if (page_select_temp == 98 || page_select_temp == 99) {
                val fragment = supportFragmentManager.findFragmentById(R.id.menu_container)
                (fragment as? IOnBackPressed)?.onBackPressed()?.not()?.let {
                    //                    super.onBackPressed()
                }
            } else {
                val page_select = 0
                if (page_select_temp != page_select) {
                    linearLayout_menu_home?.isSelected = true
                    linearLayout_menu_card?.isSelected = false
                    linearLayout_menu_status?.isSelected = false
                    linearLayout_menu_noti?.isSelected = false
                    linearLayout_menu_setting?.isSelected = false

                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

                    replaceFragment(Page_Fragment_Home())
                    page_select_temp = page_select
                }
            }

        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                return
            }

            doubleBackToExitPressedOnce = true
            Toast.makeText(
                this,
                "" + resources.getString(R.string.text_please_click_back_again_to_exit),
                Toast.LENGTH_SHORT
            ).show()

            Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
        }
    }

}
