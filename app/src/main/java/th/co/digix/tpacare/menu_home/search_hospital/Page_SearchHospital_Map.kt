package th.co.digix.tpacare.menu_home.search_hospital

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.*
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.squareup.otto.Subscribe
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.HospitalObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CallLatLng
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class Page_SearchHospital_Map : BaseActivity(), OnMapReadyCallback {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_values_hospital = httpServerURL.api_values_hospital

    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var mapFragment: SupportMapFragment? = null
    var progressBackground: RelativeLayout? = null

    var imageView_find_list: ImageView? = null
    var editText_keyword: EditText? = null

    var requestCode_LOCATION = 63

    var keyword = ""

    var page = 1
    var per_page = 10
    var total_record = 0

    var hospitalObject_List = ArrayList<HospitalObject>()

    var googleMap: GoogleMap? = null

    var current_latitude = 13.7248936 //default กทม
    var current_longitude = 102.8240698 //default กทม

    var location: Location? = null

    companion object {
        var marker_List = HashMap<Marker, Int>()
        var marker_ArrayList = ArrayList<Marker>()
        var isFirstTime = true
    }

//    private val drawPinHandler = Handler(object : Handler.Callback {
//        override fun handleMessage(msg: Message): Boolean {
//            return true
//        }
//    })


    override fun onDestroy() {
        super.onDestroy()
        isFirstTime = true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_search_hospital_map)

        val callLatLng = CallLatLng(this)
        location = callLatLng.getLocation_checkGPS()



        MapsInitializer.initialize(getApplicationContext())

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()

        callService_hospital()
    }

//    override fun onInfoWindowClick(marker: Marker?) {
//
//        Log.d("coke", "onMarkerClick")
//        if (marker != null) {
//            val position = marker_List.get(marker)
//
//            if (position != null && position < hospitalObject_List.size) {
//                val hospitalObject = hospitalObject_List.get(position)
//
//                val intent_go = Intent(this@Page_SearchHospital_Map, Page_SearchHospital_Detail::class.java)
//                intent_go.putExtra("hospitalObject", hospitalObject)
//                startActivity(intent_go)
//            }
//        }
//    }

    override fun onMapReady(googleMap: GoogleMap?) {

        this.googleMap = googleMap

        var lat = 13.7248936
        var lng = 102.8240698

        if (hospitalObject_List.size > 0) {
            try {
                lat = hospitalObject_List.get(0).latitude.toDouble()
            } catch (e: Exception) {
            }
            try {
                lng = hospitalObject_List.get(0).longitude.toDouble()
            } catch (e: Exception) {
            }
            val current = LatLng(lat, lng)
            googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 15f))
        }
        googleMap?.uiSettings!!.isMapToolbarEnabled = false

        val mapView = mapFragment?.getView()
        if (mapView != null) {
            val locationButton = (((mapView?.findViewById(Integer.parseInt("1")) as View)
                .getParent() as View)
                .findViewById(Integer.parseInt("2")) as View)
            val rlp = locationButton.getLayoutParams() as RelativeLayout.LayoutParams

            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            rlp.setMargins(0, 0, 30, 30)
        }


        getLocation_checkPermission()

    }

    fun callService_hospital() {

        var latitudeSTR = "0"
        var longitudeSTR = "0"

        if (location != null) {
            val latitude = location!!.getLatitude()
            val longitude = location!!.getLongitude()
            Log.d("android latitude", "is $latitude")
            Log.d("android longitude", "is $longitude")
            latitudeSTR = "" + latitude
            longitudeSTR = "" + longitude
        }

        val defaultParameterAPI = DefaultParameterAPI(this)
        val formBody = "?keyword=" + "" + editText_keyword?.text.toString() +
                "&page=" + "1" +
                "&pageSize=" + "99999" +
                "&sLat=" + latitudeSTR +
                "&sLng=" + longitudeSTR +
                "&order_by=" + "distance"

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_values_hospital + formBody,
            "api_values_hospital_search_map"
        )

        progressBackground?.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        BusProvider.getInstance().register(this)
    }

    override fun onPause() {
        super.onPause()
        BusProvider.getInstance().unregister(this)
    }


    private fun initialWorking() {

    }


    private fun initialView() {

        mapFragment = supportFragmentManager.findFragmentById(R.id.fragment_map_container) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        imageView_find_list = findViewById(R.id.imageView_find_list) as ImageView
        editText_keyword = findViewById(R.id.editText_keyword) as EditText
        editText_keyword?.setText("" + keyword)

        imageView_find_list?.setOnClickListener {
            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = getCurrentFocus()
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(this)
            }
            imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)

            //            hospitalObject_List.clear()
            page = 1
            callService_hospital()
        }
    }

    private fun initialIntentData() {

        try {
            hospitalObject_List = intent.getParcelableArrayListExtra("hospitalObject_List")
        } catch (e: Exception) {
        }
        try {
            keyword = intent.getStringExtra("keyword")
        } catch (e: Exception) {
        }
    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_search_hospital))
    }

    private fun getLocation_checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissionsList = ArrayList<String>()
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            }
            if (permissionsList.size > 0) {
                requestPermissions(
                    permissionsList.toTypedArray(),
                    requestCode_LOCATION
                )
            } else {
                getLocation()
            }
        } else {
            getLocation()
        }
    }

    @SuppressLint("MissingPermission")
    fun getLocation() {

        var latitudeSTR = "0"
        var longitudeSTR = "0"

        if (location != null) {
            val latitude = location!!.getLatitude()
            val longitude = location!!.getLongitude()
//            Log.d("android latitude", "is $latitude")
//            Log.d("android longitude", "is $longitude")
            current_latitude = latitude
            current_longitude = longitude

            val current = LatLng(latitude, longitude)
            if (hospitalObject_List.size == 0) {
                googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 15f))
            }
            googleMap?.isMyLocationEnabled = true

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        if (requestCode == requestCode_LOCATION) {
            val perms = HashMap<String, Int>()
            for (i in 0 until permissions.size) {
                perms.put(permissions[i], grantResults[i])
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                ) {
                    getLocation_checkPermission()
                } else {
//                    Toast.makeText(this, "Sorry, Permission Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


//    private val run_drawPin = Runnable {
//
//        //        resetDrawPinTimer()
//
//    }


    fun drawPin() {
        if (hospitalObject_List.size > 0) {
            val builder = LatLngBounds.Builder()
            googleMap?.clear()
            for (i in 0..hospitalObject_List.size - 1) {

                var lat_list = 13.7248936
                var lng_list = 102.8240698
                try {
                    lat_list = hospitalObject_List.get(i).latitude.toDouble()
                } catch (e: Exception) {
                }
                try {
                    lng_list = hospitalObject_List.get(i).longitude.toDouble()
                } catch (e: Exception) {
                }
                val current = LatLng(lat_list, lng_list)

                try {
                    val icon_pin_small = BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_small)
                    val hospital_name = "" + hospitalObject_List.get(i).hospital_name
                    val address = "" + hospitalObject_List.get(i).address


                    val myMarker = googleMap?.addMarker(
                        MarkerOptions()
                            .position(current)
                            .title(hospital_name)
                            .snippet(address)
                            .icon(icon_pin_small)
                    )
                    if (i == 0) {
                        myMarker?.showInfoWindow()
                    }
                    googleMap?.setOnInfoWindowClickListener(object : GoogleMap.OnInfoWindowClickListener {

                        override fun onInfoWindowClick(marker: Marker?) {

                            Log.d("coke", "onMarkerClick")
                            if (marker != null) {
                                val position = marker_List.get(marker)

                                if (position != null && position < hospitalObject_List.size) {
                                    val hospitalObject = hospitalObject_List.get(position)

                                    val intent_go = Intent(
                                        this@Page_SearchHospital_Map,
                                        Page_SearchHospital_Detail::class.java
                                    )
                                    intent_go.putExtra("hospitalObject", hospitalObject)
                                    startActivity(intent_go)
                                }
                            }
                        }
                    }

                    )
                    if (myMarker != null) {
//                                    if (i == 0) {
//                                        myMarker.showInfoWindow()
//                                    }
                        Page_SearchHospital_Map.marker_List.put(myMarker, i)
                        Page_SearchHospital_Map.marker_ArrayList.add(myMarker)
                        builder.include(myMarker.getPosition())
                    }


                } catch (e: Exception) {
                }


            }


            if (isFirstTime) {
                isFirstTime = false

                if (location != null) {
                    val latitude = location!!.getLatitude()
                    val longitude = location!!.getLongitude()
                    val current = LatLng(latitude, longitude)
                    googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 15f))
                } else {
                    try{
                        val bounds = builder.build()
                        val padding = 0 // offset from edges of the map in pixels
                        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                        googleMap?.moveCamera(cu)
                        googleMap?.animateCamera(cu)
                    }catch (e:Exception){
                    }
                }
            } else {
                var lat_list = 13.7248936
                var lng_list = 102.8240698
                try {
                    lat_list = hospitalObject_List.get(0).latitude.toDouble()
                } catch (e: Exception) {
                }
                try {
                    lng_list = hospitalObject_List.get(0).longitude.toDouble()
                } catch (e: Exception) {
                }
                val current_nearest = LatLng(lat_list, lng_list)

                googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(current_nearest, 15f))
//                val bounds = builder.build()
//                val padding = 0 // offset from edges of the map in pixels
//                val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
//                googleMap?.moveCamera(cu)
//                googleMap?.animateCamera(cu)
            }

        }
        progressBackground?.visibility = View.GONE

    }

//    class DrawPin_AsyncTask(
//        hospitalObject_List: ArrayList<HospitalObject>
//        , googleMap: GoogleMap?,
//        progressBackground: RelativeLayout?,
//        activity: Activity?
//    ) : AsyncTask<String, Void, String>() {
//
//        var hospitalObject_List = ArrayList<HospitalObject>()
//        var googleMap: GoogleMap? = null
//        var onInfoWindowClickListener: GoogleMap.OnInfoWindowClickListener? = null
//        var progressBackground: RelativeLayout? = null
//        var activity: Activity? = null
//
//        init {
//            this.hospitalObject_List = hospitalObject_List
//            this.googleMap = googleMap
//            this.onInfoWindowClickListener = onInfoWindowClickListener
//            this.progressBackground = progressBackground
//            this.activity = activity
//        }
//
//
//        override fun doInBackground(vararg p0: String?): String {
//            if (hospitalObject_List.size > 0) {
//                val builder = LatLngBounds.Builder()
//
//                for (i in 0..hospitalObject_List.size - 1) {
//
//                    var lat_list = 13.7248936
//                    var lng_list = 102.8240698
//                    try {
//                        lat_list = hospitalObject_List.get(i).latitude.toDouble()
//                    } catch (e: Exception) {
//                    }
//                    try {
//                        lng_list = hospitalObject_List.get(i).longitude.toDouble()
//                    } catch (e: Exception) {
//                    }
//                    val current = LatLng(lat_list, lng_list)
//
//                    try {
//                        val icon_pin_small = BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_small)
//                        val hospital_name = "" + hospitalObject_List.get(i).hospital_name
//                        val address = "" + hospitalObject_List.get(i).address
//
//                        activity?.runOnUiThread(object : Runnable {
//                            override fun run() {
//                                val myMarker = googleMap?.addMarker(
//                                    MarkerOptions()
//                                        .position(current)
//                                        .title(hospital_name)
//                                        .snippet(address)
//                                        .icon(icon_pin_small)
//                                )
//                                googleMap?.setOnInfoWindowClickListener(object : GoogleMap.OnInfoWindowClickListener {
//
//                                    override fun onInfoWindowClick(marker: Marker?) {
//
//                                        Log.d("coke", "onMarkerClick")
//                                        if (marker != null) {
//                                            val position = marker_List.get(marker)
//
//                                            if (position != null && position < hospitalObject_List.size) {
//                                                val hospitalObject = hospitalObject_List.get(position)
//
//                                                val intent_go = Intent(
//                                                    activity!!,
//                                                    Page_SearchHospital_Detail::class.java
//                                                )
//                                                intent_go.putExtra("hospitalObject", hospitalObject)
//                                                activity?.startActivity(intent_go)
//                                            }
//                                        }
//                                    }
//                                }
//
//                                )
//                                if (myMarker != null) {
////                                    if (i == 0) {
////                                        myMarker.showInfoWindow()
////                                    }
//                                    Page_SearchHospital_Map.marker_List.put(myMarker, i)
//                                    Page_SearchHospital_Map.marker_ArrayList.add(myMarker)
//                                    builder.include(myMarker.getPosition())
//                                }
//                            }
//                        })
//                    } catch (e: Exception) {
//                    }
//
//
//                }
//                activity?.runOnUiThread(object : Runnable {
//                    override fun run() {
//
//                        if (isFirstTime) {
//                            isFirstTime = false
//
//                            val callLatLng = CallLatLng(activity)
//                            val location = callLatLng?.getLocation()
//                            if (location != null) {
//                                val latitude = location!!.getLatitude()
//                                val longitude = location!!.getLongitude()
//                                val current = LatLng(latitude, longitude)
//                                googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 15f))
//                            } else {
//                                val bounds = builder.build()
//                                val padding = 0 // offset from edges of the map in pixels
//                                val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
//                                googleMap?.moveCamera(cu)
//                                googleMap?.animateCamera(cu)
//                            }
//                        } else {
//                            val bounds = builder.build()
//                            val padding = 0 // offset from edges of the map in pixels
//                            val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
//                            googleMap?.moveCamera(cu)
//                            googleMap?.animateCamera(cu)
//                        }
//
//                    }
//                })
//            }
//
//            activity?.runOnUiThread(object : Runnable {
//                override fun run() {
//                    progressBackground?.visibility = View.GONE
//                }
//            })
//
//            return ""
//        }
//
//
//    }

//    private fun resetDrawPinTimer() {
//        drawPinHandler.postDelayed(run_drawPin, 6000)
//    }


    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_values_hospital_search_map")) {

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    total_record = responseJson.getInt("total_record")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (results != null) {
                        hospitalObject_List.clear()
                        marker_List.clear()
                        marker_ArrayList.clear()
                        for (i in 0..results.length() - 1) {
                            var hospital_Json: JSONObject? = null
                            try {
                                hospital_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (hospital_Json != null) {
                                val hospitalObject = HospitalObject()
                                hospitalObject.setParam(hospital_Json)
                                hospitalObject_List.add(hospitalObject)
                            }

                        }

//                        drawPinHandler.postDelayed(run_drawPin, 0)
//                        var lat = 13.7248936
//                        var lng = 102.8240698

                        drawPin()

//                        DrawPin_AsyncTask(
//                            hospitalObject_List,
//                            googleMap,
//                            progressBackground,
//                            this
//                        ).execute()

                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_SearchHospital_Map, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_SearchHospital_Map, resources.getString(R.string.text_service_error)
                )
            }
        }

    }

}