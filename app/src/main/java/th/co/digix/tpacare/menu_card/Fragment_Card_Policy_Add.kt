package th.co.digix.tpacare.menu_card

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.LinearLayout
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.CardPolicyObject
import java.util.ArrayList

class Fragment_Card_Policy_Add : Fragment() {

    var rootView: View? = null


    var linearLayout_add_card_policy: LinearLayout? = null

    var insured_type = ""
    var idcard = ""
    var isAllowUser = true

    var cardPolicyObject_List = ArrayList<CardPolicyObject>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_card_policy_add, container, false)

        initialIntentData()

        initialView()

        setDataView()

        return rootView
    }

    private fun setDataView() {

        linearLayout_add_card_policy?.setOnClickListener {
            val intent_go = Intent(activity, Page_AddCard_Policy::class.java)
            intent_go.putExtra("insured_type", insured_type)
            intent_go.putExtra("isAllowUser", isAllowUser)
            intent_go.putExtra("cardPolicyObject_List", cardPolicyObject_List)
            val parentFrag = parentFragment as Page_Fragment_Card
            startActivityForResult(intent_go, parentFrag.requestCode_addCard_Policy)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val parentFrag = parentFragment as Page_Fragment_Card
        if (requestCode == parentFrag.requestCode_addCard_Policy) {
            if (resultCode == Activity.RESULT_OK) {
                var insurer_code = ""
                var policy_id = ""
                try {
                    insurer_code = data?.getStringExtra("insurer_code")!!
                } catch (e: Exception) {
                }
                try {
                    policy_id = data?.getStringExtra("policy_id")!!
                } catch (e: Exception) {
                }
//                parentFrag.cardPolicyObject_List.clear()
//                parentFrag.listOfPolDetObject_List.clear()

//                parentFrag.claimJSONObject_List.clear()
//                parentFrag.claimDetailObject_List.clear()

                parentFrag.callService_policy_list_add(policy_id, insurer_code)
            }
        }
    }

    private fun initialView() {

        linearLayout_add_card_policy = rootView?.findViewById(R.id.linearLayout_add_card_policy) as LinearLayout
    }

    private fun initialIntentData() {
        val bundle = this.arguments
        if (bundle != null) {
            try {
                idcard = bundle.getString("idcard", "")
            } catch (e: Exception) {
            }
            try {
                insured_type = bundle.getString("insured_type", "")
            } catch (e: Exception) {
            }
            try {
                isAllowUser = bundle.getBoolean("isAllowUser")
            } catch (e: Exception) {
            }
            try {
                cardPolicyObject_List = bundle.getParcelableArrayList("cardPolicyObject_List")
            } catch (e: Exception) {
            }
        }
    }
}