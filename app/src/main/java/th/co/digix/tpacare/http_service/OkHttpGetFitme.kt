package th.co.digix.tpacare.http_service

import android.app.Activity
import android.os.AsyncTask
import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import th.co.digix.tpacare.custom_object.UserObject
import th.co.digix.tpacare.menu_fitme.Authenication.AccessToken
import th.co.digix.tpacare.menu_fitme.Authenication.AuthenticationManager
import th.co.digix.tpacare.menu_fitme.Authenication.Fitbit_Token
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit


class OkHttpGetFitme() : AsyncTask<String, Void, String>() {

    internal var eventName = ""
    internal var responseStr = ""


    internal var activity: Activity? = null



    override fun doInBackground(vararg url: String): String? {

        var httpServer = ""
        var servicePath = ""
        eventName = ""
        val accessToken : AccessToken
        var token = ""


        try {
            httpServer = url[0]
            servicePath = url[1]
            eventName = url[2]
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            token = url[3]
        } catch (e: Exception) {
            token = ""
        }

//        app_language = sharedPreferencesManager.getLanguage()


        var request: Request? = null


        val client = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
//                .sslSocketFactory(sslContext.getSocketFactory(), x509TrustManager)
            .build()
//        Log.d("coke", "token " + token)
        var token_fitbit = Fitbit_Token.fitbit_token

        Log.d("tokenFitbit", "= " + token_fitbit)
         ///////////
            request = Request.Builder()
                .header(
                    "accept",
                    "application/json"
                )
//                .header("authorization", "Bearer " + token_fitbit)
                .header("authorization", "Bearer " + token_fitbit)
                .url(httpServer + servicePath )
                .build()




        if (request != null) {
            try {
                val response = client.newCall(request).execute()
                Log.d("response", "$eventName html response is $response")

                responseStr = response.body()!!.string()
                Log.d("responseStr", "$eventName is $responseStr")

                val service_code = response.code()
                val service_message = response.message()
                if (service_code == 401) {
                    val jsonObjectError401Authen = JSONObject()
                    try {
                        jsonObjectError401Authen.put("status", "fix_code")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
//                    AuthenticationManager.logout(activity)
//                    responseStr = jsonObjectError401Authen.toString()

//                    if (!eventName.equals("api_accountLogout") && !eventName.equals("api_globalSetting")) {
//                        if (activity != null) {
//                            val logoutManage = LogoutManage()
//                            logoutManage.logout_401(activity!!)
//                        }
//                    }
                }
                if (service_code == 503) {
                    val jsonObjectError401Authen = JSONObject()
                    try {
                        jsonObjectError401Authen.put("status", "fix_code")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    responseStr = jsonObjectError401Authen.toString()

//                    if (!eventName.equals("api_accountLogout") && !eventName.equals("api_globalSetting")) {
//                        if (activity != null) {
//                            val logoutManage = LogoutManage()
//                            logoutManage.logout_503(activity!!)
//                        }
//                    }

                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return ""

    }

    override fun onPostExecute(result: String) {

        if (responseStr == "") {
            val jsonObjectTimeOut = JSONObject()
            try {
                jsonObjectTimeOut.put("status", false)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                jsonObjectTimeOut.put("alert", "Android okHttp endTime out")
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (Locale.getDefault().language.equals("th", ignoreCase = true)) {
                try {
                    jsonObjectTimeOut.put("errorMessage", "ไม่สามารถเชื่อมต่อเซิฟเวอร์ได้")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else {
                try {
                    jsonObjectTimeOut.put("errorMessage", "Cannot connect server.")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            responseStr = jsonObjectTimeOut.toString()
        }

        BusProvider.getInstance().post(OKHttpEvent(responseStr, eventName))
    }

}