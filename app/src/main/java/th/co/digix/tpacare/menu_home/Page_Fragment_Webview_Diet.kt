package th.co.digix.tpacare.menu_home

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.R
import th.co.digix.tpacare.utility.IOnBackPressed

class Page_Fragment_Webview_Diet : Fragment(), IOnBackPressed {

    var btnBack: ImageView? = null

    var imageView_logo_title: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var webview_content: WebView? = null
    var progressBackground: RelativeLayout? = null

    var reportTitle = ""
    var currentUrl = ""
    var rootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.page_fragment_webview_diet, container, false)

        initialIntentData()

        initInstanceToolbar()

        settingWebview()

        return rootView
    }

    private fun settingWebview() {

        progressBackground = rootView?.findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        webview_content?.getSettings()?.loadWithOverviewMode = true
        webview_content?.getSettings()?.useWideViewPort = true

//        webview_content?.getSettings()?.setSupportZoom(true)
        webview_content?.getSettings()?.setBuiltInZoomControls(true)
        webview_content?.getSettings()?.setDisplayZoomControls(false)

        webview_content?.getSettings()!!.setJavaScriptEnabled(true)
        webview_content?.setWebChromeClient(WebChromeClient())
        webview_content?.setWebViewClient(object : WebViewClient() {

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBackground?.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                progressBackground?.visibility = View.GONE
            }

        })

//        currentUrl =  "https://www.google.com"

        if (currentUrl.endsWith(".pdf")) {
            val googleDrive = "https://docs.google.com/viewer?embedded=true&url="
            currentUrl = googleDrive + currentUrl
            webview_content?.loadUrl(currentUrl)
        } else {
            webview_content?.loadUrl(currentUrl)
        }

    }

    private fun initialIntentData() {
        val bundle = this.arguments
        if (bundle != null) {
            try {
                reportTitle = bundle.getString("reportTitle", "")
            } catch (e: Exception) {
            }
            try {
                currentUrl = bundle.getString("currentUrl", "")
            } catch (e: Exception) {
            }
        }
    }


    private fun initInstanceToolbar() {

        imageView_logo_title = rootView?.findViewById(R.id.imageView_logo_title) as ImageView
        imageView_logo_title?.visibility = View.GONE

        textView_toolbar_title = rootView?.findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + reportTitle)

        btnBack = rootView?.findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE

        btnBack?.setOnClickListener() {
            var canGoBack = false
            try {
                canGoBack = webview_content?.canGoBack()!!
            } catch (e: Exception) {
            }

            if (canGoBack) {
                webview_content?.goBack()
            } else {
                val activityMain: ActivityMain = activity as ActivityMain
                activityMain?.replaceFragment(Page_Fragment_Home())

                activityMain?.linearLayout_menu_home?.isSelected = true
                activityMain?.linearLayout_menu_card?.isSelected = false
                activityMain?.linearLayout_menu_status?.isSelected = false
                activityMain?.linearLayout_menu_noti?.isSelected = false
                activityMain?.linearLayout_menu_setting?.isSelected = false

                activityMain?.page_select_temp = 0
            }
        }

        webview_content = rootView?.findViewById(R.id.webview_content) as WebView
    }

    override fun onBackPressed(): Boolean {
        var canGoBack = false
        try {
            canGoBack = webview_content?.canGoBack()!!
        } catch (e: Exception) {
        }

        if (canGoBack) {
            webview_content?.goBack()
        } else {
            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.replaceFragment(Page_Fragment_Home())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 0
        }
        return true
    }
}
