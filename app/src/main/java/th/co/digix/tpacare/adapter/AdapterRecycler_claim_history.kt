package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.ClaimDetailObject
import th.co.digix.tpacare.custom_object.ListOfPolDetObject
import th.co.digix.tpacare.custom_object.SaveDetailObject
import th.co.digix.tpacare.utility.DateTimeHelper
import java.util.ArrayList


class AdapterRecycler_claim_history(
    val activity: Activity?, var claimDetailObject_List: ArrayList<ClaimDetailObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (claimDetailObject_List.size > 0) {
            val claimDetailObject = claimDetailObject_List.get(position)

            holder.linearLayout_claim_detail.setOnClickListener {

            }

            holder.textView_claim_name.setText("" + claimDetailObject.clmProvider)
            holder.textView_clmPayable.setText(
                "" + claimDetailObject.clmPayable + " " + activity?.resources?.getString(
                    R.string.text_bath
                )
            )

            val dateTimeHelper = DateTimeHelper()
            val date_clmVisitDate = dateTimeHelper.parseStringToDate3("" + claimDetailObject.clmVisitDate)
            val str_clmVisitDate = dateTimeHelper.parseDateToString3(date_clmVisitDate)

            holder.textView_clmVisitDate.setText("" + str_clmVisitDate)
            holder.textView_claim_status.setText("" + claimDetailObject.clmStatusTxt)

            if (claimDetailObject.clmStatus.equals("approved", ignoreCase = true)) {
                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_accept)
            } else if (claimDetailObject.clmStatus.equals("Paid", ignoreCase = true)) {
                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_accept)
            } else if (claimDetailObject.clmStatus.equals("wait", ignoreCase = true)) {
                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_wait)
            } else if (claimDetailObject.clmStatus.equals("Rejected", ignoreCase = true)) {
                holder.imageView_claim_status.setImageResource(R.drawable.icon_claim_cancel)
            } else {
                holder.imageView_claim_status.setImageResource(R.drawable.bg_circle_profile2)
            }

            if (position % 2 == 0) {
                holder.linearLayout_claim_detail.setBackgroundColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.bg_blue_ligth
                    )
                )
            } else {
                holder.linearLayout_claim_detail.setBackgroundColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.bg_blue_heavy
                    )
                )
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_claim_detail, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (claimDetailObject_List != null) {
            var listSize = claimDetailObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var linearLayout_claim_detail: LinearLayout
        internal var textView_claim_name: TextView
        internal var textView_claim_status: TextView
        internal var textView_clmPayable: TextView
        internal var textView_clmVisitDate: TextView
        internal var imageView_claim_status: ImageView

        init {
            linearLayout_claim_detail =
                itemView.findViewById(R.id.linearLayout_claim_detail) as LinearLayout
            textView_claim_name = itemView.findViewById(R.id.textView_claim_name) as TextView
            textView_clmPayable = itemView.findViewById(R.id.textView_clmPayable) as TextView
            textView_clmVisitDate = itemView.findViewById(R.id.textView_clmVisitDate) as TextView
            textView_claim_status = itemView.findViewById(R.id.textView_claim_status) as TextView
            imageView_claim_status = itemView.findViewById(R.id.imageView_claim_status) as ImageView
        }
    }

}