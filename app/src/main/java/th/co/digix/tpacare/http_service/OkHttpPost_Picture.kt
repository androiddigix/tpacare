package th.co.digix.tpacare.http_service

import android.app.Activity
import android.os.AsyncTask
import android.util.Log
import okhttp3.Call
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONException
import org.json.JSONObject
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.SharedPreferencesManager
import java.io.IOException
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class OkHttpPost_Picture(internal var formBody: MultipartBody.Builder, defaultParameterAPI: DefaultParameterAPI) :
    AsyncTask<String, Void, String>() {

    internal var eventName = ""
    internal var responseStr = ""

    internal var parameter_udid = ""
    internal var parameter_machine = ""
    internal var parameter_app_version = ""
    internal var parameter_network = ""
    internal var parameter_platform = ""
    internal var parameter_os_version = ""
    internal var parameter_os = ""
    internal var activity: Activity

    internal var executor = Executors.newScheduledThreadPool(1)

    internal var request: Request? = null
    internal var client: OkHttpClient? = null

    init {

        activity = defaultParameterAPI.activity
        parameter_udid = defaultParameterAPI.udid
        parameter_machine = defaultParameterAPI.machine
        parameter_app_version = defaultParameterAPI.versionApp
        parameter_os_version = defaultParameterAPI.versionOS
        parameter_network = defaultParameterAPI.network
        parameter_platform = defaultParameterAPI.platform
        parameter_os = defaultParameterAPI.os
    }

    override fun doInBackground(vararg url: String): String? {

        val debug = true
//        if (debug) {
//            Log.d("debug", "default parameter udid $parameter_udid")
//            Log.d("debug", "default parameter Machine $parameter_machine")
//            Log.d("debug", "default parameter VersionApp $parameter_app_version")
//            Log.d("debug", "default parameter VersionOS $parameter_os_version")
//            Log.d("debug", "default parameter Network $parameter_network")
//            Log.d("debug", "default parameter platform $parameter_platform")
//            Log.d("debug", "default parameter os $parameter_os")
//        }

        val sharedPreferencesManager = SharedPreferencesManager(activity)
        val app_language = "EN"
//        app_language = sharedPreferencesManager.getLanguage()
        //add fix default parameter
        val formBody = this.formBody
            .addFormDataPart("UDID", "" + parameter_udid)
            .addFormDataPart("device_name", "" + parameter_machine)
            .addFormDataPart("VersionApp", "" + parameter_app_version)
            .addFormDataPart("VersionOS", "" + parameter_os_version)
            .addFormDataPart("Network", "" + parameter_network)
            .addFormDataPart("platform", "" + parameter_platform)
            .addFormDataPart("OS", "" + parameter_os)
            .addFormDataPart("lang", "" + app_language)
            .setType(MultipartBody.FORM)
            .build()


        var httpServer = ""
        var servicePath = ""
        eventName = ""
        var token = ""

        try {
            httpServer = url[0]
            servicePath = url[1]
            eventName = url[2]
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            token = url[3]
        } catch (e: Exception) {
            token = ""
        }


        var tpa_access_token = ""


        val userObject = sharedPreferencesManager.getUserObject()
        if (!userObject.token.equals("")) {
            token = userObject.token
        }
        if (!userObject.tpa_access_token.equals("")) {
            tpa_access_token = userObject.tpa_access_token
        }

//            val certPinner = CertificatePinner.Builder()
//                    .add("appmattus.com","sha256/4hw5tz+scE+TW+mlai5YipDfFWn1dqvfLG+nU7tq1V8=")
//                    .build()

//        val resourceStream = activity.resources.openRawResource(R.raw.certificate)
//        val keyStoreType = KeyStore.getDefaultType()
//        val keyStore = KeyStore.getInstance(keyStoreType)
//        val cf: CertificateFactory = CertificateFactory.getInstance("X.509")
//        val ca: X509Certificate = resourceStream.use {
//            cf.generateCertificate(it) as X509Certificate
//        }
////        keyStore.load(resourceStream, null)
//        keyStore.apply {
//            load(null, null)
//            setCertificateEntry("ca", ca)
//        }
//
//        val trustManagerAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
//        val trustManagerFactory = TrustManagerFactory.getInstance(trustManagerAlgorithm)
//        trustManagerFactory.init(keyStore)
//
//        val sslContext = SSLContext.getInstance("TLS")
//        sslContext.init(null, trustManagerFactory.trustManagers, null)
//
//        val x509TrustManager = trustManagerFactory.trustManagers[0] as X509TrustManager

        client = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
//                .sslSocketFactory(sslContext.getSocketFactory(), x509TrustManager)
            .build()

        if (tpa_access_token.equals("")) {
            request = Request.Builder()
                .header(
                    "Accept",
                    "text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8, application/json"
                )
                .header("Authorization", "Bearer " + token)
                .url(httpServer + servicePath)
                .tag(TAG)
                .post(formBody)
                .build()
        } else {
            request = Request.Builder()
                .header(
                    "Accept",
                    "text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8, application/json"
                )
                .header("Authorization", "Bearer " + token)
                .header("TPACareAuthorization", tpa_access_token)
                .url(httpServer + servicePath)
                .tag(TAG)
                .post(formBody)
                .build()
        }

        if (request != null) {
            try {
                val response = client!!.newCall(request).execute()
                Log.d("response", "$eventName html response is $response")

                responseStr = response.body()!!.string()
                Log.d("responseStr", "$eventName is $responseStr")

                val service_code = response.code()
                val service_message = response.message()
                if (service_code == 401) {
                    val jsonObjectError401Authen = JSONObject()
                    try {
                        jsonObjectError401Authen.put("status", "fix_code")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    responseStr = jsonObjectError401Authen.toString()

//                    if (!eventName.equals("api_accountLogout") && !eventName.equals("api_globalSetting")) {
//                        if (activity != null) {
//                            val logoutManage = LogoutManage()
//                            logoutManage.logout_401(activity!!)
//                        }
//                    }
                }
                if (service_code == 503) {
                    val jsonObjectError401Authen = JSONObject()
                    try {
                        jsonObjectError401Authen.put("status", "fix_code")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    responseStr = jsonObjectError401Authen.toString()

//                    if (!eventName.equals("api_accountLogout") && !eventName.equals("api_globalSetting")) {
//                        if (activity != null) {
//                            val logoutManage = LogoutManage()
//                            logoutManage.logout_503(activity!!)
//                        }
//                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return ""
    }

    override fun onPostExecute(result: String) {

        if (responseStr == "") {
            val jsonObjectTimeOut = JSONObject()
            try {
                jsonObjectTimeOut.put("status", false)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            try {
                jsonObjectTimeOut.put("alert", "Android okHttp time out")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            if (Locale.getDefault().language.equals("th", ignoreCase = true)) {
                try {
                    jsonObjectTimeOut.put("errorMessage", "ไม่สามารถเชื่อมต่อเซิฟเวอร์ได้")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            } else {
                try {
                    jsonObjectTimeOut.put("errorMessage", "Cannot connect server.")
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }
            responseStr = jsonObjectTimeOut.toString()
        }

        BusProvider.getInstance().post(OKHttpEvent(responseStr, eventName))
    }

    fun cancelCallService() {
        var call: Call? = null
        if (client != null && request != null) {
            call = client!!.newCall(request)
        }

        val finalCall = call
        executor.schedule({ finalCall!!.cancel() }, 1, TimeUnit.SECONDS)

    }

    companion object {
        private val TAG = "BackgroundWebRunner"
    }

}