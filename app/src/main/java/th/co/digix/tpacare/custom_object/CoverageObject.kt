package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class CoverageObject() : Parcelable {

    var cov_no: String = ""
    var cov_desc: String = ""
    var cov_limit: String = ""
    var cov_utilized: String = ""

    constructor(parcel: Parcel) : this() {
        cov_no = parcel.readString()
        cov_desc = parcel.readString()
        cov_limit = parcel.readString()
        cov_utilized = parcel.readString()
    }

    fun toJsonEdit(): JSONObject {
        var jsonObject = JSONObject()
        try {
            jsonObject.put("cov_no", cov_no)
            jsonObject.put("cov_desc", cov_desc)
            jsonObject.put("cov_limit", cov_limit)
            jsonObject.put("cov_utilized", cov_utilized)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return jsonObject
    }
    fun setParam(result: JSONObject) {
        try {
            cov_no = result.getString("cov_no")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            cov_desc = result.getString("cov_desc")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            cov_limit = result.getString("cov_limit")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            cov_utilized = result.getString("cov_utilized")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(cov_no)
        parcel.writeString(cov_desc)
        parcel.writeString(cov_limit)
        parcel.writeString(cov_utilized)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CoverageObject> {
        override fun createFromParcel(parcel: Parcel): CoverageObject {
            return CoverageObject(parcel)
        }

        override fun newArray(size: Int): Array<CoverageObject?> {
            return arrayOfNulls(size)
        }
    }

}