package com.pvdproject.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.HospitalObject
import th.co.digix.tpacare.menu_card.Fragment_Card_Policy
import java.text.DecimalFormat
import java.util.ArrayList


class AdapterRecycler_card_hospital(
    val activity: Activity?,
    val fragment: Fragment_Card_Policy?,
    var hospitalObject_List: ArrayList<HospitalObject>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder

        if (position < hospitalObject_List.size) {
            val hospitalObject = hospitalObject_List.get(position)

            val hospital_name = "" + hospitalObject.hospital_name
            var hospital_distance = "" + hospitalObject.nearby

            val mFormat = DecimalFormat("###,###,##0.00")
            var nearby_int = 0.0
            try {
                nearby_int = hospital_distance.toDouble()
                hospital_distance = "" + mFormat.format(nearby_int)
            } catch (e: Exception) {
            }

            holder.textView_hospital_name.setText("" + hospital_name)
            holder.textView_hospital_distance.setText("" + hospital_distance + " " + activity!!.resources.getString(R.string.text_distance_unit))

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.item_card_hospital, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        if (hospitalObject_List != null) {
            var listSize = hospitalObject_List.size
            return listSize
        }
        return 0
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var textView_hospital_name: TextView
        internal var textView_hospital_distance: TextView

        init {
            textView_hospital_name = itemView.findViewById(R.id.textView_hospital_name) as TextView
            textView_hospital_distance = itemView.findViewById(R.id.textView_hospital_distance) as TextView
        }
    }

}