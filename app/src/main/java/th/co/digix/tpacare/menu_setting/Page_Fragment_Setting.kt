package th.co.digix.tpacare.menu_setting

import android.Manifest
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rm.rmswitch.RMSwitch
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.BuildConfig
import th.co.digix.tpacare.R
import th.co.digix.tpacare.menu_home.Page_Fragment_Home
import th.co.digix.tpacare.utility.LogoutManage
import th.co.digix.tpacare.utility.SharedPreferencesManager

class Page_Fragment_Setting : Fragment() {

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var imageView_profile: ImageView? = null
    var textView_username: TextView? = null

    var rmswitch_fingerprint: RMSwitch? = null

    var linearLayout_edit_profile: LinearLayout? = null
    var linearLayout_change_password: LinearLayout? = null
    var linearLayout_use_fingerprint: LinearLayout? = null
    var linearLayout_term_of_service: LinearLayout? = null
    var linearLayout_logout: LinearLayout? = null

    var textView_version: TextView? = null

    var rootView: View? = null

    private var mFingerprintManager: FingerprintManagerCompat? = null
    private var mKeyguardManager: KeyguardManager? = null

    var canFingerprint = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.page_fragment_setting, container, false)

        initInstanceToolbar()

        checkFingerPrint()

        initialView()

        return rootView
    }


    override fun onResume() {
        super.onResume()
        initialProfileImage()

    }

    private fun initialProfileImage() {

        imageView_profile = rootView?.findViewById(R.id.imageView_profile) as ImageView
        textView_username = rootView?.findViewById(R.id.textView_username) as TextView

        val sharedPreferencesManager = SharedPreferencesManager(rootView?.context!!)
        val userObject = sharedPreferencesManager.getUserObject()

        if (userObject.state_login.equals("login")) {
            textView_username?.setText(resources.getString(R.string.text_hello_prefix) + userObject.first_name + " " + userObject.last_name)

            if (imageView_profile != null) {

                val url_image = "" + userObject.image_profile
                val placeholder = BitmapFactory.decodeResource(getResources(), R.drawable.icon_profile_default_home)
                val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), placeholder)
                circularBitmapDrawable.isCircular = true
                Glide.with(this)
                    .load(url_image)
                    .apply(
                        RequestOptions
                            .circleCropTransform()
                            .placeholder(circularBitmapDrawable)
                            .error(circularBitmapDrawable)
                    )
                    .into(imageView_profile!!)
            }
        } else {
            textView_username?.setText("-")
        }


    }

    private fun initialView() {
        var versionCode = BuildConfig.VERSION_CODE
        var versionName = BuildConfig.VERSION_NAME

        textView_version = rootView?.findViewById(R.id.textView_version) as TextView
        textView_version?.setText(resources.getString(R.string.text_version) + " " + versionName + " ("+versionCode+")")

        linearLayout_edit_profile = rootView?.findViewById(R.id.linearLayout_edit_profile) as LinearLayout
        linearLayout_change_password = rootView?.findViewById(R.id.linearLayout_change_password) as LinearLayout
        linearLayout_use_fingerprint = rootView?.findViewById(R.id.linearLayout_use_fingerprint) as LinearLayout
        linearLayout_term_of_service = rootView?.findViewById(R.id.linearLayout_term_of_service) as LinearLayout
        rmswitch_fingerprint = rootView?.findViewById(R.id.rmswitch_fingerprint) as RMSwitch
        linearLayout_logout = rootView?.findViewById(R.id.linearLayout_logout) as LinearLayout

        val sharedPreferencesManager = SharedPreferencesManager(rootView?.context!!)
        val userObject = sharedPreferencesManager.getUserObject()

        if (userObject.state_login.equals("login")) {
            linearLayout_edit_profile?.visibility = View.VISIBLE
            linearLayout_edit_profile?.setOnClickListener() {
                val intent_go = Intent(activity, Page_EditProfile::class.java)
                startActivity(intent_go)
            }

            linearLayout_change_password?.visibility = View.VISIBLE
            linearLayout_change_password?.setOnClickListener() {
                val intent_go = Intent(activity, Page_ChangePassword::class.java)
                startActivity(intent_go)
            }

            if (canFingerprint) {
                linearLayout_use_fingerprint?.visibility = View.VISIBLE
            } else {
                linearLayout_use_fingerprint?.visibility = View.GONE
            }
            val isUseFingerPrint = sharedPreferencesManager.getUseFingerPrint()
            rmswitch_fingerprint?.isChecked = isUseFingerPrint
            rmswitch_fingerprint?.addSwitchObserver(RMSwitch.RMSwitchObserver { switchView, isChecked ->
                sharedPreferencesManager.setUseFingerPrint(isChecked)
            })

            linearLayout_term_of_service?.visibility = View.VISIBLE
            linearLayout_term_of_service?.setOnClickListener() {
                val intent_go = Intent(activity, Page_Setting_Term_of_Service::class.java)
                startActivity(intent_go)
            }

            linearLayout_logout?.visibility = View.VISIBLE
            linearLayout_logout?.setOnClickListener() {
                show_alertDialog_logout(activity!!)
            }
        } else {

            linearLayout_edit_profile?.visibility = View.GONE
            linearLayout_change_password?.visibility = View.GONE
            linearLayout_use_fingerprint?.visibility = View.GONE

            linearLayout_term_of_service?.visibility = View.VISIBLE
            linearLayout_term_of_service?.setOnClickListener() {
                val intent_go = Intent(activity, Page_Setting_Term_of_Service::class.java)
                startActivity(intent_go)
            }

            linearLayout_logout?.visibility = View.GONE
        }
    }


    private fun show_alertDialog_logout(context: Context) {
        val builder = android.support.v7.app.AlertDialog.Builder(context)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(context).inflate(R.layout.popup_logout, null)

        val textView_popup_cancel = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_cancel) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_cancel.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            var logoutManage = LogoutManage()
            logoutManage.logout_mainmenu(activity!!)
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(true)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }


    private fun initInstanceToolbar() {
        btnBack = rootView?.findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener {
            val activityMain: ActivityMain = activity as ActivityMain
            activityMain?.replaceFragment(Page_Fragment_Home())

            activityMain?.linearLayout_menu_home?.isSelected = true
            activityMain?.linearLayout_menu_card?.isSelected = false
            activityMain?.linearLayout_menu_status?.isSelected = false
            activityMain?.linearLayout_menu_noti?.isSelected = false
            activityMain?.linearLayout_menu_setting?.isSelected = false

            activityMain?.page_select_temp = 0
        }

        textView_toolbar_title = rootView?.findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_setting))
    }

    private fun checkFingerPrint() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            try {
                mKeyguardManager = activity?.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            } catch (e: Exception) {
                e.printStackTrace()
            }
            try {
                mFingerprintManager = FingerprintManagerCompat.from(context!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }


            if (ActivityCompat.checkSelfPermission(
                    activity!!, Manifest.permission.USE_FINGERPRINT
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // Checking fingerprint permission.
                Log.d("tpa", "Checking fingerprint permission.")
            } else if (mKeyguardManager == null) {
                // Checking Device support fingerprint or not.
                Log.d("tpa", "Checking Device support fingerprint or not.")
            } else if (mFingerprintManager == null) {
                // Checking Device support fingerprint or not.
                Log.d("tpa", "Checking Device support fingerprint or not.")
            } else if (!mFingerprintManager!!.isHardwareDetected()) {
                Log.d("tpa", "Checking Device support fingerprint or not.")
                // Checking Device support fingerprint or not.
            } else if (!mKeyguardManager!!.isKeyguardSecure()) {
                Log.d("tpa", "Not setting lock screen with imprint.")
                // Not setting lock screen with imprint.
            } else if (!mFingerprintManager!!.hasEnrolledFingerprints()) {
                // Not registered at least one fingerprint in Settings.
                Log.d("tpa", "Not registered at least one fingerprint in Settings.")
            } else {
                canFingerprint = true
            }

        }


    }

}
