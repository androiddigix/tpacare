package th.co.digix.tpacare.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager


class Page_FingerPrint_Set : BaseActivity() {

    var imageView_right: ImageView? = null
    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var progressBackground: RelativeLayout? = null

    var linearLayout_use_now: LinearLayout? = null
    var linearLayout_use_later: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        setContentView(R.layout.page_fingerprint_set)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        setOnClick()

    }

    private fun initInstanceToolbar() {
        imageView_right = findViewById(R.id.imageView_right) as ImageView
        imageView_right?.visibility = View.GONE


        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.GONE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.GONE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_login))
    }


    private fun initialIntentData() {
//        isOnBackButton = intent.getBooleanExtra("isOnBackButton", false)
    }

    private fun setOnClick() {

        linearLayout_use_now?.setOnClickListener() {
            val intent_go = Intent(this, ActivityMain::class.java)
            intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent_go)

            val sharedPreferencesManager =  SharedPreferencesManager(this)
            sharedPreferencesManager.setUseFingerPrint(true)

        }
        linearLayout_use_later?.setOnClickListener() {
            val intent_go = Intent(this, ActivityMain::class.java)
            intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent_go)

            val sharedPreferencesManager =  SharedPreferencesManager(this)
            sharedPreferencesManager.setUseFingerPrint(false)
        }

    }


    private fun initialView() {
        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        linearLayout_use_now = findViewById(R.id.linearLayout_use_now) as LinearLayout
        linearLayout_use_later = findViewById(R.id.linearLayout_use_later) as LinearLayout

    }

}