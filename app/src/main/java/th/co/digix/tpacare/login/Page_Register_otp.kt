package th.co.digix.tpacare.login

import android.Manifest
import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.ActivityMain
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.UserObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager


class Page_Register_otp : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authRegister = httpServerURL.api_authRegister
    val api_authRegisterRequestOTP = httpServerURL.api_authRegisterRequestOTP
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var linearLayout_root_bg: LinearLayout? = null
    var progressBackground: RelativeLayout? = null

    var textView_popup_title: TextView? = null
    var textView_card_re_otp: TextView? = null
    var linearLayout_card_re_otp: LinearLayout? = null
    var linearLayout_popup_ok: LinearLayout? = null

    var linearLayout_idcard_form: LinearLayout? = null
    var edittext_idcard_form: EditText? = null
    
    var textView_idcard_1: TextView? = null
    var textView_idcard_2: TextView? = null
    var textView_idcard_3: TextView? = null
    var textView_idcard_4: TextView? = null
    var textView_idcard_5: TextView? = null
    var textView_idcard_6: TextView? = null
    
    var userName = ""
    var password = ""
    var confirm_password = ""
    var first_name = ""
    var last_name = ""
    var phone = ""

    var otp_code = ""

    var canFingerprint = false
//    var idcard = ""

    private var mFingerprintManager: FingerprintManagerCompat? = null
    private var mKeyguardManager: KeyguardManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        setContentView(R.layout.page_register_otp)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        setOnClick()

        checkFingerPrint()

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_login))
    }

    override fun onDestroy() {
        super.onDestroy()

        BusProvider.getInstance().unregister(this)
    }

    private fun initialIntentData() {
        userName = intent.extras.getString("userName", "")
        password = intent.extras.getString("password", "")
        confirm_password = intent.extras.getString("confirm_password", "")
        first_name = intent.extras.getString("first_name", "")
        last_name = intent.extras.getString("last_name", "")
        phone = intent.extras.getString("phone_new", "")
    }

    private fun setOnClick() {

    }

    private fun initialView() {
        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        linearLayout_root_bg = findViewById(R.id.linearLayout_root_bg) as LinearLayout
        linearLayout_root_bg?.setOnClickListener { }
        linearLayout_root_bg?.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                //Find the currently focused view, so we can grab the correct window token from it.
                var view = getCurrentFocus()
                //If no view currently has focus, create a new one, just so we can grab a window token from it
                if (view == null) {
                    view = View(this)
                }
                imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)

                if (otp_code.length == 0) {
                    textView_idcard_1?.setText("")
                    textView_idcard_1?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
                } else if (otp_code.length == 1) {
                    textView_idcard_2?.setText("")
                    textView_idcard_2?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
                } else if (otp_code.length == 2) {
                    textView_idcard_3?.setText("")
                    textView_idcard_3?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
                } else if (otp_code.length == 3) {
                    textView_idcard_4?.setText("")
                    textView_idcard_4?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
                } else if (otp_code.length == 4) {
                    textView_idcard_5?.setText("")
                    textView_idcard_5?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
                } else if (otp_code.length == 5) {
                    textView_idcard_6?.setText("")
                    textView_idcard_6?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
                }
            }

        }

        textView_popup_title = findViewById(R.id.textView_popup_title) as TextView
        textView_card_re_otp = findViewById(R.id.textView_card_re_otp) as TextView
        linearLayout_card_re_otp = findViewById(R.id.linearLayout_card_re_otp) as LinearLayout
        linearLayout_popup_ok = findViewById(R.id.linearLayout_popup_ok) as LinearLayout

        
        linearLayout_idcard_form = findViewById(R.id.linearLayout_idcard_form) as LinearLayout
        edittext_idcard_form = findViewById(R.id.edittext_idcard_form) as EditText

        textView_idcard_1 = findViewById(R.id.textView_idcard_1) as TextView
        textView_idcard_2 = findViewById(R.id.textView_idcard_2) as TextView
        textView_idcard_3 = findViewById(R.id.textView_idcard_3) as TextView
        textView_idcard_4 = findViewById(R.id.textView_idcard_4) as TextView
        textView_idcard_5 = findViewById(R.id.textView_idcard_5) as TextView
        textView_idcard_6 = findViewById(R.id.textView_idcard_6) as TextView

        linearLayout_idcard_form?.setOnClickListener {
            if (otp_code.length == 0) {
                textView_idcard_1?.setText("|")
                textView_idcard_1?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
            } else if (otp_code.length == 1) {
                textView_idcard_2?.setText("|")
                textView_idcard_2?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
            } else if (otp_code.length == 2) {
                textView_idcard_3?.setText("|")
                textView_idcard_3?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
            } else if (otp_code.length == 3) {
                textView_idcard_4?.setText("|")
                textView_idcard_4?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
            } else if (otp_code.length == 4) {
                textView_idcard_5?.setText("|")
                textView_idcard_5?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
            } else if (otp_code.length == 5) {
                textView_idcard_6?.setText("|")
                textView_idcard_6?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_black))
            }

            edittext_idcard_form?.requestFocus()

            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = getCurrentFocus()
            if (view == null) {
                view = View(this)
            }
            imm.showSoftInput(view, 0)

        }

        edittext_idcard_form?.addTextChangedListener(
            onTextChangedSetControlEdittext(
            )
        )
        
        var card_telephone = "" + phone
        try {
            card_telephone =
                card_telephone.substring(0, 3) + "-XXX-X" + card_telephone.substring(7, card_telephone.length)
        } catch (e: Exception) {
        }

        try {
            val text_card_add_number = "<u>" + resources.getString(R.string.popup_card_re_otp) + "</u>"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView_card_re_otp?.setText(
                    Html.fromHtml(text_card_add_number, Html.FROM_HTML_MODE_COMPACT)
                )
            } else {
                textView_card_re_otp?.setText(
                    Html.fromHtml(text_card_add_number)
                )
            }
        } catch (e: Exception) {
        }
        textView_popup_title?.setText(resources.getString(R.string.popup_card_otp_title) + " " + card_telephone)

        linearLayout_card_re_otp?.setOnClickListener(View.OnClickListener {
            var userName_encrypt = ""
            var password_encrypt = ""
            var phone_encrypt = ""

            val cryptLib = CryptLib()
            userName_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(userName, salt_key)
            password_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(password, salt_key)
            phone_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone, salt_key)

            val defaultParameterAPI = DefaultParameterAPI(this)
            val formBody = FormBody.Builder()
            formBody.add("username", "" + userName_encrypt)
            formBody.add("password", "" + password_encrypt)
            formBody.add("first_name", "" + first_name)
            formBody.add("last_name", "" + last_name)
            formBody.add("tel", "" + phone_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url,
                api_authRegisterRequestOTP,
                "api_authRegisterRequestOTP_re"
            )

            progressBackground?.visibility = View.VISIBLE

        })

        linearLayout_popup_ok?.setOnClickListener(View.OnClickListener {

            var userName_encrypt = ""
            var password_encrypt = ""
            var phone_encrypt = ""
            var idcard_encrypt = ""

            val cryptLib = CryptLib()
            userName_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(userName, salt_key)
            password_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(password, salt_key)
            phone_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone, salt_key)
            idcard_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(otp_code, salt_key)


            val defaultParameterAPI = DefaultParameterAPI(this)
            val formBody = FormBody.Builder()
            formBody.add("otp_code", "" + idcard_encrypt)
            formBody.add("username", "" + userName_encrypt)
            formBody.add("password", "" + password_encrypt)
            formBody.add("first_name", "" + first_name)
            formBody.add("last_name", "" + last_name)
            formBody.add("tel", "" + phone_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_authRegister, "api_authRegister"
            )

            progressBackground?.visibility = View.VISIBLE

        })

    }

    private fun onTextChangedSetControlEdittext(): TextWatcher {

        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                Log.d("coke", "onTextChanged s " + s + " start " + start + " before " + before + " count " + count)

                var textChange = s.toString()
                otp_code = "" + textChange

                var otp_code_1 = ""
                var otp_code_2 = ""
                var otp_code_3 = ""
                var otp_code_4 = ""
                var otp_code_5 = ""
                var otp_code_6 = ""
                try {
                    otp_code_1 = "" + s.get(0)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_2 = "" + s.get(1)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_3 = "" + s.get(2)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_4 = "" + s.get(3)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_5 = "" + s.get(4)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_6 = "" + s.get(5)
                } catch (e: java.lang.Exception) {
                }


                textView_idcard_1?.setText("" + otp_code_1)
                textView_idcard_2?.setText("" + otp_code_2)
                textView_idcard_3?.setText("" + otp_code_3)
                textView_idcard_4?.setText("" + otp_code_4)
                textView_idcard_5?.setText("" + otp_code_5)
                textView_idcard_6?.setText("" + otp_code_6)

                textView_idcard_1?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_blue))
                textView_idcard_2?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_blue))
                textView_idcard_3?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_blue))
                textView_idcard_4?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_blue))
                textView_idcard_5?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_blue))
                textView_idcard_6?.setTextColor(ContextCompat.getColor(this@Page_Register_otp, R.color.text_blue))

                if (textChange.length == 0) {
                    textView_idcard_1?.setText("|")
                    textView_idcard_1?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_Register_otp,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 1) {
                    textView_idcard_2?.setText("|")
                    textView_idcard_2?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_Register_otp,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 2) {
                    textView_idcard_3?.setText("|")
                    textView_idcard_3?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_Register_otp,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 3) {
                    textView_idcard_4?.setText("|")
                    textView_idcard_4?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_Register_otp,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 4) {
                    textView_idcard_5?.setText("|")
                    textView_idcard_5?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_Register_otp,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 5) {
                    textView_idcard_6?.setText("|")
                    textView_idcard_6?.setTextColor(
                        ContextCompat.getColor(
                            this@Page_Register_otp,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 6) {
                    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    var view = getCurrentFocus()
                    if (view == null) {
                        view = View(this@Page_Register_otp)
                    }
                    imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        }
    }

    private fun checkFingerPrint() {

        val sharedPreferencesManager = SharedPreferencesManager(this)
        val isUseFingerPrint = sharedPreferencesManager.getUseFingerPrint()
        if (isUseFingerPrint) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                try {
                    mKeyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    mFingerprintManager =  FingerprintManagerCompat.from(this@Page_Register_otp)
                } catch (e: Exception) {
                    e.printStackTrace()
                }


                if (ActivityCompat.checkSelfPermission(
                        this, Manifest.permission.USE_FINGERPRINT
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // Checking fingerprint permission.
                    Log.d("tpa", "Checking fingerprint permission.")
                } else if (mKeyguardManager == null) {
                    // Checking Device support fingerprint or not.
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                } else if (mFingerprintManager == null) {
                    // Checking Device support fingerprint or not.
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                } else if (!mFingerprintManager!!.isHardwareDetected()) {
                    Log.d("tpa", "Checking Device support fingerprint or not.")
                    // Checking Device support fingerprint or not.
                } else if (!mKeyguardManager!!.isKeyguardSecure()) {
                    Log.d("tpa", "Not setting lock screen with imprint.")
                    // Not setting lock screen with imprint.
                } else if (!mFingerprintManager!!.hasEnrolledFingerprints()) {
                    // Not registered at least one fingerprint in Settings.
                    Log.d("tpa", "Not registered at least one fingerprint in Settings.")
                } else {
                    canFingerprint = true
                }
            }
        } else {
        }

    }


    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authRegisterRequestOTP_re")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {


                    }
                    message = "" + resources.getString(R.string.text_success)
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_Register_otp, "" + message)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_Register_otp, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register_otp, resources.getString(R.string.text_service_error)
                )
            }
        }
        if (event.getHttpName().equals("api_authRegister")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    val userObject = UserObject()
                    if (result != null) {
                        userObject.setParam(result)
                        val sharedPreferencesManager = SharedPreferencesManager(this)
                        userObject.state_login = "login"
                        sharedPreferencesManager.setUserObject(userObject)

                        if (canFingerprint) {
                            val intent_go = Intent(this@Page_Register_otp, Page_FingerPrint_Set::class.java)
                            intent_go.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent_go)
                        } else {
                            val intent_go = Intent(this@Page_Register_otp, ActivityMain::class.java)
                            intent_go.putExtra("userObject", userObject)
                            startActivity(intent_go)
                        }
                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_Register_otp, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register_otp, resources.getString(R.string.text_service_error)
                )
            }
        }

    }

}