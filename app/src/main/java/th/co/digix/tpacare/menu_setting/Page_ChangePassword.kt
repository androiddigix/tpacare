package th.co.digix.tpacare.menu_setting

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager

class Page_ChangePassword : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authChangePassword = httpServerURL.api_authChangePassword
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var progressBackground: RelativeLayout? = null

    var editText_current_password: EditText? = null
    var editText_new_password: EditText? = null
    var editText_new_password_confirm: EditText? = null

    var linearLayout_confirm_username: LinearLayout? = null
    var linearLayout_cancel_username: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_change_password)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        initialWorking()
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialWorking() {
        linearLayout_cancel_username?.setOnClickListener {
            finish()
        }
        linearLayout_confirm_username?.setOnClickListener(View.OnClickListener {

            var current_password = editText_current_password?.text.toString()
            var new_password = editText_new_password?.text.toString()
            var new_confirm_password = editText_new_password_confirm?.text.toString()

            if (current_password.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_ChangePassword,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_old_password)
                )
            } else if (new_password.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_ChangePassword,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_new_password)
                )
            } else if (new_confirm_password.equals("")) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_ChangePassword,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_new_password_confirm)
                )
            } else if (!new_confirm_password.equals("" + new_password)) {
                var alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_ChangePassword,
                    "" + resources.getString(R.string.text_alert_new_password_not_match)
                )
            } else {
                var new_password_encrypt = ""
                var oldpassword_encrypt = ""


                val sharedPreferencesManager = SharedPreferencesManager(this)
                val userObject = sharedPreferencesManager.getUserObject()

                val userName = userObject.username

                val cryptLib = CryptLib()
                oldpassword_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(current_password, salt_key)
                new_password_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(new_password, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(this)
                val formBody = FormBody.Builder()
                formBody.add("oldpassword", "" + oldpassword_encrypt)
                formBody.add("newpassword", "" + new_password_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url, api_authChangePassword, "api_authChangePassword"
                )

                progressBackground?.visibility = View.VISIBLE
            }
        })
    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        editText_current_password = findViewById(R.id.editText_current_password) as EditText
        editText_new_password = findViewById(R.id.editText_new_password) as EditText
        editText_new_password_confirm = findViewById(R.id.editText_new_password_confirm) as EditText
        editText_new_password_confirm?.addTextChangedListener(
            onTextChangedEditText_closekeyboard()
        )

        linearLayout_confirm_username = findViewById(R.id.linearLayout_confirm_username) as LinearLayout
        linearLayout_cancel_username = findViewById(R.id.linearLayout_cancel_username) as LinearLayout

    }

    private fun initialIntentData() {

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.text_changepassword_title))
    }

    private fun onTextChangedEditText_closekeyboard(): TextWatcher {

        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                Log.d("coke", "onTextChanged s " + s + " start " + start + " before " + before + " count " + count)
                var originalString = s.toString()
                if (originalString.length == 6) {

                    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    //Find the currently focused view, so we can grab the correct window token from it.
                    var view = getCurrentFocus()
                    //If no view currently has focus, create a new one, just so we can grab a window token from it
                    if (view == null) {
                        view = View(this@Page_ChangePassword)
                    }
                    imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
                }

            }

            override fun afterTextChanged(s: Editable) {

            }
        }
    }


    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authChangePassword")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result = ""
                    try {
                        result = responseJson.getString("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    message = "" + resources.getString(R.string.text_change_password_success)
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog_finish(this@Page_ChangePassword, "" + message)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_ChangePassword, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_ChangePassword, resources.getString(R.string.text_service_error)
                )
            }
        }

    }

}