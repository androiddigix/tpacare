package th.co.digix.tpacare.http_service;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by android on 20/1/2559.
 */
public class CountingFileRequestBody extends RequestBody {

    private static final int SEGMENT_SIZE = 2048; // okio.Segment.SIZE

    private final File file;
    private final ProgressListener listener;
    private final String contentType;

    public CountingFileRequestBody(File file, String contentType, ProgressListener listener) {
        this.file = file;
        this.contentType = contentType;
        this.listener = listener;
    }

    @Override
    public long contentLength() {
        return file.length();
    }

    @Override
    public MediaType contentType() {
        return MediaType.parse(contentType);
    }

    @Override
    public void writeTo(BufferedSink sink) {
        Source source = null;
        try {
            try {
                source = Okio.source(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            long total = 0;
            long read;

            if(source!=null){
                try {
                    while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                        total += read;
                        sink.flush();
                        this.listener.transferred(total);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } finally {
            Util.closeQuietly(source);
        }
    }

    public interface ProgressListener {
        void transferred(long totalUpload);
    }

}