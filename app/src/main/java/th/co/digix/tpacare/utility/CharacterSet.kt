package th.co.digix.tpacare.utility

class CharacterSet {

    fun checkUsername(baseString: String): Boolean {
//        val pattern = "[a-zA-Z0-9]+".toRegex()
//        return pattern.matches(baseString)

        var number_count = 0
        var text_count = 0
        var symbol_count = 0

        for (i in 0..baseString.length - 1) {
            var char_name = baseString[i].toChar()

            val ascii = char_name.toInt()

            if (ascii >= 97 && ascii <= 122) {
                //lower case
                text_count++
            } else if (ascii >= 65 && ascii <= 90) {
                //upper case
                text_count++
            } else if (ascii >= 48 && ascii <= 57) {
                number_count++
            } else {
                symbol_count++
            }
        }

        if (symbol_count > 0) {
            return false
        } else if (number_count == 0 && text_count == 0) {
            return false
        } else {
            return true
        }
    }

    fun checkPhone(baseString: String): Boolean {
        if (baseString.startsWith("06")) {
            return true
        } else if (baseString.startsWith("08")) {
            return true
        } else if (baseString.startsWith("09")) {
            return true
        }
        return false
    }

    fun removeAllSymbol(baseString: String): String {
        var result = ""
        result = baseString.replace("!", "")
        result = result.replace("@", "")
        result = result.replace("#", "")
        result = result.replace("$", "")
        result = result.replace("%", "")
        result = result.replace("^", "")
        result = result.replace("&", "")
        result = result.replace("*", "")
        result = result.replace("(", "")
        result = result.replace(")", "")
        result = result.replace("-", "")
        result = result.replace("_", "")
        result = result.replace("=", "")
        result = result.replace("+", "")
        result = result.replace("฿", "")
        result = result.replace("/", "")
        result = result.replace("\\", "")
        result = result.replace("|", "")
        result = result.replace("]", "")
        result = result.replace("[", "")
        result = result.replace("}", "")
        result = result.replace("{", "")
        result = result.replace(":", "")
        result = result.replace(";", "")
        result = result.replace("'", "")
        result = result.replace("\"", "")
        result = result.replace("?", "")
        result = result.replace(".", "")
        result = result.replace(">", "")
        result = result.replace("<", "")
        result = result.replace(",", "")
        result = result.replace("~", "")
        result = result.replace("`", "")
        result = result.replace(" ", "")

        return result

    }


}