package th.co.digix.tpacare.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.custom_object.UserObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.*


class Page_Register : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val api_authRegisterRequestOTP = httpServerURL.api_authRegisterRequestOTP
    val salt_key = httpServerURL.salt_key

    var btnBack: ImageView? = null
    var textView_toolbar_title: TextView? = null

    var editText_UserName: EditText? = null
    var editText_Password: EditText? = null
    var editText_Confirm_Password: EditText? = null
    var editText_first_name: EditText? = null
    var editText_last_name: EditText? = null
    var editText_phone: EditText? = null

    var progressBackground: RelativeLayout? = null
    var linearLayout_register: LinearLayout? = null

    var userName = ""
    var password = ""
    var confirm_password = ""
    var first_name = ""
    var last_name = ""
    var phone = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        setContentView(R.layout.page_register)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initInstanceToolbar()

        initialView()

        setOnClick()

    }

    private fun initInstanceToolbar() {
        btnBack = findViewById(R.id.btnBack) as ImageView
        btnBack?.visibility = View.VISIBLE
        btnBack?.setOnClickListener() {
            finish()
        }

        textView_toolbar_title = findViewById(R.id.textView_toolbar_title) as TextView
        textView_toolbar_title?.visibility = View.VISIBLE
        textView_toolbar_title?.setText("" + resources.getString(R.string.title_register))
    }

    override fun onDestroy() {
        super.onDestroy()

        BusProvider.getInstance().unregister(this)
    }

    private fun initialIntentData() {
//        isOnBackButton = intent.getBooleanExtra("isOnBackButton", false)
    }

    private fun setOnClick() {

        linearLayout_register?.setOnClickListener() {

            userName = editText_UserName?.text.toString().trim()
            password = editText_Password?.text.toString().trim()
            confirm_password = editText_Confirm_Password?.text.toString().trim()
            first_name = editText_first_name?.text.toString().trim()
            last_name = editText_last_name?.text.toString().trim()
            phone = editText_phone?.text.toString().trim()

            val characterSet = CharacterSet()

            if (userName.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_username)
                )
            } else if (!characterSet.checkUsername(userName)) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_alert_username_worng)
                )
            } else if (userName.length < 5 || 15 < userName.length) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_alert_username_worng)
                )
            } else if (password.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_password)
                )
            } else if (password.length != 6) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_password_wrong)
                )
            } else if (confirm_password.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_confirm_password)
                )
            } else if (first_name.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_first_name)
                )
            } else if (last_name.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_last_name)
                )
            } else if (phone.equals("")) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_please_put) + resources.getString(R.string.text_alert_phone)
                )
            } else if (phone.length < 10) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_alert_phone_10_number)
                )
            } else if (!characterSet.checkPhone(phone)) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_alert_phone_format)
                )
            } else if (!password.equals("" + confirm_password)) {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Register,
                    "" + resources.getString(R.string.text_alert_password_match)
                )
            } else {
                var userName_encrypt = ""
                var password_encrypt = ""
                var phone_encrypt = ""

                val cryptLib = CryptLib()
                userName_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(userName, salt_key)
                password_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(password, salt_key)
                phone_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(phone, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(this)
                val formBody = FormBody.Builder()
                formBody.add("username", "" + userName_encrypt)
                formBody.add("password", "" + password_encrypt)
                formBody.add("first_name", "" + first_name)
                formBody.add("last_name", "" + last_name)
                formBody.add("tel", "" + phone_encrypt)
//                formBody.add("token_key", "" + resources.getString(R.string.token_key))
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url,
                    api_authRegisterRequestOTP,
                    "api_authRegisterRequestOTP"
                )

                progressBackground?.visibility = View.VISIBLE
            }
        }

    }


    private fun initialView() {
        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        editText_UserName = findViewById(R.id.editText_UserName) as EditText
        editText_Password = findViewById(R.id.editText_Password) as EditText
        editText_Confirm_Password = findViewById(R.id.editText_Confirm_Password) as EditText
        editText_first_name = findViewById(R.id.editText_first_name) as EditText
        editText_last_name = findViewById(R.id.editText_last_name) as EditText
        editText_phone = findViewById(R.id.editText_phone) as EditText

        linearLayout_register = findViewById(R.id.linearLayout_register) as LinearLayout
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_authRegisterRequestOTP")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONObject? = null
                    try {
                        result = responseJson.getJSONObject("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    val userObject = UserObject()
                    if (result != null) {
                    }

                    val intent_go = Intent(this@Page_Register, Page_Register_otp::class.java)
                    intent_go.putExtra("userName", "" + userName)
                    intent_go.putExtra("password", "" + password)
                    intent_go.putExtra("confirm_password", "" + confirm_password)
                    intent_go.putExtra("first_name", "" + first_name)
                    intent_go.putExtra("last_name", "" + last_name)
                    intent_go.putExtra("phone_new", "" + phone)
                    startActivity(intent_go)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_Register, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(this@Page_Register, resources.getString(R.string.text_service_error))
            }
        }

    }

}