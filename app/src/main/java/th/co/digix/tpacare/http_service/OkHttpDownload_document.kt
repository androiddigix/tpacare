package th.co.digix.tpacare.http_service

import android.content.Context
import android.os.AsyncTask
import android.os.Environment
import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Request
import th.co.digix.tpacare.http_service.DownloadObject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.TimeUnit


class OkHttpDownload_document(internal var context: Context, internal var downloadObject: DownloadObject) : AsyncTask<String, Long, Boolean>() {

    internal var eventName = ""
    val extStore = Environment.getExternalStorageDirectory()
    internal var rootFilePart = extStore.absolutePath + "/" + Environment.DIRECTORY_DOWNLOADS + "/"
    internal var fileName = "kkufile.pdf"

    internal var downloadSuccessful = false


    override fun doInBackground(vararg ulr: String): Boolean? {

        val httpServer = downloadObject.url
        val folderName = downloadObject.folderName
        fileName = downloadObject.fileName

        eventName = ulr[0]

        var request: Request? = null

        Log.d("coke", "httpServer get $httpServer")

        val client = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()

        request = Request.Builder()
                .url(httpServer)
                .build()

        if (request != null) {
            try {
                val response = client.newCall(request).execute()
                Log.d("response", "$eventName html response is $response")

                if (!response.isSuccessful) {
                    throw IOException("Failed to download file: $response")
                }
                val folderMyUserObjectId = File(this.rootFilePart, folderName)
                Log.d("download", "rootFilePart " + rootFilePart)
                Log.d("download", "folderName " + folderName)
                Log.d("download", "is folderMyUserObjectId exists " + folderMyUserObjectId.exists())
                var createDirectoryFolderName = true
                if (!folderMyUserObjectId.exists()) {
                    createDirectoryFolderName = folderMyUserObjectId.mkdir()
                }
                if (createDirectoryFolderName) {
                    this.rootFilePart = this.rootFilePart + folderName + "/"
                    if (response.code() == 200) {
                        var inputStream: InputStream? = null
                        val file = File(this.rootFilePart, this.fileName + ".pdf")
                        val output = FileOutputStream(file)

                        try {
                            inputStream = response.body()!!.byteStream()
                            val buff = ByteArray(1024 * 4)
                            var downloaded: Long = 0
                            val contentSize = response.body()!!.contentLength()

                            publishProgress(0L, contentSize)
                            while (true) {
                                val readed = inputStream!!.read(buff)
                                if (readed == -1) {
                                    break
                                }
                                output.write(buff, 0, readed)

                                //write buff
                                downloaded += readed.toLong()
                                publishProgress(downloaded, contentSize)
                                if (isCancelled) {
                                    return false
                                }
                            }
                            output.flush()
                            output.close()
                            downloadSuccessful = true
                            return downloaded == contentSize
                        } catch (e: IOException) {
                            e.printStackTrace()
                            downloadSuccessful = false
                            return false
                        } finally {
                            inputStream?.close()
                        }

                    } else {
                        return false
                    }

                }


            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return null

    }

    override fun onProgressUpdate(vararg values: Long?) {
        super.onProgressUpdate(*values)
        val downloaded = values[0]
        val contentSize = values[1]

        if (downloaded != null && contentSize != null) {
            val progress = downloaded * 100 / contentSize
            Log.d("okhttp", "progress $progress" + " download $downloaded/$contentSize")
        }

        //        BusProvider.getInstance().post(new OKHttpDownloadingFileEvent(downloadObject, progress));
    }

}
