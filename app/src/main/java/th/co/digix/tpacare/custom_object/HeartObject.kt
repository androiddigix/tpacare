package th.co.digix.tpacare.custom_object

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class HeartObject() : Parcelable {

    var time: String = ""
    var value: String = ""

    constructor(parcel: Parcel) : this() {
        time = parcel.readString()
        value = parcel.readString()

    }

    fun setParam(result: JSONObject) {
        try {
            time = result.getString("time")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            value = result.getString("value")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(time)
        parcel.writeString(value)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HeartObject> {
        override fun createFromParcel(parcel: Parcel): HeartObject {
            return HeartObject(parcel)
        }

        override fun newArray(size: Int): Array<HeartObject?> {
            return arrayOfNulls(size)
        }
    }

}