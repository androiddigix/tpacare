package th.co.digix.tpacare.login

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.squareup.otto.Subscribe
import org.json.JSONObject
import th.co.digix.tpacare.BaseActivity
import th.co.digix.tpacare.R
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.AlertDialogUtil
import th.co.digix.tpacare.utility.CryptLib
import th.co.digix.tpacare.utility.NotificationBarColor
import th.co.digix.tpacare.utility.SharedPreferencesManager


class Page_Setting_ChangePin : BaseActivity() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val salt_key = httpServerURL.salt_key
//    val api_accountChangePasscode = httpServerURL.api_accountChangePasscode


    var textView_title_setpin: TextView? = null

    var textView_no1: TextView? = null
    var textView_no2: TextView? = null
    var textView_no3: TextView? = null
    var textView_no4: TextView? = null
    var textView_no5: TextView? = null
    var textView_no6: TextView? = null
    var textView_no7: TextView? = null
    var textView_no8: TextView? = null
    var textView_no9: TextView? = null
    var textView_no0: TextView? = null
    var imageView_no_delete: ImageView? = null

    var imageView_IndicatorDot_1: ImageView? = null
    var imageView_IndicatorDot_2: ImageView? = null
    var imageView_IndicatorDot_3: ImageView? = null
    var imageView_IndicatorDot_4: ImageView? = null
    var imageView_IndicatorDot_5: ImageView? = null
    var imageView_IndicatorDot_6: ImageView? = null

    var old_pin = ""

    var old_password = ""
    var password_number = ""
    var confirm_password_number = ""

    //    var isOldPinCheckOk = false
    var isValidatePinMode = true
    var confirmPasswordMode = false

//    var userObject = UserObject()

    var stateCheckPinOk = false
    var wrongPasswordCount = 0

    var progressBackground: RelativeLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val notificationBarColor = NotificationBarColor()
        notificationBarColor.setStatusBarGradient(this)
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.page_pin_custom)

        BusProvider.getInstance().register(this)

        initialIntentData()

        initialView()

        setOnClick()

    }


    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun initialIntentData() {
//        try {
//            userObject = intent.getParcelableExtra("userObject")
//        } catch (e: Exception) {
//        }
    }


    private fun setOnClick() {

        textView_no1?.setOnClickListener() {
            addPassword("1")
        }
        textView_no2?.setOnClickListener() {
            addPassword("2")
        }
        textView_no3?.setOnClickListener() {
            addPassword("3")
        }
        textView_no4?.setOnClickListener() {
            addPassword("4")
        }
        textView_no5?.setOnClickListener() {
            addPassword("5")
        }
        textView_no6?.setOnClickListener() {
            addPassword("6")
        }
        textView_no7?.setOnClickListener() {
            addPassword("7")
        }
        textView_no8?.setOnClickListener() {
            addPassword("8")
        }
        textView_no9?.setOnClickListener() {
            addPassword("9")
        }
        textView_no0?.setOnClickListener() {
            addPassword("0")
        }

        imageView_no_delete?.setOnClickListener() {
            deletePassword()
        }


    }


    private fun deletePassword() {

        if (isValidatePinMode) {
            if (old_password.length > 0) {
                old_password = old_password.substring(0, old_password.length - 1)
                setIndicatorDot(old_password)
            }
        } else if (!confirmPasswordMode) {
            if (password_number.length > 0) {
                password_number = password_number.substring(0, password_number.length - 1)
                setIndicatorDot(password_number)
            }
        } else {
            if (confirm_password_number.length > 0) {
                confirm_password_number = confirm_password_number.substring(0, confirm_password_number.length - 1)
                setIndicatorDot(confirm_password_number)
            }
        }
//            Log.d("coke", "password_number " + password_number)
//            Log.d("coke", "confirm_password_number " + confirm_password_number)

    }

    private fun addPassword(addPassword_str: String?) {

        if (isValidatePinMode) {
            if (old_password.length < 6) {
                old_password = old_password + addPassword_str
            }
            setIndicatorDot(old_password)
        } else if (!confirmPasswordMode) {
            if (password_number.length < 6) {
                password_number = password_number + addPassword_str
            }
            setIndicatorDot(password_number)
        } else {
            if (confirm_password_number.length < 6) {
                confirm_password_number = confirm_password_number + addPassword_str
            }
            setIndicatorDot(confirm_password_number)
        }

//        Log.d("coke", "password_number " + password_number)
//        Log.d("coke", "confirm_password_number " + confirm_password_number)
    }

    private fun initialView() {

        progressBackground = findViewById(R.id.progressBackground) as RelativeLayout
        progressBackground?.setOnClickListener { }

        textView_no1 = findViewById(R.id.textView_no1) as TextView
        textView_no2 = findViewById(R.id.textView_no2) as TextView
        textView_no3 = findViewById(R.id.textView_no3) as TextView
        textView_no4 = findViewById(R.id.textView_no4) as TextView
        textView_no5 = findViewById(R.id.textView_no5) as TextView
        textView_no6 = findViewById(R.id.textView_no6) as TextView
        textView_no7 = findViewById(R.id.textView_no7) as TextView
        textView_no8 = findViewById(R.id.textView_no8) as TextView
        textView_no9 = findViewById(R.id.textView_no9) as TextView
        textView_no0 = findViewById(R.id.textView_no0) as TextView
        imageView_no_delete = findViewById(R.id.imageView_no_delete) as ImageView

        imageView_IndicatorDot_1 = findViewById(R.id.imageView_IndicatorDot_1) as ImageView
        imageView_IndicatorDot_2 = findViewById(R.id.imageView_IndicatorDot_2) as ImageView
        imageView_IndicatorDot_3 = findViewById(R.id.imageView_IndicatorDot_3) as ImageView
        imageView_IndicatorDot_4 = findViewById(R.id.imageView_IndicatorDot_4) as ImageView
        imageView_IndicatorDot_5 = findViewById(R.id.imageView_IndicatorDot_5) as ImageView
        imageView_IndicatorDot_6 = findViewById(R.id.imageView_IndicatorDot_6) as ImageView

    }

    private fun setIndicatorDot(check_number_indicator: String) {

        if (check_number_indicator.length >= 1) {
            imageView_IndicatorDot_1?.isSelected = true
        } else {
            imageView_IndicatorDot_1?.isSelected = false
        }
        if (check_number_indicator.length >= 2) {
            imageView_IndicatorDot_2?.isSelected = true
        } else {
            imageView_IndicatorDot_2?.isSelected = false
        }
        if (check_number_indicator.length >= 3) {
            imageView_IndicatorDot_3?.isSelected = true
        } else {
            imageView_IndicatorDot_3?.isSelected = false
        }
        if (check_number_indicator.length >= 4) {
            imageView_IndicatorDot_4?.isSelected = true
        } else {
            imageView_IndicatorDot_4?.isSelected = false
        }
        if (check_number_indicator.length >= 5) {
            imageView_IndicatorDot_5?.isSelected = true
        } else {
            imageView_IndicatorDot_5?.isSelected = false
        }
        if (check_number_indicator.length >= 6) {
            imageView_IndicatorDot_6?.isSelected = true
        } else {
            imageView_IndicatorDot_6?.isSelected = false
        }

        if (check_number_indicator.length >= 6) {

//            val sharedPreferencesManager = SharedPreferencesManager(this)
//            val pin_app = sharedPreferencesManager.getPIN()

            if (isValidatePinMode) {

//                if (pin_app.equals(old_password)) {
//                    isValidatePinMode = false
//                } else {
//                    isValidatePinMode = true
//                }
                isValidatePinMode = false

                if (!isValidatePinMode) {
                    textView_title_setpin?.setText(resources.getString(R.string.text_new_pin))
                    textView_title_setpin?.setTextColor(ContextCompat.getColor(this, R.color.text_gray))
                    setIndicatorDot(password_number)
                } else {
                    textView_title_setpin?.setText(resources.getString(R.string.text_pin_wrong))
                    textView_title_setpin?.setTextColor(ContextCompat.getColor(this, R.color.text_red))
                    old_password = ""
                    setIndicatorDot(old_password)
                }
            } else {
                if (!confirmPasswordMode) {
                    confirmPasswordMode = true

                    textView_title_setpin?.setText(resources.getString(R.string.text_confirm_pin))
                    textView_title_setpin?.setTextColor(ContextCompat.getColor(this, R.color.text_gray))
                    setIndicatorDot(confirm_password_number)
                } else {
                    if (password_number.equals(confirm_password_number)) {
                        if (!stateCheckPinOk) {
                            stateCheckPinOk = true
                            old_pin = old_password
                            val sharedPreferencesManager = SharedPreferencesManager(this)
                            sharedPreferencesManager.setPIN(password_number)

                            var pin_app_encrypt = ""
                            var password_number_encrypt = ""

                            val cryptLib = CryptLib()
                            pin_app_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(old_password, salt_key)
                            password_number_encrypt =
                                cryptLib.encryptPlainTextWithRandomIV_fixDigix(password_number, salt_key)

//                            val defaultParameterAPI = DefaultParameterAPI(this)
//                            val formBody = FormBody.Builder()
//                            formBody.add("old_passcode", "" + pin_app_encrypt)
//                            formBody.add("new_passcode", "" + password_number_encrypt)
//                            OkHttpPost(formBody, defaultParameterAPI).execute(
//                                root_url_fitbit,
//                                api_accountChangePasscode,
//                                "api_accountChangePasscode"
//                            )
//
//                            progressBackground?.visibility = View.VISIBLE

                        }
                    } else {
                        if (wrongPasswordCount < 2) {
                            wrongPasswordCount++
                            confirm_password_number = ""
                            setIndicatorDot(confirm_password_number)
                            textView_title_setpin?.setText(resources.getString(R.string.text_confirm_pin_wrong))
                            textView_title_setpin?.setTextColor(ContextCompat.getColor(this, R.color.text_red))
                        } else {
                            wrongPasswordCount = 0
                            confirmPasswordMode = false
                            password_number = ""
                            confirm_password_number = ""
                            setIndicatorDot(password_number)
                            textView_title_setpin?.setText(resources.getString(R.string.text_new_pin))
                            textView_title_setpin?.setTextColor(ContextCompat.getColor(this, R.color.text_gray))
                        }
                    }
                }
            }
        }
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {

        if (event.getHttpName().equals("api_accountChangePasscode")) {
            progressBackground?.visibility = View.GONE

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                val sharedPreferencesManager = SharedPreferencesManager(this)
                sharedPreferencesManager.setPIN(old_pin)
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }

                if (status.equals("fix_code", ignoreCase = true)) {
                } else if (status.equals("true", ignoreCase = true)) {

//                    val alertDialogUtil = AlertDialogUtil()
//                    alertDialogUtil.showMessageDialog(this@Page_Setting_ChangePin, "" + message)
//
//                    finish()

                    var message = resources.getString(R.string.text_change_popup_message)
                    show_alertDialog_success(this, message)


                } else {
                    val sharedPreferencesManager = SharedPreferencesManager(this)
                    sharedPreferencesManager.setPIN(old_pin)

                    stateCheckPinOk = false

                    wrongPasswordCount = 0
                    confirmPasswordMode = false
                    isValidatePinMode = true
                    old_password = ""
                    password_number = ""
                    confirm_password_number = ""
                    setIndicatorDot(old_password)
//                    textView_title_setpin?.setText(resources.getString(R.string.text_current_pin))
                    textView_title_setpin?.setTextColor(ContextCompat.getColor(this, R.color.text_gray))

                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(this@Page_Setting_ChangePin, "" + message)
                }
            } else {
                val sharedPreferencesManager = SharedPreferencesManager(this)
                sharedPreferencesManager.setPIN(old_pin)

                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(
                    this@Page_Setting_ChangePin,
                    resources.getString(R.string.text_service_error)
                )
            }
        }

    }

    private fun show_alertDialog_message(context: Context, message: String) {
        val builder = android.support.v7.app.AlertDialog.Builder(context)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(context).inflate(R.layout.popup_message, null)

        val textView_popup_message = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_message) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_message.setText("" + message)

        textView_popup_ok.setOnClickListener(View.OnClickListener {
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }

    private fun show_alertDialog_success(context: Context, message: String) {
        val builder = android.support.v7.app.AlertDialog.Builder(context)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(context).inflate(R.layout.popup_message, null)

        val textView_popup_message = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_message) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_message.setText("" + message)

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            finish()
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }

}