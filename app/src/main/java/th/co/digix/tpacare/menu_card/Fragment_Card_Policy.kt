package th.co.digix.tpacare.menu_card

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bumptech.glide.Glide
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.pvdproject.adapter.AdapterRecycler_card_hospital
import com.pvdproject.adapter.AdapterRecycler_card_number
import com.squareup.otto.Subscribe
import okhttp3.FormBody
import org.json.JSONArray
import org.json.JSONObject
import th.co.digix.tpacare.R
import th.co.digix.tpacare.adapter.RecyclerViewListener
import th.co.digix.tpacare.custom_object.CardPolicyObject
import th.co.digix.tpacare.custom_object.HospitalObject
import th.co.digix.tpacare.http_service.DefaultParameterAPI
import th.co.digix.tpacare.http_service.HttpServerURL
import th.co.digix.tpacare.http_service.OkHttpGet
import th.co.digix.tpacare.http_service.OkHttpPost
import th.co.digix.tpacare.otto_bus.BusProvider
import th.co.digix.tpacare.otto_bus.OKHttpEvent
import th.co.digix.tpacare.utility.*
import java.util.*
import java.util.concurrent.TimeUnit

class Fragment_Card_Policy : Fragment() {

    val httpServerURL = HttpServerURL()
    var root_url = httpServerURL.host_server
    val salt_key = httpServerURL.salt_key

    var api_values_hospital = httpServerURL.api_values_hospital
    val api_policy_request_otp = httpServerURL.api_policy_request_otp
    val api_policy_activate = httpServerURL.api_policy_activate
    val api_auto_policy_activate = httpServerURL.api_auto_policy_activate
    val api_eligibility_check = httpServerURL.api_eligibility_check
    val api_cancel_check = httpServerURL.api_cancel_check
    val api_insurance_delete_card = httpServerURL.api_insurance_delete_card

    var rootView: View? = null

    var cardView_cardPolicy: CardView? = null
    var linearLayout_cardPolicy_companee: LinearLayout? = null
    var view_card_delete_dammy: View? = null
    var imageView_card_delete: ImageView? = null
    var imageView_card_logo: ImageView? = null
    var textView_companee_name_main: TextView? = null
    var textView_user_name: TextView? = null
    var textView_card_policy_no: TextView? = null
    var textView_card_policy_no_field: TextView? = null
    var textView_card_time: TextView? = null
    var textView_company_name: TextView? = null

    var linearLayout_idcard_form: LinearLayout? = null
    var edittext_idcard_form: EditText? = null

    var textView_idcard_1: TextView? = null
    var textView_idcard_2: TextView? = null
    var textView_idcard_3: TextView? = null
    var textView_idcard_4: TextView? = null
    var textView_idcard_5: TextView? = null
    var textView_idcard_6: TextView? = null

    var linearLayout_card_detail: LinearLayout? = null
    var textView_card_detail_button: TextView? = null

    var cardPolicyObject = CardPolicyObject()
    var cardPolicyObject_List: ArrayList<CardPolicyObject> = ArrayList()
    val cardPolicyObject_List_phone = ArrayList<CardPolicyObject>()

    val hospitalObject_List = ArrayList<HospitalObject>()

    var isAllowUser = true
    var isRegisterBus = false
    var idcard = ""
    var selected_telephone = ""

    var record_id = ""

    var otp_code = ""

    var position = 0

    var alertDialog_used_counting: AlertDialog? = null

    //    var alertDialog_NeedHelp: android.support.v7.app.AlertDialog? = null

    var fusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_card_policy, container, false)

        if (!isRegisterBus) {
            BusProvider.getInstance().register(this)
            isRegisterBus = true
        }

        if (activity != null) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)
        }

        initialIntentData()

        initialView()

        setDataView()

        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        BusProvider.getInstance().unregister(this)
    }

    private fun setDataView() {

        val dateTimeHelper = DateTimeHelper()
        var effFrom_str = ""
        var effTo_str = ""
        try {
            val effFrom_date = dateTimeHelper.parseStringToDate3(cardPolicyObject.effFrom)
            effFrom_str = dateTimeHelper.parseDateToString(effFrom_date)
        } catch (e: Exception) {
        }
        try {
            val effTo_date = dateTimeHelper.parseStringToDate3(cardPolicyObject.effTo)
            effTo_str = dateTimeHelper.parseDateToString(effTo_date)
        } catch (e: java.lang.Exception) {
        }


        var polNo = cardPolicyObject.polNo
        var staffNo = cardPolicyObject.staffNo

        if (!isAllowUser) {
            if (!cardPolicyObject.policyFrom.equals("dummy")) {
                if (polNo.length > 4) {
                    val polNo_first = polNo.substring(0, 2)
                    val polNo_mid = polNo.substring(2, polNo.length - 2)
                    val polNo_end = polNo.substring(polNo.length - 2, polNo.length)

                    var text_x = ""
                    for (i in 0..polNo_mid.length) {
                        text_x = text_x + "X"
                    }
                    polNo = polNo_first + text_x + polNo_end
                } else if (polNo.length > 2) {
                    val polNo_first = polNo.substring(0, 1)
                    val polNo_mid = polNo.substring(1, polNo.length - 1)
                    val polNo_end = polNo.substring(polNo.length - 1, polNo.length)

                    var text_x = ""
                    for (i in 0..polNo_mid.length) {
                        text_x = text_x + "X"
                    }
                    polNo = polNo_first + text_x + polNo_end
                } else {

                    var text_x = ""
                    for (i in 0..polNo.length) {
                        text_x = text_x + "X"
                    }
                    polNo = text_x
                }
            }
        }

        if (cardPolicyObject.policyFrom.equals("dummy")) {
            //dummy card
            //ลบได้
            view_card_delete_dammy?.visibility = View.GONE
            imageView_card_delete?.visibility = View.VISIBLE
        } else {
            //tpa card
            if (isAllowUser) {
                //ลบไม่ได้
                view_card_delete_dammy?.visibility = View.VISIBLE
                imageView_card_delete?.visibility = View.GONE
            } else {
                //ลบไม่ได้
                view_card_delete_dammy?.visibility = View.VISIBLE
                imageView_card_delete?.visibility = View.GONE
            }
        }


        imageView_card_delete?.setOnClickListener(View.OnClickListener {
            show_alertDialog_deleteCard(activity!!)
        })

        if (imageView_card_logo != null) {
            val logoImage = cardPolicyObject.logoImage
            if (logoImage.equals("")) {
                imageView_card_logo?.visibility = View.GONE
            } else {
                imageView_card_logo?.visibility = View.VISIBLE
                val url_image = "" + logoImage
                Glide.with(this)
                    .load(url_image)
                    .into(imageView_card_logo!!)
            }
        }

        var companyName = cardPolicyObject.companyName
        if (companyName.equals("null")) {
            companyName = ""
        }
        textView_companee_name_main?.setText(companyName)

        val sharedPreferencesManager = SharedPreferencesManager(activity!!)
        val userObject = sharedPreferencesManager.getUserObject()
        val citizen_id = userObject.citizen_id
        if (!citizen_id.equals("")) {
            var user_fullname = cardPolicyObject.name + " " + cardPolicyObject.surname
            if (user_fullname.equals("")) {
                user_fullname = "-"
            }
            textView_user_name?.setText(user_fullname)
        } else {
            if (cardPolicyObject.policyFrom.equals("dummy")) {
                var user_fullname = cardPolicyObject.name + " " + cardPolicyObject.surname
                if (user_fullname.equals("")) {
                    user_fullname = "-"
                }
                textView_user_name?.setText(user_fullname)
            }
        }

        if (cardPolicyObject.cardType.startsWith("self", ignoreCase = true)) {
            textView_card_policy_no_field?.setText("" + resources.getString(R.string.title_card_no_policy_self))
            textView_card_policy_no?.setText("" + staffNo)
        } else {
            textView_card_policy_no_field?.setText("" + resources.getString(R.string.title_card_no_policy))
            textView_card_policy_no?.setText("" + polNo)
        }
        textView_card_time?.setText("" + cardPolicyObject.effFrom + "-" + cardPolicyObject.effTo)
        var insurerName = cardPolicyObject.insurerName
        if (insurerName.equals("null")) {
            insurerName = ""
        }
        textView_company_name?.setText("" + insurerName)

        if (isAllowUser) {
            textView_card_detail_button?.setText("" + resources.getString(R.string.button_card_detail_used))
        } else {
            textView_card_detail_button?.setText("" + resources.getString(R.string.button_card_detail))
//            callService()
        }

        val currentTime = Calendar.getInstance().getTime()
        var effTo_date = Calendar.getInstance().getTime()

        try {
            effTo_date = dateTimeHelper.parseStringToDate3(cardPolicyObject.effTo)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (currentTime.after(effTo_date) || currentTime == effTo_date) {
            cardView_cardPolicy?.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.bg_card_expire))
            linearLayout_cardPolicy_companee?.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.bg_card_companee_expire
                )
            )
            linearLayout_card_detail?.visibility = View.INVISIBLE
            linearLayout_card_detail?.setOnClickListener {

            }
        } else {
            cardView_cardPolicy?.setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.bg_white))
            linearLayout_cardPolicy_companee?.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.bg_company
                )
            )

            if (!cardPolicyObject.policyFrom.equals("dummy")) {
                linearLayout_card_detail?.visibility = View.VISIBLE
                linearLayout_card_detail?.setOnClickListener {
                    if (isAllowUser) {
                        callService_hospital()
                    } else {
                        showDialog_card_list_phone(activity!!)
                    }
                }
            } else {
                linearLayout_card_detail?.visibility = View.INVISIBLE
                linearLayout_card_detail?.setOnClickListener {

                }
            }

        }
    }

    private fun show_alertDialog_cancelCard(context: Context) {
        val builder = android.support.v7.app.AlertDialog.Builder(context)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(context).inflate(R.layout.popup_cancel_card, null)

        val textView_popup_cancel = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_cancel) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_cancel.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
            var record_id_encrypt = ""

            val cryptLib = CryptLib()
            if (!record_id.equals("")) {
                record_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(record_id, salt_key)
            }

            val defaultParameterAPI = DefaultParameterAPI(activity!!)
            val formBody = FormBody.Builder()
            formBody.add("record_id", "" + record_id_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_cancel_check, "api_cancel_check" + position
            )

            alertDialog_NeedHelp?.dismiss()

            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.VISIBLE
            } catch (e: Exception) {
            }
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(true)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }


    private fun show_alertDialog_deleteCard(context: Context) {
        val builder = android.support.v7.app.AlertDialog.Builder(context)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(context).inflate(R.layout.popup_delete_card, null)

        val textView_popup_cancel = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_cancel) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        textView_popup_cancel.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
            val pol_no = cardPolicyObject.polNo

            val cryptLib = CryptLib()
            val pol_no_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(pol_no, salt_key)

            val defaultParameterAPI = DefaultParameterAPI(activity!!)
            val formBody = FormBody.Builder()
            formBody.add("pol_no", "" + pol_no_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_insurance_delete_card, "api_insurance_delete_card" + position
            )
            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.VISIBLE
            } catch (e: Exception) {
            }
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(true)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }

    fun callService_hospital() {

        val callLatLng = CallLatLng(activity!!)
        val location = callLatLng.getLocation()
        var latitudeSTR = "0"
        var longitudeSTR = "0"

        if (location != null) {
            val latitude = location!!.getLatitude()
            val longitude = location!!.getLongitude()
            Log.d("android latitude", "is $latitude")
            Log.d("android longitude", "is $longitude")

            latitudeSTR = "" + latitude
            longitudeSTR = "" + longitude
        }

        val defaultParameterAPI = DefaultParameterAPI(activity!!)
        val formBody = "?page=" + "1" +
                "&pageSize=" + "99999" +
                "&keyword=" + "" +
                "&sLat=" + latitudeSTR +
                "&sLng=" + longitudeSTR +
                "&distance=" + TempData.hospital_distance

        OkHttpGet(defaultParameterAPI).execute(
            root_url,
            api_values_hospital + formBody,
            "api_values_hospital" + position
        )

        try {
            val parentFrag = parentFragment as Page_Fragment_Card
            parentFrag.progressBackground?.visibility = View.VISIBLE
        } catch (e: Exception) {
        }
    }

    fun showDialog_card_list_phone(
        activity: Activity
    ) {

        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_card_list_phone, null)

        val textView_card_add_number =
            dialogLayout_NeedHelp?.findViewById(R.id.textView_card_add_number) as TextView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout
        val linearLayout_card_add_number =
            dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_card_add_number) as LinearLayout
        val recyclerView_otp_number_list =
            dialogLayout_NeedHelp?.findViewById(R.id.recyclerView_otp_number_list) as RecyclerView

        recyclerView_otp_number_list?.layoutManager =
            LinearLayoutManager(rootView?.context!!, LinearLayout.VERTICAL, false)
        recyclerView_otp_number_list?.setNestedScrollingEnabled(false)

        try {
            val text_card_add_number =
                "<u>" + activity!!.resources.getString(R.string.popup_card_add_number_html) + "<br>" + activity!!.resources.getString(
                    R.string.popup_card_add_number_html2
                ) + "</u>"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView_card_add_number.setText(
                    Html.fromHtml(text_card_add_number, Html.FROM_HTML_MODE_COMPACT)
                )
            } else {
                textView_card_add_number.setText(
                    Html.fromHtml(text_card_add_number)
                )
            }
        } catch (e: Exception) {
        }

        val adapterRecycler_card_number = AdapterRecycler_card_number(
            activity, parentFragment, this, cardPolicyObject_List_phone, alertDialog_NeedHelp, position
        )
        recyclerView_otp_number_list?.setAdapter(adapterRecycler_card_number)

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        if (cardPolicyObject.cardType.startsWith("Individual", ignoreCase = true)) {
            linearLayout_card_add_number.visibility = View.VISIBLE
            textView_card_add_number.setOnClickListener(View.OnClickListener {
                showDialog_card_id(activity)
                alertDialog_NeedHelp?.dismiss()
            })
        } else if (cardPolicyObject.cardType.startsWith("Group", ignoreCase = true)) {
//            linearLayout_card_add_number.visibility = View.VISIBLE
            textView_card_add_number.setText("" + resources.getString(R.string.text_usernameforget_message4))
        } else if (cardPolicyObject.cardType.startsWith("self", ignoreCase = true)) {
//            linearLayout_card_add_number.visibility = View.VISIBLE
            textView_card_add_number.setText("" + resources.getString(R.string.text_usernameforget_message4))
        } else {
            linearLayout_card_add_number.visibility = View.GONE
        }

        linearLayout_card_add_number.setOnClickListener(View.OnClickListener {

        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }

    fun showDialog_list_hospital(
        activity: Activity
    ) {

        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_card_list_hospital, null)

        val imageView_popup_close = dialogLayout_NeedHelp?.findViewById(R.id.imageView_popup_close) as ImageView
        val textView_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_ok) as TextView
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout
        val linearLayout_card_add_number =
            dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_card_add_number) as LinearLayout
        val recyclerView_otp_number_list =
            dialogLayout_NeedHelp?.findViewById(R.id.recyclerView_otp_number_list) as RecyclerView

        val textView_hospital_message = dialogLayout_NeedHelp?.findViewById(R.id.textView_hospital_message) as TextView
        val message = resources.getString(R.string.popup_card_hospital_message) +
                " " + TempData.hospital_distance + " " +
                resources.getString(R.string.text_no_hospital_km)
        textView_hospital_message.setText(message)

        recyclerView_otp_number_list?.layoutManager =
            LinearLayoutManager(rootView?.context!!, LinearLayout.VERTICAL, false)
        recyclerView_otp_number_list?.setNestedScrollingEnabled(false)

        val adapterRecycler_card_number = AdapterRecycler_card_hospital(
            activity, this, hospitalObject_List
        )
        recyclerView_otp_number_list?.setAdapter(adapterRecycler_card_number)

        recyclerView_otp_number_list?.addOnItemTouchListener(
            RecyclerViewListener(
                activity,
                recyclerView_otp_number_list!!,
                object : RecyclerViewListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        if (hospitalObject_List.size > position) {
                            val hospitalObject = hospitalObject_List.get(position)
                            alertDialog_NeedHelp?.dismiss()
                            showDialog_confirm_use_card(activity!!, hospitalObject)
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {
                    }

                })
        )


        imageView_popup_close.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        textView_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_NeedHelp?.dismiss()
        })

        linearLayout_card_add_number.setOnClickListener(View.OnClickListener {

        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }


    fun showDialog_confirm_use_card(
        activity: Activity, hospitalObject: HospitalObject
    ) {

        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp =
            LayoutInflater.from(activity).inflate(R.layout.popup_card_confirm_use_card, null)

        val imageView_card_logo = dialogLayout_NeedHelp?.findViewById(R.id.imageView_card_logo) as ImageView
        val textView_companee_name = dialogLayout_NeedHelp?.findViewById(R.id.textView_companee_name) as TextView
        val textView_user_name = dialogLayout_NeedHelp?.findViewById(R.id.textView_user_name) as TextView
        val textView_card_time = dialogLayout_NeedHelp?.findViewById(R.id.textView_card_time) as TextView
        val textView_card_policy_no = dialogLayout_NeedHelp?.findViewById(R.id.textView_card_policy_no) as TextView
        val textView_card_policy_no_field =
            dialogLayout_NeedHelp?.findViewById(R.id.textView_card_policy_no_field) as TextView
        val textView_company_name = dialogLayout_NeedHelp?.findViewById(R.id.textView_company_name) as TextView

        val linearLayout_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_ok) as LinearLayout
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        val dateTimeHelper = DateTimeHelper()
        var effFrom_str = ""
        var effTo_str = ""
        try {
            val effFrom_date = dateTimeHelper.parseStringToDate(cardPolicyObject.effFrom)
            effFrom_str = dateTimeHelper.parseDateToString(effFrom_date)
        } catch (e: Exception) {
        }
        try {
            val effTo_date = dateTimeHelper.parseStringToDate(cardPolicyObject.effTo)
            effTo_str = dateTimeHelper.parseDateToString(effTo_date)
        } catch (e: java.lang.Exception) {
        }

        var polNo = cardPolicyObject.polNo
        var staffNo = cardPolicyObject.staffNo
        if (!isAllowUser) {
            if (!cardPolicyObject.policyFrom.equals("dummy")) {
                if (polNo.length > 4) {
                    val polNo_first = polNo.substring(0, 2)
                    val polNo_mid = polNo.substring(2, polNo.length - 2)
                    val polNo_end = polNo.substring(polNo.length - 2, polNo.length)

                    var text_x = ""
                    for (i in 0..polNo_mid.length) {
                        text_x = text_x + "X"
                    }
                    polNo = polNo_first + text_x + polNo_end
                } else if (polNo.length > 2) {
                    val polNo_first = polNo.substring(0, 1)
                    val polNo_mid = polNo.substring(1, polNo.length - 1)
                    val polNo_end = polNo.substring(polNo.length - 1, polNo.length)

                    var text_x = ""
                    for (i in 0..polNo_mid.length) {
                        text_x = text_x + "X"
                    }
                    polNo = polNo_first + text_x + polNo_end
                } else {
                    var text_x = ""
                    for (i in 0..polNo.length) {
                        text_x = text_x + "X"
                    }
                    polNo = text_x
                }
            }
        }

        if (imageView_card_logo != null) {
            val logoImage = cardPolicyObject.logoImage
            if (logoImage.equals("")) {
                imageView_card_logo?.visibility = View.GONE
            } else {
                imageView_card_logo?.visibility = View.VISIBLE
                val url_image = "" + logoImage
                Glide.with(this)
                    .load(url_image)
                    .into(imageView_card_logo!!)
            }
        }


        var companyName = cardPolicyObject.companyName
        if (companyName.equals("null")) {
            companyName = ""
        }
        textView_companee_name?.setText(companyName)

        var user_fullname = cardPolicyObject.name + " " + cardPolicyObject.surname
        if (user_fullname.equals("")) {
            user_fullname = "-"
        }
        textView_user_name?.setText(user_fullname)

        if (cardPolicyObject.cardType.startsWith("self", ignoreCase = true)) {
            textView_card_policy_no_field?.setText("" + resources.getString(R.string.title_card_no_policy_self))
            textView_card_policy_no?.setText("" + staffNo)
        } else {
            textView_card_policy_no_field?.setText("" + resources.getString(R.string.title_card_no_policy))
            textView_card_policy_no?.setText("" + polNo)
        }
        textView_card_time?.setText("" + cardPolicyObject.effFrom + "-" + cardPolicyObject.effTo)
        var insurerName = cardPolicyObject.insurerName
        if (insurerName.equals("null")) {
            insurerName = ""
        }
        textView_company_name?.setText("" + insurerName)

        linearLayout_popup_ok.setOnClickListener(View.OnClickListener {

            var personal_id_encrypt = ""
            var provider_code_encrypt = ""
            var name_encrypt = ""
            var surname_encrypt = ""
            var insurer_code_encrypt = ""
            var policy_no_encrypt = ""
            var tms_username_encrypt = ""

            val sharedPreferencesManager = SharedPreferencesManager(activity)
            val userObject = sharedPreferencesManager.getUserObject()

            personal_id_encrypt = userObject.citizen_id
            tms_username_encrypt = userObject.username

            val cryptLib = CryptLib()
            provider_code_encrypt =
                cryptLib.encryptPlainTextWithRandomIV_fixDigix(hospitalObject.provider_code, salt_key)
            name_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(cardPolicyObject.name, salt_key)
            surname_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(cardPolicyObject.surname, salt_key)
            insurer_code_encrypt =
                cryptLib.encryptPlainTextWithRandomIV_fixDigix(cardPolicyObject.insurerCode, salt_key)
            policy_no_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(cardPolicyObject.polNo, salt_key)

            val defaultParameterAPI = DefaultParameterAPI(activity!!)
            val formBody = FormBody.Builder()
            formBody.add("personal_id", "" + personal_id_encrypt)//
            formBody.add("provider_code", "" + provider_code_encrypt)
            formBody.add("name", "" + name_encrypt)
            formBody.add("surname", "" + surname_encrypt)
            formBody.add("insurer_code", "" + insurer_code_encrypt)//
            formBody.add("policy_no", "" + policy_no_encrypt)
            formBody.add("tms_username", "" + tms_username_encrypt)//
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_eligibility_check, "api_eligibility_check" + position
            )

            alertDialog_NeedHelp?.dismiss()

            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.VISIBLE
            } catch (e: Exception) {
            }
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()

    }

    fun showDialog_card_used_counting(
        activity: Activity
    ) {

        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        alertDialog_used_counting = builder.create()
        val dialogLayout_used_counting = LayoutInflater.from(activity).inflate(R.layout.popup_card_used_counting, null)


        val imageView_card_logo = dialogLayout_used_counting?.findViewById(R.id.imageView_card_logo) as ImageView
        val textView_companee_name = dialogLayout_used_counting?.findViewById(R.id.textView_companee_name) as TextView
        val textView_user_name = dialogLayout_used_counting?.findViewById(R.id.textView_user_name) as TextView
        val textView_card_time = dialogLayout_used_counting?.findViewById(R.id.textView_card_time) as TextView
        val textView_card_policy_no = dialogLayout_used_counting?.findViewById(R.id.textView_card_policy_no) as TextView
        val textView_card_policy_no_field =
            dialogLayout_used_counting?.findViewById(R.id.textView_card_policy_no_field) as TextView
        val textView_company_name = dialogLayout_used_counting?.findViewById(R.id.textView_company_name) as TextView

        val textView_card_counting = dialogLayout_used_counting?.findViewById(R.id.textView_card_counting) as TextView

        val linearLayout_popup_cancel =
            dialogLayout_used_counting?.findViewById(R.id.linearLayout_popup_cancel) as LinearLayout
        val linearLayout_popup_ok = dialogLayout_used_counting?.findViewById(R.id.linearLayout_popup_ok) as LinearLayout
        val linearLayout_popup_bg = dialogLayout_used_counting?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        val dateTimeHelper = DateTimeHelper()
        var effFrom_str = ""
        var effTo_str = ""
        try {
            val effFrom_date = dateTimeHelper.parseStringToDate(cardPolicyObject.effFrom)
            effFrom_str = dateTimeHelper.parseDateToString(effFrom_date)
        } catch (e: Exception) {
        }
        try {
            val effTo_date = dateTimeHelper.parseStringToDate(cardPolicyObject.effTo)
            effTo_str = dateTimeHelper.parseDateToString(effTo_date)
        } catch (e: java.lang.Exception) {
        }

        var polNo = cardPolicyObject.polNo
        var staffNo = cardPolicyObject.staffNo
        if (!isAllowUser) {
            if (!cardPolicyObject.policyFrom.equals("dummy")) {
                if (polNo.length > 4) {
                    val polNo_first = polNo.substring(0, 2)
                    val polNo_mid = polNo.substring(2, polNo.length - 2)
                    val polNo_end = polNo.substring(polNo.length - 2, polNo.length)

                    var text_x = ""
                    for (i in 0..polNo_mid.length) {
                        text_x = text_x + "X"
                    }
                    polNo = polNo_first + text_x + polNo_end
                } else if (polNo.length > 2) {
                    val polNo_first = polNo.substring(0, 1)
                    val polNo_mid = polNo.substring(1, polNo.length - 1)
                    val polNo_end = polNo.substring(polNo.length - 1, polNo.length)

                    var text_x = ""
                    for (i in 0..polNo_mid.length) {
                        text_x = text_x + "X"
                    }
                    polNo = polNo_first + text_x + polNo_end
                } else {
                    var text_x = ""
                    for (i in 0..polNo.length) {
                        text_x = text_x + "X"
                    }
                    polNo = text_x
                }
            }
        }

        if (imageView_card_logo != null) {
            val logoImage = cardPolicyObject.logoImage
            if (logoImage.equals("")) {
                imageView_card_logo?.visibility = View.GONE
            } else {
                imageView_card_logo?.visibility = View.VISIBLE
                val url_image = "" + logoImage
                Glide.with(this)
                    .load(url_image)
                    .into(imageView_card_logo!!)
            }
        }

        var countdown_min = 5
        try {
            countdown_min = TempData.countdown_min.toInt()
        } catch (e: Exception) {
        }

        val card_used_countdown: Long = countdown_min * 60 * 1000L

        object : CountDownTimer(card_used_countdown, 1000) {
            override fun onFinish() {
                textView_card_counting.setText("00:00")
            }

            override fun onTick(millisUntilFinished: Long) {
                val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                val minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                val hour = TimeUnit.MILLISECONDS.toHours(millisUntilFinished)

                textView_card_counting.setText(
                    String.format("%02d", minutes % 60) +
                            ":" + String.format("%02d", seconds % 60)
                )
            }
        }.start()
        var companyName = cardPolicyObject.companyName
        if (companyName.equals("null")) {
            companyName = ""
        }
        textView_companee_name?.setText(companyName)
        var user_fullname = cardPolicyObject.name + " " + cardPolicyObject.surname
        if (user_fullname.equals("")) {
            user_fullname = "-"
        }
        textView_user_name?.setText(user_fullname)

        if (cardPolicyObject.cardType.startsWith("self", ignoreCase = true)) {
            textView_card_policy_no_field?.setText("" + resources.getString(R.string.title_card_no_policy_self))
            textView_card_policy_no?.setText("" + staffNo)
        } else {
            textView_card_policy_no_field?.setText("" + resources.getString(R.string.title_card_no_policy))
            textView_card_policy_no?.setText("" + polNo)
        }
        textView_card_time?.setText("" + cardPolicyObject.effFrom + "-" + cardPolicyObject.effTo)
        var insurerName = cardPolicyObject.insurerName
        if (insurerName.equals("null")) {
            insurerName = ""
        }
        textView_company_name?.setText("" + insurerName)

        linearLayout_popup_cancel.setOnClickListener(View.OnClickListener {
            show_alertDialog_cancelCard(activity!!)
        })
        linearLayout_popup_ok.setOnClickListener(View.OnClickListener {
            alertDialog_used_counting?.dismiss()
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_used_counting?.setCanceledOnTouchOutside(false)
        alertDialog_used_counting?.setView(dialogLayout_used_counting)
        if (alertDialog_used_counting?.window != null) {
            alertDialog_used_counting?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_used_counting?.show()

    }

    public fun showDialog_card_id(activity: Activity) {

        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_card_id, null)

        val editText_card_id_1 = dialogLayout_NeedHelp?.findViewById(R.id.editText_card_id_1) as EditText
        val editText_card_id_2 = dialogLayout_NeedHelp?.findViewById(R.id.editText_card_id_2) as EditText
        val editText_card_id = dialogLayout_NeedHelp?.findViewById(R.id.editText_card_id) as EditText
        val editText_card_id_3 = dialogLayout_NeedHelp?.findViewById(R.id.editText_card_id_3) as EditText
        val editText_card_id_4 = dialogLayout_NeedHelp?.findViewById(R.id.editText_card_id_4) as EditText

        val linearLayout_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_ok) as LinearLayout
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        var card_id_1 = ""
        var card_id_2 = ""
        var card_id_3 = ""
        var card_id_4 = ""

        var polNo = cardPolicyObject.polNo
        if (polNo.length > 4) {
            val polNo_first = polNo.substring(0, 2)
            val polNo_mid = polNo.substring(2, polNo.length - 2)
            val polNo_end = polNo.substring(polNo.length - 2, polNo.length)

            try {
                card_id_1 = polNo_first.get(0).toString()
            } catch (e: Exception) {
            }
            try {
                card_id_2 = polNo_first.get(1).toString()
            } catch (e: Exception) {
            }

            try {
                card_id_3 = polNo_end.get(0).toString()
            } catch (e: Exception) {
            }
            try {
                card_id_4 = polNo_end.get(1).toString()
            } catch (e: Exception) {
            }


        } else if (polNo.length > 2) {
            val polNo_first = polNo.substring(0, 1)
            val polNo_mid = polNo.substring(1, polNo.length - 1)
            val polNo_end = polNo.substring(polNo.length - 1, polNo.length)

            try {
                card_id_1 = polNo_first.get(0).toString()
            } catch (e: Exception) {
            }
            card_id_2 = "X"

            card_id_3 = "X"
            try {
                card_id_4 = polNo_end.get(0).toString()
            } catch (e: Exception) {
            }

            editText_card_id_2.visibility = View.GONE
            editText_card_id_3.visibility = View.GONE
        } else {
            card_id_1 = "X"
            card_id_2 = "X"

            card_id_3 = "X"
            card_id_4 = "X"

            editText_card_id_1.visibility = View.GONE
            editText_card_id_2.visibility = View.GONE
            editText_card_id_3.visibility = View.GONE
            editText_card_id_4.visibility = View.GONE
        }

        editText_card_id_1.setText("" + card_id_1)
        editText_card_id_2.setText("" + card_id_2)
        editText_card_id_3.setText("" + card_id_3)
        editText_card_id_4.setText("" + card_id_4)

        linearLayout_popup_ok.setOnClickListener(View.OnClickListener {
            var card_id_put = ""
            if (polNo.length > 4) {
                card_id_put = card_id_put + card_id_1 + card_id_2
                card_id_put = card_id_put + editText_card_id?.text.toString()
                card_id_put = card_id_put + card_id_3 + card_id_4
            } else if (polNo.length >= 2) {
                card_id_put = card_id_put + card_id_1
                card_id_put = card_id_put + editText_card_id?.text.toString()
                card_id_put = card_id_put + card_id_4
            } else {
                card_id_put = card_id_put + editText_card_id?.text.toString()
            }

            if (card_id_put.equals("" + cardPolicyObject.polNo)) {


                val sharedPreferencesManager = SharedPreferencesManager(activity!!)
                val userObject = sharedPreferencesManager.getUserObject()
                val username = userObject.username

                val cryptLib = CryptLib()
                val idcard_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(idcard, salt_key)

                val defaultParameterAPI = DefaultParameterAPI(activity!!)
                val formBody = FormBody.Builder()
                formBody.add("username", "" + username)
                formBody.add("citizen_id", "" + idcard_encrypt)
                OkHttpPost(formBody, defaultParameterAPI).execute(
                    root_url,
                    api_auto_policy_activate,
                    "api_auto_policy_activate"
                )

                try {
                    val parentFrag = parentFragment as Page_Fragment_Card
                    parentFrag.progressBackground?.visibility = View.VISIBLE
                } catch (e: Exception) {
                }
            }


            alertDialog_NeedHelp?.dismiss()
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        alertDialog_NeedHelp?.show()


    }


    public fun showDialog_card_otp(
        activity: Activity, selected_telephone: String
    ) {
        otp_code = ""
        val builder = android.support.v7.app.AlertDialog.Builder(activity)

        val alertDialog_NeedHelp = builder.create()
        val dialogLayout_NeedHelp = LayoutInflater.from(activity).inflate(R.layout.popup_card_otp, null)

        val textView_card_re_otp = dialogLayout_NeedHelp?.findViewById(R.id.textView_card_re_otp) as TextView
        val textView_popup_title = dialogLayout_NeedHelp?.findViewById(R.id.textView_popup_title) as TextView
        val linearLayout_card_re_otp =
            dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_card_re_otp) as LinearLayout
        val linearLayout_popup_ok = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_ok) as LinearLayout
        val linearLayout_popup_bg = dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_popup_bg) as LinearLayout

        linearLayout_idcard_form =
            dialogLayout_NeedHelp?.findViewById(R.id.linearLayout_idcard_form) as LinearLayout
        edittext_idcard_form = dialogLayout_NeedHelp?.findViewById(R.id.edittext_idcard_form) as EditText

        textView_idcard_1 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_1) as TextView
        textView_idcard_2 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_2) as TextView
        textView_idcard_3 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_3) as TextView
        textView_idcard_4 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_4) as TextView
        textView_idcard_5 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_5) as TextView
        textView_idcard_6 = dialogLayout_NeedHelp?.findViewById(R.id.textView_idcard_6) as TextView

        linearLayout_idcard_form?.setOnClickListener {
            if (otp_code.length == 0) {
                textView_idcard_1?.setText("|")
                textView_idcard_1?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 1) {
                textView_idcard_2?.setText("|")
                textView_idcard_2?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 2) {
                textView_idcard_3?.setText("|")
                textView_idcard_3?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 3) {
                textView_idcard_4?.setText("|")
                textView_idcard_4?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 4) {
                textView_idcard_5?.setText("|")
                textView_idcard_5?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            } else if (otp_code.length == 5) {
                textView_idcard_6?.setText("|")
                textView_idcard_6?.setTextColor(ContextCompat.getColor(activity, R.color.text_black))
            }

            Log.d("coke", "linearLayout_idcard_form")

            edittext_idcard_form?.requestFocus()


            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.getCurrentFocus()
            if (view == null) {
                view = View(activity)
            }
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)


        }

        edittext_idcard_form?.addTextChangedListener(
            onTextChangedSetControlEdittext(
            )
        )

        var card_telephone = selected_telephone
        try {
            card_telephone = card_telephone.substring(0, 3) +
                    "-XXX-X" +
                    card_telephone.substring(7, card_telephone.length)
        } catch (e: Exception) {
        }

        try {
            val text_card_add_number = "<u>" + activity!!.resources.getString(R.string.popup_card_re_otp) + "</u>"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView_card_re_otp.setText(
                    Html.fromHtml(text_card_add_number, Html.FROM_HTML_MODE_COMPACT)
                )
            } else {
                textView_card_re_otp.setText(
                    Html.fromHtml(text_card_add_number)
                )
            }
        } catch (e: Exception) {
        }
        textView_popup_title.setText(resources.getString(R.string.popup_card_otp_title) + " " + card_telephone)

        linearLayout_card_re_otp.setOnClickListener(View.OnClickListener {
            var tel_encrypt = ""

            val cryptLib = CryptLib()
            tel_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(selected_telephone, salt_key)

            val defaultParameterAPI = DefaultParameterAPI(activity!!)
            val formBody = FormBody.Builder()
            formBody.add("tel", "" + tel_encrypt)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_policy_request_otp, "api_policy_request_otp" + position
            )

            alertDialog_NeedHelp?.dismiss()

            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.VISIBLE
            } catch (e: Exception) {
            }
        })

        linearLayout_popup_ok.setOnClickListener(View.OnClickListener {


            val sharedPreferencesManager = SharedPreferencesManager(activity)
            val userObject = sharedPreferencesManager.getUserObject()
            val citizen_id = idcard
            val username = userObject.username

            val cryptLib = CryptLib()
            val otp_code_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(otp_code, salt_key)
            val citizen_id_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(citizen_id, salt_key)
            val username_encrypt = cryptLib.encryptPlainTextWithRandomIV_fixDigix(username, salt_key)

            val defaultParameterAPI = DefaultParameterAPI(activity!!)
            val formBody = FormBody.Builder()
            formBody.add("otp_code", "" + otp_code_encrypt)
            formBody.add("citizen_id", "" + citizen_id_encrypt)
            formBody.add("username", "" + username)
            OkHttpPost(formBody, defaultParameterAPI).execute(
                root_url, api_policy_activate, "api_policy_activate"
            )

            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.VISIBLE
            } catch (e: Exception) {
            }

            alertDialog_NeedHelp?.dismiss()
        })

        linearLayout_popup_bg.setOnClickListener(View.OnClickListener {
            //            alertDialog_NeedHelp?.dismiss()
        })

        alertDialog_NeedHelp?.setCanceledOnTouchOutside(false)
        alertDialog_NeedHelp?.setView(dialogLayout_NeedHelp)
        if (alertDialog_NeedHelp?.window != null) {
            alertDialog_NeedHelp?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
//        alertDialog_NeedHelp?.getWindow()?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
//                or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        alertDialog_NeedHelp?.show()


    }

    private fun onTextChangedSetControlEdittext(): TextWatcher {

        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
//                Log.d("coke", "onTextChanged s " + s + " start " + start + " before " + before + " count " + count)

                var textChange = s.toString()
                otp_code = "" + textChange

                var otp_code_1 = ""
                var otp_code_2 = ""
                var otp_code_3 = ""
                var otp_code_4 = ""
                var otp_code_5 = ""
                var otp_code_6 = ""
                try {
                    otp_code_1 = "" + s.get(0)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_2 = "" + s.get(1)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_3 = "" + s.get(2)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_4 = "" + s.get(3)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_5 = "" + s.get(4)
                } catch (e: java.lang.Exception) {
                }
                try {
                    otp_code_6 = "" + s.get(5)
                } catch (e: java.lang.Exception) {
                }


                textView_idcard_1?.setText("" + otp_code_1)
                textView_idcard_2?.setText("" + otp_code_2)
                textView_idcard_3?.setText("" + otp_code_3)
                textView_idcard_4?.setText("" + otp_code_4)
                textView_idcard_5?.setText("" + otp_code_5)
                textView_idcard_6?.setText("" + otp_code_6)

                textView_idcard_1?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_2?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_3?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_4?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_5?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))
                textView_idcard_6?.setTextColor(ContextCompat.getColor(activity!!, R.color.text_blue))

                if (textChange.length == 0) {
                    textView_idcard_1?.setText("|")
                    textView_idcard_1?.setTextColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 1) {
                    textView_idcard_2?.setText("|")
                    textView_idcard_2?.setTextColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 2) {
                    textView_idcard_3?.setText("|")
                    textView_idcard_3?.setTextColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 3) {
                    textView_idcard_4?.setText("|")
                    textView_idcard_4?.setTextColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 4) {
                    textView_idcard_5?.setText("|")
                    textView_idcard_5?.setTextColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 5) {
                    textView_idcard_6?.setText("|")
                    textView_idcard_6?.setTextColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.text_black
                        )
                    )
                } else if (textChange.length == 6) {
                    val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    var view = activity?.getCurrentFocus()
                    if (view == null) {
                        view = View(activity!!)
                    }
//                    imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        }
    }

    private fun initialView() {
        cardView_cardPolicy = rootView?.findViewById(R.id.cardView_cardPolicy) as CardView
        linearLayout_cardPolicy_companee =
            rootView?.findViewById(R.id.linearLayout_cardPolicy_companee) as LinearLayout
        view_card_delete_dammy = rootView?.findViewById(R.id.view_card_delete_dammy)
        imageView_card_delete = rootView?.findViewById(R.id.imageView_card_delete) as ImageView
        imageView_card_logo = rootView?.findViewById(R.id.imageView_card_logo) as ImageView
        textView_companee_name_main = rootView?.findViewById(R.id.textView_companee_name) as TextView
        textView_user_name = rootView?.findViewById(R.id.textView_user_name) as TextView
        textView_card_time = rootView?.findViewById(R.id.textView_card_time) as TextView
        textView_card_policy_no = rootView?.findViewById(R.id.textView_card_policy_no) as TextView
        textView_card_policy_no_field = rootView?.findViewById(R.id.textView_card_policy_no_field) as TextView
        textView_company_name = rootView?.findViewById(R.id.textView_company_name) as TextView

        linearLayout_card_detail = rootView?.findViewById(R.id.linearLayout_card_detail) as LinearLayout
        textView_card_detail_button = rootView?.findViewById(R.id.textView_card_detail_button) as TextView
    }

    private fun initialIntentData() {
        val bundle = this.arguments
        if (bundle != null) {
            try {
                idcard = bundle.getString("idcard", "")
            } catch (e: Exception) {
            }
            try {
                isAllowUser = bundle.getBoolean("isAllowUser", true)
            } catch (e: Exception) {
            }
            try {
                cardPolicyObject_List = bundle.getParcelableArrayList("cardPolicyObject_List")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            try {
                position = bundle.getInt("position", 0)
                if (cardPolicyObject_List != null) {
                    if (cardPolicyObject_List?.size!! > position) {
                        cardPolicyObject = cardPolicyObject_List?.get(position)!!
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        cardPolicyObject_List_phone.clear()

        val cardPolicyObject_List_temp = ArrayList<CardPolicyObject>()
        if (cardPolicyObject_List.size > 0) {
            for (i in 0..cardPolicyObject_List.size - 1) {
                if (!cardPolicyObject_List.get(i).telNo.equals("")) {
                    cardPolicyObject_List_temp.add(cardPolicyObject_List.get(i))
                }
            }
        }
        if (cardPolicyObject_List_temp.size > 0) {
            for (i in 0..cardPolicyObject_List_temp.size - 1) {

                val telNo = cardPolicyObject_List_temp.get(i).telNo

                if (cardPolicyObject_List_phone.size == 0) {
                    if (!telNo.equals("") && !telNo.equals(" ")) {
                        cardPolicyObject_List_phone.add(cardPolicyObject_List_temp.get(i))
                    }
                } else {
                    var isHavePhone = false
                    for (y in 0..cardPolicyObject_List_phone.size - 1) {
                        val telNo_phone = cardPolicyObject_List_phone.get(y).telNo
                        if (telNo.equals("" + telNo_phone)
                        ) {
                            isHavePhone = true
                        }
                    }
                    if (!isHavePhone) {
                        if (!telNo.equals("") && !telNo.equals(" ")) {
                            cardPolicyObject_List_phone.add(cardPolicyObject_List_temp.get(i))
                        }
                    }
                }
            }
        }
    }

    @Subscribe
    fun handleOKHttpEvent(event: OKHttpEvent) {
        if (event.getHttpName().equals("api_policy_request_otp" + position)) {
            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.GONE
            } catch (e: Exception) {
            }

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {

                    }

                    showDialog_card_otp(activity!!, selected_telephone)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
        if (event.getHttpName().equals("api_eligibility_check" + position)) {
            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.GONE
            } catch (e: Exception) {
            }

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {
                        if (result.length() > 0) {
                            var result_object: JSONObject? = null
                            try {
                                result_object = result.get(0) as JSONObject
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            if (result_object != null) {
                                try {
                                    record_id = result_object.getString("recordID")
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    }

                    showDialog_card_used_counting(activity!!)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
        if (event.getHttpName().equals("api_cancel_check" + position)) {
            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.GONE
            } catch (e: Exception) {
            }

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var result: JSONArray? = null
                    try {
                        result = responseJson.getJSONArray("result")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (result != null) {

                    }

                    alertDialog_used_counting?.dismiss()
//                    message = "" + resources.getString(R.string.text_cancel_card)
//                    val alertDialogUtil = AlertDialogUtil()
//                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
        if (event.getHttpName().equals("api_values_hospital" + position)) {
            try {
                val parentFrag = parentFragment as Page_Fragment_Card
                parentFrag.progressBackground?.visibility = View.GONE
            } catch (e: Exception) {
            }

            val responseStr = event.responseStr
            var responseJson: JSONObject? = null
            try {
                responseJson = JSONObject(responseStr)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (responseJson != null) {

                var status: String = ""
                var message: String = ""
                var status_code: String = ""

                try {
                    status = responseJson.getString("status")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                try {
                    message = responseJson.getString("errorMessage")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (message.equals("null", ignoreCase = true) || message.equals("")) {
                    message = resources.getString(R.string.text_cannot_connect_server)
                }
                try {
                    status_code = responseJson.getString("status_code")
                } catch (e: Exception) {

                }

                if (status.equals("true", ignoreCase = true)) {
                    var results: JSONArray? = null
                    try {
                        results = responseJson.getJSONArray("results")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    hospitalObject_List.clear()
                    if (results != null) {
                        for (i in 0..results.length() - 1) {
                            var hospital_Json: JSONObject? = null
                            try {
                                hospital_Json = results.get(i) as JSONObject
                            } catch (e: Exception) {
                            }
                            if (hospital_Json != null) {
                                val hospitalObject = HospitalObject()
                                hospitalObject.setParam(hospital_Json)
                                hospitalObject_List.add(hospitalObject)
                            }

                        }

                    }

                    if (hospitalObject_List.size > 0) {
                        showDialog_list_hospital(activity!!)
                    } else {
                        message = resources.getString(R.string.text_no_hospital) +
                                " " + TempData.hospital_distance + " " +
                                resources.getString(R.string.text_no_hospital_km)
                        val alertDialogUtil = AlertDialogUtil()
                        alertDialogUtil.showMessageDialog(activity!!, "" + message)
                    }
                } else {
                    val alertDialogUtil = AlertDialogUtil()
                    alertDialogUtil.showMessageDialog(activity!!, "" + message)
                }
            } else {
                val alertDialogUtil = AlertDialogUtil()
                alertDialogUtil.showMessageDialog(activity!!, resources.getString(R.string.text_service_error))
            }
        }
    }
}