package th.co.digix.tpacare.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import th.co.digix.tpacare.custom_object.CardPolicyObject;
import th.co.digix.tpacare.menu_card.Fragment_Card_Policy;
import th.co.digix.tpacare.menu_card.Fragment_Card_Policy_Add;

import java.util.ArrayList;


/**
 * Created by android on 29/3/2560.
 */

public class PagerAdapter_Card_Policy extends FragmentStatePagerAdapter {
    ArrayList<CardPolicyObject> cardPolicyObject_List;

    Boolean isAllowUser = true;
    String idcard = "";
    String insured_type = "";

    public PagerAdapter_Card_Policy(FragmentManager fm,
                                    ArrayList<CardPolicyObject> cardPolicyObject_List,
                                    Boolean isAllowUser, String idcard, String insured_type) {
        super(fm);
        this.cardPolicyObject_List = cardPolicyObject_List;
        this.isAllowUser = isAllowUser;
        this.idcard = idcard;
        this.insured_type = insured_type;
    }

    public void setAllowUser(Boolean isAllowUser) {
        this.isAllowUser = isAllowUser;
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {

        if (position != cardPolicyObject_List.size()) {
            Fragment_Card_Policy fragment = new Fragment_Card_Policy();
            Bundle args = new Bundle();
            args.putParcelableArrayList("cardPolicyObject_List", cardPolicyObject_List);
            args.putInt("position", position);
            args.putString("idcard", idcard);
            args.putBoolean("isAllowUser", isAllowUser);
            fragment.setArguments(args);

            return fragment;
        } else {
            Fragment_Card_Policy_Add fragment = new Fragment_Card_Policy_Add();
            Bundle args = new Bundle();
//            args.putParcelableArrayList("cardPolicyObject_List", cardPolicyObject_List);
            args.putInt("position", position);
            args.putString("insured_type", insured_type);
            args.putString("idcard", idcard);
            args.putParcelableArrayList("cardPolicyObject_List", cardPolicyObject_List);
            args.putBoolean("isAllowUser", isAllowUser);
            fragment.setArguments(args);

            return fragment;
        }

    }

    @Override
    public int getCount() {
        if (cardPolicyObject_List == null ) {
            return 0;
        }
        return cardPolicyObject_List.size() + 1;
    }

}